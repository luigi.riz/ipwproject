DROP TABLE SERVIZIPROVINCIALI CASCADE;
DROP TABLE FARMACI CASCADE;
DROP TABLE FARMACIE CASCADE;
DROP TABLE FOTOPAZIENTI CASCADE;
DROP TABLE MED_HAS_PAZ CASCADE;
DROP TABLE MEDICI CASCADE;
DROP TABLE PAZIENTI CASCADE;
DROP TABLE PRESCRIZIONE_ESAMI CASCADE;
DROP TABLE PRESCRIZIONE_FARMACI CASCADE;
DROP TABLE PRESCRIZIONE_VISITESPEC CASCADE;
DROP TABLE TICKET CASCADE;
DROP TABLE ESAMI CASCADE;