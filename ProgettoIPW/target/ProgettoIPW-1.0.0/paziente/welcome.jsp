<%-- 
    Document   : welcome
    Created on : 30 dic 2019, 10:58:00
    Author     : Luigi
--%>

<%@page import="com.itextpdf.text.Document"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Main page PAZIENTE</title>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
        <link href="https://unpkg.com/ionicons@4.5.10-0/dist/css/ionicons.min.css" rel="stylesheet">
        
        <link rel="stylesheet" href="../css/Login-Form-Clean.css">
        <link rel="stylesheet" href="../css/Navigation-with-Button.css">
        <link rel="stylesheet" href="../css/Projects-Clean.css">
        <link rel="stylesheet" href="../css/styles.css">
    </head>
    <body class="my_body" onload="testPopup(${mustShowChangePsw}, ${showChangeMed}); loadTables()">
        <nav class="navbar navbar-expand-md navbar-light fixed-top my_nav">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">Portale Azienda per i Servizi Sanitari</a>
                <a href="<c:url context="${pageContext.request.contextPath}" value="/paziente/logout.handler" />">Logout</a>
            </div>
        </nav>
        <div class="container-fluid my_container my_margin">
            <div class="row my_margin">
                <div class="col-md-6">
                    <c:choose>
                        <c:when test="${!empty paziente.elencofoto}">
                            <div class="carousel slide my_margin my_carousel" data-ride="carousel" id="carousel-1">
                                <div class="carousel-inner" role="listbox">
                                    <div class="carousel-item active">
                                        <img class="w-100 d-block" src= "${paziente.elencofoto[0]}" alt="Slide Image" height="480" width="480">
                                    </div>
                                    <c:forEach items="${paziente.elencofoto}" begin="1" var="item">
                                        <div class="carousel-item">
                                            <img class="w-100 d-block" src= "${item}" alt="Slide Image"  height="480" width="480">
                                        </div>
                                    </c:forEach>
                                </div>
                                <div>
                                    <a class="carousel-control-prev" href="#carousel-1" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carousel-1" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <ol class="carousel-indicators">
                                    <li data-target="#carousel-1" data-slide-to="0" class="active"></li>
                                    <c:forEach items="${paziente.elencofoto}" begin="1" var="item" varStatus="loop">
                                        <li data-target="#carousel-1" data-slide-to="${loop.index}"></li>
                                    </c:forEach>
                                </ol>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <img class="w-100 d-block" src="../images/placeholder.png" alt="Slide Image" style="margin:5px">
                        </c:otherwise>
                    </c:choose>
                    <form method="POST" action="${pageContext.request.contextPath}/paziente/welcome.handler" enctype="multipart/form-data">
                        <div class="input-group mb-3">
                            <div class="custom-file">
                                <input type="file" class="custom-file-input" id="fileSrc" name="fileSrc" onchange="visualizzaPath()" accept=".jpg,.png">
                                <label class="custom-file-label" for="inputGroupFile03" id="fileSrcLabel">Aggiungi foto</label>
                            </div>
                            
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="submit">Upload</button>
                            </div>
                        </div>
                    </form>
                    <div style="text-align: end">
                        <span class="badge badge-secondary">Dimensioni consigliate : 480 x 480</span>
                    </div> 
                </div>        
                <div class="col">
                    <form action="${pageContext.request.contextPath}/paziente/modificadati.handler" class="my_margin" method="POST">
                        <fieldset>
                            <legend>Dati Personali</legend>
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="ion ion-md-person my_icon"></span>
                                    </div>
                                    <input type="text" class="form-control" name="nome" value="${paziente.nome}" readonly>
                                    <input type="text" class="form-control" name="cognome" value="${paziente.cognome}" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="ion ion-md-card my_icon"></span>
                                    </div>
                                    <input type="text" class="form-control" name="cf" value="${paziente.codicefiscale}" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="ion ion-md-calendar my_icon"></span>
                                    </div>
                                    <input type="text" class="form-control" name="datanascita" value="${paziente.datanascita}" readonly>
                                    <input type="text" class="form-control" name="luogonascita" value="${paziente.luogonascita}" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="ion ion-md-mail my_icon"></span>
                                    </div>
                                    <input type="email" class="form-control"  name="email" value="${paziente.email}" oninput="activateButton()" >
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="ion ion-md-call my_icon"></span>
                                    </div>
                                    <input type="tel" class="form-control"  name="numerotelefono" value="${paziente.numerotelefono}" oninput="activateButton()" required minlength="9" maxlength="11">
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Dati di Residenza</legend>
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="ion ion-md-home my_icon"></span>
                                    </div>
                                    <input type="text" class="form-control" name="provincia" value="${paziente.provincia}" readonly>
                                    <input type="text" class="form-control" name="citta" value="${paziente.citta}" readonly>
                                </div>
                            </div>
                        </fieldset>    
                        <button class="btn btn-primary" type="submit" name="ModificaDati" value="Modifica" id="ModificaDati" disabled="true">Modifica</button>
                    </form>
                    <a href="" data-toggle="modal" data-target="#pswModal">Cambia password</a>
                    <!-- Modal per cambio password -->
                    <div class="modal fade" id="pswModal" tabindex="-1" role="dialog" aria-labelledby="pswModal" aria-hidden="true" <c:if test="${mustChangePsw == true}">data-keyboard="false" data-backdrop="static"></c:if>>
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="Title">Modifica password</h5>
                                    <c:if test="${mustChangePsw == false}">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="<c:set var = "mustShowChangePsw" scope = "session" value = "false"/>">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </c:if>  
                                </div>
                                <div class="modal-body">
                                    <c:if test="${PSWError1 == true}">
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <strong>Errore!</strong> Le due password non coincidono
                                        </div>
                                        <c:set var="PSWError1" scope="session" value="${false}"/>
                                    </c:if>
                                    <c:if test="${PSWError2 == true}">
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <strong>Errore!</strong> Inserisci correttamente la vecchia password
                                        </div>
                                        <c:set var="PSWError2" scope="session" value="${false}"/>
                                    </c:if>
                                    <form action="change_psw.handler" method="POST" class="my_form" id="form_psw">
                                        <div class="form-group">
                                            <c:if test="${mustChangePsw == false}">
                                                Vecchia password:<br>
                                                <input type="password" name="old_psw" class="form-control">
                                            </c:if>
                                            Nuova password:<br>
                                            <input type="password" name="new_psw_1" id="new_psw_1" class="form-control">
                                            Conferma password:<br>
                                            <input type="password" name="new_psw_2" id="new_psw_2" class="form-control">
                                        </div>
                                    </form>
                                </div>
                              <div class="modal-footer">
                                <c:if test="${mustChangePsw == false}">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                </c:if>
                                <button type="submit" class="btn btn-primary" form="form_psw">Modifica Password</button>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid my_container my_margin">                        
            <div class="row my_margin">
                <div class="col-md-6 my_margin">
                    <h3>
                        Il tuo medico
                    </h3>
                    <c:out value = "dr. ${paziente.medicodibase.nome} ${paziente.medicodibase.cognome}" /> <a href="${pageContext.request.contextPath}/paziente/medici.handler">Cambia medico</a>

                    <!-- Modal per cambio medico -->
                    <div class="modal fade" id="medModal" tabindex="-1" role="dialog" aria-labelledby="medModal" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="Title">Cambia medico</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="<c:set var = "showChangeMed" scope = "session" value = "false"/>">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="${pageContext.request.contextPath}/paziente/medici.handler" method="post" id="form_med">
                                        <c:forEach items="${listaMedici}" var="item">
                                            <div class="form-check">
                                                <input type="radio" name="medico" value="${item.cf}">
                                                Dr. ${item.nome} ${item.cognome}<br>
                                                ${item.numerotelefono}<br>
                                                ${item.citta}<br><br>
                                            </div>
                                        </c:forEach>
                                    </form>                                        
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                    <button type="submit" class="btn btn-primary" form="form_med">Cambia Medico</button>
                                </div>
                            </div>
                        </div>
                    </div>

                    <br>
                    <c:out value = "${paziente.medicodibase.citta}" />
                    <br>
                    <c:out value = "${paziente.medicodibase.numerotelefono}" />
                    <br><br>
                </div>
                <div class="col my_margin">
                    <h3>
                        Data ultima visita
                    </h3>
                    <c:out value = "${paziente.dataultimavisita}" />
                    <br><br>
                    <form action="${pageContext.request.contextPath}/paziente/scaricaticketpdf.handler">
                        <button class="btn btn-primary" type="submit" name="ScaricaTicket" value="ScaricaTicket" id="ScaricaTicket">Scarica l'elenco dei tuoi ticket</button>
                    </form>
                </div>
            </div>
        </div>
        <div class="container-fluid my_container my_margin">
            <h2>
                Ricette Farmacologiche
            </h2>
            <table id="RicetteFarm" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Medico</th>
                        <th>Id Farmaco</th>
                        <th>Nome Farmaco</th>
                        <th>Data prescrizione</th>
                        <th>Data erogazione</th>
                        <th>Id Farmacia</th>
                        <th>Nome Farmacia</th>
                        <th>Scarica Ricetta</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${paziente.ricette}" var="item">
                        <tr>
                            <td>${item.id}</td>
                            <td>Dr. ${item.nome_med}</td>
                            <td>${item.idfarmaco}</td>
                            <td>${item.nomefarmaco}</td>
                            <td>${item.data_prescr}</td>
                            <td>${item.data_erog}</td>
                            <td>${item.idfarmacia}</td>
                            <td>${item.nomefarmacia}</td>
                            <td>
                                <form action="${pageContext.request.contextPath}/paziente/scaricaricetta.handler" class="my_margin">
                                    <input type="hidden"
                                           name="idricetta"
                                           value=${item.id}>
                                    <button type="submit" class="btn btn-primary ion ion-md-download my_icon">
                                </form>
                            </td>
                        </tr>
                    </c:forEach>   
                </tbody>
            </table>   
        </div>
        <div class="container-fluid my_container my_margin">
            <h2>
                Visite Specialistiche
            </h2>
            <table id="VisiteSpec" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Prescrivente</th>
                        <th>Data prescrizione</th>
                        <th>Tipo Visita</th>
                        <th>Data erogazione</th>
                        <th>Nome medico specialista</th>
                        <th>Risultati</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${paziente.visite}" var="item">
                        <tr>
                            <td>${item.id}</td>
                            <td>
                                <c:if test="${!empty item.cf_med}">Dr. ${item.nome_med}</c:if>
                                <c:if test="${item.id_ssp!=0}">SSP: ${item.id_ssp}</c:if>
                            </td>
                            <td>${item.timestamp}</td>
                            <td>${item.tipovisita}</td>
                            <td>${item.erog_time}</td>
                            <td><c:if test="${!empty item.cf_medspec}">Dr. ${item.nome_medspec}</c:if></td>
                            <td>${item.risultati}</td>
                        </tr>
                    </c:forEach>   
                </tbody>
            </table>  
        </div>
        <div class="container-fluid my_container my_margin">
            <h2>
                Esami
            </h2>
            <table id="Esami" class=" table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Prescrivente</th>
                        <th>Data prescrizione</th>
                        <th>Tipo Esame</th>
                        <th>Servizio Sanitario Provinciale</th>
                        <th>Data erogazione</th>
                        <th>Risultati</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${paziente.esami}" var="item">
                        <tr>
                            <td>${item.id}</td>
                            <td>
                                <c:if test="${!empty item.cf_med}">Dr. ${item.nome_med}</c:if>
                                <c:if test="${item.id_ssp!=0}">SSP: ${item.id_ssp}</c:if>
                            </td>
                            <td>${item.timestamp}</td>
                            <td>${item.esame.tipoesame}</td>
                            <td>
                                <c:if test="${item.id_ssp_erog!=0}">SSP: ${item.id_ssp_erog}</c:if>
                            </td>
                            <td>${item.data_erog}</td>
                            <td>${item.risultati}</td>
                        </tr>
                    </c:forEach>   
                </tbody>
            </table>
        </div>
        <div class="container-fluid my_container my_margin">
            <h2>
                Consulta qui la lista degli esami
            </h2>
            <table id="EsamiAll" class="table table-striped table-bordered dt-responsive" style="width:100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Tipo Esame</th>
                        <th>Descrizione</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${EsamiList}" var="item">
                        <tr>
                            <td>${item.idesame}</td>
                            <td>${item.tipoesame}</td>
                            <td>${item.descrizione}</td>
                        </tr>
                    </c:forEach>   
                </tbody>
            </table>  
        </div>
                    
                    

        <script>
            function activateButton() {
                document.getElementById("ModificaDati").disabled=false;
            }

            function visualizzaPath() {
                let path = document.getElementById("fileSrc").value;
                path = path.replace("C:\\fakepath\\", "");
                document.getElementById("fileSrcLabel").innerHTML=path;
            }
            
            function verifyPsw() {
                if(document.getElementById("new_psw_1").value === document.getElementById("new_psw_2").value && document.getElementById("new_psw_1").value !== '' && document.getElementById("new_psw_2").value !== '') {
                    return true;
                }else{
                    alert("Le due password non coincidono");
                    return false;
                }
            }
            
            function testPopup(psw, med) {
                if(psw) {
                    $('#pswModal').modal('show');
                }
                if(med) {
                    $('#medModal').modal('show');
                }
            }
            
            function loadTables() {
                $(document).ready(function() {
                    $('#RicetteFarm').DataTable();
                    $('#VisiteSpec').DataTable();
                    $('#Esami').DataTable();
                    $('#EsamiAll').DataTable();
                } );
            }
        </script>
        
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </body>
</html>
