<%-- 
    Document   : welcome_new
    Created on : 25 gen 2020, 17:49:51
    Author     : Luigi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Main page FARMACIA</title>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
        <link href="https://unpkg.com/ionicons@4.5.10-0/dist/css/ionicons.min.css" rel="stylesheet">
        
        <link rel="stylesheet" href="../css/Login-Form-Clean.css">
        <link rel="stylesheet" href="../css/Navigation-with-Button.css">
        <link rel="stylesheet" href="../css/Projects-Clean.css">
        <link rel="stylesheet" href="../css/styles.css">
    </head>
    <body class="my_body" onload="testPopup(${mustShowChangePsw})">
        <nav class="navbar navbar-expand-md navbar-light fixed-top my_nav">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">Portale Azienda per i Servizi Sanitari</a>
                <a href="<c:url context="${pageContext.request.contextPath}" value="/farmacia/logout.handler" />">Logout</a>
            </div>
        </nav>
        <div class="container-fluid my_container my_margin">
            <div class="row my_margin">
                <div class="col">
                    <form action="${pageContext.request.contextPath}/farmacia/modificadati.handler" class="my_margin" method="POST">
                        <fieldset>
                            <legend>Dati Personali</legend>
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="ion ion-md-person my_icon"></span>
                                    </div>
                                    <input type="text" class="form-control" name="nome" value="${farm.nomefarmacia}" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="ion ion-md-card my_icon"></span>
                                    </div>
                                    <input type="text" class="form-control" name="cf" value="${farm.idfarmacia}" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="ion ion-md-mail my_icon"></span>
                                    </div>
                                    <input type="email" class="form-control"  name="email" value="${farm.email}" oninput="activateButton()" >
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Indirizzo</legend>
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="ion ion-md-home my_icon"></span>
                                    </div>
                                    <input type="text" class="form-control" name="provincia" value="${farm.provincia}" readonly>
                                    <input type="text" class="form-control" name="citta" value="${farm.citta}" readonly>
                                    <input type="text" class="form-control" name="cap" value="${farm.cap}" readonly>
                                    <input type="text" class="form-control" name="indirizzo" value="${farm.indirizzo}" readonly>
                                </div>
                            </div>
                        </fieldset>    
                        <button class="btn btn-primary" type="submit" name="ModificaDati" value="Modifica" id="ModificaDati" disabled="true">Modifica</button>
                    </form>
                                
                    <a href="" data-toggle="modal" data-target="#pswModal">Cambia password</a>
                    <!--Modal per cambio password-->
                    <div class="modal fade" id="pswModal" tabindex="-1" role="dialog" aria-labelledby="pswModal" aria-hidden="true" <c:if test="${mustChangePsw == true}">data-keyboard="false" data-backdrop="static"></c:if>>
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="Title">Modifica password</h5>
                                    <c:if test="${mustChangePsw == false}">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="<c:set var = "mustShowChangePsw" scope = "session" value = "false"/>">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </c:if>   
                                </div>
                                <div class="modal-body">
                                    <c:if test="${PSWError1 == true}">
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <strong>Errore!</strong> Le due password non coincidono
                                        </div>
                                        <c:set var="PSWError1" scope="session" value="${false}"/>
                                    </c:if>
                                    <c:if test="${PSWError2 == true}">
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <strong>Errore!</strong> Inserisci correttamente la vecchia password
                                        </div>
                                        <c:set var="PSWError2" scope="session" value="${false}"/>
                                    </c:if>
                                    <form action="change_psw.handler" method="POST" class="my_form" id="form_psw">
                                        <div class="form-group">
                                            <c:if test="${mustChangePsw == false}">
                                                Vecchia password:<br>
                                                <input type="password" name="old_psw" class="form-control">
                                            </c:if>
                                            Nuova password:<br>
                                            <input type="password" name="new_psw_1" id="new_psw_1" class="form-control">
                                            Conferma password:<br>
                                            <input type="password" name="new_psw_2" id="new_psw_2" class="form-control">
                                        </div>
                                    </form>
                                </div>
                              <div class="modal-footer">
                                <c:if test="${mustChangePsw == false}">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                </c:if>
                                <button type="submit" class="btn btn-primary" form="form_psw">Modifica Password</button>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid my_container my_margin">
            <form action="${pageContext.request.contextPath}/farmacia/ricettepaziente.handler" class="my_margin" method="POST">
                <label for="pazienti">Cerca i pazienti: </label>
                <div class="input-group mb-3">
                    <input list="pazienti" name="cf" type="text" class="form-control" placeholder="Codice Fiscale" required pattern="[A-Z]{6}[0-9]{2}[A-Z]{1}[0-9]{2}[A-Z]{1}[0-9]{3}[A-Z]{1}">
                    <datalist id="pazienti">
                        <c:forEach items="${PazientiList}" var="paziente">
                                <option value=${paziente.codicefiscale}>${paziente.nome} ${paziente.cognome}</option>
                        </c:forEach>
                    </datalist>
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="submit">Visualizza scheda paziente</button>
                    </div>
                </div>
            </form>
        </div>
    
        <script>
            function activateButton() {
                document.getElementById("ModificaDati").disabled=false;
            }
            
            function verifyPsw() {
                if(document.getElementById("new_psw_1").value === document.getElementById("new_psw_2").value && document.getElementById("new_psw_1").value !== '' && document.getElementById("new_psw_2").value !== '') {
                    return true;
                }else{
                    alert("Le due password non coincidono");
                    return false;
                }
            }
            
            function testPopup(psw) {
                if(psw) {
                    $('#pswModal').modal('show');
                }
            }
        </script>
        
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </body>
</html>