<%-- 
    Document   : schedapaziente_new
    Created on : 24 gen 2020, 16:16:35
    Author     : Luigi
--%>

<%@page import="com.itextpdf.text.Document"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Cartella ${paziente.cognome} ${paziente.nome}</title>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
        <link href="https://unpkg.com/ionicons@4.5.10-0/dist/css/ionicons.min.css" rel="stylesheet">
        
        <link rel="stylesheet" href="../css/Login-Form-Clean.css">
        <link rel="stylesheet" href="../css/Navigation-with-Button.css">
        <link rel="stylesheet" href="../css/Projects-Clean.css">
        <link rel="stylesheet" href="../css/styles.css">
    </head>
    <body class="my_body" onload="loadTables()">
        <nav class="navbar navbar-expand-md navbar-light fixed-top my_nav">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">Portale Azienda per i Servizi Sanitari</a>
                <a href="<c:url context="${pageContext.request.contextPath}" value="/medicospec/schedapaziente.handler" />" class="ion ion-md-arrow-round-back my_icon" style="margin: initial"> Indietro </a>
            </div>
        </nav>
        <div class="container-fluid my_container my_margin">
            <div class="row my_margin">
                <div class="col-md-6">
                    <c:choose>
                        <c:when test="${!empty paziente.elencofoto}">
                            <div class="carousel slide my_margin my_carousel" data-ride="carousel" id="carousel-1">
                                <div class="carousel-inner" role="listbox">
                                    <div class="carousel-item active">
                                        <c:set var = "tmp" scope="request" value="${fn:substringAfter(paziente.elencofoto[0], '.')}"/>
                                        <img class="w-100 d-block" src= "../paziente${tmp}" alt="Slide Image">
                                    </div>
                                    <c:forEach items="${paziente.elencofoto}" begin="1" var="item">
                                        <div class="carousel-item">
                                            <c:set var = "item" value="${fn:substringAfter(item, '.')}"/>
                                            <img class="w-100 d-block" src= "../paziente${item}" alt="Slide Image">
                                        </div>
                                    </c:forEach>
                                </div>
                                <div>
                                    <a class="carousel-control-prev" href="#carousel-1" role="button" data-slide="prev">
                                        <span class="carousel-control-prev-icon"></span>
                                        <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carousel-1" role="button" data-slide="next">
                                        <span class="carousel-control-next-icon"></span>
                                        <span class="sr-only">Next</span>
                                    </a>
                                </div>
                                <ol class="carousel-indicators">
                                    <li data-target="#carousel-1" data-slide-to="0" class="active"></li>
                                    <c:forEach items="${paziente.elencofoto}" begin="1" var="item" varStatus="loop">
                                        <li data-target="#carousel-1" data-slide-to="${loop.index}"></li>
                                    </c:forEach>
                                </ol>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <img class="w-100 d-block my_placeholder" src="../images/placeholder.png" alt="Slide Image" >
                        </c:otherwise>
                    </c:choose>
                </div>        
                <div class="col">
                    <form class="my_margin">
                        <fieldset>
                            <legend>Dati Personali</legend>
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="ion ion-md-person my_icon"></span>
                                    </div>
                                    <input type="text" class="form-control" name="nome" value="${paziente.nome}" readonly>
                                    <input type="text" class="form-control" name="cognome" value="${paziente.cognome}" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="ion ion-md-card my_icon"></span>
                                    </div>
                                    <input type="text" class="form-control" name="cf" value="${paziente.codicefiscale}" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="ion ion-md-calendar my_icon"></span>
                                    </div>
                                    <input type="text" class="form-control" name="datanascita" value="${paziente.datanascita}" readonly>
                                    <input type="text" class="form-control" name="luogonascita" value="${paziente.luogonascita}" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="ion ion-md-mail my_icon"></span>
                                    </div>
                                    <input type="email" class="form-control"  name="email" value="${paziente.email}" oninput="activateButton()" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="ion ion-md-call my_icon"></span>
                                    </div>
                                    <input type="tel" class="form-control"  name="numerotelefono" value="${paziente.numerotelefono}" oninput="activateButton()" required minlength="9" maxlength="11" readonly>
                                </div>
                            </div>
                        </fieldset>
                        <fieldset>
                            <legend>Dati di Residenza</legend>
                            <div class="form-group">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="ion ion-md-home my_icon"></span>
                                    </div>
                                    <input type="text" class="form-control" name="provincia" value="${paziente.provincia}" readonly>
                                    <input type="text" class="form-control" name="citta" value="${paziente.citta}" readonly>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <div class="container-fluid my_container my_margin">
            <h2>
                Visite Specialistiche
            </h2>
            <table id="VisiteSpec" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th data-priority="1">Id</th>
                        <th>Prescrivente</th>
                        <th>Data prescrizione</th>
                        <th>Tipo Visita</th>
                        <th data-priority="2">Data erogazione</th>
                        <th>Nome medico specialista</th>
                        <th>Risultati</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${paziente.visite}" var="item">
                        <tr>
                            <td>${item.id}</td>
                            <td>
                                <c:if test="${!empty item.cf_med}">Dr. ${item.nome_med}</c:if>
                                <c:if test="${item.id_ssp!=0}">SSP: ${item.id_ssp}</c:if>
                            </td>
                            <td>${item.timestamp}</td>
                            <td>${item.tipovisita}</td>
                            <c:choose>
                                <c:when test="${!empty item.erog_time}">
                                    <td>${item.erog_time}</td>
                                    <td>Dr. ${item.nome_medspec}</td>
                                    <td>${item.risultati}</td>
                                </c:when>
                                <c:otherwise>
                                    <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#erogModal${item.id}">Eroga</button></td>
                                    <!-- Modal per prescrizione esame -->
                                    <div class="modal fade" id="erogModal${item.id}" tabindex="-1" role="dialog" aria-labelledby="erogModal${item.id}" aria-hidden="true">
                                        <div class="modal-dialog modal-dialog-centered" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="Title">Eroga Visita Specialistica</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="${pageContext.request.contextPath}/medicospec/erogavisita.handler" method="POST" id="form_esame">
                                                        <input type="hidden" name="visita_id" value=${item.id}>
                                                        <input type="hidden" name="idssp" value="${item.id_ssp}">
                                                        Risultati:<br>
                                                        <textarea class="form-control" rows="5" name="risultati" pattern=".{1,255}" required title="Inserisci da 1 a 255 caratteri" maxlength="255"></textarea>
                                                    </form>
                                                </div>
                                              <div class="modal-footer">
                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                                    <button type="submit" class="btn btn-primary" form="form_esame">Eroga</button>
                                              </div>
                                            </div>
                                        </div>
                                    </div>
                                    <td></td>
                                    <td></td>
                                </c:otherwise>
                            </c:choose>
                                
                        </tr>
                    </c:forEach>   
                </tbody>
            </table>  
        </div>
        <div class="container-fluid my_container my_margin">
            <h2>
                Ricette Farmacologiche
            </h2>
            <table id="RicetteFarm" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Medico</th>
                        <th>Id Farmaco</th>
                        <th>Nome Farmaco</th>
                        <th>Data prescrizione</th>
                        <th>Data erogazione</th>
                        <th>Id Farmacia</th>
                        <th>Nome Farmacia</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${paziente.ricette}" var="item">
                        <tr>
                            <td>${item.id}</td>
                            <td>Dr. ${item.nome_med}</td>
                            <td>${item.idfarmaco}</td>
                            <td>${item.nomefarmaco}</td>
                            <td>${item.data_prescr}</td>
                            <td>${item.data_erog}</td>
                            <td><c:if test="${item.idfarmacia != 0}">${item.idfarmacia}</c:if></td>
                            <td>${item.nomefarmacia}</td>
                        </tr>
                    </c:forEach>   
                </tbody>
            </table>   
        </div>
        <div class="container-fluid my_container my_margin">
            <h2>
                Esami
            </h2>
            <table id="Esami" class=" table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Prescrivente</th>
                        <th>Data prescrizione</th>
                        <th>Tipo Esame</th>
                        <th>Servizio Sanitario Provinciale</th>
                        <th>Data erogazione</th>
                        <th>Risultati</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${paziente.esami}" var="item">
                        <tr>
                            <td>${item.id}</td>
                            <td>
                                <c:if test="${!empty item.cf_med}">Dr. ${item.nome_med}</c:if>
                                <c:if test="${item.id_ssp!=0}">SSP: ${item.id_ssp}</c:if>
                            </td>
                            <td>${item.timestamp}</td>
                            <td>${item.esame.tipoesame}</td>
                            <td>${item.id_ssp_erog}</td>
                            <td>${item.data_erog}</td>
                            <td>${item.risultati}</td>
                        </tr>
                    </c:forEach>   
                </tbody>
            </table>
        </div>
                    
                    

        <script>            
            function loadTables() {
                $(document).ready(function() {
                    $('#RicetteFarm').DataTable();
                    $('#VisiteSpec').DataTable();
                    $('#Esami').DataTable();
                } );
            }
        </script>
        
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </body>
</html>