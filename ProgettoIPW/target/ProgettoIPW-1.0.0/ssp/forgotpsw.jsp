<%-- 
    Document   : forgotpsw
    Created on : 27 dic 2019, 15:29:17
    Author     : Luigi
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Recupero password</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <c:if test="${IDError == true}">
            <c:out value="${'Inserisci un codice identificativo valido'}"/><br>  
        </c:if>
        <h3>
            Inserisci qui il tuo codice identificativo e ti verrà inviata una mail con una password temporanea valida per il prossimo accesso, dopo il quale ti verrà chiesto di cambiarla.
        </h3>
        <form action="forgot_psw.handler" method="POST">
            Codice Identificativo:<br>
            <input type="text" name="id_ssp" required pattern="[0-9]{6}"><br><br>
            <input type="submit" name="Recupera Password" value="Recupera password">
        </form>
    </body>
</html>
