<%-- 
    Document   : esamipaziente_new
    Created on : 27 gen 2020, 16:16:53
    Author     : Luigi
--%>

<%@page import="com.itextpdf.text.Document"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Esami ${actual_paz_cf}</title>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css">
        <link href="https://unpkg.com/ionicons@4.5.10-0/dist/css/ionicons.min.css" rel="stylesheet">
        
        <link rel="stylesheet" href="../css/Login-Form-Clean.css">
        <link rel="stylesheet" href="../css/Navigation-with-Button.css">
        <link rel="stylesheet" href="../css/Projects-Clean.css">
        <link rel="stylesheet" href="../css/styles.css">
    </head>
    <body class="my_body" onload="loadTables()">
        <nav class="navbar navbar-expand-md navbar-light fixed-top my_nav">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">Portale Azienda per i Servizi Sanitari</a>
                <a href="<c:url context="${pageContext.request.contextPath}" value="/ssp/esamipaziente.handler" />" class="ion ion-md-arrow-round-back my_icon" style="margin: initial"> Indietro </a>
            </div>
        </nav>
        <div class="container-fluid my_container my_margin" style="text-align: center">
            <h1>
                Paziente: ${cf_paz}
            </h1>
        </div>
        <div class="container-fluid my_container my_margin">
            <h2>
                Esami Non Erogati
            </h2>
            <table id="EsamiNonErog" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Prescrivente</th>
                        <th>Data prescrizione</th>
                        <th>Tipo Esame</th>
                        <th data-priority="1">Erogazione</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${esaminonerog}" var="item">
                        <tr>
                            <td>${item.id}</td>
                            <td>
                                <c:if test="${not empty item.cf_med}">Dr. ${item.nome_med}</c:if>
                                <c:if test="${item.id_ssp!=0}">SSP. ${item.id_ssp}</c:if> 
                            </td>
                            <td>${item.timestamp}</td>
                            <td>${item.esame.tipoesame}</td>
                            <td><button type="button" class="btn btn-primary" data-toggle="modal" data-target="#erogModal${item.id}">Eroga</button></td>
                            <!-- Modal per erogazione farmaco -->
                            <div class="modal fade" id="erogModal${item.id}" tabindex="-1" role="dialog" aria-labelledby="erogModal${item.id}" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="Title">Eroga Esame</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="${pageContext.request.contextPath}/ssp/erogaesame.handler" method="POST" id="form_erog">
                                                <input type="hidden" name="esame_id" value=${item.id}>
                                                <input type="hidden" name="idssp"  value=${item.id_ssp}>
                                                Sei sicuro di voler erogare l'esame?
                                            </form>
                                        </div>
                                      <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                            <button type="submit" class="btn btn-primary" form="form_erog">Eroga</button>
                                      </div>
                                    </div>
                                </div>
                            </div>
                                
                        </tr>
                    </c:forEach>   
                </tbody>
            </table>  
        </div>
        <div class="container-fluid my_container my_margin">
            <h2>
                Esami Erogati
            </h2>
            <table id="EsamiErog" class="table table-striped table-bordered dt-responsive nowrap" style="width:100%">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Prescrivente</th>
                        <th>Data prescrizione</th>
                        <th>Tipo Esame</th>
                        <th>Servizio Sanitario Provinciale</th>
                        <th>Data erogazione</th>
                        <th>Risultati</th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${esamierog}" var="item">
                        <tr>
                            <td>${item.id}</td>
                            <td>
                                <c:if test="${not empty item.nome_med}">Dr. ${item.nome_med}</c:if>
                                <c:if test="${item.id_ssp!=0}">SSP. ${item.id_ssp}</c:if> 
                            </td>
                            <td>${item.timestamp}</td>
                            <td>${item.esame.tipoesame}</td>
                            <td>${item.id_ssp_erog}</td>
                            <td>${item.data_erog}</td>
                            <td>${item.risultati}</td>
                        </tr>
                    </c:forEach>   
                </tbody>
            </table>   
        </div>                          
                    

        <script>            
            function loadTables() {
                $(document).ready(function() {
                    $('#EsamiErog').DataTable();
                    $('#EsamiNonErog').DataTable();
                } );
            }
        </script>
        
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.3/js/responsive.bootstrap4.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </body>
</html>

