<%-- 
    Document   : welcome_new
    Created on : 27 gen 2020, 12:02:25
    Author     : Luigi
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Main page SSP</title>
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link href="https://unpkg.com/ionicons@4.5.10-0/dist/css/ionicons.min.css" rel="stylesheet">
        
        <link rel="stylesheet" href="../css/Login-Form-Clean.css">
        <link rel="stylesheet" href="../css/Navigation-with-Button.css">
        <link rel="stylesheet" href="../css/Projects-Clean.css">
        <link rel="stylesheet" href="../css/styles.css">
    </head>
    <body class="my_body" onload="testPopup(${mustShowChangePsw}, ${EsameError})">
        <nav class="navbar navbar-expand-md navbar-light fixed-top my_nav">
            <div class="container-fluid">
                <a class="navbar-brand" href="#">Portale Azienda per i Servizi Sanitari</a>
                <a href="<c:url context="${pageContext.request.contextPath}" value="/ssp/logout.handler" />">Logout</a>
            </div>
        </nav>
        <div class="container-fluid my_container my_margin">
            <div class="row my_margin">
                <div class="col">
                                
                    <a href="" data-toggle="modal" data-target="#pswModal">Cambia password</a>
                    <!--Modal per cambio password-->
                    <div class="modal fade" id="pswModal" tabindex="-1" role="dialog" aria-labelledby="pswModal" aria-hidden="true" <c:if test="${mustChangePsw == true}">data-keyboard="false" data-backdrop="static"></c:if>>
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="Title">Modifica password</h5>
                                    <c:if test="${mustChangePsw == false}">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="<c:set var = "mustShowChangePsw" scope = "session" value = "false"/>">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </c:if>
                                </div>
                                <div class="modal-body">
                                    <c:if test="${PSWError1 == true}">
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <strong>Errore!</strong> Le due password non coincidono
                                        </div>
                                        <c:set var="PSWError1" scope="session" value="${false}"/>
                                    </c:if>
                                    <c:if test="${PSWError2 == true}">
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <strong>Errore!</strong> Inserisci correttamente la vecchia password
                                        </div>
                                        <c:set var="PSWError2" scope="session" value="${false}"/>
                                    </c:if>
                                    <form action="change_psw.handler" method="POST" class="my_form" id="form_psw">
                                        <div class="form-group">
                                            <c:if test="${mustChangePsw == false}">
                                                Vecchia password:<br>
                                                <input type="password" name="old_psw" class="form-control">
                                            </c:if>
                                            Nuova password:<br>
                                            <input type="password" name="new_psw_1" id="new_psw_1" class="form-control">
                                            Conferma password:<br>
                                            <input type="password" name="new_psw_2" id="new_psw_2" class="form-control">
                                        </div>
                                    </form>
                                </div>
                              <div class="modal-footer">
                                <c:if test="${mustChangePsw == false}">
                                     <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button> 
                                </c:if>
                                <button type="submit" class="btn btn-primary" form="form_psw">Modifica Password</button>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid my_container my_margin">
            <form action="${pageContext.request.contextPath}/ssp/esamipaziente.handler" class="my_margin" method="POST">
                <label for="pazienti">Cerca i pazienti: </label>
                <div class="input-group mb-3">
                    <input list="pazienti" name="cf" type="text" class="form-control" placeholder="Codice Fiscale" required pattern="[A-Z]{6}[0-9]{2}[A-Z]{1}[0-9]{2}[A-Z]{1}[0-9]{3}[A-Z]{1}">
                    <datalist id="pazienti">
                        <c:forEach items="${PazientiList}" var="paziente">
                                <option value=${paziente.codicefiscale}>${paziente.nome} ${paziente.cognome}</option>
                        </c:forEach>
                    </datalist>
                    <div class="input-group-append">
                        <button class="btn btn-outline-secondary" type="submit">Visualizza esami paziente</button>
                    </div>
                </div>
            </form>
        </div>
                
        <div class="container-fluid my_container my_margin">
            <div class="row my_margin">
                <div class="col-md" style="text-align: left">
                    <h3>Prescrivi Esame di Richiamo</h3><br>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#esameModal"><span class="ion ion-md-clipboard my_icon_big"></span></button>
                    <!-- Modal per prescrizione esame -->
                    <div class="modal fade" id="esameModal" tabindex="-1" role="dialog" aria-labelledby="esameModal" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="Title">Prescrivi Esame</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <c:if test="${EsameError}">
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                            <strong>Attenzione!</strong> Inserire ID esame valido
                                        </div>
                                        <c:set var="EsameError" value="${false}" scope="session"/>
                                    </c:if>
                                    <form action="${pageContext.request.contextPath}/ssp/prescrivirichiamo.handler" method="POST" id="form_esame" onsubmit="return checkDatesEsami()">
                                        Nati dal:<br>
                                        <input class="form-control" type="date" name="min" id="minE" required>
                                        al:<br>
                                        <input class="form-control" type="date" name="max" id="maxE" required>
                                        Id dell'esame:<br>
                                        <input list="esami" name="esame" class="form-control">
                                        <datalist id="esami">
                                            <c:forEach items="${EsamiList}" var="esame">
                                                <option value=${esame.idesame}>${esame.tipoesame}</option>
                                            </c:forEach>
                                        </datalist><br>
                                    </form>
                                </div>
                              <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                    <button type="submit" class="btn btn-primary" form="form_esame">Prescrivi</button>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row my_margin">
                <div class="col" style="text-align: left">
                    <h3>Prescrivi Visita Specialistica di Richiamo</h3><br>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#visitaModal"><span class="ion ion-md-pulse my_icon_big"></span></button>
                    <!-- Modal per prescrizione visita -->
                    <div class="modal fade" id="visitaModal" tabindex="-1" role="dialog" aria-labelledby="visitaModal" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="Title">Prescrivi Visita Specialistica di Richiamo</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form action="${pageContext.request.contextPath}/ssp/prescrivirichiamo.handler" method="POST" id="form_visita" onsubmit="return checkDatesVisite()">
                                        Nati dal:<br>
                                        <input class="form-control" type="date" name="min" id="minV" required>
                                        al:<br>
                                        <input class="form-control" type="date" name="max" id="maxV" required>
                                        Tipo di visita:<br>
                                        <input type="text" name="visita" class="form-control" required minlength="1">
                                    </form>
                                </div>
                              <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                    <button type="submit" class="btn btn-primary" form="form_visita">Prescrivi</button>
                              </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid my_container my_margin">
            <div class="row my_margin">
                <div class="col" style="text-align: left; margin: auto">
                    <h3>Scarica report completo dei servizi erogati</h3><br>
                    <form action="${pageContext.request.contextPath}/ssp/esportareportxls.handler">
                        <button type="submit" class="btn btn-primary" ><span class="ion ion-md-document my_icon_big"></span></button>
                    </form>
                </div>
            </div>
        </div>
    
        <script>            
            function testPopup(psw, esame) {
                if(psw) {
                    $('#pswModal').modal('show');
                }
                if(esame) {
                    $('#esameModal').modal('show');
                }
            }
            
            function checkDatesVisite() {
                if(Date.parse(document.getElementById("maxV").value)=Date.parse(document.getElementById("minV").value)) {
                    alert("Inserisci date valide!!!");
                    return false;
                }else{
                    return true;
                }
            }
            
            function checkDatesEsami() {
                if(Date.parse(document.getElementById("maxE").value)=Date.parse(document.getElementById("minE").value)) {
                    alert("Inserisci date valide!!!");
                    return false;
                }else{
                    return true;
                }
            }
        </script>
        
        <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </body>
</html>
