<%-- 
    Document   : forgotpsw
    Created on : 27 dic 2019, 15:13:05
    Author     : Luigi
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Recupero password</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
    </head>
    <body>
        <c:if test="${CFError == true}">
            <c:out value="${'Inserisci un codice fiscale valido'}"/><br>  
        </c:if>
        <h3>
            Inserisci qui il tuo codice fiscale e ti verrà inviata una mail con una password temporanea valida per il prossimo accesso, dopo il quale ti verrà chiesto di cambiarla.
        </h3>
        <form action="forgot_psw.handler" method="POST">
            Codice Fiscale:<br>
            <input type="text" name="cf_med" required pattern="[A-Z]{6}[0-9]{2}[A-Z]{1}[0-9]{2}[A-Z]{1}[0-9]{3}[A-Z]{1}"><br><br>
            <input type="submit" name="Recupera Password" value="Recupera password">
        </form>
    </body>
</html>
