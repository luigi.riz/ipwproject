<%-- 
    Document   : login.jsp
    Created on : 28 gen 2020, 17:13:59
    Author     : Luigi
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <title>Main Page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
        
        
        <link href="css/Login-Form-Clean.css" rel="stylesheet">
        <link href="css/Navigation-with-Button.css" rel="stylesheet">
        <link href="css/Projects-Clean.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
    </head>
    <body onload="getRicordami(); testError(${Error});" class="my_body">
        <script>   
            function getRicordami() {
                var key = localStorage.getItem("primary_key");
                var psw = localStorage.getItem("psw");
                var selected = localStorage.getItem("selected");
                if (key!==null && psw !==null) {  
                    document.getElementById("primary_key").setAttribute("value", key);
                    document.getElementById("password").setAttribute("value", psw);
                    if(selected === "paz") {
                        document.getElementById("Paziente").setAttribute("checked", true);
                    }
                    if(selected === "med") {
                        document.getElementById("Medico").setAttribute("checked", true);
                    }
                    if(selected === "farm") {
                        document.getElementById("Farmacia").setAttribute("checked", true);
                    }
                    if(selected === "ssp") {
                        document.getElementById("SSP").setAttribute("checked", true);
                    }
                    document.getElementById("rememberMe").setAttribute("checked", true);
                }
                setLogin();
            }
            function setRicordami() {
                var key = document.forms["form"]["primary_key"];
                var password = document.forms["form"]["password"];
                var pazSelected = document.getElementById("Paziente").checked;
                var medSelected = document.getElementById("Medico").checked;
                var farmSelected = document.getElementById("Farmacia").checked;
                var sspSelected = document.getElementById("SSP").checked;
                
                if (document.getElementById("rememberMe").checked) {
                    localStorage.setItem("primary_key", key.value);
                    localStorage.setItem("psw", password.value);
                    if(pazSelected) {
                        localStorage.setItem("selected", "paz");
                    }
                    if(medSelected) {
                        localStorage.setItem("selected", "med");
                    }
                    if(farmSelected) {
                        localStorage.setItem("selected", "farm");
                    }
                    if(sspSelected) {
                        localStorage.setItem("selected", "ssp");
                    }
                }else{
                    localStorage.removeItem("primary_key");
                    localStorage.removeItem("psw");
                    localStorage.removeItem("selected");
                }
            }

            function validateForm(){
                var key = document.forms["form"]["primary_key"];
                var password = document.forms["form"]["password"];

                if (key.value === ""){
                    if(document.getElementById("Paziente").checked || document.getElementById("Medico").checked) {
                        window.alert("Inserisci un Codice Fiscale!"); 
                    }else{
                        window.alert("Inserisci un Codice Identificativo!");
                    }
                    key.focus(); 
                    return false; 
                } 

                if (password.value === "" ){ 
                    window.alert("Inserisci una password!");
                    password.focus();
                    return false; 
                }
                
                var farmSelected = document.getElementById("Farmacia").checked;
                var sspSelected = document.getElementById("SSP").checked;
                
                if (farmSelected || sspSelected) {
                    if(isNaN(key.value)) {
                        window.alert("Inserisci id valido");
                        return false;
                    } 
                }
                setRicordami();
                return true;
            }
            
            function setLogin() {
                var pazSelected = document.getElementById("Paziente").checked;
                var medSelected = document.getElementById("Medico").checked;
                var farmSelected = document.getElementById("Farmacia").checked;
                var sspSelected = document.getElementById("SSP").checked;
                
                if(pazSelected || medSelected) {
                    document.getElementById("primary_key").setAttribute("placeholder", "Codice Fiscale");
                    if (pazSelected) {
                        document.getElementById("form").setAttribute("action", "paziente/login.handler");
                    }else{
                        document.getElementById("form").setAttribute("action", "medico/login.handler");
                    }
                }else{
                    document.getElementById("primary_key").setAttribute("placeholder", "Codice Identificativo");
                    if (farmSelected) {
                        document.getElementById("form").setAttribute("action", "farmacia/login.handler");
                    }else{
                        document.getElementById("form").setAttribute("action", "ssp/login.handler");
                    }
                }
                
            }
        </script>
        
        <nav class="navbar navbar-expand-md navbar-light fixed-top my_nav">
            <div class="container"><a class="navbar-brand" href="home.html">Portale Azienda per i Servizi Sanitari</a></div>
        </nav>
        <div class="login-clean my_form">
            <form method="post" action="paziente/login.handler" onsubmit="return validateForm()" id="form" name="form">
                <h2 class="sr-only">Login Form</h2>
                <div class="illustration">
                    <img class="img-fluid" src="images/Icona.png" style="max-height: 350px">
                </div>
                <div class="form-group">
                    <input class="form-control" type="text" id="primary_key" name="primary_key" placeholder="Codice Fiscale">
                </div>
                <div class="form-group">
                    <input class="form-control" type="password" id="password" name="password" placeholder="Password">
                </div>
                <div class="form-group form-group-balanced">
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" id="Paziente" name="gruppo1" checked="" onclick="setLogin()">
                        <label class="form-check-label" for="formCheck-1">Paziente</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" id="Medico" name="gruppo1" onclick="setLogin()">
                        <label class="form-check-label" for="formCheck-2">Medico</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" id="Farmacia" name="gruppo1" onclick="setLogin()">
                        <label class="form-check-label" for="formCheck-3">Farmacia</label>
                    </div>
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" id="SSP" name="gruppo1" onclick="setLogin()">
                        <label class="form-check-label" for="formCheck-4">SSP</label>
                    </div>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" id="rememberMe" name="rememberMe">
                    <label class="form-check-label" for="formCheck-5">Ricordami</label>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary btn-block" type="submit">Log In</button>
                </div>
                <a class="forgot" href="" data-toggle="modal" data-target="#recoverPswModal">Password dimenticata?</a>
            </form>
        </div>
        <!--Modal Recupero password-->
        <div class="modal fade" id="recoverPswModal" tabindex="-1" role="dialog" aria-labelledby="recoverPswModal" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="Title">Recupero password</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" >
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <c:if test="${Error == true}">
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <strong>Errore!</strong> Credenziali di accesso non valide
                            </div>
                        </c:if>
                        <p class="text-justify font-italic">
                            Inserisci qui il tuo username e ti verr&agrave; inviata una mail con una password temporanea valida per il prossimo accesso, dopo il quale ti verr&agrave; chiesto di cambiarla.
                        </p>
                        <form action="forgot_psw.handler" method="POST" class="my_form" id="form_rec">
                            <div class="form-group">
                                Username/Codice di accesso al sistema:<br>
                                <input type="text" name="user" id="user" required pattern="[A-Z]{6}[0-9]{2}[A-Z]{1}[0-9]{2}[A-Z]{1}[0-9]{3}[A-Z]{1}|[0-9]{6}" class="form-control">
                            </div>
                        </form>
                    </div>
                  <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                        <button type="submit" class="btn btn-primary" form="form_rec" style="margin: inherit;padding: .375rem .75rem;border: 1px solid transparent;">Modifica Password</button>
                  </div>
                </div>
            </div>
        </div>
        
        <script>
            function testError(error) {
                if(error) {
                    $('#recoverPswModal').modal('show');
                }
            }
        </script>
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
    </body>
</html>
