<%-- 
    Document   : cercapaziente
    Created on : 27-ago-2019, 9.39.43
    Author     : Sean
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cerca paziente</title>
    </head>
    <body>
         <script>
        function validateForm()
        {
            var cf = document.forms["form_richiamo"]["cf"];
           
        if (cf.value == "")                                  
                { 
                    window.alert("Inserisci un Codice fiscale!"); 
                    cf.focus(); 
                    return false; 
                } 
                
           return true;
        }
        </script>
       <form action="${pageContext.request.contextPath}/medicospec/visitepaziente.handler" onsubmit="return validateForm()" name="form_richiamo">
        Inserisci codice fiscale del paziente:<br>
        <input list="pazienti" name="cf">
        <datalist id="pazienti">
            <c:forEach items="${PazientiList}" var="paziente">
                    <option value=${paziente.codicefiscale}>${paziente.nome} ${paziente.cognome}</option>
            </c:forEach>
        </datalist><br>
        <input type="submit" value="Cerca">
       </form>
    </body>
</html>
