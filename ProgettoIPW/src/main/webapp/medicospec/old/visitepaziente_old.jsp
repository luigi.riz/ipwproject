<%-- 
    Document   : visitepaziente
    Created on : 27-ago-2019, 9.40.04
    Author     : Sean
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <script>
        function validateForm()
        {
            var risultati = document.forms["form_richiamo"]["risultati"];
           
        if (risultati.value == "")                                  
                { 
                    window.alert("Inserisci i risultati della visita!"); 
                    risultati.focus(); 
                    return false; 
                } 
                
           return true;
        }
        </script>
        <h2>
            Visite erogate per il paziente ***
        </h2>
        <c:forEach items="${visiteerog}" var="visita">
          ${visita.id} ${visita.cf_med} ${visita.timestamp} ${visita.tipovisita} ${visita.erog_time} ${visita.risultati}
          <br>
        </c:forEach>
          <h2>
              Visite non erogate per il paziente ***
          </h2>
         <c:forEach items="${visitenonerog}" var="visita">
          ${visita.id} ${visita.cf_med} ${visita.id_ssp} ${visita.timestamp} ${visita.tipovisita} 
          <form action="${pageContext.request.contextPath}/medicospec/erogavisita.handler" onsubmit="return validateForm()" name="form_richiamo" method="post">
                <input type="hidden"
                       name="visita_id"
                       value=${visita.id}>
                <input type="hidden"
                       name="idssp"
                       value="${visita.id_ssp}">
                <input type="text"
                       name="risultati">
                <input type="submit" 
                       value="Eroga">
            </form>
          <br>
        </c:forEach>
    </body>
</html>