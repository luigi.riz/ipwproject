<%-- 
    Document   : welcome
    Created on : 23-ago-2019, 14.16.10
    Author     : Sean
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Pagina Medico Specialista</title>
    </head>
    <body>
        <h1>
            <c:choose>
                <c:when test="${medicospec.sesso == 'M'}">
                    Benvenuto
                </c:when>
                <c:otherwise>
                    Benvenuta
                </c:otherwise>
            </c:choose>
        </h1>
        <c:out value= "${medicospec.nome} ${medicospec.cognome}" ></c:out>
        <form action="${pageContext.request.contextPath}/medicospec/modificadati.handler" method="POST">
            <fieldset>
                <legend>Dati Personali:</legend>
                    Codice Fiscale<br>
                    <input type="text" name="cf" value="${medicospec.codicefiscale}" disabled><br>
                    Nome:<br>
                    <input type="text" name="nome" value="${medicospec.nome}" disabled><br>
                    Cognome:<br>
                    <input type="text" name="cognome" value="${medicospec.cognome}" disabled><br>
                    Luogo di nascita:<br>
                    <input type="text" name="luogonascita" value="${medicospec.luogonascita}" disabled><br>
                    Data di nascita<br>
                    <input type="text" name="datanascita" value="${medicospec.datanascita}" disabled><br>
                    Email:<br>
                    <input type="email" name="email" value="${medicospec.email}" oninput="activateButton()" ><br>
                    Numero di telefono:<br>
                    <input type="tel" name="numerotelefono" value="${medicospec.numerotelefono}" oninput="activateButton()" required pattern="[0-9]{3}/[0-9]{7}">
                    <small>Formato: 123/4567890</small><br>
            </fieldset>
            <fieldset>
                    <legend> Dati di residenza:</legend>
                    Provincia:<br>
                    <input type="text" name="provincia" value="${medicospec.provincia}" disabled><br>  
                    Città:<br>
                    <input type="text" name="citta" value="${medicospec.citta}" disabled><br>
            </fieldset>
            <input type="submit" name="ModificaDati" value="Modifica" id="ModificaDati" disabled="true">
        </form>
        <br><a href="${pageContext.request.contextPath}/medicospec/cambiopsw.jsp">Cambia password</a><br>
        <br>
        <a href="cercapaziente.jsp">Eroga visita</a>
        <br>
        <label for="pazienti">Cerca tutti i pazienti: </label>
        <form action="${pageContext.request.contextPath}/medicospec/schedapaziente.handler">
        <input list="pazienti" name="cf">
        <datalist id="pazienti">
            <c:forEach items="${PazientiList}" var="paziente">
                    <option value=${paziente.codicefiscale}>${paziente.nome} ${paziente.cognome}</option>
            </c:forEach>
        </datalist>
        <input type="submit" value="Visualizza scheda paziente">
        </form>
        <br>
        <a href="<c:url context="${pageContext.request.contextPath}" value="/medico/logout.handler" />">Logout</a>
        
        <script>
        function activateButton() {
            document.getElementById("ModificaDati").disabled=false;
        }
        </script>
    </body>
</html>
