<%-- 
    Document   : schedapaziente
    Created on : 22-ago-2019, 15.15.13
    Author     : Sean
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        ${paziente.nome} ${paziente.cognome} <br>
        ${paziente.datanascita} <br>
        ${paziente.luogonascita} <br>
        
        
        <c:forEach items="${paziente.elencofoto}" var="item">
            <c:set var = "item" value="${fn:substringAfter(item, '.')}"/>
            <img src= "../paziente${item}"><br>
        </c:forEach>
        
    <h2>
            Ricette Farmacologiche
        </h2>
        <c:forEach items="${paziente.ricette}" var="item">
            ${item.id} Dr. ${item.nome_med} ${item.idfarmaco} ${item.nomefarmaco} ${item.data_prescr} ${item.data_erog} ${item.idfarmacia} ${item.nomefarmacia} <br>
        </c:forEach>
        <br><br>
        <h2>
            Visite specialistiche
        </h2>
        <c:forEach items="${paziente.visite}" var="item">
            ${item.id} Dr. ${item.nome_med} ${item.timestamp} ${item.tipovisita} ${item.erog_time} Dr. ${item.nome_medspec} ${item.risultati} <br>
        </c:forEach>
        <h2>
            Esami
        </h2>
        <c:forEach items="${paziente.esami}" var="item">
            ${item.id} Dr. ${item.nome_med} ${item.timestamp} ${item.esame.tipoesame} ${item.data_erog} ${item.risultati} <br>
        </c:forEach>
    </body>
</html>
