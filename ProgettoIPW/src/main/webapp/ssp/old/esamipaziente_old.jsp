<%-- 
    Document   : esamipaziente
    Created on : 25-ago-2019, 14.38.54
    Author     : Sean
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h2>
            Esami erogati per il paziente ***
        </h2>
        <c:forEach items="${esamierog}" var="esame">
          ${esame.id} ${esame.esame.tipoesame} ${esame.timestamp} ${esame.data_erog} ${esame.risultati}
          <br>
        </c:forEach>
          <h2>
              Esami non erogati per il paziente ***
          </h2>
         <c:forEach items="${esaminonerog}" var="esame">
             ${esame.id} ${esame.id_ssp} ${esame.cf_med}${esame.esame.tipoesame} ${esame.timestamp}
            <form action="${pageContext.request.contextPath}/ssp/erogaesame.handler">
                <input type="hidden"
                       name="esame_id"
                       value=${esame.id}>
                <input type="hidden"
                       name="idssp"
                       value=${esame.id_ssp}>
                <input type="submit" 
                       value="Eroga">
            </form>
          <br>
        </c:forEach>
    </body>
</html>
