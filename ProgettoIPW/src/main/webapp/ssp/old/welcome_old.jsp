<%-- 
    Document   : welcome
    Created on : 21-ago-2019, 16.09.16
    Author     : Sean
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Main page Servizio Sanitario Provinciale</title>
    </head>
    <body>
        <h1>
            Benvenuto
        </h1>
        <c:out value= "${ssp.userinfo}" ></c:out>
        <br>
        <br><a href="${pageContext.request.contextPath}/ssp/cambiopsw.jsp">Cambia password</a><br>
        <br>
        <a href="cercapaziente.jsp">Eroga esame</a><br>
        <a href="datirichiamo.jsp">Effettua un richiamo per età</a>
        
        <form action="${pageContext.request.contextPath}/ssp/esportareportxls.handler">
                <input type="submit" 
                       value="Scarica report completo dei servizi erogati">
        </form>
        
        
        
        <a href="<c:url context="${pageContext.request.contextPath}" value="/ssp/logout.handler" />">Logout</a>
    </body>
</html>
