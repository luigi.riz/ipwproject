<%-- 
    Document   : erogaesame
    Created on : 25-ago-2019, 14.30.30
    Author     : Sean
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Informazioni Richiamo per età</title>
    </head>
    <body>
        <script>
        function validateForm()
        {
            var datamin = document.forms["form_richiamo"]["min"];
            var datamax = document.forms["form_richiamo"]["max"];
            var esame = document.forms["form_richiamo"]["esame"];
           
        if (esame.value == "")                                  
                { 
                    window.alert("Inserisci un esame valido!"); 
                    esame.focus(); 
                    return false; 
                } 
                
        if (datamin.value >= datamax.value )                                  
                { 
                    window.alert("Inserisci delle date valide!");  
                    return false; 
                } 
           
           return true;
        }
        </script>
        
       <form action="${pageContext.request.contextPath}/ssp/prescrivirichiamo.handler" id = "richiamoinfo" onsubmit="return validateForm()" name="form_richiamo" >
        Età minima :
        <input type="date" name="min"><br>
        Età massima :
        <input type ="date" name ="max"><br>
        Esame/Visita specialistica :
        <input type ="text" name = "esame"> <br>
        Tipologia del richiamo: <br>
        <input type="radio" name="tipo" value="1" checked required/> Visita specialistica<br>
        <input type="radio" name="tipo" value="2"> Esame<br>
        <input type="submit" value="Effettua richiamo">
       </form>
    </body>
</html>