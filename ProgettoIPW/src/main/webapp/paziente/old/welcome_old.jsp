<%-- 
    Document   : welcome
    Created on : 23 lug 2019, 21:26:33
    Author     : Luigi
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Main page PAZIENTE</title>
    </head>
    <body>
        <h1>
            <c:choose>
                <c:when test="${paziente.sesso == 'M'}">
                    Benvenuto
                </c:when>
                <c:otherwise>
                    Benvenuta
                </c:otherwise>
            </c:choose>
        </h1>
        <!–– <c:out value= "${paziente.nome} ${paziente.cognome}" ></c:out>-->
        <form action="${pageContext.request.contextPath}/paziente/modificadati.handler" method="POST">
            <fieldset>
                <legend>Dati Personali:</legend>
                Codice Fiscale<br>
                <input type="text" name="cf" value="${paziente.codicefiscale}" disabled><br>
                Nome:<br>
                <input type="text" name="nome" value="${paziente.nome}" disabled><br>
                Cognome:<br>
                <input type="text" name="cognome" value="${paziente.cognome}" disabled><br>
                Luogo di nascita:<br>
                <input type="text" name="luogonascita" value="${paziente.luogonascita}" disabled><br>
                Data di nascita<br>
                <input type="text" name="datanascita" value="${paziente.datanascita}" disabled><br>
                Email:<br>
                <input type="email" name="email" value="${paziente.email}" oninput="activateButton()" ><br>
                Numero di telefono:<br>
                <input type="tel" name="numerotelefono" value="${paziente.numerotelefono}" oninput="activateButton()" required pattern="[0-9]{3}/[0-9]{7}">
                <small>Formato: 123/4567890</small><br>
            </fieldset>
            <fieldset>
                <legend> Dati di residenza:</legend>
                Provincia:<br>
                <input type="text" name="provincia" value="${paziente.provincia}" disabled><br>  
                Città:<br>
                <input type="text" name="citta" value="${paziente.citta}" disabled><br>
            </fieldset>
            <input type="submit" name="ModificaDati" value="Modifica" id="ModificaDati" disabled="true">
        </form>
        <br><a href="${pageContext.request.contextPath}/paziente/cambiopsw.jsp">Cambia password</a><br>
        <br>
        <c:out value = "dr. ${paziente.medicodibase.nome} ${paziente.medicodibase.cognome}" /> <a href="${pageContext.request.contextPath}/paziente/medici.handler">Cambia medico</a>
        <br>
        <c:out value = "${paziente.medicodibase.citta}" />
        <br>
        <c:out value = "${paziente.medicodibase.numerotelefono}" />
        <br><br>
        <h2>
            Data ultima visita
        </h2>
        <c:out value = "${paziente.dataultimavisita}" />
        <h2>
            Ricette Farmacologiche
        </h2>
        <c:forEach items="${paziente.ricette}" var="item">
            ${item.id} Dr. ${item.nome_med} ${item.idfarmaco} ${item.nomefarmaco} ${item.data_prescr} ${item.data_erog} ${item.idfarmacia} ${item.nomefarmacia} <br>
            <form action="${pageContext.request.contextPath}/paziente/scaricaricetta.handler">
                <input type="hidden"
                       name="idricetta"
                       value=${item.id}>
                <input type="submit" 
                       value="Download Ricetta">
            </form>
        </c:forEach>
        <br><br>
        <h2>
            Visite specialistiche
        </h2>
        <c:forEach items="${paziente.visite}" var="item">
            ${item.id} Dr. ${item.nome_med} ${item.timestamp} ${item.tipovisita} ${item.erog_time} Dr. ${item.nome_medspec} ${item.risultati} <br>
        </c:forEach>
        <h2>
            Esami
        </h2>
        <c:forEach items="${paziente.esami}" var="item">
            ${item.id} Dr. ${item.nome_med} ${item.timestamp} ${item.esame.tipoesame} ${item.data_erog} ${item.risultati} <br>
        </c:forEach>
        <form action="${pageContext.request.contextPath}/paziente/scaricaticketpdf.handler">
                <input type="submit" 
                       value="Scarica l'elenco dei tuoi ticket">
        </form>
        <%--<h2>
            Gli esami possibili
        </h2>
        <c:forEach items="${EsamiList}" var="item">
            ${item.idesame} ${item.tipoesame} ${item.descrizione} <br>
        </c:forEach>--%>
        <h2>
            Le tue Foto
        </h2>
        <c:forEach items="${paziente.elencofoto}" var="item">
           <img src= "${item}"><br>
        </c:forEach>
        <form method="POST" action="${pageContext.request.contextPath}/paziente/welcome.handler" enctype="multipart/form-data" >
            File:
            <input type="file" name="fileSrc"  > <br/>
            <input type="submit" value="Upload" name="uploadbutton" >
        </form>
        <a href="<c:url context="${pageContext.request.contextPath}" value="/paziente/logout.handler" />">Logout</a>
        
        
        
        <script>
        function activateButton() {
            document.getElementById("ModificaDati").disabled=false;
        }
        </script>
    </body>
</html>
