<%-- 
    Document   : medici
    Created on : 19 ago 2019, 22:02:53
    Author     : Luigi
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Cambia medico</title>
    </head>
    <body>
        <h1>Questi sono i medici disponibili</h1>
        <form action="${pageContext.request.contextPath}/paziente/medici.handler" method="post">
            <c:forEach items="${listaMedici}" var="item">
                <input type="radio" name="medico" value="${item.cf}">
                Dr. ${item.nome} ${item.cognome}<br>
                ${item.numerotelefono}<br>
                ${item.citta}<br><br>
            </c:forEach>
            <button type="submit" name="Back" >Back</button>
        </form>
    </body>
</html>
