<%-- 
    Document   : prescresame
    Created on : 29-ago-2019, 10.02.43
    Author     : Sean
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Prescrizione Esame</title>
    </head>
    <body>
        <script>
        function validateForm()
        {
            var cf = document.forms["form_richiamo"]["cf"];
            var tipoesame = document.forms["form_richiamo"]["tipoesame"];
           
        if (cf.value == "")                                  
                { 
                    window.alert("Inserisci un Codice fiscale!"); 
                    cf.focus(); 
                    return false; 
                } 
        
        if (idesame.value == "")                                  
                { 
                    window.alert("Inserisci un esame!"); 
                    tipoesame.focus(); 
                    return false; 
                } 
           return true;
        }
        </script>
        <form action="${pageContext.request.contextPath}/medico/prescresame.handler" onsubmit="return validateForm()" name="form_richiamo">
        Codice Fiscale del paziente:<br>
        <input list="pazienti" name="cf">
        <datalist id="pazienti">
            <c:forEach items="${medico.pazienti}" var="paziente">
                <option value=${paziente.codicefiscale}>${paziente.nome} ${paziente.cognome}</option>
            </c:forEach>
        </datalist>
         <br>
        Tipo di esame:<br>
        <input list="esami" name = "idesame">
        <datalist id="esami">
            <c:forEach items="${EsamiList}" var="esame">
                <option value=${esame.idesame}>${esame.tipoesame}</option>
            </c:forEach>
        </datalist><br>
        <input type="submit" value="Prescrivi">
        </form>
    </body>
</html>
