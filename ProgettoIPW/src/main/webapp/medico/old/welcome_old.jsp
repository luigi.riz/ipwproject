<%-- 
    Document   : welcome
    Created on : 21-ago-2019, 16.09.16
    Author     : Sean
--%>
<script src="//code.jquery.com/jquery-1.12.4.js"></script>
<script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Main page MEDICO</title>
    </head>
    <body>
        <h1>
            <c:choose>
                <c:when test="${medico.sesso == 'M'}">
                    Benvenuto
                </c:when>
                <c:otherwise>
                    Benvenuta
                </c:otherwise>
            </c:choose>
        </h1>
        <c:out value= "${medico.nome} ${medico.cognome}" ></c:out>
        <br>
        <form action="${pageContext.request.contextPath}/medico/modificadati.handler" method="POST">
            <fieldset>
                <legend>Dati Personali:</legend>
                    Codice Fiscale<br>
                    <input type="text" name="cf" value="${medico.codicefiscale}" disabled><br>
                    Nome:<br>
                    <input type="text" name="nome" value="${medico.nome}" disabled><br>
                    Cognome:<br>
                    <input type="text" name="cognome" value="${medico.cognome}" disabled><br>
                    Luogo di nascita:<br>
                    <input type="text" name="luogonascita" value="${medico.luogonascita}" disabled><br>
                    Data di nascita<br>
                    <input type="text" name="datanascita" value="${medico.datanascita}" disabled><br>
                    Email:<br>
                    <input type="email" name="email" value="${medico.email}" oninput="activateButton()" ><br>
                    Numero di telefono:<br>
                    <input type="tel" name="numerotelefono" value="${medico.numerotelefono}" oninput="activateButton()" required pattern="[0-9]{3}/[0-9]{7}">
                    <small>Formato: 123/4567890</small><br>
            </fieldset>
            <fieldset>
                    <legend> Dati di residenza:</legend>
                    Provincia:<br>
                    <input type="text" name="provincia" value="${medico.provincia}" disabled><br>  
                    Città:<br>
                    <input type="text" name="citta" value="${medico.citta}" disabled><br>
            </fieldset>
            <input type="submit" name="ModificaDati" value="Modifica" id="ModificaDati" disabled="true">
        </form>
        <br><a href="${pageContext.request.contextPath}/medico/cambiopsw.jsp">Cambia password</a><br>
        <br>
        <h2>
            Elenco dei tuoi pazienti
        </h2>
        <c:forEach items="${medico.pazienti}" var="paziente">
          ${paziente.nome} ${paziente.cognome} ${paziente.datanascita} ${paziente.luogonascita} 
            <form action="${pageContext.request.contextPath}/medico/schedapaziente.handler">
                <input type="hidden"
                       name="cf"
                       value=${paziente.codicefiscale}>
                <input type="submit" 
                       value="Visualizza scheda paziente">
            </form>
          <br>
        </c:forEach>
          <br>
         <a href="prescrfarmaco.jsp">Prescrivi farmaco</a>
          <br>
         <a href="prescrvisita.jsp">Prescrivi visita specialistica</a>
         <br>
         <a href="prescresame.jsp">Prescrivi esame</a>
         <br>
        <label for="pazienti">Cerca i tuoi pazienti: </label>
        <form action="${pageContext.request.contextPath}/medico/schedapaziente.handler">
        <input list="pazienti" name="cf">
        <datalist id="pazienti">
            <c:forEach items="${medico.pazienti}" var="paziente">
                    <option value=${paziente.codicefiscale}>${paziente.nome} ${paziente.cognome}</option>
            </c:forEach>
        </datalist>
         <input type="submit" value="Visualizza scheda paziente">
         </form>
        <label for="esami">Cerca esami: </label>
        <input list="esami">
        <datalist id="esami">
            <c:forEach items="${EsamiList}" var="esame">
                <option value=${esame.tipoesame}>${esame.tipoesame}</option>
            </c:forEach>
        </datalist>
        <br>
          
        <a href="<c:url context="${pageContext.request.contextPath}" value="/medico/logout.handler" />">Logout</a>
        
        <script>
        function activateButton() {
            document.getElementById("ModificaDati").disabled=false;
        }
        </script>
    </body>
</html>
