<%-- 
    Document   : prescrvisita
    Created on : 29-ago-2019, 10.02.53
    Author     : Sean
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Prescrizione Visita</title>
    </head>
    <body>
        <script>
        function validateForm()
        {
            var cf = document.forms["form_richiamo"]["cf"];
            var tipovisita = document.forms["form_richiamo"]["tipovisita"];
           
        if (cf.value == "")                                  
                { 
                    window.alert("Inserisci un Codice fiscale!"); 
                    cf.focus(); 
                    return false; 
                } 
        
        if (tipovisita.value == "")                                  
                { 
                    window.alert("Inserisci il tipo di visita!"); 
                    tipovisita.focus(); 
                    return false; 
                } 
           return true;
        }
        </script>
        <form action="${pageContext.request.contextPath}/medico/prescrvisita.handler" onsubmit="return validateForm()" name="form_richiamo">
        Codice Fiscale del paziente:<br>
        <input list="pazienti" name="cf">
        <datalist id="pazienti">
            <c:forEach items="${medico.pazienti}" var="paziente">
                    <option value=${paziente.codicefiscale}>${paziente.nome} ${paziente.cognome}</option>
            </c:forEach>
        </datalist><br>
        Tipo di visita:<br>
        <input type="text" name="tipovisita"><br><br>
        <input type="submit" value="Prescrivi">
        </form>
    </body>
</html>
