<%-- 
    Document   : prescrfarmaco
    Created on : 29-ago-2019, 10.02.30
    Author     : Sean
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Prescrizione Farmaco</title>
    </head>
    <body>
        <script>
        function validateForm()
        {
            var cf = document.forms["form_richiamo"]["cf"];
            var idfarm = document.forms["form_richiamo"]["idfarm"];
           
        if (cf.value == "")                                  
                { 
                    window.alert("Inserisci un Codice fiscale!"); 
                    cf.focus(); 
                    return false; 
                } 
        
        if (idfarm.value == "")                                  
                { 
                    window.alert("Inserisci l'id del farmaco!"); 
                    idfarm.focus(); 
                    return false; 
                } 
           return true;
        }
        </script>
        <form action="${pageContext.request.contextPath}/medico/prescrfarmaco.handler" onsubmit="return validateForm()" name="form_richiamo">
        Codice Fiscale del paziente:<br>
        <input list="pazienti" name="cf">
        <datalist id="pazienti">
            <c:forEach items="${medico.pazienti}" var="paziente">
                <option value=${paziente.codicefiscale}>${paziente.nome} ${paziente.cognome}</option>
            </c:forEach>
        </datalist><br>
        Id del farmaco:<br>
        <input list="farmaci" name="idfarm">
        <datalist id="farmaci">
            <c:forEach items="${FarmaciList}" var="farmaco">
                <option value=${farmaco.idfarmaco}>${farmaco.nomefarmaco}</option>
            </c:forEach>
        </datalist><br>
        <input type="submit" value="Prescrivi">
        </form>
    </body>
</html>
