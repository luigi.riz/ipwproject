<%-- 
    Document   : welcome
    Created on : 21-ago-2019, 16.09.16
    Author     : Sean
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Main page Farmacia</title>
    </head>
    <body>
        <h1>
            Benvenuto
        </h1>
        ${farm.nomefarmacia} ${farm.indirizzo} ${farm.citta} ${farm.cap}
        <br>
        <form action="${pageContext.request.contextPath}/farmacia/modificadati.handler" method="POST">
            <fieldset>
                <legend>Dati Farmacia:</legend>
                Codice Fiscale<br>
                <input type="text" name="id" value="${farm.idfarmacia}" disabled><br>
                Nome:<br>
                <input type="text" name="nome" value="${farm.nomefarmacia}" disabled><br>
                Provincia:<br>
                <input type="text" name="provincia" value="${farm.provincia}" disabled><br>
                Città:<br>
                <input type="text" name="citta" value="${farm.citta}" disabled><br>
                Indirizzo:<br>
                <input type="text" name="indirizzo" value="${farm.indirizzo}" disabled><br>
                Email:<br>
                <input type="email" name="email" value="${farm.email}" oninput="activateButton()" ><br>
            </fieldset>
            <input type="submit" name="ModificaDati" value="Modifica" id="ModificaDati" disabled="true">
        </form>
        <br><a href="${pageContext.request.contextPath}/farmacia/cambiopsw.jsp">Cambia password</a><br>
        <br>
        <a href="cercapaziente.jsp">Eroga ricetta</a>
        <br>
        <a href="<c:url context="${pageContext.request.contextPath}" value="/farmacia/logout.handler" />">Logout</a>
    
        <script>
        function activateButton() {
            document.getElementById("ModificaDati").disabled=false;
        }
        </script>
    </body>
</html>
