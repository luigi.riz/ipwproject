<%-- 
    Document   : ricettepaziente
    Created on : 25-ago-2019, 14.38.54
    Author     : Sean
--%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h2>
            Ricette erogate per il paziente ***
        </h2>
        <c:forEach items="${ricetteerog}" var="ric">
          ${ric.id} ${ric.idfarmaco} ${ric.data_prescr} ${ric.data_erog}
          <br>
        </c:forEach>
          <h2>
              Ricette non erogate per il paziente ***
          </h2>
         <c:forEach items="${ricettenonerog}" var="ric">
          ${ric.id} ${ric.idfarmaco} ${ric.data_prescr}
            <form action="${pageContext.request.contextPath}/farmacia/erogaricetta.handler">
                <input type="hidden"
                       name="ric_id"
                       value=${ric.id}>
                <input type="submit" 
                       value="Eroga">
            </form>
          <br>
        </c:forEach>
    </body>
</html>
