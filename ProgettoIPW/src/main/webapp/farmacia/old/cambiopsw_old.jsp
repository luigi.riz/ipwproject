<%-- 
    Document   : cambiopsw
    Created on : 27 dic 2019, 12:35:29
    Author     : Luigi
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Modifica password</title>
    </head>
    <body>
        <%--Sarebbe carino avere delle alert che controllano che tu non arrivi qui perchè hai sbagliato password al passaggio precedente e/o perchè le due password non coincidono --%> 
        <c:if test="${PSWError1 == true}">
            <c:out value="${'Le due password non corrispondono'}"/><br>  
        </c:if>
        <c:if test="${PSWError2 == true}">
            <c:out value="${'Inserisci correttamente la vecchia password'}"/><br>
        </c:if>
        <form action="change_psw.handler" method="POST" onsubmit="return verifyPsw()">
            <c:if test="${mustChangePsw == false}">
                Vecchia password:<br>
                <input type="password" name="old_psw"><br>
            </c:if>
            Nuova password:<br>
            <input type="password" name="new_psw_1" id="new_psw_1"><br>
            Conferma password:<br>
            <input type="password" name="new_psw_2" id="new_psw_2"><br><br>
            <input type="submit" name="ModificaPsw" value="Modifica password" id="ModificaPsw">
        </form>
            
        <script>
            function verifyPsw() {
                if(document.getElementById("new_psw_1").value === document.getElementById("new_psw_2").value && document.getElementById("new_psw_1").value !== '' && document.getElementById("new_psw_2").value !== '') {
                    return true;
                }else{
                    alert("Le due password non coincidono");
                    return false;
                }
            }
        </script>
    </body>
</html>
