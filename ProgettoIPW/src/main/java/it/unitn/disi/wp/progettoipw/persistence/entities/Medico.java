/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.persistence.entities;

import java.sql.Date;
import java.util.ArrayList;

/**
 * The entity that describe a {@code medico} entity.
 * 
 * @author Luigi Riz;
 * @since 2019.07.25
 */
public class Medico{
    private String nome;
    private String cognome;
    private String password;
    private Boolean isPswTemporary;
    private String sesso;
    private String luogonascita;
    private Date datanascita;
    private String numerotelefono;
    private String codicefiscale;
    private String citta;
    private String provincia;
    private String email;
    private Boolean isSpec;
    private ArrayList<Paziente> pazienti;
    private ArrayList<Paziente> tuttipazienti;

    public Medico() {
    }

    public Medico(String codicefiscale) {
        this.codicefiscale = codicefiscale;
    }

    public Medico(String nome, String cognome, String password, Boolean isPswTemporary, String sesso, String luogonascita, Date datanascita, String numerotelefono, String codicefiscale, String citta, String provincia, String email, Boolean isSpec, ArrayList<Paziente> pazienti, ArrayList<Paziente> tuttipazienti) {
        this.nome = nome;
        this.cognome = cognome;
        this.password = password;
        this.isPswTemporary = isPswTemporary;
        this.sesso = sesso;
        this.luogonascita = luogonascita;
        this.datanascita = datanascita;
        this.numerotelefono = numerotelefono;
        this.codicefiscale = codicefiscale;
        this.citta = citta;
        this.provincia = provincia;
        this.email = email;
        this.isSpec = isSpec;
        this.pazienti = pazienti;
        this.tuttipazienti = tuttipazienti;
    }

    
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSesso() {
        return sesso;
    }

    public void setSesso(String sesso) {
        this.sesso = sesso;
    }

    public String getLuogonascita() {
        return luogonascita;
    }

    public void setLuogonascita(String luogonascita) {
        this.luogonascita = luogonascita;
    }

    public Date getDatanascita() {
        return datanascita;
    }

    public void setDatanascita(Date datanascita) {
        this.datanascita = datanascita;
    }

    public String getNumerotelefono() {
        return numerotelefono;
    }

    public void setNumerotelefono(String numerotelefono) {
        this.numerotelefono = numerotelefono;
    }

    public String getCodicefiscale() {
        return codicefiscale;
    }

    public void setCodicefiscale(String codicefiscale) {
        this.codicefiscale = codicefiscale;
    }

    public String getCitta() {
        return citta;
    }

    public void setCitta(String citta) {
        this.citta = citta;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getIsSpec() {
        return isSpec;
    }

    public void setIsSpec(Boolean isSpec) {
        this.isSpec = isSpec;
    }

    public ArrayList<Paziente> getPazienti() {
        return pazienti;
    }

    public void setPazienti(ArrayList<Paziente> pazienti) {
        this.pazienti = pazienti;
    }
    
    public ArrayList<Paziente> getTuttipazienti() {
        return tuttipazienti;
    }

    public void setTuttipazienti(ArrayList<Paziente> tuttipazienti) {
        this.tuttipazienti = tuttipazienti;
    }

    public Boolean getIsPswTemporary() {
        return isPswTemporary;
    }

    public void setIsPswTemporary(Boolean isPswTemporary) {
        this.isPswTemporary = isPswTemporary;
    }
}
