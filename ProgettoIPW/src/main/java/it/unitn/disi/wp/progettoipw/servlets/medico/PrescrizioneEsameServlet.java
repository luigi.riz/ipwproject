/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.servlets.medico;

import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.progettoipw.mail.Mail;
import it.unitn.disi.wp.progettoipw.persistence.dao.MedicoDAO;
import it.unitn.disi.wp.progettoipw.persistence.dao.PazienteDAO;
import it.unitn.disi.wp.progettoipw.persistence.entities.Esame;
import it.unitn.disi.wp.progettoipw.persistence.entities.Medico;
import it.unitn.disi.wp.progettoipw.persistence.entities.Paziente;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;

/**
 *
 * @author Sean
 */
public class PrescrizioneEsameServlet extends HttpServlet {
    
    private MedicoDAO medDao;
    private PazienteDAO pazDao;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            medDao = daoFactory.getDAO(MedicoDAO.class);
            pazDao = daoFactory.getDAO(PazienteDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        int idesame = Integer.parseInt(request.getParameter("idesame"));
        Boolean idValid = false;
        for (Esame farm: (ArrayList<Esame>)request.getServletContext().getAttribute("EsamiList")) {
            if (farm.getIdesame().equals(idesame)) {
                idValid =true;
                break;
            }
        }
        
        if (idValid) {
            Paziente paz= (Paziente)request.getSession().getAttribute("paziente");
            String cf_paz =paz.getCodicefiscale();
            Medico actual_med =(Medico)request.getSession().getAttribute("medico");

            try{
                medDao.prescriviEsame(idesame,actual_med.getCodicefiscale(), cf_paz);
                //Aggiorno la sessione
                request.getSession().setAttribute("medico", medDao.getByPrimaryKey(actual_med.getCodicefiscale()));
            
                Paziente paz_tmp = pazDao.getByPrimaryKey(paz.getCodicefiscale());
                ArrayList<String> elencofoto_tmp = new ArrayList<>();
                for(String s: paz_tmp.getElencofoto()) {
                    String tmp = "../paziente"+s.substring(1);
                    elencofoto_tmp.add(tmp);
                }
                paz_tmp.setElencofoto(elencofoto_tmp);
                request.getSession().setAttribute("paziente", paz_tmp);
                //Aggiorno la lista dei pazienti
                getServletContext().setAttribute("PazientiList", pazDao.getAll());
                //Invio la mail
                String email = paz.getEmail();
                String subject = "Nuova ricetta";
                String text = "Hai una nuova ricetta da visualizzare: \nEsame\nEffettuata da: Dr. "+actual_med.getNome()+" "+actual_med.getCognome();
                Mail.sendMail(getServletContext(), email, subject, text);
                //Redirigo
                request.getSession().setAttribute("EsameError", false);
                response.sendRedirect(response.encodeRedirectURL(contextPath + "medico/schedapaziente.jsp"));
            } catch (DAOException ex){
                throw new ServletException("Impossible to perform action");
            } catch (ParseException e){
                
            }
        } else {
            request.getSession().setAttribute("EsameError", true);
            response.sendRedirect(response.encodeRedirectURL(contextPath + "medico/schedapaziente.jsp"));
        }
        
            
    }
    
    
} 

