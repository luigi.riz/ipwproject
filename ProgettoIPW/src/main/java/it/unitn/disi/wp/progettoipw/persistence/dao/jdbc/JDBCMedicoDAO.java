/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.persistence.dao.jdbc;

import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.jdbc.JDBCDAO;
import it.unitn.disi.wp.progettoipw.other.InfoMedico;
import it.unitn.disi.wp.progettoipw.persistence.dao.MedicoDAO;
import it.unitn.disi.wp.progettoipw.persistence.entities.Esame;
import it.unitn.disi.wp.progettoipw.persistence.entities.Medico;
import it.unitn.disi.wp.progettoipw.persistence.entities.Paziente;
import it.unitn.disi.wp.progettoipw.persistence.entities.PrescrizioneEsame;
import it.unitn.disi.wp.progettoipw.persistence.entities.PrescrizioneFarmaco;
import it.unitn.disi.wp.progettoipw.persistence.entities.PrescrizioneVisitaSpec;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;
import java.text.ParseException;

/**
 *
 * @author Luigi
 */
public class JDBCMedicoDAO extends JDBCDAO<Medico, String> implements MedicoDAO   {
    /**
     * The default constructor of the class.
     *
     * @param con the connection to the persistence system.
     */
    public JDBCMedicoDAO(Connection con) {
        super(con);
    }

    /**
     * Returns the number of {@link Medico medici} stored on the persistence system
     * of the application.
     *
     * @return the number of records present into the storage system.
     * @throws DAOException if an error occurred during the information
     * retrieving.
     */
    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM medici");
            if (counter.next()) {
                return counter.getLong(1);
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to count medici", ex);
        }

        return 0L;
    }

    /**
     * Returns the {@link Medico medico} with the primary key equals to the one
     * passed as parameter.
     *
     * @param primaryKey the {@code cf} of the {@code medico} to get.
     * @return the {@code medico} with the cf equals to the one passed as
     * parameter or {@code null} if no entities with that cf are present into
     * the storage system.
     * @throws DAOException if an error occurred during the information
     * retrieving.
     */
    @Override
    public Medico getByPrimaryKey(String primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException("primaryKey is null");
        }
        try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM medici WHERE codicefiscale = ?")) {
            stm.setString(1, primaryKey);
            try (ResultSet rs = stm.executeQuery()) {
                int count = 0;
                while(rs.next()) {
                    count++;
                    if (count > 1) {
                        throw new DAOException("Unique constraint violated! There are more than one user with the same email! WHY???");
                    }
                    Medico med = new Medico();
                    med.setCodicefiscale(rs.getString("codicefiscale"));
                    med.setNome(rs.getString("nome"));
                    med.setCognome(rs.getString("cognome"));
                    med.setPassword(rs.getString("password"));
                    med.setIsPswTemporary(rs.getBoolean("flag_recuperopassword"));
                    med.setSesso(rs.getString("sesso"));
                    med.setLuogonascita(rs.getString("luogonascita"));
                    med.setDatanascita(rs.getDate("datanascita"));
                    med.setNumerotelefono(rs.getString("numerotelefono"));
                    med.setEmail(rs.getString("email"));
                    med.setCitta(rs.getString("citta"));
                    med.setProvincia(rs.getString("provincia"));
                    med.setIsSpec(rs.getBoolean("is_spec"));


                    try (PreparedStatement stm_in = CON.prepareStatement("SELECT * \n" +
                                                                            "FROM med_has_paz \n" +
                                                                            "JOIN pazienti  ON med_has_paz.cf_paz=pazienti.codicefiscale\n" +
                                                                            "WHERE cf_med = ?\n")) {
                        stm_in.setString(1, med.getCodicefiscale());
                        try (ResultSet rs_in = stm_in.executeQuery()) {
                            ArrayList<Paziente> pazlist = new ArrayList<>();
                            while(rs_in.next()) {
                                //PENSO CI SIA UN MODO PIU' INTELLIGENTE PER FARLA STA COSA...
                                Paziente paz = new Paziente();
                                paz.setCodicefiscale(rs_in.getString("codicefiscale"));
                                paz.setNome(rs_in.getString("nome"));
                                paz.setCognome(rs_in.getString("cognome"));
                                paz.setPassword(rs_in.getString("password"));
                                paz.setSesso(rs_in.getString("sesso"));
                                paz.setLuogonascita(rs_in.getString("luogonascita"));
                                paz.setDatanascita(rs_in.getDate("datanascita"));
                                paz.setDataultimavisita(rs_in.getDate("dataultimavisita"));
                                paz.setNumerotelefono(rs_in.getString("numerotelefono"));
                                paz.setEmail(rs_in.getString("email"));
                                paz.setCitta(rs_in.getString("citta"));
                                paz.setProvincia(rs_in.getString("provincia"));
                                InfoMedico im= new InfoMedico();
                                im.setNome(med.getNome());
                                im.setCognome(med.getCognome());
                                im.setCf(med.getCodicefiscale());
                                im.setCitta(med.getCitta());
                                im.setNumerotelefono(med.getNumerotelefono());
                                paz.setMedicodibase(im);

                                //PRESCRIZIONE FARMACI DEL PAZIENTE
                                   try (PreparedStatement stm_in2 = CON.prepareStatement("SELECT PF.id, PF.cf_med, M.nome, M.cognome, PF.cf_paz, PF.idfarmaco, F.nomefarmaco, PF.data_prescr, PF.data_erog, T.idfarmacia, F1.nomefarmacia\n" +
                                                                                        "FROM prescrizione_farmaci PF\n" +
                                                                                        "LEFT JOIN farmaci F on PF.idfarmaco=F.idfarmaco\n" +
                                                                                        "LEFT JOIN medici M on PF.cf_med=M.codicefiscale\n" +
                                                                                        "LEFT JOIN ticket T on PF.id=T.id\n" +
                                                                                        "LEFT JOIN farmacie F1 on T.idfarmacia=F1.idfarmacia\n" +
                                                                                        "WHERE cf_paz= ? " +
                                                                                        "ORDER BY PF.data_prescr DESC")) {
                                    stm_in2.setString(1, paz.getCodicefiscale());
                                    try (ResultSet rs_in2 = stm_in2.executeQuery()) {
                                        ArrayList<PrescrizioneFarmaco> pflist = new ArrayList<>();
                                        while(rs_in2.next()) {
                                            PrescrizioneFarmaco pf= new PrescrizioneFarmaco();
                                            pf.setId(rs_in2.getInt("id"));
                                            pf.setCf_med(rs_in2.getString("cf_med"));
                                            pf.setNome_med(rs_in2.getString("nome")+" "+rs_in2.getString("cognome"));
                                            pf.setCf_paz(rs_in2.getString("cf_paz"));
                                            pf.setIdfarmaco(rs_in2.getInt("idfarmaco"));
                                            pf.setNomefarmaco(rs_in2.getString("nomefarmaco"));
                                            pf.setData_prescr(rs_in2.getTimestamp("data_prescr"));
                                            pf.setData_erog(rs_in2.getTimestamp("data_erog"));
                                            pf.setIdfarmacia(rs_in2.getInt("idfarmacia"));
                                            pf.setNomefarmacia(rs_in2.getString("nomefarmacia"));
                                            pflist.add(pf);
                                        }
                                        paz.setRicette(pflist);
                                    }
                                   }

                                   //VISITE SPEC
                                    try (PreparedStatement stm_in4 = CON.prepareStatement("SELECT PVS.id, PVS.cf_med, PVS.id_ssp, M1.nome AS nome_med, M1.cognome AS cognome_med, PVS.cf_paz, PVS.timestamp, PVS.tipovisita, PVS.erog_time, PVS.cf_medspec, M2.nome AS nome_medspec, M2.cognome AS cognome_medspec, PVS.risultati\n" +
                                                                                        "FROM prescrizione_visitespec PVS\n" +
                                                                                        "LEFT JOIN medici M1 on PVS.cf_med=M1.codicefiscale\n" +
                                                                                        "LEFT JOIN medici M2 on PVS.cf_medspec=M2.codicefiscale\n" +
                                                                                        "WHERE cf_paz =  ? " +
                                                                                        "ORDER BY PVS.timestamp DESC")) {
                                        stm_in4.setString(1, paz.getCodicefiscale());
                                        try (ResultSet rs_in4 = stm_in4.executeQuery()) {
                                            ArrayList<PrescrizioneVisitaSpec> pvslist = new ArrayList<>();
                                            while(rs_in4.next()) {
                                                PrescrizioneVisitaSpec pvs= new PrescrizioneVisitaSpec();
                                                pvs.setId(rs_in4.getInt("id"));
                                                pvs.setCf_med(rs_in4.getString("cf_med"));
                                                pvs.setNome_med(rs_in4.getString("nome_med")+" "+rs_in4.getString("cognome_med"));
                                                pvs.setId_ssp(rs_in4.getInt("id_ssp"));
                                                pvs.setCf_paz(rs_in4.getString("cf_paz"));
                                                pvs.setTimestamp(rs_in4.getTimestamp("timestamp"));
                                                pvs.setTipovisita(rs_in4.getString("tipovisita"));
                                                pvs.setErog_time(rs_in4.getTimestamp("erog_time"));
                                                pvs.setCf_medspec(rs_in4.getString("cf_medspec"));
                                                String tmp = rs_in4.getString("nome_medspec")== null ? "" : rs_in4.getString("nome_medspec")+" "+rs_in4.getString("cognome_medspec");
                                                pvs.setNome_medspec(tmp);
                                                pvs.setRisultati(rs_in4.getString("risultati"));
                                                pvslist.add(pvs);
                                            }
                                            paz.setVisite(pvslist);
                                        }
                                    }

                                    //ESAMI
                                    try (PreparedStatement stm_in5 = CON.prepareStatement("SELECT PE.id, PE.cf_med, M.nome, M.cognome, PE.id_ssp, PE.cf_paz, PE.id_esame, E.tipoesame, E.descrizione, PE.timestamp,  PE.data_erog, PE.risultati, idssp AS id_ssp_erog\n" +
                                                                                        "FROM prescrizione_esami PE\n" +
                                                                                        "JOIN esami E on PE.id_esame=E.id\n" +
                                                                                        "LEFT JOIN medici M on PE.cf_med=M.codicefiscale\n" +
                                                                                        "LEFT JOIN ticket T on PE.id=T.id\n" +
                                                                                        "WHERE cf_paz= ?\n" +
                                                                                        "ORDER BY PE.timestamp DESC")) {
                                        stm_in5.setString(1, paz.getCodicefiscale());
                                        try (ResultSet rs_in5 = stm_in5.executeQuery()) {
                                            ArrayList<PrescrizioneEsame> pelist = new ArrayList<>();
                                            while(rs_in5.next()) {
                                                PrescrizioneEsame pe= new PrescrizioneEsame();
                                                pe.setId(rs_in5.getInt("id"));
                                                pe.setCf_med(rs_in5.getString("cf_med"));
                                                pe.setNome_med(rs_in5.getString("nome")+" "+rs_in.getString("cognome"));
                                                pe.setId_ssp(rs_in5.getInt("id_ssp"));
                                                pe.setCf_paz(rs_in5.getString("cf_paz"));
                                                Esame e = new Esame();
                                                e.setIdesame(rs_in5.getInt("id_esame"));
                                                e.setTipoesame(rs_in5.getString("tipoesame"));
                                                e.setDescrizione(rs_in5.getString("descrizione"));
                                                pe.setEsame(e);
                                                pe.setTimestamp(rs_in5.getTimestamp("timestamp"));
                                                pe.setData_erog(rs_in5.getTimestamp("data_erog"));
                                                pe.setRisultati(rs_in5.getString("risultati"));
                                                pe.setId_ssp_erog(rs_in5.getInt("id_ssp_erog"));
                                                pelist.add(pe);
                                            }
                                            paz.setEsami(pelist);
                                        }
                                    }

                                     //creo l'array con le foto pazienti
                                    try (PreparedStatement stm_in3 = CON.prepareStatement("select url\n" +
                                                                                        "from pazienti JOIN fotopazienti ON fotopazienti.cf_paz = pazienti.codicefiscale\n" +
                                                                                        "where pazienti.codicefiscale = ? \n" +
                                                                                        "order by url")) {
                                        stm_in3.setString(1, paz.getCodicefiscale());
                                        try (ResultSet rs_in3 = stm_in3.executeQuery()) {
                                            String path = System.getProperty("user.dir");
                                            ArrayList<String> piclist = new ArrayList<>();
                                            while(rs_in3.next()){
                                                String url = rs_in3.getString("url");
                                                String urlcompleto = ".."+"\\paziente\\"+url;
                                                piclist.add( urlcompleto.replace('\\','/') );
                                            }
                                            paz.setElencofoto(piclist);
                                            paz.setPercorsofoto("/fotopazienti"+paz.getCodicefiscale()+"/");
                                        }
                                    }

                                pazlist.add(paz);
                            }
                            med.setPazienti(pazlist);
                        }
                    }
                    /*
                    //popolo tuttipazienti
                    try (PreparedStatement stm_in7 = CON.prepareStatement("select * from pazienti")) {
                                        try (ResultSet rs_in7 = stm_in7.executeQuery()) {
                                            ArrayList<Paziente> allpaz = new ArrayList<>();
                                            while(rs_in7.next()){
                                                Paziente paz = new Paziente();
                                                paz.setCodicefiscale(rs_in7.getString("codicefiscale"));
                                                paz.setNome(rs_in7.getString("nome"));
                                                paz.setCognome(rs_in7.getString("cognome"));
                                                paz.setPassword(rs_in7.getString("password"));
                                                paz.setSesso(rs_in7.getString("sesso"));
                                                paz.setLuogonascita(rs_in7.getString("luogonascita"));
                                                paz.setDatanascita(rs_in7.getDate("datanascita"));
                                                paz.setNumerotelefono(rs_in7.getString("numerotelefono"));
                                                paz.setEmail(rs_in7.getString("email"));
                                                paz.setCitta(rs_in7.getString("citta"));
                                                paz.setProvincia(rs_in7.getString("provincia"));
                                               allpaz.add(paz);
                                            }
                                            med.setTuttipazienti(allpaz);
                                        }
                                    }
                    
                    //popolo tuttiesami

                    try (PreparedStatement stm_in8 = CON.prepareStatement("select * from esami")) {
                                        try (ResultSet rs_in8 = stm_in8.executeQuery()) {
                                            ArrayList<Esame> allexams = new ArrayList<>();
                                            while(rs_in8.next()){
                                               Esame esame = new Esame();
                                               esame.setIdesame(rs_in8.getInt("id"));
                                               esame.setTipoesame(rs_in8.getString("tipoesame"));
                                               esame.setDescrizione(rs_in8.getString("descrizione"));
                                               allexams.add(esame);
                                            }
                                            med.setTuttiesami(allexams);
                                        }
                                    }*/

                    return med;
                }
                return null;    
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the user for the passed primary key", ex);
        }
    }

    /**
     * Returns the {@link Medico med} with the given {@code cf} and
     * {@code password}.
     *
     * @param cf the codice fiscale of the medico to get.
     * @param password the password of the medico to get.
     * @return the {@link Medico med} with the given {@code cf} and
     * {@code password}..
     * @throws DAOException if an error occurred during the information
     * retrieving.
     */
    @Override
    public Medico getByCfAndPassword(String cf, String password) throws DAOException {
        if ((cf == null) || (password == null)) {
            throw new DAOException("cf and password are mandatory fields", new NullPointerException("cf or password are null"));
        }
        try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM medici WHERE codicefiscale = ? AND password = ?")) {
            stm.setString(1, cf);
            stm.setString(2, password);
            try (ResultSet rs = stm.executeQuery()) {
                int count = 0;
                while(rs.next()) {
                    count++;
                    if (count > 1) {
                        throw new DAOException("Unique constraint violated! There are more than one user with the same email! WHY???");
                    }
                    Medico med = new Medico();
                    med.setCodicefiscale(rs.getString("codicefiscale"));
                    med.setNome(rs.getString("nome"));
                    med.setCognome(rs.getString("cognome"));
                    med.setPassword(rs.getString("password"));
                    med.setIsPswTemporary(rs.getBoolean("flag_recuperopassword"));
                    med.setSesso(rs.getString("sesso"));
                    med.setLuogonascita(rs.getString("luogonascita"));
                    med.setDatanascita(rs.getDate("datanascita"));
                    med.setNumerotelefono(rs.getString("numerotelefono"));
                    med.setEmail(rs.getString("email"));
                    med.setCitta(rs.getString("citta"));
                    med.setProvincia(rs.getString("provincia"));
                    med.setIsSpec(rs.getBoolean("is_spec"));


                    try (PreparedStatement stm_in = CON.prepareStatement("SELECT * \n" +
                                                                            "FROM med_has_paz \n" +
                                                                            "JOIN pazienti  ON med_has_paz.cf_paz=pazienti.codicefiscale\n" +
                                                                            "WHERE cf_med = ?\n")) {
                        stm_in.setString(1, med.getCodicefiscale());
                        try (ResultSet rs_in = stm_in.executeQuery()) {
                            ArrayList<Paziente> pazlist = new ArrayList<>();
                            while(rs_in.next()) {
                                //PENSO CI SIA UN MODO PIU' INTELLIGENTE PER FARLA STA COSA...
                                Paziente paz = new Paziente();
                                paz.setCodicefiscale(rs_in.getString("codicefiscale"));
                                paz.setNome(rs_in.getString("nome"));
                                paz.setCognome(rs_in.getString("cognome"));
                                paz.setPassword(rs_in.getString("password"));
                                paz.setSesso(rs_in.getString("sesso"));
                                paz.setLuogonascita(rs_in.getString("luogonascita"));
                                paz.setDatanascita(rs_in.getDate("datanascita"));
                                paz.setDataultimavisita(rs_in.getDate("dataultimavisita"));
                                paz.setNumerotelefono(rs_in.getString("numerotelefono"));
                                paz.setEmail(rs_in.getString("email"));
                                paz.setCitta(rs_in.getString("citta"));
                                paz.setProvincia(rs_in.getString("provincia"));
                                InfoMedico im= new InfoMedico();
                                im.setNome(med.getNome());
                                im.setCognome(med.getCognome());
                                im.setCf(med.getCodicefiscale());
                                im.setCitta(med.getCitta());
                                im.setNumerotelefono(med.getNumerotelefono());
                                paz.setMedicodibase(im);

                                //PRESCRIZIONE FARMACI DEL PAZIENTE
                                   try (PreparedStatement stm_in2 = CON.prepareStatement("SELECT PF.id, PF.cf_med, M.nome, M.cognome, PF.cf_paz, PF.idfarmaco, F.nomefarmaco, PF.data_prescr, PF.data_erog, T.idfarmacia, F1.nomefarmacia\n" +
                                                                                        "FROM prescrizione_farmaci PF\n" +
                                                                                        "LEFT JOIN farmaci F on PF.idfarmaco=F.idfarmaco\n" +
                                                                                        "LEFT JOIN medici M on PF.cf_med=M.codicefiscale\n" +
                                                                                        "LEFT JOIN ticket T on PF.id=T.id\n" +
                                                                                        "LEFT JOIN farmacie F1 on T.idfarmacia=F1.idfarmacia\n" +
                                                                                        "WHERE cf_paz= ? " +
                                                                                        "ORDER BY PF.data_prescr DESC")) {
                                    stm_in2.setString(1, paz.getCodicefiscale());
                                    try (ResultSet rs_in2 = stm_in2.executeQuery()) {
                                        ArrayList<PrescrizioneFarmaco> pflist = new ArrayList<>();
                                        while(rs_in2.next()) {
                                            PrescrizioneFarmaco pf= new PrescrizioneFarmaco();
                                            pf.setId(rs_in2.getInt("id"));
                                            pf.setCf_med(rs_in2.getString("cf_med"));
                                            pf.setNome_med(rs_in2.getString("nome")+" "+rs_in2.getString("cognome"));
                                            pf.setCf_paz(rs_in2.getString("cf_paz"));
                                            pf.setIdfarmaco(rs_in2.getInt("idfarmaco"));
                                            pf.setNomefarmaco(rs_in2.getString("nomefarmaco"));
                                            pf.setData_prescr(rs_in2.getTimestamp("data_prescr"));
                                            pf.setData_erog(rs_in2.getTimestamp("data_erog"));
                                            pf.setIdfarmacia(rs_in2.getInt("idfarmacia"));
                                            pf.setNomefarmacia(rs_in2.getString("nomefarmacia"));
                                            pflist.add(pf);
                                        }
                                        paz.setRicette(pflist);
                                    }
                                   }

                                   //VISITE SPEC
                                    try (PreparedStatement stm_in4 = CON.prepareStatement("SELECT PVS.id, PVS.cf_med, PVS.id_ssp, M1.nome AS nome_med, M1.cognome AS cognome_med, PVS.cf_paz, PVS.timestamp, PVS.tipovisita, PVS.erog_time, PVS.cf_medspec, M2.nome AS nome_medspec, M2.cognome AS cognome_medspec, PVS.risultati\n" +
                                                                                        "FROM prescrizione_visitespec PVS\n" +
                                                                                        "LEFT JOIN medici M1 on PVS.cf_med=M1.codicefiscale\n" +
                                                                                        "LEFT JOIN medici M2 on PVS.cf_medspec=M2.codicefiscale\n" +
                                                                                        "WHERE cf_paz =  ? " +
                                                                                        "ORDER BY PVS.timestamp DESC")) {
                                        stm_in4.setString(1, paz.getCodicefiscale());
                                        try (ResultSet rs_in4 = stm_in4.executeQuery()) {
                                            ArrayList<PrescrizioneVisitaSpec> pvslist = new ArrayList<>();
                                            while(rs_in4.next()) {
                                                PrescrizioneVisitaSpec pvs= new PrescrizioneVisitaSpec();
                                                pvs.setId(rs_in4.getInt("id"));
                                                pvs.setCf_med(rs_in4.getString("cf_med"));
                                                pvs.setNome_med(rs_in4.getString("nome_med")+" "+rs_in4.getString("cognome_med"));
                                                pvs.setId_ssp(rs_in4.getInt("id_ssp"));
                                                pvs.setCf_paz(rs_in4.getString("cf_paz"));
                                                pvs.setTimestamp(rs_in4.getTimestamp("timestamp"));
                                                pvs.setTipovisita(rs_in4.getString("tipovisita"));
                                                pvs.setErog_time(rs_in4.getTimestamp("erog_time"));
                                                pvs.setCf_medspec(rs_in4.getString("cf_medspec"));
                                                String tmp = rs_in4.getString("nome_medspec")== null ? "" : rs_in4.getString("nome_medspec")+" "+rs_in4.getString("cognome_medspec");
                                                pvs.setNome_medspec(tmp);
                                                pvs.setRisultati(rs_in4.getString("risultati"));
                                                pvslist.add(pvs);
                                            }
                                            paz.setVisite(pvslist);
                                        }
                                    }

                                    //ESAMI
                                    try (PreparedStatement stm_in5 = CON.prepareStatement("SELECT PE.id, PE.cf_med, M.nome, M.cognome, PE.id_ssp, PE.cf_paz, PE.id_esame, E.tipoesame, E.descrizione, PE.timestamp,  PE.data_erog, PE.risultati, idssp AS id_ssp_erog\n" +
                                                                                        "FROM prescrizione_esami PE\n" +
                                                                                        "JOIN esami E on PE.id_esame=E.id\n" +
                                                                                        "LEFT JOIN medici M on PE.cf_med=M.codicefiscale\n" +
                                                                                        "LEFT JOIN ticket T on PE.id=T.id\n" +
                                                                                        "WHERE cf_paz= ?\n" +
                                                                                        "ORDER BY PE.timestamp DESC")) {
                                        stm_in5.setString(1, paz.getCodicefiscale());
                                        try (ResultSet rs_in5 = stm_in5.executeQuery()) {
                                            ArrayList<PrescrizioneEsame> pelist = new ArrayList<>();
                                            while(rs_in5.next()) {
                                                PrescrizioneEsame pe= new PrescrizioneEsame();
                                                pe.setId(rs_in5.getInt("id"));
                                                pe.setCf_med(rs_in5.getString("cf_med"));
                                                pe.setNome_med(rs_in5.getString("nome")+" "+rs_in.getString("cognome"));
                                                pe.setId_ssp(rs_in5.getInt("id_ssp"));
                                                pe.setCf_paz(rs_in5.getString("cf_paz"));
                                                Esame e = new Esame();
                                                e.setIdesame(rs_in5.getInt("id_esame"));
                                                e.setTipoesame(rs_in5.getString("tipoesame"));
                                                e.setDescrizione(rs_in5.getString("descrizione"));
                                                pe.setEsame(e);
                                                pe.setTimestamp(rs_in5.getTimestamp("timestamp"));
                                                pe.setData_erog(rs_in5.getTimestamp("data_erog"));
                                                pe.setRisultati(rs_in5.getString("risultati"));
                                                pe.setId_ssp_erog(rs_in5.getInt("id_ssp_erog"));
                                                pelist.add(pe);
                                            }
                                            paz.setEsami(pelist);
                                        }
                                    }

                                     //creo l'array con le foto pazienti
                                    try (PreparedStatement stm_in3 = CON.prepareStatement("select url\n" +
                                                                                        "from pazienti JOIN fotopazienti ON fotopazienti.cf_paz = pazienti.codicefiscale\n" +
                                                                                        "where pazienti.codicefiscale = ? \n" +
                                                                                        "order by url")) {
                                        stm_in3.setString(1, paz.getCodicefiscale());
                                        try (ResultSet rs_in3 = stm_in3.executeQuery()) {
                                            String path = System.getProperty("user.dir");
                                            ArrayList<String> piclist = new ArrayList<>();
                                            while(rs_in3.next()){
                                                String url = rs_in3.getString("url");
                                                String urlcompleto = ".."+"\\paziente\\"+url;
                                                piclist.add( urlcompleto.replace('\\','/') );
                                            }
                                            paz.setElencofoto(piclist);
                                            paz.setPercorsofoto("/fotopazienti"+paz.getCodicefiscale()+"/");
                                        }
                                    }

                                pazlist.add(paz);
                            }
                            med.setPazienti(pazlist);
                        }
                    }
                    /*
                    //popolo tuttipazienti
                    try (PreparedStatement stm_in7 = CON.prepareStatement("select * from pazienti")) {
                                        try (ResultSet rs_in7 = stm_in7.executeQuery()) {
                                            ArrayList<Paziente> allpaz = new ArrayList<>();
                                            while(rs_in7.next()){
                                                Paziente paz = new Paziente();
                                                paz.setCodicefiscale(rs_in7.getString("codicefiscale"));
                                                paz.setNome(rs_in7.getString("nome"));
                                                paz.setCognome(rs_in7.getString("cognome"));
                                                paz.setPassword(rs_in7.getString("password"));
                                                paz.setSesso(rs_in7.getString("sesso"));
                                                paz.setLuogonascita(rs_in7.getString("luogonascita"));
                                                paz.setDatanascita(rs_in7.getDate("datanascita"));
                                                paz.setNumerotelefono(rs_in7.getString("numerotelefono"));
                                                paz.setEmail(rs_in7.getString("email"));
                                                paz.setCitta(rs_in7.getString("citta"));
                                                paz.setProvincia(rs_in7.getString("provincia"));
                                               allpaz.add(paz);
                                            }
                                            med.setTuttipazienti(allpaz);
                                        }
                                    }
                    
                    //popolo tuttiesami

                    try (PreparedStatement stm_in8 = CON.prepareStatement("select * from esami")) {
                                        try (ResultSet rs_in8 = stm_in8.executeQuery()) {
                                            ArrayList<Esame> allexams = new ArrayList<>();
                                            while(rs_in8.next()){
                                               Esame esame = new Esame();
                                               esame.setIdesame(rs_in8.getInt("id"));
                                               esame.setTipoesame(rs_in8.getString("tipoesame"));
                                               esame.setDescrizione(rs_in8.getString("descrizione"));
                                               allexams.add(esame);
                                            }
                                            med.setTuttiesami(allexams);
                                        }
                                    }*/

                    return med;
                }
                return null; 
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the user for the passed primary key", ex);
        }
    }

    /**
     * Returns the list of all the valid {@link User users} stored by the
     * storage system.
     *
     * @return the list of all the valid {@code users}.
     * @throws DAOException if an error occurred during the information
     * retrieving.
     */
    @Override
    public List<Medico> getAll() throws DAOException {
        List<Medico> medici = new ArrayList<>();

        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery("SELECT * FROM medico ORDER BY cognome")) {

                //PreparedStatement shoppingListsStatement = CON.prepareStatement("SELECT count(*) FROM users_shopping_lists WHERE id_user = ?");

                while (rs.next()) {
                   Medico med = new Medico();
                    med.setCodicefiscale(rs.getString("codicefiscale"));
                    med.setNome(rs.getString("nome"));
                    med.setCognome(rs.getString("cognome"));
                    med.setPassword(rs.getString("password"));
                    med.setSesso(rs.getString("sesso"));
                    med.setLuogonascita(rs.getString("luogonascita"));
                    med.setDatanascita(rs.getDate("datanascita"));
                    med.setNumerotelefono(rs.getString("numerotelefono"));
                    med.setEmail(rs.getString("email"));
                    med.setCitta(rs.getString("citta"));
                    med.setProvincia(rs.getString("provincia"));
                    med.setIsSpec(rs.getBoolean("is_spec"));

                    try (PreparedStatement stm_in = CON.prepareStatement("SELECT * \n" +
                                                                            "FROM med_has_paz R\n" +
                                                                            "JOIN pazienti P ON R.cf_paz=P.codicefiscale\n" +
                                                                            "WHERE cf_med = ?\n")) {
                        stm_in.setString(1, med.getCodicefiscale());
                        try (ResultSet rs_in = stm_in.executeQuery()) {
                            ArrayList<Paziente> pazlist = new ArrayList<>();
                            while(rs_in.next()) {
                                Paziente paz = new Paziente();
                                paz.setCodicefiscale(rs.getString("P.codicefiscale"));
                                paz.setNome(rs.getString("P.nome"));
                                paz.setCognome(rs.getString("P.cognome"));
                                paz.setPassword(rs.getString("P.password"));
                                paz.setSesso(rs.getString("P.sesso"));
                                paz.setLuogonascita(rs.getString("P.luogonascita"));
                                paz.setDatanascita(rs.getDate("P.datanascita"));
                                paz.setNumerotelefono(rs.getString("P.numerotelefono"));
                                paz.setEmail(rs.getString("P.email"));
                                paz.setCitta(rs.getString("P.citta"));
                                paz.setProvincia(rs.getString("P.provincia"));
                                InfoMedico im= new InfoMedico();
                                im.setNome(med.getNome());
                                im.setCognome(med.getCognome());
                                im.setCf(med.getCodicefiscale());
                                im.setCitta(med.getCitta());
                                im.setNumerotelefono(med.getNumerotelefono());
                                //Missing parte delle prescrizioni...
                                pazlist.add(paz);
                            }
                            med.setPazienti(pazlist);
                        }
                    }
                    medici.add(med);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }

        return medici;
    }
    
    @Override
    public ArrayList<PrescrizioneVisitaSpec> getVisiteNonErogate (String cf) throws DAOException{
        
        ArrayList<PrescrizioneVisitaSpec> visitenonerog = new ArrayList<>();
        
        //popolo l'array di esami non erogati
        
        try (PreparedStatement stm = CON.prepareStatement("select *\n" +
                                                        "from prescrizione_visitespec\n" +
                                                        "where erog_time IS NULL AND cf_paz = ?")) {
            stm.setString(1, cf);
            try (ResultSet rs = stm.executeQuery()) {
                while(rs.next()){
                    PrescrizioneVisitaSpec pr_visita = new PrescrizioneVisitaSpec();
                    pr_visita.setId(rs.getInt("id"));
                    pr_visita.setId_ssp(rs.getInt("id_ssp"));
                    pr_visita.setCf_med(rs.getString("cf_med"));
                    pr_visita.setCf_paz(rs.getString("cf_paz"));
                    pr_visita.setTipovisita(rs.getString("tipovisita"));
                    pr_visita.setTimestamp(rs.getTimestamp("timestamp"));
                    pr_visita.setErog_time(rs.getTimestamp("erog_time"));
                    pr_visita.setCf_medspec(rs.getString("cf_medspec"));
                    pr_visita.setRisultati(rs.getString("risultati"));
                    visitenonerog.add(pr_visita);
                }
                
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get visite for the passed cf", ex);
        }
        return  visitenonerog;
    }
    
    @Override
    public ArrayList<PrescrizioneVisitaSpec> getVisiteErogate (String cf) throws DAOException{
        
        ArrayList<PrescrizioneVisitaSpec> visiteerog = new ArrayList<>();
        
        //popolo l'array di esami erogati
        
        try (PreparedStatement stm = CON.prepareStatement("select *\n" +
                                                        "from prescrizione_visitespec\n" +
                                                        "where erog_time IS NOT NULL AND cf_paz = ?")) {
            stm.setString(1, cf);
            try (ResultSet rs = stm.executeQuery()) {
                while(rs.next()){
                    PrescrizioneVisitaSpec pr_visita = new PrescrizioneVisitaSpec();
                    pr_visita.setId(rs.getInt("id"));
                    pr_visita.setCf_med(rs.getString("cf_med"));
                    pr_visita.setId_ssp(rs.getInt("id_ssp"));
                    pr_visita.setCf_paz(rs.getString("cf_paz"));
                    pr_visita.setTipovisita(rs.getString("tipovisita"));
                    pr_visita.setTimestamp(rs.getTimestamp("timestamp"));
                    pr_visita.setErog_time(rs.getTimestamp("erog_time"));
                    pr_visita.setCf_medspec(rs.getString("cf_medspec"));
                    pr_visita.setRisultati(rs.getString("risultati"));
                    visiteerog.add(pr_visita);
                }
                
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get visite for the passed cf", ex);
        }
        return  visiteerog;
    }
    
    @Override
    public void erogaVisita (int visita_id , String cf_medspec, String risultati, int prezzo) throws DAOException{
        
        //faccio l'update della data prescrizione
        
        //creo la data
             /*
            ZoneId z = ZoneId.systemDefault();
            Date date = new Date();
            LocalDate localDate = date.toInstant().atZone(z).toLocalDate();
            int year  = localDate.getYear();
            int month = localDate.getMonthValue();
            int day   = localDate.getDayOfMonth();
            String sdata = year+"-"+month+"-"+day;
            Date data_f =new SimpleDateFormat("yyyy-MM-dd").parse(sdata);
            java.sql.Date datesql =new java.sql.Date(millis);*/
            long millis=System.currentTimeMillis();  
            Timestamp time = new Timestamp(millis);
            
            try (PreparedStatement stm = CON.prepareStatement("UPDATE prescrizione_visitespec\n" +
                                                            "SET erog_time = ? , cf_medspec = ?, risultati = ?\n" +
                                                            "WHERE id = ?")) {
           
            stm.setTimestamp(1, time);
            stm.setString(2, cf_medspec);
            stm.setString(3, risultati);
            stm.setInt(4, visita_id);
            stm.executeUpdate();
            }
            
         catch (SQLException ex) {
            throw new DAOException("Impossible to update tuple in database", ex);
        }
            
         //faccio l'insert nella tabella ticket
         try (PreparedStatement stm2 = CON.prepareStatement("INSERT INTO ticket (id, tipo, prezzo, idmedspec)\n" +
                                                            "VALUES (?, ? ,?, ?);")) {
            stm2.setInt(1, visita_id);
            stm2.setString(2, "VISITA");
            stm2.setInt(3, prezzo);
            stm2.setString(4, cf_medspec);
            stm2.execute();
            }
         catch (SQLException ex) {
            throw new DAOException("Impossible to insert tuple in database", ex);
        }
    }
    
    @Override
    public void prescriviFarmaco (int id_farm , String cf_med, String cf_paz) throws DAOException, ParseException{
        
        //creo il nuovo id
        int newid = 0;
        try (PreparedStatement stm = CON.prepareStatement("select max(id)\n" +
                                                        "from prescrizione_farmaci")) {
            
            try (ResultSet rs = stm.executeQuery()) {
                while(rs.next()){
                    newid = rs.getInt("max");
                    newid += 10;
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get maxid", ex);
        }
        
        System.out.println(newid);
        //faccio l'insert
        
        //data
        /*long millis=System.currentTimeMillis();  
        java.sql.Date datesql =new java.sql.Date(millis);*/
        long millis=System.currentTimeMillis();  
        Timestamp time = new Timestamp(millis);
            
        try (PreparedStatement ins = CON.prepareStatement("INSERT INTO prescrizione_farmaci (id, cf_med, cf_paz, idfarmaco, data_prescr)\n" +
                                                            "VALUES (?, ? ,?, ?, ?);")) {
            ins.setInt(1, newid);
            ins.setString(2, cf_med);
            ins.setString(3, cf_paz);
            ins.setInt(4, id_farm);
            ins.setTimestamp(5, time);
            ins.execute();
            
        } catch (SQLException ex){
            throw new DAOException ("Failed to insert tuple in database",ex);
        }
        
        //aggiorno dataultimavisita
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-mm-dd");
        String strdate = sf.format(Calendar.getInstance().getTime());
        java.util.Date utilDate = new Date();
            try {
                 utilDate = new SimpleDateFormat("yyyy-mm-dd").parse(strdate);
            }
            catch (ParseException e){
                if (e == null) {
                    throw new ParseException("source is null",0);
                }
            }
            
        java.sql.Date ultimavisita = new java.sql.Date(utilDate.getTime());
        //aggiorno la data ultima visita
        try (PreparedStatement upd = CON.prepareStatement("UPDATE pazienti SET dataultimavisita = ? WHERE codicefiscale = ?")){
            
            upd.setDate(1, ultimavisita);
            upd.setString(2,cf_paz);
             if (upd.executeUpdate() != 1) {
                throw new DAOException("Impossible to update the user");
            }
        } catch (SQLException ex){
            throw new DAOException ("Failed to update tuple in database",ex);
        }
        
    }
    
    @Override
    public void prescriviEsame (int idesame , String cf_med, String cf_paz) throws DAOException, ParseException{
        
    //creo il nuovo id
        int newid = 0;
        try (PreparedStatement stm = CON.prepareStatement("select max(id)\n" +
                                                        "from prescrizione_esami")) {
            
            try (ResultSet rs = stm.executeQuery()) {
                while(rs.next()){
                    newid = rs.getInt("max");
                    newid += 10;
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get maxid", ex);
        }
        
        //faccio l'insert
        
        //timestamp
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            
        try (PreparedStatement ins = CON.prepareStatement("INSERT INTO prescrizione_esami (id, cf_med, cf_paz, id_esame, timestamp)\n" +
                                                            "VALUES (?, ? ,?, ?, ?);")) {
            ins.setInt(1, newid);
            ins.setString(2, cf_med);
            ins.setString(3, cf_paz);
            ins.setInt (4, idesame);
            ins.setTimestamp(5, timestamp);
            ins.execute();
            
        } catch (SQLException ex){
            throw new DAOException ("Failed to insert tuple in database",ex);
        }
        
        //aggiorno dataultimavisita
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-mm-dd");
        String strdate = sf.format(Calendar.getInstance().getTime());
        java.util.Date utilDate = new Date();
            try {
                 utilDate = new SimpleDateFormat("yyyy-mm-dd").parse(strdate);
            }
            catch (ParseException e){
                if (e == null) {
                    throw new ParseException("source is null",0);
                }
            }
            
        java.sql.Date ultimavisita = new java.sql.Date(utilDate.getTime());
        //aggiorno la data ultima visita
        try (PreparedStatement upd = CON.prepareStatement("UPDATE pazienti SET dataultimavisita = ? WHERE codicefiscale = ?")){
            
            upd.setDate(1, ultimavisita);
            upd.setString(2,cf_paz);
             if (upd.executeUpdate() != 1) {
                throw new DAOException("Impossible to update the user");
            }
        } catch (SQLException ex){
            throw new DAOException ("Failed to update tuple in database",ex);
        }
    }
    
    @Override
    public void prescriviVisita (String tipovisita , String cf_med, String cf_paz) throws DAOException, ParseException{
       
    //creo il nuovo id
        int newid = 0;
        try (PreparedStatement stm = CON.prepareStatement("select max(id)\n" +
                                                        "from prescrizione_visitespec")) {
            
            try (ResultSet rs = stm.executeQuery()) {
                while(rs.next()){
                    newid = rs.getInt("max");
                    newid += 10;
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get maxid", ex);
        }
        
        //faccio l'insert
        
        //timestamp
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            
        try (PreparedStatement ins = CON.prepareStatement("INSERT INTO prescrizione_visitespec (id, cf_med, cf_paz, tipovisita, timestamp)\n" +
                                                            "VALUES (?, ? ,?, ?, ?);")) {
            ins.setInt(1, newid);
            ins.setString(2, cf_med);
            ins.setString(3, cf_paz);
            ins.setString(4, tipovisita);
            ins.setTimestamp(5, timestamp);
            ins.execute();
            
        } catch (SQLException ex){
            throw new DAOException ("Failed to insert tuple in database",ex);
        }
        
        //aggiorno dataultimavisita
        SimpleDateFormat sf = new SimpleDateFormat("yyyy-mm-dd");
        String strdate = sf.format(Calendar.getInstance().getTime());
        java.util.Date utilDate = new Date();
            try {
                 utilDate = new SimpleDateFormat("yyyy-mm-dd").parse(strdate);
            }
            catch (ParseException e){
                if (e == null) {
                    throw new ParseException("source is null",0);
                }
            }
            
        java.sql.Date ultimavisita = new java.sql.Date(utilDate.getTime());
        //aggiorno la data ultima visita
        try (PreparedStatement upd = CON.prepareStatement("UPDATE pazienti SET dataultimavisita = ? WHERE codicefiscale = ?")){
            
            upd.setDate(1, ultimavisita);
            upd.setString(2,cf_paz);
             if (upd.executeUpdate() != 1) {
                throw new DAOException("Impossible to update the user");
            }
        } catch (SQLException ex){
            throw new DAOException ("Failed to update tuple in database",ex);
        }
    }
    /**
     * Update the user passed as parameter and returns it.
     *
     * @param med the med used to update the persistence system.
     * @return the updated med.
     * @throws DAOException if an error occurred during the action.
     */
    @Override
    public Medico update(Medico med) throws DAOException {
        if (med == null) {
            throw new DAOException("parameter not valid", new IllegalArgumentException("The passed user is null"));
        }

        try (PreparedStatement std = CON.prepareStatement("UPDATE medici SET nome = ?, cognome = ?, password = ?, flag_recuperopassword = ?, sesso = ?, luogonascita = ?, datanascita = ?, numerotelefono = ?, email = ?, citta = ?, provincia = ?, is_spec = ? WHERE codicefiscale = ?")) {
            
            std.setString(1, med.getNome());
            std.setString(2, med.getCognome());
            std.setString(3, med.getPassword());
            std.setBoolean(4, med.getIsPswTemporary());
            std.setString(5, med.getSesso());
            std.setString(6, med.getLuogonascita());
            std.setDate(7, med.getDatanascita());
            std.setString(8, med.getNumerotelefono());
            std.setString(9, med.getEmail());
            std.setString(10, med.getCitta());
            std.setString(11, med.getProvincia());
            std.setBoolean(12, med.getIsSpec());
            std.setString(13, med.getCodicefiscale());
            if (std.executeUpdate() == 1) {
                return med;
            } else {
                throw new DAOException("Impossible to update the user");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to update the user", ex);
        }
    }

    @Override
    public void resetPsw(String cf, String enc_psw) throws DAOException {
        try (PreparedStatement stm=CON.prepareStatement("UPDATE medici SET password = ?, flag_recuperopassword = ? WHERE codicefiscale = ?")) {
            stm.setString(1, enc_psw);
            stm.setBoolean(2, true);
            stm.setString(3, cf);
            stm.execute();
        }catch (SQLException ex) {
            throw  new DAOException("Impossible to insert into fotopazienti", ex);
        }
    }
}
