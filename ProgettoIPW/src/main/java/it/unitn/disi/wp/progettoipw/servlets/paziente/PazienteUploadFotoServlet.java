/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.servlets.paziente;

import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.progettoipw.persistence.dao.PazienteDAO;
import it.unitn.disi.wp.progettoipw.persistence.entities.Paziente;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author Sean
 */

/*@MultipartConfig(fileSizeThreshold=1024*1024, // 1MB
             maxFileSize=1024*1024*3,      // 3MB
             maxRequestSize=1024*1024*50)*/
@MultipartConfig
public class PazienteUploadFotoServlet extends HttpServlet {

    private PazienteDAO pazDao;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            pazDao = daoFactory.getDAO(PazienteDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }
    
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
                throws ServletException, IOException {
                response.setContentType("text/html;charset=UTF-8");
                Paziente paz = (Paziente)request.getSession().getAttribute("paziente");
                String imageFolder = getServletContext().getInitParameter("imageFolder");
                if (imageFolder == null) {
                    throw new ServletException("Image folder not configured");
                }

                imageFolder = getServletContext().getRealPath(imageFolder);
                String savePath = imageFolder+"\\"+paz.getCodicefiscale();      //Attenzione qui
                
                Part filePart = request.getPart("fileSrc");
                if ((filePart != null) && (filePart.getSize() > 0)) {
                    String filename = paz.getNome().toLowerCase()+"_"+paz.getCognome().toLowerCase()+paz.getElencofoto().size();
                    
                    //Get the file extension
                    String extension = "";

                    int i = Paths.get(filePart.getSubmittedFileName()).getFileName().toString().lastIndexOf('.');
                    if (i >= 0) {
                        extension = Paths.get(filePart.getSubmittedFileName()).getFileName().toString().substring(i);
                    }
                    
                    if(extension.equals(".png")||extension.equals(".jpg")) {
                        filename+=extension;
                        System.out.println("filename: "+filename);
                        try (InputStream fileContent = filePart.getInputStream()) {
                            File file = new File(savePath, filename);
                            //file.createNewFile();
                            Files.copy(fileContent, file.toPath());
                            try {
                                pazDao.uploadFoto((Paziente)request.getSession().getAttribute("paziente"), paz.getPercorsofoto()+filename);
                                    } catch (DAOException ex) {
                                        //TODO: log exception
                                        request.getServletContext().log("Impossible to upload the photo", ex);
                            }
                            ArrayList<String> tmp = paz.getElencofoto();
                            tmp.add(paz.getPercorsofoto()+filename);
                            paz.setElencofoto(tmp);
                        } catch (FileAlreadyExistsException ex) {
                            getServletContext().log("File \"" + filename + "\" already exists on the server");
                        } catch (RuntimeException ex) {
                            //TODO: handle the exception
                            getServletContext().log("impossible to upload the file", ex);
                        }
                    }
                }
                String contextPath = getServletContext().getContextPath();
                if (!contextPath.endsWith("/")) {
                    contextPath += "/";
                }

                if (!response.isCommitted()) {
                    response.sendRedirect(response.encodeRedirectURL(contextPath + "paziente/welcome.jsp"));
                }
               
        }
    
        private String extractFileName(Part part) {
            String contentDisp = part.getHeader("content-disposition");
            String[] items = contentDisp.split(";");
            for (String s : items) {
                if (s.trim().startsWith("filename")) {
                    return s.substring(s.indexOf("=") + 2, s.length()-1);
                }
            }
            return "";
        }
}
