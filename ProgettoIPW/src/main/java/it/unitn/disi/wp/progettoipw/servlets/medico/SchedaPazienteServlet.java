/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.servlets.medico;

import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.progettoipw.persistence.dao.MedicoDAO;
import it.unitn.disi.wp.progettoipw.persistence.entities.Medico;
import it.unitn.disi.wp.progettoipw.persistence.entities.Paziente;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 *
 * @author Sean
 */
public class SchedaPazienteServlet extends HttpServlet {
    
    private MedicoDAO medDao;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            medDao = daoFactory.getDAO(MedicoDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
    
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        request.getSession().setAttribute("paziente", null);
        request.getSession().setAttribute("FarmError", null);
        request.getSession().setAttribute("EsameError", null);
        Medico med = (Medico)request.getSession().getAttribute("medico");
        if (med == null) {
            response.sendRedirect(response.encodeRedirectURL(contextPath+"medicospec/welcome.jsp")); 
        }else{
            response.sendRedirect(response.encodeRedirectURL(contextPath+"medico/welcome.jsp")); 
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        String cf_rec=request.getParameter("cf");
        Paziente paz = null;
        ArrayList<Paziente> tuttipaz = (ArrayList<Paziente>)request.getServletContext().getAttribute("PazientiList");
        Medico med = (Medico)request.getSession().getAttribute("medico");
        //Medico medspec = (Medico)request.getSession().getAttribute("medicospec");
        
        //sono uno specialista
        if(med == null){
            for (Paziente tpaz: tuttipaz){
                if (tpaz.getCodicefiscale().equals(cf_rec)){
                    paz = tpaz;
                    break;
                }
            }
            if(paz==null) {
                response.sendRedirect(response.encodeRedirectURL(contextPath+"medicospec/welcome.jsp"));
            }else{
                request.getSession().setAttribute("paziente",paz);
                response.sendRedirect(response.encodeRedirectURL(contextPath+"medicospec/schedapaziente.jsp")); 
            }
        } else {
            //sono un medico normale
            //Verifico che la mia sessione sia aggiornata
            try {
                request.getSession().setAttribute("medico", medDao.getByPrimaryKey(med.getCodicefiscale()));
            } catch (DAOException ex) {
                Logger.getLogger(SchedaPazienteServlet.class.getName()).log(Level.SEVERE, null, ex);
            }
            med = (Medico)request.getSession().getAttribute("medico");
            for (Paziente tpaz: med.getPazienti()) {
                if (tpaz.getCodicefiscale().equals(cf_rec)){
                    paz = tpaz;
                    break;
                }
            }
            if(paz==null) {
                response.sendRedirect(response.encodeRedirectURL(contextPath+"medico/welcome.jsp"));
            }else{
                request.getSession().setAttribute("paziente",paz);
                request.getSession().setAttribute("FarmError", false);
                request.getSession().setAttribute("EsameError", false);
                response.sendRedirect(response.encodeRedirectURL(contextPath+"medico/schedapaziente.jsp"));
            }  
        }
    }
}
