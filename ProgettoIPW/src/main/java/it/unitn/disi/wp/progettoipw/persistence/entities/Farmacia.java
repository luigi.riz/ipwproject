/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.persistence.entities;

/**
 * The entity that describe a {@code farmacia} entity.
 * 
 * @author Luigi Riz;
 * @since 2019.07.25
 */
public class Farmacia{
    private Integer idfarmacia;
    private String indirizzo;
    private String nomefarmacia;
    private int cap;
    private String citta;
    private String provincia;
    private String password;
    private Boolean isPswTemporary;
    private String email;

    public Farmacia() {
    }

    public Farmacia(Integer idfarmacia, String indirizzo, String nomefarmacia, int cap, String citta, String provincia, String password, Boolean isPswTemporary, String email) {
        this.idfarmacia = idfarmacia;
        this.indirizzo = indirizzo;
        this.nomefarmacia = nomefarmacia;
        this.cap = cap;
        this.citta = citta;
        this.provincia = provincia;
        this.password = password;
        this.isPswTemporary = isPswTemporary;
        this.email = email;
    }

    public Integer getIdfarmacia() {
        return idfarmacia;
    }

    public void setIdfarmacia(Integer idfarmacia) {
        this.idfarmacia = idfarmacia;
    }

    public String getIndirizzo() {
        return indirizzo;
    }

    public void setIndirizzo(String indirizzo) {
        this.indirizzo = indirizzo;
    }

    public String getNomefarmacia() {
        return nomefarmacia;
    }

    public void setNomefarmacia(String nomefarmacia) {
        this.nomefarmacia = nomefarmacia;
    }

    public int getCap() {
        return cap;
    }

    public void setCap(int cap) {
        this.cap = cap;
    }

    public String getCitta() {
        return citta;
    }

    public void setCitta(String citta) {
        this.citta = citta;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getIsPswTemporary() {
        return isPswTemporary;
    }

    public void setIsPswTemporary(Boolean isPswTemporary) {
        this.isPswTemporary = isPswTemporary;
    }
    
}
