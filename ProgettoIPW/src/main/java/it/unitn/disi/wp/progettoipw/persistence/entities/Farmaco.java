/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.persistence.entities;

/**
 * The entity that describe a {@code farmaco} entity.
 * 
 * @author Luigi Riz;
 * @since 2019.07.25
 */
public class Farmaco{
    private Integer idfarmaco;
    private String nomefarmaco;

    public Farmaco() {
    }

    public Farmaco(Integer idfarmaco) {
        this.idfarmaco = idfarmaco;
    }

    public Farmaco(Integer idfarmaco, String nomefarmaco) {
        this.idfarmaco = idfarmaco;
        this.nomefarmaco = nomefarmaco;
    }

    public Integer getIdfarmaco() {
        return idfarmaco;
    }

    public void setIdfarmaco(Integer idfarmaco) {
        this.idfarmaco = idfarmaco;
    }

    public String getNomefarmaco() {
        return nomefarmaco;
    }

    public void setNomefarmaco(String nomefarmaco) {
        this.nomefarmaco = nomefarmaco;
    }
}
