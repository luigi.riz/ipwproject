/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.persistence.dao.jdbc;

import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.jdbc.JDBCDAO;
import it.unitn.disi.wp.progettoipw.persistence.dao.EsamiListDAO;
import it.unitn.disi.wp.progettoipw.persistence.entities.Esame;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Luigi
 */
public class JDBCEsamiListDAO extends JDBCDAO<Esame, Integer> implements EsamiListDAO{
    
    /**
     * The default constructor of the class.
     *
     * @param con the connection to the persistence system.
     */
    public JDBCEsamiListDAO(Connection con) {
        super(con);
    }
    
    @Override
    public List<Esame> getAll() throws DAOException {
        try (Statement stmt = CON.createStatement()) {
            ResultSet rs = stmt.executeQuery("SELECT * FROM esami");
            ArrayList<Esame> Esami = new ArrayList();
            while(rs.next()) {
                Esame e = new Esame();
                e.setIdesame(rs.getInt("id"));
                e.setTipoesame(rs.getString("tipoesame"));
                e.setDescrizione(rs.getString("descrizione"));
                Esami.add(e);
            }
            return Esami;
        }catch (SQLException ex) {
            throw new DAOException("Impossible to get esami", ex);
        }
    }

    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM esami");
            if (counter.next()) {
                return counter.getLong(1);
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to count pazienti", ex);
        }

        return 0L;
    }

    @Override
    public Esame getByPrimaryKey(Integer p) throws DAOException {
        if (p == null) {
            throw new DAOException("primaryKey is null");
        }
        try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM esami WHERE id= ?")) {
            stm.setInt(1, p);
            try (ResultSet rs = stm.executeQuery()) {
                Esame e = new Esame();
                e.setDescrizione(rs.getString("descrizione"));
                e.setIdesame(p);
                e.setTipoesame(rs.getString("tipoesame"));
                return e;
            } catch (SQLException ex) {
            throw new DAOException("Impossible to get the esami for the passed primary key", ex);
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to count pazienti", ex);
        }
    }
    
}
