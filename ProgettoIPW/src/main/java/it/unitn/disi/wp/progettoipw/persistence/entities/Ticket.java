/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.persistence.entities;

import java.sql.Timestamp;

/**
 *
 * @author Sean
 */

/*
La data di erogazione è unica e può riferirsi sia a quella indicata su un ticket di tipo ESAME che FARMACO
*/
public class Ticket {

    private Integer id;
    private String tipo;
    private Integer prezzo;
    private Integer idfarmacia;
    private Integer idfarmaco;
    private String idmedspec;
    private Integer idssp;
    private String nomefarmacia;
    private String nomefarmaco;
    private String nomeesame;
    private String tipovisita;
    private Timestamp erogtime;
    private Timestamp data_erog;
    
    public Ticket ()
    {
        
    }
    
     public Ticket(Integer id, String tipo, Integer prezzo, Integer idfarmacia, Integer idfarmaco, String idmedspec, Integer idssp, String nomefarmacia, Timestamp erogtime, Timestamp data_erog, String nomeesame, String nomefarmaco, String tipovisita) {
        this.id = id;
        this.tipo = tipo;
        this.prezzo = prezzo;
        this.idfarmacia = idfarmacia;
        this.idfarmaco = idfarmaco;
        this.idmedspec = idmedspec;
        this.idssp = idssp;
        this.nomefarmacia = nomefarmacia;
        this.erogtime = erogtime;
        this.data_erog = data_erog;
        this.nomefarmaco = nomefarmaco;
        this.nomeesame = nomeesame;
        this.tipovisita = tipovisita;
    }
    
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Integer getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(Integer prezzo) {
        this.prezzo = prezzo;
    }

    public Integer getIdfarmacia() {
        return idfarmacia;
    }

    public void setIdfarmacia(Integer idfarmacia) {
        this.idfarmacia = idfarmacia;
    }
    
    public Integer getIdfarmaco() {
        return idfarmaco;
    }

    public void setIdfarmaco(Integer idfarmaco) {
        this.idfarmaco = idfarmaco;
    }
    
    public String getIdmedspec() {
        return idmedspec;
    }

    public void setIdmedspec(String idmedspec) {
        this.idmedspec = idmedspec;
    }

    public Integer getIdssp() {
        return idssp;
    }

    public void setIdssp(Integer idssp) {
        this.idssp = idssp;
    }

    public String getNomefarmacia() {
        return nomefarmacia;
    }

    public void setNomefarmacia(String nomefarmacia) {
        this.nomefarmacia = nomefarmacia;
    }
    
    public String getNomefarmaco(){
        return nomefarmaco;
    }
    
    public void setNomefarmaco (String nomefarmaco){
        this.nomefarmaco = nomefarmaco;
    }
    
    public String getNomeesame() {
        return nomeesame;
    }
    
    public void setNomeesame(String nomeesame){
        this.nomeesame = nomeesame;
    }
    
    public String getTipovisita() {
        return tipovisita;
    }
    
    public void setTipovisita(String tipovisita){
        this.tipovisita = tipovisita;
    }
    
    public Timestamp getErogtime() {
        return erogtime;
    }

    public void setErogtime(Timestamp erogtime) {
        this.erogtime = erogtime;
    }

    public Timestamp getData_erog() {
        return data_erog;
    }

    public void setData_erog(Timestamp data_erog) {
        this.data_erog = data_erog;
    }
    
}
