/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.servlets.ssp;

import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.progettoipw.persistence.dao.ServizioProvincialeDAO;
import it.unitn.disi.wp.progettoipw.persistence.entities.ServizioProvinciale;
import it.unitn.disi.wp.progettoipw.persistence.entities.Ticket;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.Row;
/**
 *
 * @author Sean
 */
//@WebServlet(name = "EsportaReportPDFServlet", urlPatterns = {"/EsportaReportPDFServlet"})
public class EsportaReportXLSServlet extends HttpServlet {
    
    private ServizioProvincialeDAO sspDao;
    
    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            sspDao = daoFactory.getDAO(ServizioProvincialeDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        String pdfFolder = getServletContext().getInitParameter("reportFolder");
        if (pdfFolder == null) {
            throw new ServletException("PDFs folder not configured");
        }
        
        pdfFolder = getServletContext().getRealPath(pdfFolder);
        Integer sspId;
        try {
            ServizioProvinciale actual_ssp =(ServizioProvinciale)request.getSession().getAttribute("ssp");
            sspId = actual_ssp.getId();
        } catch (NumberFormatException | NullPointerException ex) {
            throw new ServletException("Impossible to retrieve ssp id");
        }
        
        ArrayList<Ticket> array_te = new ArrayList();
        try{
            array_te = sspDao.getReportTicketEsami(sspId);
        } catch (DAOException ex){
            throw new ServletException("Impossible to retrieve exams tickets");
        }
        
        ArrayList<Ticket> array_tf = new ArrayList();
        try{
            array_tf = sspDao.getReportTicketFarmaci(sspId);
        } catch (DAOException ex){
            throw new ServletException("Impossible to retrieve drugs tickets");
        }
        
        ArrayList<Ticket> array_tv = new ArrayList();
        try{
            array_tv = sspDao.getReportTicketVisiteSpec(sspId);
        } catch (DAOException ex){
            throw new ServletException("Impossible to retrieve medspec tickets");
        }
       
        Workbook exceldoc;
        exceldoc = new HSSFWorkbook();
        
        //I 3 fogli
        
        Sheet tfarmaci = exceldoc.createSheet("Ticket Farmaci");
        Sheet tvisite = exceldoc.createSheet("Ticket Visite");
        Sheet tesami = exceldoc.createSheet("Ticket Esami");
        
        String [] titlestf = {"ID", "ID farmacia", "Nome farmacia", "ID farmaco", "Nome farmaco", "Data erogazione", "Prezzo  €"};
        String [] titlestv = {"ID", "Medico Specialista", "Tipo visita", "Data erogazione", "Prezzo  €"};
        String [] titleste = {"ID", "SSP", "Tipo esame", "Data erogazione", "Prezzo  €"};
        
        Row hrtf = tfarmaci.createRow(0);
        hrtf.setHeightInPoints(12.75f);
        Row hrtv = tvisite.createRow(0);
        hrtv.setHeightInPoints(12.75f);
        Row hrte = tesami.createRow(0);
        
        //Linea di intestazione per i 3 fogli
        
        for (int i = 0; i < titlestf.length; i++) {
            Cell cell = hrtf.createCell(i);
            cell.setCellValue(titlestf[i]);
        }
        
        for (int i = 0; i < titlestv.length; i++) {
            Cell cell = hrtv.createCell(i);
            cell.setCellValue(titlestv[i]);
        }
        
        for (int i = 0; i < titleste.length; i++) {
            Cell cell = hrte.createCell(i);
            cell.setCellValue(titleste[i]);
        }
        
        
        int j = 1;
        //Dati per ticket farmaci
        for (Ticket ticket : array_tf){
            String str_time = new SimpleDateFormat("MM-dd-yyyy").format(ticket.getData_erog());
            Row row = tfarmaci.createRow(j);
            Cell ID = row.createCell(0);
            Cell IDfarmacia = row.createCell(1);
            Cell NomeFarmacia = row.createCell(2);
            Cell IDFarmaco = row.createCell(3);
            Cell NomeFarmaco = row.createCell(4);
            Cell DataErog = row.createCell(5);
            Cell Prezzo = row.createCell(6);
            
            ID.setCellValue(ticket.getId());
            IDfarmacia.setCellValue(ticket.getIdfarmacia());
            NomeFarmacia.setCellValue(ticket.getNomefarmacia());
            IDFarmaco.setCellValue(ticket.getIdfarmaco());
            NomeFarmaco.setCellValue(ticket.getNomefarmaco());
            DataErog.setCellValue(str_time);
            Prezzo.setCellValue(ticket.getPrezzo());
            j++;
        }
        
        int k = 1;
        //Dati per ticket visite
        for (Ticket ticket : array_tv){
            String str_time = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss").format(ticket.getErogtime());
            Row row = tvisite.createRow(k);
            Cell ID = row.createCell(0);
            Cell MedicoSpecialista = row.createCell(1);
            Cell Tipovisita = row.createCell(2);
            Cell DataErog = row.createCell(3);
            Cell Prezzo = row.createCell(4);
            
            ID.setCellValue(ticket.getId());
            MedicoSpecialista.setCellValue(ticket.getIdmedspec());
            Tipovisita.setCellValue(ticket.getTipovisita());
            DataErog.setCellValue(str_time);
            Prezzo.setCellValue(ticket.getPrezzo());
            k++;
        }
        
        int l = 1;
        //Dati per ticket esami
        for (Ticket ticket : array_te){
            String str_time = new SimpleDateFormat("MM-dd-yyyy").format(ticket.getData_erog());
            Row row = tesami.createRow(l);
            Cell ID = row.createCell(0);
            Cell SSP = row.createCell(1);
            Cell Tipoesame = row.createCell(2);
            Cell DataErog = row.createCell(3);
            Cell Prezzo = row.createCell(4);
            
            ID.setCellValue(ticket.getId());
            SSP.setCellValue(ticket.getIdssp());
            Tipoesame.setCellValue(ticket.getNomeesame());
            DataErog.setCellValue(str_time);
            Prezzo.setCellValue(ticket.getPrezzo());
            l++;
        }
        
        //ridimensiono le colonne
        
        for (int i = 0; i<titlestf.length; i++){
            tfarmaci.autoSizeColumn(i);
        }
        
        for (int i = 0; i<titlestv.length; i++){
            tvisite.autoSizeColumn(i);
        }
        
        for (int i = 0; i<titleste.length; i++){
            tesami.autoSizeColumn(i);
        }
        //salvo il file
        
        String nomefile="report"+"_"+Calendar.getInstance().getTimeInMillis()+".xls";
        response.setContentType("application/vnd.ms-excel");
        response.setHeader("Content-disposition", "attachment; filename="+nomefile);
        ServletOutputStream out =response.getOutputStream();
        exceldoc.write(out);
        out.close();
        exceldoc.close();
        
            
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        response.sendRedirect(response.encodeRedirectURL(contextPath + "ssp/welcome.jsp"));
    }
}

