/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.servlets.farmacia;


import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.progettoipw.persistence.dao.FarmaciaDAO;
import java.io.IOException;
import javax.servlet.ServletException;
import it.unitn.disi.wp.progettoipw.persistence.entities.Farmacia;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sean
 */
public class ErogaRicettaServlet extends HttpServlet {
    
    private FarmaciaDAO farmDao;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            farmDao = daoFactory.getDAO(FarmaciaDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        String ric_id =request.getParameter("ric_id");
        Farmacia actual_farm =(Farmacia)request.getSession().getAttribute("farm");
        
        try{
            farmDao.erogaRicetta(Integer.parseInt(ric_id), actual_farm.getIdfarmacia());
            //Aggiorno la sessione
            request.getSession().setAttribute("ricetteerog", farmDao.getRicetteErogate((String)request.getSession().getAttribute("cf_paz")));
            request.getSession().setAttribute("ricettenonerog", farmDao.getRicetteNonErogate((String)request.getSession().getAttribute("cf_paz")));
        } catch (DAOException ex){
            throw new ServletException("Impossible to perform action");
        }
        response.sendRedirect(response.encodeRedirectURL(contextPath + "farmacia/ricettepaziente.jsp"));
    }

}
