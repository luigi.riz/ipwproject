/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.servlets.ssp;

import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.progettoipw.mail.Mail;
import it.unitn.disi.wp.progettoipw.persistence.dao.ServizioProvincialeDAO;
import it.unitn.disi.wp.progettoipw.persistence.entities.Paziente;
import it.unitn.disi.wp.progettoipw.persistence.entities.PrescrizioneEsame;
import java.io.IOException;
import javax.servlet.ServletException;
import it.unitn.disi.wp.progettoipw.persistence.entities.ServizioProvinciale;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sean
 */
public class ErogaEsameServlet extends HttpServlet {

    private ServizioProvincialeDAO sspDao;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            sspDao = daoFactory.getDAO(ServizioProvincialeDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        
        String idssp = request.getParameter("idssp");
        String esame_id =request.getParameter("esame_id");
        ServizioProvinciale actual_ssp =(ServizioProvinciale)request.getSession().getAttribute("ssp");
        int prezzo = 0;
        
         if (idssp == null || idssp.equals("") || idssp.equals("0")){
            //esame normale
            prezzo = 11;
        } else {
            //esame di richiamo
            prezzo = 0;
        }
        try{
            sspDao.erogaEsame(Integer.parseInt(esame_id), actual_ssp.getId(), prezzo);
            //Aggiorno la sessione
            String actual_paz_cf = (String)request.getSession().getAttribute("actual_paz_cf");
            ArrayList<PrescrizioneEsame> esamierog = new ArrayList<>();
            ArrayList<PrescrizioneEsame> esaminonerog = new ArrayList<>();
            //Invio la mail
            String email="error";
            List<Paziente> pazlist = (List<Paziente>)getServletContext().getAttribute("PazientiList");
            for (Paziente paz : pazlist) {
                if(paz.getCodicefiscale().equals(actual_paz_cf)) {
                    email=paz.getEmail();
                    break;
                }
            }
            String subject = "Nuovo referto";
            String text = "Hai un nuovo referto da leggere: \nEsame\nEffettuato da: "+actual_ssp.getUserinfo().replaceAll("_", " ");
            Mail.sendMail(getServletContext(), email, subject, text);

            try{
                esamierog = sspDao.getEsamiErogati(actual_paz_cf);
            } catch (DAOException ex){
                throw new ServletException("Impossible to retrieve esami erogati");
            }

            try{
                esaminonerog = sspDao.getEsamiNonErogati(actual_paz_cf);
            } catch (DAOException ex){
                throw new ServletException("Impossible to retrieve esami non erogati");
            }

            request.getSession().setAttribute("esamierog", esamierog);
            request.getSession().setAttribute("esaminonerog", esaminonerog);
        } catch (DAOException ex){
            throw new ServletException("Impossible to perform action");
        }
        response.sendRedirect(response.encodeRedirectURL(contextPath + "ssp/esamipaziente_new.jsp"));
    }

}
