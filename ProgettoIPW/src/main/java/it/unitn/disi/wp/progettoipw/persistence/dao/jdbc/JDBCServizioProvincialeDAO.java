/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.persistence.dao.jdbc;

import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.jdbc.JDBCDAO;
import it.unitn.disi.wp.progettoipw.persistence.dao.ServizioProvincialeDAO;
import it.unitn.disi.wp.progettoipw.persistence.entities.Esame;
import it.unitn.disi.wp.progettoipw.persistence.entities.PrescrizioneEsame;
import it.unitn.disi.wp.progettoipw.persistence.entities.ServizioProvinciale;
import it.unitn.disi.wp.progettoipw.persistence.entities.Paziente;
import it.unitn.disi.wp.progettoipw.persistence.entities.Ticket;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.sql.Date;  
import java.util.Random;
import java.util.List;
/**
 *
 * @author Sean
 */
public class JDBCServizioProvincialeDAO extends JDBCDAO<ServizioProvinciale, Integer> implements ServizioProvincialeDAO {
    
    public JDBCServizioProvincialeDAO(Connection con) {
        super(con);
    }
    
     @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM serviziprovinciali");
            if (counter.next()) {
                return counter.getLong(1);
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to count serviziprovinciali", ex);
        }

        return 0L;
    }
    
     @Override
    public ServizioProvinciale getByIdAndPassword (Integer id, String password) throws DAOException {
        if (id == null) {
            throw new DAOException("id is null");
        }
        
        try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM serviziprovinciali WHERE id = ? AND password = ?")) {
            stm.setInt(1, id);
            stm.setString(2, password);
            try (ResultSet rs = stm.executeQuery()) {
                int count = 0;
                while(rs.next()) {
                    count++;
                    if (count > 1) {
                        throw new DAOException("Unique constraint violated! There are more than one user with the same id! WHY???");
                    }
                    ServizioProvinciale ssp = new ServizioProvinciale();
                    ssp.setEmail(rs.getString("email"));
                    ssp.setUserinfo(rs.getString("userinfo"));
                    ssp.setId(Integer.valueOf(rs.getString("id")));
                    ssp.setPassword(rs.getString("password"));
                    ssp.setIsPswTemporary(rs.getBoolean("flag_recuperopassword"));

                    return ssp;
                }
                return null;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the user for the passed id", ex);
        }
    }
    
    @Override
    public ArrayList<PrescrizioneEsame> getEsamiNonErogati (String cf) throws DAOException{
         ArrayList<PrescrizioneEsame> esaminonerog = new ArrayList<>();
        
        //popolo l'array di esami non erogati
        
        try (PreparedStatement stm_in = CON.prepareStatement("SELECT PE.id, PE.cf_med, M.nome, M.cognome, PE.id_ssp, PE.cf_paz, PE.id_esame, E.tipoesame, E.descrizione, PE.timestamp,  PE.data_erog, PE.risultati, idssp AS id_ssp_erog\n" +
                                                            "FROM prescrizione_esami PE\n" +
                                                            "JOIN esami E on PE.id_esame=E.id\n" +
                                                            "LEFT JOIN medici M on PE.cf_med=M.codicefiscale\n" +
                                                            "LEFT JOIN ticket T on PE.id=T.id\n" +
                                                            "WHERE cf_paz= ? and data_erog IS NULL\n" +
                                                            "ORDER BY PE.timestamp DESC")) {
            stm_in.setString(1, cf);
            try (ResultSet rs_in = stm_in.executeQuery()) {
                while(rs_in.next()) {
                    PrescrizioneEsame pe= new PrescrizioneEsame();
                    pe.setId(rs_in.getInt("id"));
                    pe.setCf_med(rs_in.getString("cf_med"));
                    pe.setNome_med(rs_in.getString("nome")+" "+rs_in.getString("cognome"));
                    pe.setId_ssp(rs_in.getInt("id_ssp"));
                    pe.setCf_paz(rs_in.getString("cf_paz"));
                    Esame e = new Esame();
                    e.setIdesame(rs_in.getInt("id_esame"));
                    e.setTipoesame(rs_in.getString("tipoesame"));
                    e.setDescrizione(rs_in.getString("descrizione"));
                    pe.setEsame(e);
                    pe.setTimestamp(rs_in.getTimestamp("timestamp"));
                    pe.setData_erog(rs_in.getTimestamp("data_erog"));
                    pe.setRisultati(rs_in.getString("risultati"));
                    pe.setId_ssp_erog(rs_in.getInt("id_ssp_erog"));
                    esaminonerog.add(pe);
                }
            }
        }catch (SQLException ex) {
            throw new DAOException("Impossible to get ricette for the passed cf", ex);
        }
        
        /*try (PreparedStatement stm = CON.prepareStatement("select PE.id, cf_med,id_ssp, cf_paz,id_esame, tipoesame, descrizione, \"timestamp\", data_erog, risultati \n" +
                                                            "from prescrizione_esami PE\n" +
                                                            "join esami E on PE.id_esame=E.id\n" +
                                                            "WHERE cf_paz = ? AND data_erog IS NULL")) {
            stm.setString(1, cf);
            try (ResultSet rs = stm.executeQuery()) {
                while(rs.next()){
                    PrescrizioneEsame pr_esame = new PrescrizioneEsame();
                    pr_esame.setId(rs.getInt("id"));
                    pr_esame.setCf_med(rs.getString("cf_med"));
                    pr_esame.setId_ssp(rs.getInt("id_ssp"));
                    pr_esame.setCf_paz(rs.getString("cf_paz"));
                    Esame e= new Esame();
                    e.setIdesame(rs.getInt("id_esame"));
                    e.setTipoesame(rs.getString("tipoesame"));
                    e.setDescrizione(rs.getString("descrizione"));
                    pr_esame.setEsame(e);
                    pr_esame.setTimestamp(rs.getTimestamp("timestamp"));
                    pr_esame.setData_erog(rs.getTimestamp("data_erog"));
                    pr_esame.setRisultati(rs.getString("risultati"));
                    esaminonerog.add(pr_esame);
                }
                
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get ricette for the passed cf", ex);
        }*/
        return  esaminonerog;
    }
    
    
    @Override
    public ArrayList<PrescrizioneEsame> getEsamiErogati (String cf) throws DAOException{
        ArrayList<PrescrizioneEsame> esamierog = new ArrayList<>();
        
        try (PreparedStatement stm_in = CON.prepareStatement("SELECT PE.id, PE.cf_med, M.nome, M.cognome, PE.id_ssp, PE.cf_paz, PE.id_esame, E.tipoesame, E.descrizione, PE.timestamp,  PE.data_erog, PE.risultati, idssp AS id_ssp_erog\n" +
                                                            "FROM prescrizione_esami PE\n" +
                                                            "JOIN esami E on PE.id_esame=E.id\n" +
                                                            "LEFT JOIN medici M on PE.cf_med=M.codicefiscale\n" +
                                                            "LEFT JOIN ticket T on PE.id=T.id\n" +
                                                            "WHERE cf_paz= ? and data_erog IS NOT NULL\n" +
                                                            "ORDER BY PE.timestamp DESC")) {
            stm_in.setString(1, cf);
            try (ResultSet rs_in = stm_in.executeQuery()) {
                while(rs_in.next()) {
                    PrescrizioneEsame pe= new PrescrizioneEsame();
                    pe.setId(rs_in.getInt("id"));
                    pe.setCf_med(rs_in.getString("cf_med"));
                    pe.setNome_med(rs_in.getString("nome")+" "+rs_in.getString("cognome"));
                    pe.setId_ssp(rs_in.getInt("id_ssp"));
                    pe.setCf_paz(rs_in.getString("cf_paz"));
                    Esame e = new Esame();
                    e.setIdesame(rs_in.getInt("id_esame"));
                    e.setTipoesame(rs_in.getString("tipoesame"));
                    e.setDescrizione(rs_in.getString("descrizione"));
                    pe.setEsame(e);
                    pe.setTimestamp(rs_in.getTimestamp("timestamp"));
                    pe.setData_erog(rs_in.getTimestamp("data_erog"));
                    pe.setRisultati(rs_in.getString("risultati"));
                    pe.setId_ssp_erog(rs_in.getInt("id_ssp_erog"));
                    esamierog.add(pe);
                }
            }
        }catch (SQLException ex) {
            throw new DAOException("Impossible to get ricette for the passed cf", ex);
        }
        
        return  esamierog;
    }
    
    @Override
    public ArrayList<Ticket> getReportTicketFarmaci(Integer id) throws DAOException{
        ArrayList<Ticket> rtf = new ArrayList<>();
        Integer idssp = id;
        String provincia;
        if (idssp == 900888){
            provincia = "TN";
        } else {
            provincia = "BZ";
        }
        
        try (PreparedStatement stm = CON.prepareStatement("select ticket.id, ticket.tipo, ticket.prezzo,ticket.idfarmacia, farmacie.nomefarmacia, prescrizione_farmaci.data_erog, farmaci.idfarmaco, farmaci.nomefarmaco\n" +
                                                        "from ticket join farmacie on ticket.idfarmacia = farmacie.idfarmacia join prescrizione_farmaci on prescrizione_farmaci.id = ticket.id join farmaci ON prescrizione_farmaci.idfarmaco = farmaci.idfarmaco\n" +
                                                        "where ticket.idfarmacia IN (select idfarmacia\n" +
                                                        "from farmacie where provincia = ?)")){
            stm.setString(1,provincia);
             try (ResultSet rs = stm.executeQuery()) {
                while(rs.next()){
                Ticket ticket = new Ticket();
                ticket.setId(rs.getInt("id"));
                ticket.setTipo(rs.getString("tipo"));
                ticket.setPrezzo(rs.getInt("prezzo"));
                ticket.setIdfarmacia(rs.getInt("idfarmacia"));
                ticket.setNomefarmacia(rs.getString("nomefarmacia"));
                ticket.setData_erog(rs.getTimestamp("data_erog"));
                ticket.setNomefarmaco(rs.getString("nomefarmaco"));
                ticket.setIdfarmaco(rs.getInt("idfarmaco"));
                rtf.add(ticket);
                }
                
            }
        }
        catch (SQLException ex){
            throw new DAOException("Impossible to get tickets for the passed id", ex);
        }
        
        return rtf;
    }
    
    @Override
    public ArrayList<Ticket> getReportTicketEsami(Integer id) throws DAOException{
        ArrayList<Ticket> rte = new ArrayList<>();
         Integer idssp = id;
        String provincia;
        if (idssp == 900888){
            provincia = "TN";
        } else {
            provincia = "BZ";
        }
        
        try (PreparedStatement stm = CON.prepareStatement("select ticket.id, ticket.tipo, ticket.prezzo, ticket.idssp, prescrizione_esami.data_erog, esami.tipoesame\n" +
                                                            "from ticket join prescrizione_esami on ticket.id = prescrizione_esami.id join esami on prescrizione_esami.id_esame = esami.id\n" +
                                                            "where ticket.idssp = ?")){
            stm.setInt(1,idssp);
             try (ResultSet rs = stm.executeQuery()) {
                while(rs.next()){
                Ticket ticket = new Ticket();
                ticket.setId(rs.getInt("id"));
                ticket.setTipo(rs.getString("tipo"));
                ticket.setPrezzo(rs.getInt("prezzo"));
                ticket.setIdssp(rs.getInt("idssp"));
                ticket.setData_erog(rs.getTimestamp("data_erog"));
                ticket.setNomeesame(rs.getString("tipoesame"));
                rte.add(ticket);
                }
                
            }
        }
        catch (SQLException ex){
            throw new DAOException("Impossible to get tickets for the passed id", ex);
        }
        
        return rte;
    }
    
    @Override
    public ArrayList<Ticket> getReportTicketVisiteSpec(Integer id) throws DAOException{
        ArrayList<Ticket> rtv = new ArrayList<>();
          Integer idssp = id;
        String provincia;
        if (idssp == 900888){
            provincia = "TN";
        } else {
            provincia = "BZ";
        }
        
        try (PreparedStatement stm = CON.prepareStatement("select ticket.id, ticket.tipo, ticket.prezzo, ticket.idmedspec, prescrizione_visitespec.erog_time, prescrizione_visitespec.tipovisita\n" +
                                                        "from ticket join prescrizione_visitespec on ticket.id = prescrizione_visitespec.id\n" +
                                                        "where ticket.idmedspec IN (select codicefiscale\n" +
                                                        "from medici where is_spec = TRUE and provincia = ?)")){
            stm.setString(1,provincia);
             try (ResultSet rs = stm.executeQuery()) {
                while(rs.next()){
                Ticket ticket = new Ticket();
                ticket.setId(rs.getInt("id"));
                ticket.setTipo(rs.getString("tipo"));
                ticket.setPrezzo(rs.getInt("prezzo"));
                ticket.setIdmedspec(rs.getString("idmedspec"));
                ticket.setErogtime(rs.getTimestamp("erog_time"));
                ticket.setTipovisita(rs.getString("tipovisita"));
                rtv.add(ticket);
                }
                
            }
        }
        catch (SQLException ex){
            throw new DAOException("Impossible to get tickets for the passed id", ex);
        }
        
        return rtv;
    }
    
    @Override
    public void erogaEsame (int esame_id , int ssp_id, int prezzo) throws DAOException{
        
        
        //faccio l'update della data prescrizione
        
        //creo la data
            long millis=System.currentTimeMillis();  
            Timestamp time = new Timestamp(millis); 
            ArrayList<String> risultati = new ArrayList<>();
            risultati.add("Valori nella norma");
            risultati.add("Valori alti");
            risultati.add("Valori bassi");
            
            try (PreparedStatement stm = CON.prepareStatement("UPDATE prescrizione_esami\n" +
                                                            "SET data_erog = ? , risultati = ?\n" +
                                                            "WHERE id = ?")) {
            Random randomGenerator = new Random();
            int index = randomGenerator.nextInt(risultati.size());
            stm.setTimestamp(1, time);
            stm.setString(2, risultati.get(index));
            stm.setInt(3, esame_id);
            stm.executeUpdate();
            }
            
         catch (SQLException ex) {
            throw new DAOException("Impossible to update tuple in database", ex);
        }
            
         //faccio l'insert nella tabella ticket
         try (PreparedStatement stm2 = CON.prepareStatement("INSERT INTO ticket (id, tipo, prezzo, idssp)\n" +
                                                            "VALUES (?, ? ,?, ?);")) {
            stm2.setInt(1, esame_id);
            stm2.setString(2, "ESAME");
            stm2.setInt(3, prezzo);
            stm2.setInt(4, ssp_id);
            stm2.execute();
            }
         catch (SQLException ex) {
            throw new DAOException("Impossible to insert tuple in database", ex);
        }
        
    }
    
    @Override
    public void prescriviRichiamoEsame (String min, String max, String provincia, int id_ssp, String tipoesame) throws DAOException{
        
        //prelevo l'id max
        int newid = 0;
        try (PreparedStatement stm = CON.prepareStatement("select max(id)\n" +
                                                        "from prescrizione_esami")) {
            
            try (ResultSet rs = stm.executeQuery()) {
                while(rs.next()){
                    newid = rs.getInt("max");
                    newid += 10;
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get maxid", ex);
        }
        
        //prelevo ogni paziente a cui devo prescrivere l'esame
        
        ArrayList<Paziente> listapaz = new ArrayList<>();
        
        try (PreparedStatement stm = CON.prepareStatement("select *\n" +
                                                        "from pazienti " +
                                                          "where datanascita < ? AND datanascita > ? AND provincia = ?")) {
            
            
            stm.setDate(1,Date.valueOf(max));
            stm.setDate(2,Date.valueOf(min));
            stm.setString(3,provincia);
            System.out.println("Eseguo la query");
            try (ResultSet rs_sel = stm.executeQuery()) {
                while(rs_sel.next()){
                    Paziente paz = new Paziente();
                            paz.setCodicefiscale(rs_sel.getString("codicefiscale"));
                            paz.setNome(rs_sel.getString("nome"));
                            paz.setCognome(rs_sel.getString("cognome"));
                            paz.setPassword(rs_sel.getString("password"));
                            paz.setSesso(rs_sel.getString("sesso"));
                            paz.setLuogonascita(rs_sel.getString("luogonascita"));
                            paz.setDatanascita(rs_sel.getDate("datanascita"));
                            paz.setNumerotelefono(rs_sel.getString("numerotelefono"));
                            paz.setEmail(rs_sel.getString("email"));
                            paz.setCitta(rs_sel.getString("citta"));
                            paz.setProvincia(rs_sel.getString("provincia"));
                            System.out.println("Ho aggiunto "+ paz.getCodicefiscale());
                    listapaz.add(paz);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get pazienti", ex);
        }
        
         //timestamp
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        
        //inserisco la prescrizione per ogni paziente
        for (Paziente paz : listapaz){
            
            System.out.println("Inserisco per "+ paz.getCodicefiscale());
             try (PreparedStatement ins = CON.prepareStatement("INSERT INTO prescrizione_esami (id, id_ssp, cf_paz, timestamp, id_esame)\n" +
                                                            "VALUES (?, ? ,?, ?, ?);")) {
            ins.setInt(1, newid);
            ins.setInt(2, id_ssp);
            ins.setString(3, paz.getCodicefiscale());
            ins.setTimestamp(4, timestamp);
            ins.setInt(5, Integer.parseInt(tipoesame));
            ins.execute();
            newid+= 10;
            }
         catch (SQLException ex) {
            throw new DAOException("Impossible to insert tuple in database", ex);
        }
            
        }
        
        
    }
    
    @Override
    public void prescriviRichiamoVisita(String min, String max, String provincia, int id_ssp, String tipovisita) throws DAOException{
        
        //prelevo l'id max
        int newid = 0;
        try (PreparedStatement stm = CON.prepareStatement("select max(id)\n" +
                                                        "from prescrizione_visitespec")) {
            
            try (ResultSet rs = stm.executeQuery()) {
                while(rs.next()){
                    newid = rs.getInt("max");
                    newid += 10;
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get maxid", ex);
        }
        
        //prelevo ogni paziente a cui devo prescrivere l'esame
        
        ArrayList<Paziente> listapaz = new ArrayList<>();
        
        try (PreparedStatement stm = CON.prepareStatement("select *\n" +
                                                        "from pazienti " +
                                                          "where datanascita < ? AND datanascita > ? AND provincia = ?")) {
            
            
            stm.setDate(1,Date.valueOf(max));
            stm.setDate(2,Date.valueOf(min));
            stm.setString(3,provincia);
            try (ResultSet rs_sel = stm.executeQuery()) {
                while(rs_sel.next()){
                    Paziente paz = new Paziente();
                            paz.setCodicefiscale(rs_sel.getString("codicefiscale"));
                            paz.setNome(rs_sel.getString("nome"));
                            paz.setCognome(rs_sel.getString("cognome"));
                            paz.setPassword(rs_sel.getString("password"));
                            paz.setSesso(rs_sel.getString("sesso"));
                            paz.setLuogonascita(rs_sel.getString("luogonascita"));
                            paz.setDatanascita(rs_sel.getDate("datanascita"));
                            paz.setNumerotelefono(rs_sel.getString("numerotelefono"));
                            paz.setEmail(rs_sel.getString("email"));
                            paz.setCitta(rs_sel.getString("citta"));
                            paz.setProvincia(rs_sel.getString("provincia"));
                            System.out.println(paz.getCodicefiscale());
                    listapaz.add(paz);
                }
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get pazienti", ex);
        }
        
         //timestamp
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        
        //inserisco la prescrizione per ogni paziente
        for (Paziente paz : listapaz){
            
            try (PreparedStatement ins = CON.prepareStatement("INSERT INTO prescrizione_visitespec (id, id_ssp, cf_paz, timestamp, tipovisita)\n" +
                                                            "VALUES (?, ? ,?, ?, ?);")) {
                ins.setInt(1, newid);
                ins.setInt(2, id_ssp);
                ins.setString(3, paz.getCodicefiscale());
                ins.setTimestamp(4, timestamp);
                ins.setString(5, tipovisita);
                ins.execute();
                newid+= 10;
                System.out.println("inserisco a "+paz.getCodicefiscale());
            }
         catch (SQLException ex) {
            throw new DAOException("Impossible to insert tuple in database", ex);
        }
            
        }
        
    }
    @Override
    public ServizioProvinciale getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException("id is null");
        }
        
        try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM serviziprovinciali WHERE id = ?")) {
            stm.setInt(1, primaryKey);
            
            try (ResultSet rs = stm.executeQuery()) {
                int count = 0;
                while(rs.next()) {
                    count++;
                    if (count > 1) {
                        throw new DAOException("Unique constraint violated! There are more than one user with the same id! WHY???");
                    }
                    ServizioProvinciale ssp = new ServizioProvinciale();
                    ssp.setEmail(rs.getString("email"));
                    ssp.setUserinfo(rs.getString("userinfo"));
                    ssp.setId(Integer.valueOf(rs.getString("id")));
                    ssp.setPassword(rs.getString("password"));
                    ssp.setIsPswTemporary(rs.getBoolean("flag_recuperopassword"));

                    return ssp;
                }
                return null;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the user for the passed id", ex);
        }
    }

    @Override
    public List<ServizioProvinciale> getAll() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public ServizioProvinciale update(ServizioProvinciale ssp) throws DAOException {
        if (ssp == null) {
            throw new DAOException("parameter not valid", new IllegalArgumentException("The passed user is null"));
        }

        try (PreparedStatement std = CON.prepareStatement("UPDATE serviziprovinciali SET userinfo = ?, password = ?, flag_recuperopassword = ?, email = ? WHERE id = ?")) {
            
            std.setString(1, ssp.getUserinfo());
            std.setString(2, ssp.getPassword());
            std.setBoolean(3, ssp.getIsPswTemporary());
            std.setString(4, ssp.getEmail());
            std.setInt(5, ssp.getId());
            if (std.executeUpdate() == 1) {
                return ssp;
            } else {
                throw new DAOException("Impossible to update the user");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to update the user", ex);
        }
    }

    @Override
    public void resetPsw(Integer id_ssp, String enc_psw) throws DAOException {
        try (PreparedStatement stm=CON.prepareStatement("UPDATE serviziprovinciali SET password = ?, flag_recuperopassword = ? WHERE id = ?")) {
            stm.setString(1, enc_psw);
            stm.setBoolean(2, true);
            stm.setInt(3, id_ssp);
            stm.execute();
        }catch (SQLException ex) {
            throw  new DAOException("Impossible to insert into fotopazienti", ex);
        }
    }
}
