/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.servlets.common;

import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.progettoipw.mail.Mail;
import it.unitn.disi.wp.progettoipw.persistence.dao.FarmaciaDAO;
import it.unitn.disi.wp.progettoipw.persistence.dao.MedicoDAO;
import it.unitn.disi.wp.progettoipw.persistence.dao.PazienteDAO;
import it.unitn.disi.wp.progettoipw.persistence.dao.ServizioProvincialeDAO;
import it.unitn.disi.wp.progettoipw.persistence.entities.Farmacia;
import it.unitn.disi.wp.progettoipw.persistence.entities.Medico;
import it.unitn.disi.wp.progettoipw.persistence.entities.Paziente;
import it.unitn.disi.wp.progettoipw.persistence.entities.ServizioProvinciale;
import it.unitn.disi.wp.progettoipw.randomstring.RandomString;
import it.unitn.disi.wp.progettoipw.sha256.SHA256;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Luigi
 */
public class ForgotPsw extends HttpServlet {
    private PazienteDAO pazDao;
    private MedicoDAO medDao;
    private FarmaciaDAO farmDao;
    private ServizioProvincialeDAO sspDao;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            pazDao = daoFactory.getDAO(PazienteDAO.class);
            medDao = daoFactory.getDAO(MedicoDAO.class);
            farmDao = daoFactory.getDAO(FarmaciaDAO.class);
            sspDao = daoFactory.getDAO(ServizioProvincialeDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String new_psw = RandomString.generate(10);
        String user = (String)request.getParameter("user");
        
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        Paziente paz = null;
        try {
            paz = pazDao.getByPrimaryKey(user);
        } catch (DAOException ex) {
            Logger.getLogger(ForgotPsw.class.getName()).log(Level.SEVERE, null, ex);
        }
                
        if (paz != null) {
            //Invio la mail
            String email = paz.getEmail();
            String subject = "Nuova password temporanea";
            String text = "La nuova password temporanea per "+user+" è:\n" + new_psw +"\nAl prossimo accesso ti verrà richiesto di cambiarla";
            Mail.sendMail(getServletContext(), email, subject, text);

            //Modifico la psw nel DB
            try {
                pazDao.resetPsw(user , SHA256.encrypt(new_psw));
            } catch (DAOException ex) {
                throw new ServletException("Impossible to get dao factory for user storage system", ex);
            }
            //Reindirizzo alla home
            request.getSession().setAttribute("Error", null);
            response.sendRedirect(response.encodeRedirectURL(contextPath + "login.html"));
            return;
        }
        
        Medico med = null;
        try {
            med = medDao.getByPrimaryKey(user);
        } catch (DAOException ex) {
            Logger.getLogger(ForgotPsw.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if (med != null) {
            //Invio la mail
            String email = med.getEmail();
            String subject = "Nuova password temporanea";
            String text = "La nuova password temporanea per "+user+" è:\n" + new_psw +"\nAl prossimo accesso ti verrà richiesto di cambiarla";
            Mail.sendMail(getServletContext(), email, subject, text);

            //Modifico la psw nel DB
            try {
                medDao.resetPsw(user , SHA256.encrypt(new_psw));
            } catch (DAOException ex) {
                throw new ServletException("Impossible to get dao factory for user storage system", ex);
            }
            //Reindirizzo alla home
            request.getSession().setAttribute("Error", null);
            response.sendRedirect(response.encodeRedirectURL(contextPath + "login.html"));
            return;
        }
        
        Integer user_int = -1;
        try {
            user_int = Integer.parseInt(user);
        } catch (NumberFormatException ex) {
            //Reindirizzo alla home
            //ERRORE
            request.setAttribute("Error", true);
            getServletContext().getRequestDispatcher("/login.jsp").forward(request, response);
            return;
        }
        
        Farmacia farm = null;
        try {
            farm = farmDao.getByPrimaryKey(user_int);
        } catch (DAOException ex) {
            Logger.getLogger(ForgotPsw.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if (farm != null) {
            //Invio la mail
            String email = farm.getEmail();
            String subject = "Nuova password temporanea";
            String text = "La nuova password temporanea per "+user+" è:\n" + new_psw +"\nAl prossimo accesso ti verrà richiesto di cambiarla";
            Mail.sendMail(getServletContext(), email, subject, text);

            //Modifico la psw nel DB
            try {
                farmDao.resetPassword(user_int, SHA256.encrypt(new_psw));
            } catch (DAOException ex) {
                throw new ServletException("Impossible to get dao factory for user storage system", ex);
            }
            //Reindirizzo alla home
            request.getSession().setAttribute("Error", null);
            response.sendRedirect(response.encodeRedirectURL(contextPath + "login.html"));
            return;
        }
        
        ServizioProvinciale ssp = null;
        try {
            ssp = sspDao.getByPrimaryKey(user_int);
        } catch (DAOException ex) {
            Logger.getLogger(ForgotPsw.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if (ssp != null) {
            //Invio la mail
            String email = ssp.getEmail();
            String subject = "Nuova password temporanea";
            String text = "La nuova password temporanea per "+user+" è:\n" + new_psw +"\nAl prossimo accesso ti verrà richiesto di cambiarla";
            Mail.sendMail(getServletContext(), email, subject, text);

            //Modifico la psw nel DB
            try {
                sspDao.resetPsw(user_int, SHA256.encrypt(new_psw));
            } catch (DAOException ex) {
                throw new ServletException("Impossible to get dao factory for user storage system", ex);
            }
            //Reindirizzo alla home
            request.getSession().setAttribute("Error", null);
            response.sendRedirect(response.encodeRedirectURL(contextPath + "login.html"));
            return;
        }
        
        
        //ERRORE
        request.getSession().setAttribute("Error", true);
        response.sendRedirect(response.encodeRedirectURL(contextPath + "login.jsp"));
    }
}
