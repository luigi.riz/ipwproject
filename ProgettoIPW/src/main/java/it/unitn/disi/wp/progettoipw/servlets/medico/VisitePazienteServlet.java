/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.servlets.medico;

import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.progettoipw.persistence.dao.MedicoDAO;
import it.unitn.disi.wp.progettoipw.persistence.entities.PrescrizioneVisitaSpec;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sean
 */

public class VisitePazienteServlet extends HttpServlet {
    
    private MedicoDAO medDao;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            medDao = daoFactory.getDAO(MedicoDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        String cf_rec=request.getParameter("cf");
        request.getSession().setAttribute("actual_paz_cf", cf_rec);
        ArrayList<PrescrizioneVisitaSpec> visiteerog = new ArrayList<>();
        ArrayList<PrescrizioneVisitaSpec> visitenonerog = new ArrayList<>();
        
        try{
            visiteerog = medDao.getVisiteErogate(cf_rec);
        } catch (DAOException ex){
            throw new ServletException("Impossible to retrieve visite erogate");
        }
        
        try{
            visitenonerog = medDao.getVisiteNonErogate(cf_rec);
        } catch (DAOException ex){
            throw new ServletException("Impossible to retrieve visite non erogate");
        }
        
        request.getSession().setAttribute("visiteerog", visiteerog);
        request.getSession().setAttribute("visitenonerog", visitenonerog);
        
        response.sendRedirect(response.encodeRedirectURL(contextPath + "medicospec/visitepaziente.jsp"));
    }
   
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
}
