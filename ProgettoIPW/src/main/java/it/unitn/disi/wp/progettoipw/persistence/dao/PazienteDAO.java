/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.persistence.dao;

import it.unitn.disi.wp.progettoipw.commons.persistence.dao.DAO;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.other.InfoMedico;
import it.unitn.disi.wp.progettoipw.persistence.entities.Paziente;
import java.util.List;

/**
 * The entity that describe a {@code ssp} entity.
 * 
 * @author Luigi Riz;
 * @since 2019.07.25
 */
public interface PazienteDAO extends DAO<Paziente, String>{
    /**
     * Returns the {@link Paziente paziente} with the given {@code cf} and
     * {@code password}.
     * @param cf the cf of the user to get.
     * @param password the password of the user to get.
     * @return the {@link Paziente paziente} with the given {@code cf} and
     * {@code password}..
     * @throws DAOException if an error occurred during the information
     * retrieving.
     */
    public Paziente getByCfAndPassword(String cf, String password) throws DAOException;
        
    /**
     * Update the paziente passed as parameter and returns it.
     * @param paz the user used to update the persistence system.
     * @return the updated user.
     * @throws DAOException if an error occurred during the action.
     */
    public Paziente update(Paziente paz) throws DAOException;
    
    /**
     * Ritorna i Medici di base disponibili, per un eventuale cambio medico
     * @param paz il paziente per cui si richiede la lista
     * @return la lista di medici disponibili per un dato paziente
     * @throws DAOException if an error occurred during the action.
     */
    public List<InfoMedico> getDispMed(Paziente paz) throws DAOException;
    
    /**
     * Ritorna il Paziente, dopo averne modificato il medico
     * @param paz il paziente per cui si richiede il cambio medico
     * @param cf_med il codice fiscale del nuovo medico
     * @return il Paziente aggiornato
     * @throws DAOException if an error occurred during the action.
     */
    public Paziente changeMed(Paziente paz, String cf_med) throws DAOException;
    public Paziente uploadFoto (Paziente paz, String percorsofile) throws DAOException;
    
    public void resetPsw(String cf, String enc_psw) throws DAOException;
}
