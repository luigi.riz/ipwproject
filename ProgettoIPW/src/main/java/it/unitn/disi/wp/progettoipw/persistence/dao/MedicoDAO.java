/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.persistence.dao;

import it.unitn.disi.wp.progettoipw.commons.persistence.dao.DAO;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.persistence.entities.Medico;
import it.unitn.disi.wp.progettoipw.persistence.entities.PrescrizioneVisitaSpec;
import it.unitn.disi.wp.progettoipw.persistence.entities.PrescrizioneEsame;
import java.util.ArrayList;
import java.text.ParseException;

/**
 *
 * @author Luigi
 */
public interface MedicoDAO extends DAO<Medico,String>{
    /**
     * Returns the {@link Medico medico} with the given {@code cf} and
     * {@code password}.
     * @param cf the cf of the medico to get.
     * @param password the password of the medico to get.
     * @return the {@link Medico medico} with the given {@code cf} and
     * {@code password}..
     * @throws DAOException if an error occurred during the information
     * retrieving.
     */
    public Medico getByCfAndPassword(String cf, String password) throws DAOException;
        
    /**
     * Update the medico passed as parameter and returns it.
     * @param med the medico used to update the persistence system.
     * @return the updated medico.
     * @throws DAOException if an error occurred during the action.
     */
    public Medico update(Medico med) throws DAOException;
    
    public ArrayList<PrescrizioneVisitaSpec> getVisiteNonErogate (String cf) throws DAOException;
    public ArrayList<PrescrizioneVisitaSpec> getVisiteErogate (String cf) throws DAOException;
    public void erogaVisita (int visita_id , String cf_medspec, String risultati, int prezzo) throws DAOException;
    public void prescriviFarmaco (int id_farm , String cf_med, String cf_paz) throws DAOException, ParseException;
    public void prescriviEsame (int idesame , String cf_med, String cf_paz) throws DAOException, ParseException;
    public void prescriviVisita (String tipovisita , String cf_med, String cf_paz) throws DAOException, ParseException;
    public void resetPsw(String cf, String enc_psw) throws DAOException;
}
