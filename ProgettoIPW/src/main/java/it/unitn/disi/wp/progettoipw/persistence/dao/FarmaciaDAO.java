/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.persistence.dao;

import it.unitn.disi.wp.progettoipw.commons.persistence.dao.DAO;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.persistence.entities.Farmacia;
import it.unitn.disi.wp.progettoipw.persistence.entities.PrescrizioneFarmaco;
import java.util.ArrayList;

/**
 *
 * @author Luigi
 */
public interface FarmaciaDAO extends DAO<Farmacia,Integer> {
    /**
     * Returns the {@link Farmacia farmacia} with the given {@code cf} and
     * {@code password}.
     * @param id the code of the farmacia to get.
     * @param password the password of the farmacia to get.
     * @return the {@link Farmacia farmacia} with the given {@code code} and
     * {@code password}..
     * @throws DAOException if an error occurred during the information
     * retrieving.
     */
    public Farmacia getByIdAndPassword(Integer id, String password) throws DAOException;
        
    /**
     * Update the farmacia passed as parameter and returns it.
     * @param farmacia the farmacia used to update the persistence system.
     * @return the updated farmacia.
     * @throws DAOException if an error occurred during the action.
     */
    public Farmacia update(Farmacia farmacia) throws DAOException;
    public ArrayList<PrescrizioneFarmaco> getRicetteNonErogate (String cf) throws DAOException;
    public ArrayList<PrescrizioneFarmaco> getRicetteErogate (String cf) throws DAOException;
    public void erogaRicetta (int ric_id , int farm_id) throws DAOException;
    public void resetPassword (int farm_id, String enc_psw) throws DAOException;
}
