/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.servlets.ssp;

import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.progettoipw.persistence.dao.ServizioProvincialeDAO;
import it.unitn.disi.wp.progettoipw.persistence.entities.Esame;
import java.io.IOException;
import javax.servlet.ServletException;
import it.unitn.disi.wp.progettoipw.persistence.entities.ServizioProvinciale;
import java.util.ArrayList;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 *
 * @author Sean
 */
public class PrescriviRichiamoServlet extends HttpServlet {
    
    
    private ServizioProvincialeDAO sspDao;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            sspDao = daoFactory.getDAO(ServizioProvincialeDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        String min =request.getParameter("min");
        String max =request.getParameter("max");
        String tipoesame = request.getParameter("esame");
        int radiob = Integer.valueOf(request.getParameter("tipo"));
        ServizioProvinciale actual_ssp =(ServizioProvinciale)request.getSession().getAttribute("ssp");
        String provincia = "";
        
        if (actual_ssp.getId() == 900888){
            provincia = "TN";
        } else {
            provincia = "BZ";
        }
        
        if (radiob == 1){
            
            //faccio un richiamo visita specialistica
            //System.out.println("Inserisco nelle visitespecialistiche");
           try {
               sspDao.prescriviRichiamoVisita(min, max, provincia, actual_ssp.getId(), tipoesame);
           } catch (DAOException ex){
               throw new ServletException("Impossible to perform action");
           }
        } else {
            //faccio un richiamo esame
            //System.out.println("Inserisco negli esami");
            try {
               sspDao.prescriviRichiamoEsame(min, max, provincia, actual_ssp.getId(), tipoesame);
           } catch (DAOException ex){
               throw new ServletException("Impossible to perform action");
           }
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        String min =request.getParameter("min");
        String max =request.getParameter("max");
        String tipovisita = request.getParameter("visita");
        String tipoesame = request.getParameter("esame");
        ServizioProvinciale actual_ssp =(ServizioProvinciale)request.getSession().getAttribute("ssp");
        String provincia = "";
        
        if (actual_ssp.getId() == 900888){
            provincia = "TN";
        } else {
            provincia = "BZ";
        }
        
        if (tipoesame == null){
            
            //faccio un richiamo visita specialistica
           try {
               sspDao.prescriviRichiamoVisita(min, max, provincia, actual_ssp.getId(), tipovisita);
           } catch (DAOException ex){
               throw new ServletException("Impossible to perform action");
           }
        } else {
            //faccio un richiamo esame
            ArrayList <Esame> esami = (ArrayList<Esame>)getServletContext().getAttribute("EsamiList");
            Boolean check = false;
            Integer esame_id=-1;
            try {
                esame_id = Integer.parseInt(tipoesame); 
            }catch (NumberFormatException e){
                esame_id = -1;
            }
            
            for(Esame e: esami ) {
                if(e.getIdesame().equals(esame_id)) {
                    check = true;
                    break;
                }
            }
            if(check) {
                try {
                   sspDao.prescriviRichiamoEsame(min, max, provincia, actual_ssp.getId(), tipoesame);
                   request.getSession().setAttribute("EsameError", false);
                } catch (DAOException ex){
                   throw new ServletException("Impossible to perform action");
                }
            }else{
                request.getSession().setAttribute("EsameError", true);
            }
            
        }
        response.sendRedirect(response.encodeRedirectURL(contextPath + "ssp/welcome.jsp"));
    }
    
    
}



