/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.servlets.medico;


import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.progettoipw.mail.Mail;
import it.unitn.disi.wp.progettoipw.persistence.dao.MedicoDAO;
import it.unitn.disi.wp.progettoipw.persistence.dao.PazienteDAO;
import java.io.IOException;
import javax.servlet.ServletException;
import it.unitn.disi.wp.progettoipw.persistence.entities.Medico;
import it.unitn.disi.wp.progettoipw.persistence.entities.Paziente;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sean
 */

public class ErogaVisitaServlet extends HttpServlet {
    
     private MedicoDAO medDao;
     private PazienteDAO pazDao;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            medDao = daoFactory.getDAO(MedicoDAO.class);
            pazDao = daoFactory.getDAO(PazienteDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        String visita_id =request.getParameter("visita_id");
        String risultati =request.getParameter("risultati");
        String idssp = request.getParameter("idssp");
        Medico actual_med =(Medico)request.getSession().getAttribute("medicospec");
        int prezzo = 0;
        
        if (idssp == null || idssp.equals("")){
            //visita normale
            prezzo = 50;
        } else {
            //visita di richiamo
            prezzo = 0;
        }
        try{
            medDao.erogaVisita(Integer.parseInt(visita_id), actual_med.getCodicefiscale(), risultati, prezzo);
            
            //Aggiorno sessione new
            String paz_cf = ((Paziente)request.getSession().getAttribute("paziente")).getCodicefiscale();
            Paziente paz = pazDao.getByPrimaryKey(paz_cf);
            request.getSession().setAttribute("paziente", paz);
            
            //Aggiorno lista pazienti
            getServletContext().setAttribute("PazientiList", pazDao.getAll());
            
            //Mando la mail
            String email= paz.getEmail();
            String subject = "Nuovo referto";
            String text = "Hai un nuovo referto da leggere: \nVisita specialistica\nEffettuata da:"+actual_med.getNome()+" "+actual_med.getCognome();
            Mail.sendMail(getServletContext(), email, subject, text);
        } catch (DAOException ex){
            throw new ServletException("Impossible to retrieve data");
        }
        response.sendRedirect(response.encodeRedirectURL(contextPath + "medicospec/schedapaziente.jsp"));
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
