/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.persistence.dao.jdbc;

import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.factories.jdbc.JDBCDAOFactory;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.jdbc.JDBCDAO;
import it.unitn.disi.wp.progettoipw.other.InfoMedico;
import it.unitn.disi.wp.progettoipw.persistence.dao.PazienteDAO;
import it.unitn.disi.wp.progettoipw.persistence.entities.Esame;
import it.unitn.disi.wp.progettoipw.persistence.entities.Paziente;
import it.unitn.disi.wp.progettoipw.persistence.entities.PrescrizioneEsame;
import it.unitn.disi.wp.progettoipw.persistence.entities.PrescrizioneFarmaco;
import it.unitn.disi.wp.progettoipw.persistence.entities.PrescrizioneVisitaSpec;
import it.unitn.disi.wp.progettoipw.persistence.entities.Ticket;
import java.sql.Connection;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.Date;
import java.time.LocalDate;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Luigi
 */
public class JDBCPazienteDAO extends JDBCDAO<Paziente, String> implements PazienteDAO  {
    /**
     * The default constructor of the class.
     *
     * @param con the connection to the persistence system.
     */
    public JDBCPazienteDAO(Connection con) {
        super(con);
    }

    /**
     * Returns the number of {@link Paziente pazienti} stored on the persistence system
     * of the application.
     *
     * @return the number of records present into the storage system.
     * @throws DAOException if an error occurred during the information
     * retrieving.
     */
    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM pazienti");
            if (counter.next()) {
                return counter.getLong(1);
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to count pazienti", ex);
        }

        return 0L;
    }

    /**
     * Returns the {@link Paziente paziente} with the primary key equals to the one
     * passed as parameter.
     *
     * @param primaryKey the {@code cf} of the {@code paziente} to get.
     * @return the {@code paziente} with the cf equals to the one passed as
     * parameter or {@code null} if no entities with that cf are present into
     * the storage system.
     * @throws DAOException if an error occurred during the information
     * retrieving.
     */
    @Override
    public Paziente getByPrimaryKey(String primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException("primaryKey is null");
        }
        try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM pazienti WHERE codicefiscale = ?")) {
            stm.setString(1, primaryKey);
            try (ResultSet rs = stm.executeQuery()) {
                
                int count = 0;
                while (rs.next()) {
                    count++;
                    if (count > 1) {
                        throw new DAOException("Unique constraint violated! There are more than one user with the same email! WHY???");
                    }
                    Paziente paz = new Paziente();
                    paz.setCodicefiscale(rs.getString("codicefiscale"));
                    paz.setNome(rs.getString("nome"));
                    paz.setCognome(rs.getString("cognome"));
                    paz.setPassword(rs.getString("password"));
                    paz.setIs_psw_temporary(rs.getBoolean("flag_recuperopassword"));
                    paz.setSesso(rs.getString("sesso"));
                    paz.setLuogonascita(rs.getString("luogonascita"));
                    paz.setDatanascita(rs.getDate("datanascita"));
                    paz.setDataultimavisita(rs.getDate("dataultimavisita"));
                    paz.setNumerotelefono(rs.getString("numerotelefono"));
                    paz.setEmail(rs.getString("email"));
                    paz.setCitta(rs.getString("citta"));
                    paz.setProvincia(rs.getString("provincia"));
                    
                    try (PreparedStatement stm_in = CON.prepareStatement("SELECT * \n" +
                                                                        "FROM med_has_paz R\n" +
                                                                        "JOIN medici M ON R.cf_med = M.codicefiscale\n" +
                                                                        "WHERE cf_paz = ?")) {
                        stm_in.setString(1, paz.getCodicefiscale());
                        try (ResultSet rs_in = stm_in.executeQuery()) {
                            rs_in.next();
                            InfoMedico im=new InfoMedico();
                            im.setCf(rs_in.getString("codicefiscale"));
                            im.setCitta(rs_in.getString("citta"));
                            im.setCognome(rs_in.getString("cognome"));
                            im.setNome(rs_in.getString("nome"));
                            im.setNumerotelefono(rs_in.getString("numerotelefono"));
                            paz.setMedicodibase(im);
                        }
                    }
                    
                    try (PreparedStatement stm_in = CON.prepareStatement("SELECT PF.id, PF.cf_med, M.nome, M.cognome, PF.cf_paz, PF.idfarmaco, F.nomefarmaco, PF.data_prescr, PF.data_erog, T.idfarmacia, F1.nomefarmacia\n" +
                                                                        "FROM prescrizione_farmaci PF\n" +
                                                                        "LEFT JOIN farmaci F on PF.idfarmaco=F.idfarmaco\n" +
                                                                        "LEFT JOIN medici M on PF.cf_med=M.codicefiscale\n" +
                                                                        "LEFT JOIN ticket T on PF.id=T.id\n" +
                                                                        "LEFT JOIN farmacie F1 on T.idfarmacia=F1.idfarmacia\n" +
                                                                        "WHERE cf_paz= ? " +
                                                                        "ORDER BY PF.data_prescr DESC")) {
                        stm_in.setString(1, paz.getCodicefiscale());
                        try (ResultSet rs_in = stm_in.executeQuery()) {
                            ArrayList<PrescrizioneFarmaco> pflist = new ArrayList<>();
                            while(rs_in.next()) {
                                PrescrizioneFarmaco pf= new PrescrizioneFarmaco();
                                pf.setId(rs_in.getInt("id"));
                                pf.setCf_med(rs_in.getString("cf_med"));
                                pf.setNome_med(rs_in.getString("nome")+" "+rs_in.getString("cognome"));
                                pf.setCf_paz(rs_in.getString("cf_paz"));
                                pf.setIdfarmaco(rs_in.getInt("idfarmaco"));
                                pf.setNomefarmaco(rs_in.getString("nomefarmaco"));
                                pf.setData_prescr(rs_in.getTimestamp("data_prescr"));
                                pf.setData_erog(rs_in.getTimestamp("data_erog"));
                                pf.setIdfarmacia(rs_in.getInt("idfarmacia"));
                                pf.setNomefarmacia(rs_in.getString("nomefarmacia"));
                                pflist.add(pf);
                            }
                            paz.setRicette(pflist);
                        }
                    }
                    
                    try (PreparedStatement stm_in = CON.prepareStatement("SELECT PVS.id, PVS.cf_med, PVS.id_ssp, M1.nome AS nome_med, M1.cognome AS cognome_med, PVS.cf_paz, PVS.timestamp, PVS.tipovisita, PVS.erog_time, PVS.cf_medspec, M2.nome AS nome_medspec, M2.cognome AS cognome_medspec, PVS.risultati\n" +
                                                                        "FROM prescrizione_visitespec PVS\n" +
                                                                        "LEFT JOIN medici M1 on PVS.cf_med=M1.codicefiscale\n" +
                                                                        "LEFT JOIN medici M2 on PVS.cf_medspec=M2.codicefiscale\n" +
                                                                        "WHERE cf_paz =  ? " +
                                                                        "ORDER BY PVS.timestamp DESC")) {
                        stm_in.setString(1, paz.getCodicefiscale());
                        try (ResultSet rs_in = stm_in.executeQuery()) {
                            ArrayList<PrescrizioneVisitaSpec> pvslist = new ArrayList<>();
                            while(rs_in.next()) {
                                PrescrizioneVisitaSpec pvs= new PrescrizioneVisitaSpec();
                                pvs.setId(rs_in.getInt("id"));
                                pvs.setCf_med(rs_in.getString("cf_med"));
                                pvs.setNome_med(rs_in.getString("nome_med")+" "+rs_in.getString("cognome_med"));
                                pvs.setId_ssp(rs_in.getInt("id_ssp"));
                                pvs.setCf_paz(rs_in.getString("cf_paz"));
                                pvs.setTimestamp(rs_in.getTimestamp("timestamp"));
                                pvs.setTipovisita(rs_in.getString("tipovisita"));
                                pvs.setErog_time(rs_in.getTimestamp("erog_time"));
                                pvs.setCf_medspec(rs_in.getString("cf_medspec"));
                                String tmp = rs_in.getString("nome_medspec")== null ? "" : rs_in.getString("nome_medspec")+" "+rs_in.getString("cognome_medspec");
                                pvs.setNome_medspec(tmp);
                                pvs.setRisultati(rs_in.getString("risultati"));
                                pvslist.add(pvs);
                            }
                            paz.setVisite(pvslist);
                        }
                    }
                    
                    try (PreparedStatement stm_in = CON.prepareStatement("SELECT PE.id, PE.cf_med, M.nome, M.cognome, PE.id_ssp, PE.cf_paz, PE.id_esame, E.tipoesame, E.descrizione, PE.timestamp,  PE.data_erog, PE.risultati, idssp AS id_ssp_erog\n" +
                                                                        "FROM prescrizione_esami PE\n" +
                                                                        "JOIN esami E on PE.id_esame=E.id\n" +
                                                                        "LEFT JOIN medici M on PE.cf_med=M.codicefiscale\n" +
                                                                        "LEFT JOIN ticket T on PE.id=T.id\n" +
                                                                        "WHERE cf_paz= ?\n" +
                                                                        "ORDER BY PE.timestamp DESC")) {
                        stm_in.setString(1, paz.getCodicefiscale());
                        try (ResultSet rs_in = stm_in.executeQuery()) {
                            ArrayList<PrescrizioneEsame> pelist = new ArrayList<>();
                            while(rs_in.next()) {
                                PrescrizioneEsame pe= new PrescrizioneEsame();
                                pe.setId(rs_in.getInt("id"));
                                pe.setCf_med(rs_in.getString("cf_med"));
                                pe.setNome_med(rs_in.getString("nome")+" "+rs_in.getString("cognome"));
                                pe.setId_ssp(rs_in.getInt("id_ssp"));
                                pe.setCf_paz(rs_in.getString("cf_paz"));
                                Esame e = new Esame();
                                e.setIdesame(rs_in.getInt("id_esame"));
                                e.setTipoesame(rs_in.getString("tipoesame"));
                                e.setDescrizione(rs_in.getString("descrizione"));
                                pe.setEsame(e);
                                pe.setTimestamp(rs_in.getTimestamp("timestamp"));
                                pe.setData_erog(rs_in.getTimestamp("data_erog"));
                                pe.setRisultati(rs_in.getString("risultati"));
                                pe.setId_ssp_erog(rs_in.getInt("id_ssp_erog"));
                                pelist.add(pe);
                            }
                            paz.setEsami(pelist);
                        }
                    }
                    
                     //creo l'array con le foto pazienti
                    try (PreparedStatement stm_in = CON.prepareStatement("select url\n" +
                                                                        "from pazienti JOIN fotopazienti ON fotopazienti.cf_paz = pazienti.codicefiscale\n" +
                                                                        "where pazienti.codicefiscale = ? \n" +
                                                                        "order by url")) {
                        stm_in.setString(1, paz.getCodicefiscale());
                        try (ResultSet rs_in = stm_in.executeQuery()) {
                            String path = System.getProperty("user.dir");
                            ArrayList<String> piclist = new ArrayList<>();
                            while(rs_in.next()){
                                String url = rs_in.getString("url");
                                String urlcompleto = '.'+ "\\"+url;
                                piclist.add( urlcompleto.replace('\\','/') );
                            }
                            paz.setElencofoto(piclist);
                            paz.setPercorsofoto("fotopazienti/"+paz.getCodicefiscale()+"/");
                        }
                    }
                    
                    //popolo gli array ticketesami
                    
                    try (PreparedStatement stm8 = CON.prepareStatement("select ticket.id, ticket.tipo, ticket.prezzo, ticket.idssp, prescrizione_esami.data_erog, esami.tipoesame, prescrizione_esami.cf_paz\n" +
                                                                        "from ticket join prescrizione_esami on ticket.id = prescrizione_esami.id join esami on prescrizione_esami.id_esame = esami.id\n" +
                                                                        "where prescrizione_esami.cf_paz = ?")){
                        stm8.setString(1,paz.getCodicefiscale());
                         try (ResultSet rs8 = stm8.executeQuery()) {
                            ArrayList<Ticket> ticketesami = new ArrayList<>();
                            while(rs8.next()){
                            Ticket ticket = new Ticket();
                            ticket.setId(rs8.getInt("id"));
                            ticket.setTipo(rs8.getString("tipo"));
                            ticket.setPrezzo(rs8.getInt("prezzo"));
                            ticket.setIdssp(rs8.getInt("idssp"));
                            ticket.setData_erog(rs8.getTimestamp("data_erog"));
                            ticket.setNomeesame(rs8.getString("tipoesame"));
                            ticketesami.add(ticket);
                            }
                        paz.setTicketesami(ticketesami);
                    }
                }
                    
                    //popolo array ticket visitespec
                    try (PreparedStatement stm9 = CON.prepareStatement("select ticket.id, ticket.tipo, ticket.prezzo, ticket.idmedspec, prescrizione_visitespec.erog_time, prescrizione_visitespec.tipovisita, prescrizione_visitespec.cf_paz\n" +
                                                                    "from ticket join prescrizione_visitespec on ticket.id = prescrizione_visitespec.id \n" +
                                                                    "where prescrizione_visitespec.cf_paz = ?")){
                        stm9.setString(1,paz.getCodicefiscale());
                         try (ResultSet rs9 = stm9.executeQuery()) {
                            ArrayList<Ticket> ticketvisite = new ArrayList<>();
                            while(rs9.next()){
                            Ticket ticket = new Ticket();
                            ticket.setId(rs9.getInt("id"));
                            ticket.setTipo(rs9.getString("tipo"));
                            ticket.setPrezzo(rs9.getInt("prezzo"));
                            ticket.setIdmedspec(rs9.getString("idmedspec"));
                            ticket.setErogtime(rs9.getTimestamp("erog_time"));
                            ticket.setTipovisita(rs9.getString("tipovisita"));
                            ticketvisite.add(ticket);
                            }
                        paz.setTicketvisite(ticketvisite);
                    }
                }
                    
                    
                    //popolo array ticketfarmaci
                    try (PreparedStatement stm10 = CON.prepareStatement("select ticket.id, ticket.tipo, ticket.prezzo,ticket.idfarmacia, farmacie.nomefarmacia, prescrizione_farmaci.data_erog, farmaci.idfarmaco, farmaci.nomefarmaco, prescrizione_farmaci.cf_paz\n" +
                                                                        "from ticket join farmacie on ticket.idfarmacia = farmacie.idfarmacia join prescrizione_farmaci on prescrizione_farmaci.id = ticket.id join farmaci ON prescrizione_farmaci.idfarmaco = farmaci.idfarmaco\n" +
                                                                        "where prescrizione_farmaci.cf_paz = ?")){
                        stm10.setString(1,paz.getCodicefiscale());
                         try (ResultSet rs10 = stm10.executeQuery()) {
                            ArrayList<Ticket> ticketfarmaci = new ArrayList<>();
                            while(rs10.next()){
                            Ticket ticket = new Ticket();
                            ticket.setId(rs10.getInt("id"));
                            ticket.setTipo(rs10.getString("tipo"));
                            ticket.setPrezzo(rs10.getInt("prezzo"));
                            ticket.setIdfarmacia(rs10.getInt("idfarmacia"));
                            ticket.setNomefarmacia(rs10.getString("nomefarmacia"));
                            ticket.setData_erog(rs10.getTimestamp("data_erog"));
                            ticket.setNomefarmaco(rs10.getString("nomefarmaco"));
                            ticket.setIdfarmaco(rs10.getInt("idfarmaco"));
                            ticketfarmaci.add(ticket);
                            }
                        paz.setTicketfarmaci(ticketfarmaci);
                    }
                }
                    
                    
                    return paz;
                }

                return null;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the user for the passed primary key", ex);
        }
    }

    /**
     * Returns the {@link Paziente paz} with the given {@code cf} and
     * {@code password}.
     *
     * @param cf the codice fiscale of the paziente to get.
     * @param password the password of the paziente to get.
     * @return the {@link Paziente paz} with the given {@code cf} and
     * {@code password}..
     * @throws DAOException if an error occurred during the information
     * retrieving.
     */
    @Override
    public Paziente getByCfAndPassword(String cf, String password) throws DAOException {
        if ((cf == null) || (password == null)) {
            throw new DAOException("cf and password are mandatory fields", new NullPointerException("cf or password are null"));
        }

        try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM pazienti WHERE codicefiscale = ? AND password = ?")) {
            stm.setString(1, cf);
            stm.setString(2, password);
            try (ResultSet rs = stm.executeQuery()) {
                
                int count = 0;
                while (rs.next()) {
                    count++;
                    if (count > 1) {
                        throw new DAOException("Unique constraint violated! There are more than one user with the same email! WHY???");
                    }
                    Paziente paz = new Paziente();
                    paz.setCodicefiscale(rs.getString("codicefiscale"));
                    paz.setNome(rs.getString("nome"));
                    paz.setCognome(rs.getString("cognome"));
                    paz.setPassword(rs.getString("password"));
                    paz.setIs_psw_temporary(rs.getBoolean("flag_recuperopassword"));
                    paz.setSesso(rs.getString("sesso"));
                    paz.setLuogonascita(rs.getString("luogonascita"));
                    paz.setDatanascita(rs.getDate("datanascita"));
                    paz.setDataultimavisita(rs.getDate("dataultimavisita"));
                    paz.setNumerotelefono(rs.getString("numerotelefono"));
                    paz.setEmail(rs.getString("email"));
                    paz.setCitta(rs.getString("citta"));
                    paz.setProvincia(rs.getString("provincia"));
                    
                    try (PreparedStatement stm_in = CON.prepareStatement("SELECT * \n" +
                                                                        "FROM med_has_paz R\n" +
                                                                        "JOIN medici M ON R.cf_med = M.codicefiscale\n" +
                                                                        "WHERE cf_paz = ?")) {
                        stm_in.setString(1, paz.getCodicefiscale());
                        try (ResultSet rs_in = stm_in.executeQuery()) {
                            rs_in.next();
                            InfoMedico im=new InfoMedico();
                            im.setCf(rs_in.getString("codicefiscale"));
                            im.setCitta(rs_in.getString("citta"));
                            im.setCognome(rs_in.getString("cognome"));
                            im.setNome(rs_in.getString("nome"));
                            im.setNumerotelefono(rs_in.getString("numerotelefono"));
                            paz.setMedicodibase(im);
                        }
                    }
                    
                    try (PreparedStatement stm_in = CON.prepareStatement("SELECT PF.id, PF.cf_med, M.nome, M.cognome, PF.cf_paz, PF.idfarmaco, F.nomefarmaco, PF.data_prescr, PF.data_erog, T.idfarmacia, F1.nomefarmacia\n" +
                                                                        "FROM prescrizione_farmaci PF\n" +
                                                                        "LEFT JOIN farmaci F on PF.idfarmaco=F.idfarmaco\n" +
                                                                        "LEFT JOIN medici M on PF.cf_med=M.codicefiscale\n" +
                                                                        "LEFT JOIN ticket T on PF.id=T.id\n" +
                                                                        "LEFT JOIN farmacie F1 on T.idfarmacia=F1.idfarmacia\n" +
                                                                        "WHERE cf_paz= ? " +
                                                                        "ORDER BY PF.data_prescr DESC")) {
                        stm_in.setString(1, paz.getCodicefiscale());
                        try (ResultSet rs_in = stm_in.executeQuery()) {
                            ArrayList<PrescrizioneFarmaco> pflist = new ArrayList<>();
                            while(rs_in.next()) {
                                PrescrizioneFarmaco pf= new PrescrizioneFarmaco();
                                pf.setId(rs_in.getInt("id"));
                                pf.setCf_med(rs_in.getString("cf_med"));
                                pf.setNome_med(rs_in.getString("nome")+" "+rs_in.getString("cognome"));
                                pf.setCf_paz(rs_in.getString("cf_paz"));
                                pf.setIdfarmaco(rs_in.getInt("idfarmaco"));
                                pf.setNomefarmaco(rs_in.getString("nomefarmaco"));
                                pf.setData_prescr(rs_in.getTimestamp("data_prescr"));
                                pf.setData_erog(rs_in.getTimestamp("data_erog"));
                                pf.setIdfarmacia(rs_in.getInt("idfarmacia"));
                                pf.setNomefarmacia(rs_in.getString("nomefarmacia"));
                                pflist.add(pf);
                            }
                            paz.setRicette(pflist);
                        }
                    }
                    
                    try (PreparedStatement stm_in = CON.prepareStatement("SELECT PVS.id, PVS.cf_med, PVS.id_ssp, M1.nome AS nome_med, M1.cognome AS cognome_med, PVS.cf_paz, PVS.timestamp, PVS.tipovisita, PVS.erog_time, PVS.cf_medspec, M2.nome AS nome_medspec, M2.cognome AS cognome_medspec, PVS.risultati\n" +
                                                                        "FROM prescrizione_visitespec PVS\n" +
                                                                        "LEFT JOIN medici M1 on PVS.cf_med=M1.codicefiscale\n" +
                                                                        "LEFT JOIN medici M2 on PVS.cf_medspec=M2.codicefiscale\n" +
                                                                        "WHERE cf_paz =  ? " +
                                                                        "ORDER BY PVS.timestamp DESC")) {
                        stm_in.setString(1, paz.getCodicefiscale());
                        try (ResultSet rs_in = stm_in.executeQuery()) {
                            ArrayList<PrescrizioneVisitaSpec> pvslist = new ArrayList<>();
                            while(rs_in.next()) {
                                PrescrizioneVisitaSpec pvs= new PrescrizioneVisitaSpec();
                                pvs.setId(rs_in.getInt("id"));
                                pvs.setCf_med(rs_in.getString("cf_med"));
                                pvs.setNome_med(rs_in.getString("nome_med")+" "+rs_in.getString("cognome_med"));
                                pvs.setId_ssp(rs_in.getInt("id_ssp"));
                                pvs.setCf_paz(rs_in.getString("cf_paz"));
                                pvs.setTimestamp(rs_in.getTimestamp("timestamp"));
                                pvs.setTipovisita(rs_in.getString("tipovisita"));
                                pvs.setErog_time(rs_in.getTimestamp("erog_time"));
                                pvs.setCf_medspec(rs_in.getString("cf_medspec"));
                                String tmp = rs_in.getString("nome_medspec")== null ? "" : rs_in.getString("nome_medspec")+" "+rs_in.getString("cognome_medspec");
                                pvs.setNome_medspec(tmp);
                                pvs.setRisultati(rs_in.getString("risultati"));
                                pvslist.add(pvs);
                            }
                            paz.setVisite(pvslist);
                        }
                    }
                    
                    try (PreparedStatement stm_in = CON.prepareStatement("SELECT PE.id, PE.cf_med, M.nome, M.cognome, PE.id_ssp, PE.cf_paz, PE.id_esame, E.tipoesame, E.descrizione, PE.timestamp,  PE.data_erog, PE.risultati, idssp AS id_ssp_erog\n" +
                                                                        "FROM prescrizione_esami PE\n" +
                                                                        "JOIN esami E on PE.id_esame=E.id\n" +
                                                                        "LEFT JOIN medici M on PE.cf_med=M.codicefiscale\n" +
                                                                        "LEFT JOIN ticket T on PE.id=T.id\n" +
                                                                        "WHERE cf_paz= ?\n" +
                                                                        "ORDER BY PE.timestamp DESC")) {
                        stm_in.setString(1, paz.getCodicefiscale());
                        try (ResultSet rs_in = stm_in.executeQuery()) {
                            ArrayList<PrescrizioneEsame> pelist = new ArrayList<>();
                            while(rs_in.next()) {
                                PrescrizioneEsame pe= new PrescrizioneEsame();
                                pe.setId(rs_in.getInt("id"));
                                pe.setCf_med(rs_in.getString("cf_med"));
                                pe.setNome_med(rs_in.getString("nome")+" "+rs_in.getString("cognome"));
                                pe.setId_ssp(rs_in.getInt("id_ssp"));
                                pe.setCf_paz(rs_in.getString("cf_paz"));
                                Esame e = new Esame();
                                e.setIdesame(rs_in.getInt("id_esame"));
                                e.setTipoesame(rs_in.getString("tipoesame"));
                                e.setDescrizione(rs_in.getString("descrizione"));
                                pe.setEsame(e);
                                pe.setTimestamp(rs_in.getTimestamp("timestamp"));
                                pe.setData_erog(rs_in.getTimestamp("data_erog"));
                                pe.setRisultati(rs_in.getString("risultati"));
                                pe.setId_ssp_erog(rs_in.getInt("id_ssp_erog"));
                                pelist.add(pe);
                            }
                            paz.setEsami(pelist);
                        }
                    }
                    
                     //creo l'array con le foto pazienti
                    try (PreparedStatement stm_in = CON.prepareStatement("select url\n" +
                                                                        "from pazienti JOIN fotopazienti ON fotopazienti.cf_paz = pazienti.codicefiscale\n" +
                                                                        "where pazienti.codicefiscale = ? \n" +
                                                                        "order by url")) {
                        stm_in.setString(1, paz.getCodicefiscale());
                        try (ResultSet rs_in = stm_in.executeQuery()) {
                            String path = System.getProperty("user.dir");
                            ArrayList<String> piclist = new ArrayList<>();
                            while(rs_in.next()){
                                String url = rs_in.getString("url");
                                String urlcompleto = '.'+ "\\"+url;
                                piclist.add( urlcompleto.replace('\\','/') );
                            }
                            paz.setElencofoto(piclist);
                            paz.setPercorsofoto("fotopazienti/"+paz.getCodicefiscale()+"/");
                        }
                    }
                    
                    //popolo gli array ticketesami
                    
                    try (PreparedStatement stm8 = CON.prepareStatement("select ticket.id, ticket.tipo, ticket.prezzo, ticket.idssp, prescrizione_esami.data_erog, esami.tipoesame, prescrizione_esami.cf_paz\n" +
                                                                        "from ticket join prescrizione_esami on ticket.id = prescrizione_esami.id join esami on prescrizione_esami.id_esame = esami.id\n" +
                                                                        "where prescrizione_esami.cf_paz = ?")){
                        stm8.setString(1,paz.getCodicefiscale());
                         try (ResultSet rs8 = stm8.executeQuery()) {
                            ArrayList<Ticket> ticketesami = new ArrayList<>();
                            while(rs8.next()){
                            Ticket ticket = new Ticket();
                            ticket.setId(rs8.getInt("id"));
                            ticket.setTipo(rs8.getString("tipo"));
                            ticket.setPrezzo(rs8.getInt("prezzo"));
                            ticket.setIdssp(rs8.getInt("idssp"));
                            ticket.setData_erog(rs8.getTimestamp("data_erog"));
                            ticket.setNomeesame(rs8.getString("tipoesame"));
                            ticketesami.add(ticket);
                            }
                        paz.setTicketesami(ticketesami);
                    }
                }
                    
                    //popolo array ticket visitespec
                    try (PreparedStatement stm9 = CON.prepareStatement("select ticket.id, ticket.tipo, ticket.prezzo, ticket.idmedspec, prescrizione_visitespec.erog_time, prescrizione_visitespec.tipovisita, prescrizione_visitespec.cf_paz\n" +
                                                                    "from ticket join prescrizione_visitespec on ticket.id = prescrizione_visitespec.id \n" +
                                                                    "where prescrizione_visitespec.cf_paz = ?")){
                        stm9.setString(1,paz.getCodicefiscale());
                         try (ResultSet rs9 = stm9.executeQuery()) {
                            ArrayList<Ticket> ticketvisite = new ArrayList<>();
                            while(rs9.next()){
                            Ticket ticket = new Ticket();
                            ticket.setId(rs9.getInt("id"));
                            ticket.setTipo(rs9.getString("tipo"));
                            ticket.setPrezzo(rs9.getInt("prezzo"));
                            ticket.setIdmedspec(rs9.getString("idmedspec"));
                            ticket.setErogtime(rs9.getTimestamp("erog_time"));
                            ticket.setTipovisita(rs9.getString("tipovisita"));
                            ticketvisite.add(ticket);
                            }
                        paz.setTicketvisite(ticketvisite);
                    }
                }
                    
                    
                    //popolo array ticketfarmaci
                    try (PreparedStatement stm10 = CON.prepareStatement("select ticket.id, ticket.tipo, ticket.prezzo,ticket.idfarmacia, farmacie.nomefarmacia, prescrizione_farmaci.data_erog, farmaci.idfarmaco, farmaci.nomefarmaco, prescrizione_farmaci.cf_paz\n" +
                                                                        "from ticket join farmacie on ticket.idfarmacia = farmacie.idfarmacia join prescrizione_farmaci on prescrizione_farmaci.id = ticket.id join farmaci ON prescrizione_farmaci.idfarmaco = farmaci.idfarmaco\n" +
                                                                        "where prescrizione_farmaci.cf_paz = ?")){
                        stm10.setString(1,paz.getCodicefiscale());
                         try (ResultSet rs10 = stm10.executeQuery()) {
                            ArrayList<Ticket> ticketfarmaci = new ArrayList<>();
                            while(rs10.next()){
                            Ticket ticket = new Ticket();
                            ticket.setId(rs10.getInt("id"));
                            ticket.setTipo(rs10.getString("tipo"));
                            ticket.setPrezzo(rs10.getInt("prezzo"));
                            ticket.setIdfarmacia(rs10.getInt("idfarmacia"));
                            ticket.setNomefarmacia(rs10.getString("nomefarmacia"));
                            ticket.setData_erog(rs10.getTimestamp("data_erog"));
                            ticket.setNomefarmaco(rs10.getString("nomefarmaco"));
                            ticket.setIdfarmaco(rs10.getInt("idfarmaco"));
                            ticketfarmaci.add(ticket);
                            }
                        paz.setTicketfarmaci(ticketfarmaci);
                    }
                }
                    
                    return paz;
                }

                return null;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of pazienti", ex);
        }
    }

    /**
     * Returns the list of all the valid {@link User users} stored by the
     * storage system.
     *
     * @return the list of all the valid {@code users}.
     * @throws DAOException if an error occurred during the information
     * retrieving.
     */
    @Override
    public List<Paziente> getAll() throws DAOException {
        List<Paziente> pazienti = new ArrayList<>();

        try (Statement stm = CON.createStatement()) {
            try (ResultSet rs = stm.executeQuery("SELECT codicefiscale FROM pazienti ORDER BY cognome")) {
                DAOFactory daoFactory = JDBCDAOFactory.getInstance();
                PazienteDAO pazDAO= daoFactory.getDAO(PazienteDAO.class);
                while (rs.next()) {
                    pazienti.add(pazDAO.getByPrimaryKey(rs.getString("codicefiscale")));
                }
            } catch (DAOFactoryException ex) {
                Logger.getLogger(JDBCPazienteDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the list of users", ex);
        }

        return pazienti;
    }
    
    /**
     * Update the user passed as parameter and returns it.
     *
     * @param paz the paz used to update the persistence system.
     * @return the updated paz.
     * @throws DAOException if an error occurred during the action.
     */
    @Override
    public Paziente update(Paziente paz) throws DAOException {
        if (paz == null) {
            throw new DAOException("parameter not valid", new IllegalArgumentException("The passed user is null"));
        }

        try (PreparedStatement std = CON.prepareStatement("UPDATE pazienti SET nome = ?, cognome = ?, password = ?, flag_recuperopassword = ?, sesso = ?, luogonascita = ?, datanascita = ?, dataultimavisita = ?, numerotelefono = ?, email = ?, citta = ?, provincia = ? WHERE codicefiscale = ?")) {
            
            std.setString(1, paz.getNome());
            std.setString(2, paz.getCognome());
            std.setString(3, paz.getPassword());
            std.setBoolean(4, paz.getIs_psw_temporary());
            std.setString(5, paz.getSesso());
            std.setString(6, paz.getLuogonascita());
            std.setDate(7, paz.getDatanascita());
            std.setDate(8, paz.getDataultimavisita());
            std.setString(9, paz.getNumerotelefono());
            std.setString(10, paz.getEmail());
            std.setString(11, paz.getCitta());
            std.setString(12, paz.getProvincia());
            std.setString(13, paz.getCodicefiscale());
            if (std.executeUpdate() == 1) {
                return paz;
            } else {
                throw new DAOException("Impossible to update the user");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to update the user", ex);
        }
    }    

    @Override
    public List<InfoMedico> getDispMed(Paziente paz) throws DAOException {
        try (PreparedStatement stm=CON.prepareStatement("SELECT * FROM Medici WHERE is_spec = FALSE AND provincia=?")) {
            stm.setString(1, paz.getProvincia());
            ResultSet rs = stm.executeQuery();
            ArrayList<InfoMedico> toRet = new ArrayList();
            while(rs.next()) {
                InfoMedico med = new InfoMedico();
                med.setNome(rs.getString("nome"));
                med.setCognome(rs.getString("cognome"));
                med.setCitta(rs.getString("citta"));
                med.setNumerotelefono(rs.getString("numerotelefono"));
                med.setCf(rs.getString("codicefiscale"));
                toRet.add(med);
            }
            return toRet;
        }catch (SQLException ex) {
            throw  new DAOException("Impossible to get the medici di base", ex);
        }
    }

    @Override
    public Paziente changeMed(Paziente paz, String cf_med) throws DAOException {
        Paziente to_ret = paz;
        
        try (PreparedStatement stm=CON.prepareStatement("UPDATE med_has_paz SET cf_med = ? WHERE cf_paz = ?")) {
            stm.setString(1, cf_med);
            stm.setString(2, paz.getCodicefiscale());
            stm.executeUpdate();
        }catch (SQLException ex) {
            throw  new DAOException("Impossible to update med_has_paz", ex);
        }
        
        try (PreparedStatement stm=CON.prepareStatement("SELECT * FROM Medici WHERE codicefiscale=?")) {
            stm.setString(1, cf_med);
            try (ResultSet rs = stm.executeQuery()) {
                rs.next();
                InfoMedico im=new InfoMedico();
                im.setCf(rs.getString("codicefiscale"));
                im.setCitta(rs.getString("citta"));
                im.setCognome(rs.getString("cognome"));
                im.setNome(rs.getString("nome"));
                im.setNumerotelefono(rs.getString("numerotelefono"));
                to_ret.setMedicodibase(im);
            }
        }catch (SQLException ex) {
            throw  new DAOException("Impossible to get Med_info", ex);
        }
        
        return to_ret;
    }
    
    @Override
    public Paziente uploadFoto (Paziente paz, String percorsofile) throws DAOException{
        
        Paziente to_ret = paz;
        
        try (PreparedStatement stm=CON.prepareStatement("INSERT INTO fotopazienti VALUES (? , ?, ?)")) {
            stm.setString(1, paz.getCodicefiscale());
            stm.setString(2, percorsofile.replace('/', '\\'));
            stm.setDate(3, Date.valueOf(LocalDate.now()));
            stm.execute();
        }catch (SQLException ex) {
            throw  new DAOException("Impossible to insert into fotopazienti", ex);
        }
        
        return to_ret;
    }

    @Override
    public void resetPsw(String cf, String enc_psw) throws DAOException {
        try (PreparedStatement stm=CON.prepareStatement("UPDATE pazienti SET password = ?, flag_recuperopassword = ? WHERE codicefiscale = ?")) {
            stm.setString(1, enc_psw);
            stm.setBoolean(2, true);
            stm.setString(3, cf);
            stm.execute();
        }catch (SQLException ex) {
            throw  new DAOException("Impossible to insert into fotopazienti", ex);
        }
    }
}
