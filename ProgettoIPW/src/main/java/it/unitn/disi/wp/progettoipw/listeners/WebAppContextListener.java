/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.progettoipw.listeners;

import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.factories.jdbc.JDBCDAOFactory;
import it.unitn.disi.wp.progettoipw.persistence.dao.EsamiListDAO;
import it.unitn.disi.wp.progettoipw.persistence.dao.FarmaciListDAO;
import it.unitn.disi.wp.progettoipw.persistence.dao.PazienteDAO;
import java.util.logging.Logger;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;


/**
 *
 * @author Luigi
 */
public class WebAppContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        String dburl = sce.getServletContext().getInitParameter("dburl");
        try {
            JDBCDAOFactory.configure(dburl);
            DAOFactory daoFactory = JDBCDAOFactory.getInstance();
            sce.getServletContext().setAttribute("daoFactory", daoFactory);
            EsamiListDAO elDAO= daoFactory.getDAO(EsamiListDAO.class);
            sce.getServletContext().setAttribute("EsamiList", elDAO.getAll());
            FarmaciListDAO flDAO = daoFactory.getDAO(FarmaciListDAO.class);
            sce.getServletContext().setAttribute("FarmaciList", flDAO.getAll());
            PazienteDAO pazDAO = daoFactory.getDAO(PazienteDAO.class);
            sce.getServletContext().setAttribute("PazientiList", pazDAO.getAll());
        } catch (DAOFactoryException | DAOException ex) {
            Logger.getLogger(getClass().getName()).severe(ex.toString());
            throw new RuntimeException(ex);
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        sce.getServletContext().setAttribute("EsamiList", null);
        sce.getServletContext().setAttribute("FarmaciList", null);
        DAOFactory daoFactory = (DAOFactory) sce.getServletContext().getAttribute("daoFactory");
        if (daoFactory != null) {
            daoFactory.shutdown();
        }
        daoFactory = null;
    }
}

