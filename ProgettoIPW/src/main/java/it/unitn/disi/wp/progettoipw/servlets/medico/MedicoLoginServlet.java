/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.servlets.medico;

import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.progettoipw.persistence.dao.MedicoDAO;
import it.unitn.disi.wp.progettoipw.persistence.entities.Medico;
import it.unitn.disi.wp.progettoipw.sha256.SHA256;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sean
 */
public class MedicoLoginServlet extends HttpServlet {
    
    private MedicoDAO medDAO;
    
     @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            medDAO = daoFactory.getDAO(MedicoDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String cf = request.getParameter("primary_key");
        String password = request.getParameter("password");
        request.getSession().setAttribute("password", password);

        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }

        try {
            Medico med = medDAO.getByCfAndPassword(cf, SHA256.encrypt(password));
            
            if (med == null) {
                response.sendRedirect(response.encodeRedirectURL(contextPath + "login.html"));
            } else {
                if(!med.getIsPswTemporary()) {
                    request.getSession().setAttribute("mustChangePsw", false);
                    request.getSession().setAttribute("mustShowChangePsw", false);
                }else{
                    request.getSession().setAttribute("mustChangePsw", true);
                    request.getSession().setAttribute("mustShowChangePsw", true);
                }
                if(med.getIsSpec()) {
                    request.getSession().setAttribute("medicospec", med);
                    response.sendRedirect(response.encodeRedirectURL(contextPath+"medicospec/welcome.jsp"));
                } else {
                    request.getSession().setAttribute("medico", med);
                    response.sendRedirect(response.encodeRedirectURL(contextPath+"medico/welcome.jsp"));
                }
            }
        } catch (DAOException ex) {
            //TODO: log exception
            request.getServletContext().log("Impossible to retrieve the user", ex);
        }
    }
}