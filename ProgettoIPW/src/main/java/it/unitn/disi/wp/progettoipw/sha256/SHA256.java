/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package it.unitn.disi.wp.progettoipw.sha256;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 *
 * @author Luigi
 */
public class SHA256 {


    private static MessageDigest sha256;

    // generated password is stored encrypted (using also user name for hashing)
    public synchronized static String encrypt(String hash) {
        StringBuilder builder = new StringBuilder();
        builder.append(hash);

        // first time , encrypt user name , password and static key
        String encryptedCredentials = encryptionIterator(builder.toString());
        return encryptedCredentials;
    }

    private static String encryptionIterator(String content) {
        try {
            sha256 = MessageDigest.getInstance("SHA-256");
            // append the static key to each iteration
            byte[] passBytes = (content).getBytes();
            sha256.reset();
            byte[] digested = sha256.digest(passBytes);
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < digested.length; i++) {
                sb.append(String.format("%02x", 0xff & digested[i]));
            }

            return sb.toString();
        } catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }

        return "";
    }

    // generate password for developers
    public static void main(String[] args) {
        String hash = "password";
        String encrypt = encrypt(hash);
        System.out.println("Your Password Is '" + encrypt + "'");
    }    
}
