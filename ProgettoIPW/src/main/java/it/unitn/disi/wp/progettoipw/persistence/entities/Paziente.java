/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.persistence.entities;

import it.unitn.disi.wp.progettoipw.other.InfoMedico;
import java.util.ArrayList;
import java.sql.Date;

/**
 * The entity that describe a {@code paziente} entity.
 * 
 * @author Luigi Riz;
 * @since 2019.07.25
 */
public class Paziente {
    //Attributi contenuti nella tabella PAZIENTI
    private String nome;
    private String cognome;
    private String password;
    private Boolean is_psw_temporary;
    private String sesso;
    private String luogonascita;
    private Date datanascita;
    private Date dataultimavisita;
    private String numerotelefono;
    private String codicefiscale;
    private String email;
    private String citta;
    private String provincia;
    private String percorsofoto;
    //Info medico
    private InfoMedico medicodibase;
    //Altri attributi
    private ArrayList<PrescrizioneFarmaco> ricette;
    private ArrayList<PrescrizioneVisitaSpec> visite;
    private ArrayList<PrescrizioneEsame> esami;
    private ArrayList<String> elencofoto;
    private ArrayList<Ticket> ticketesami;
    private ArrayList<Ticket> ticketfarmaci;
    private ArrayList <Ticket> ticketvisite;

    public Paziente() {
    }

    public Paziente(String codicefiscale) {
        this.codicefiscale = codicefiscale;
    }

    public Paziente(String nome, String cognome, String password, Boolean is_psw_valid, String sesso, String luogonascita, Date datanascita, Date dataultimavisita, String numerotelefono, String codicefiscale, String email, String citta, String provincia, String percorsofoto, InfoMedico medicodibase, ArrayList<PrescrizioneFarmaco> ricette, ArrayList<PrescrizioneVisitaSpec> visite, ArrayList<PrescrizioneEsame> esami, ArrayList<String> elencofoto,  ArrayList<Ticket> ticketesami,  ArrayList<Ticket> ticketfarmaci,  ArrayList<Ticket> ticketvisite) {
        this.nome = nome;
        this.cognome = cognome;
        this.password = password;
        this.is_psw_temporary = is_psw_valid;
        this.sesso = sesso;
        this.luogonascita = luogonascita;
        this.datanascita = datanascita;
        this.dataultimavisita = dataultimavisita;
        this.numerotelefono = numerotelefono;
        this.codicefiscale = codicefiscale;
        this.email = email;
        this.citta = citta;
        this.provincia = provincia;
        this.percorsofoto = percorsofoto;
        this.medicodibase = medicodibase;
        this.ricette = ricette;
        this.visite = visite;
        this.esami = esami;
        this.elencofoto = elencofoto;
        this.ticketesami = ticketesami;
        this.ticketvisite = ticketvisite;
        this.ticketfarmaci = ticketfarmaci;
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSesso() {
        return sesso;
    }

    public void setSesso(String sesso) {
        this.sesso = sesso;
    }

    public String getLuogonascita() {
        return luogonascita;
    }

    public void setLuogonascita(String luogonascita) {
        this.luogonascita = luogonascita;
    }

    public Date getDatanascita() {
        return datanascita;
    }

    public void setDatanascita(Date datanascita) {
        this.datanascita = datanascita;
    }

    public String getNumerotelefono() {
        return numerotelefono;
    }

    public void setNumerotelefono(String numerotelefono) {
        this.numerotelefono = numerotelefono;
    }

    public String getCodicefiscale() {
        return codicefiscale;
    }

    public void setCodicefiscale(String codicefiscale) {
        this.codicefiscale = codicefiscale;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCitta() {
        return citta;
    }

    public void setCitta(String citta) {
        this.citta = citta;
    }

    public String getProvincia() {
        return provincia;
    }

    public void setProvincia(String provincia) {
        this.provincia = provincia;
    }

    public ArrayList<PrescrizioneFarmaco> getRicette() {
        return ricette;
    }

    public void setRicette(ArrayList<PrescrizioneFarmaco> ricette) {
        this.ricette=ricette;
    }

    public ArrayList<PrescrizioneVisitaSpec> getVisite() {
        return visite;
    }

    public void setVisite(ArrayList<PrescrizioneVisitaSpec> visite) {
        this.visite=visite;
    }

    public ArrayList<PrescrizioneEsame> getEsami() {
        return esami;
    }

    public void setEsami(ArrayList<PrescrizioneEsame> esami) {
        this.esami=esami;
    }
    
    public ArrayList<String> getElencofoto(){
        return elencofoto;
    }

    public void setElencofoto(ArrayList<String> elencofoto) {
        this.elencofoto = elencofoto;
    }
    
    public String getPercorsofoto(){
        return percorsofoto;
    }
    
    public void setPercorsofoto(String percorsofoto){
        this.percorsofoto = percorsofoto;
    }
    
    public InfoMedico getMedicodibase() {
        return medicodibase;
    }

    public void setMedicodibase(InfoMedico medicodibase) {
        this.medicodibase = medicodibase;
    }

    public Boolean getIs_psw_temporary() {
        return is_psw_temporary;
    }

    public void setIs_psw_temporary(Boolean is_psw_temporary) {
        this.is_psw_temporary = is_psw_temporary;
    }
    
     public ArrayList<Ticket> getTicketesami() {
        return ticketesami;
    }

    public void setTicketesami(ArrayList<Ticket> ticketesami) {
        this.ticketesami = ticketesami;
    }
    
     public ArrayList<Ticket> getTicketfarmaci() {
        return ticketfarmaci;
    }

    public void setTicketfarmaci(ArrayList<Ticket> ticketfarmaci) {
        this.ticketfarmaci = ticketfarmaci;
    }
    
     public ArrayList<Ticket> getTicketvisite() {
        return ticketvisite;
    }

    public void setTicketvisite(ArrayList<Ticket> ticketvisite) {
        this.ticketvisite = ticketvisite;
    }

    
    public Date getDataultimavisita() {
        return dataultimavisita;
    }

    public void setDataultimavisita(Date dataultimavisita) {
        this.dataultimavisita = dataultimavisita;
    }
}
