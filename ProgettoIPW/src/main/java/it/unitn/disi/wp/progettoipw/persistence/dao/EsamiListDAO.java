/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.persistence.dao;

import it.unitn.disi.wp.progettoipw.commons.persistence.dao.DAO;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.persistence.entities.Esame;
import java.util.List;

/**
 *
 * @author Luigi
 */
public interface EsamiListDAO extends DAO<Esame, Integer>{
    
    @Override
    public List<Esame> getAll() throws DAOException;
    
}
