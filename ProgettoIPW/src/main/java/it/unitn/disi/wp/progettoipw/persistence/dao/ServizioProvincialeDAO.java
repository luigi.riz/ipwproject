/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.persistence.dao;

import it.unitn.disi.wp.progettoipw.commons.persistence.dao.DAO;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.persistence.entities.ServizioProvinciale;
import it.unitn.disi.wp.progettoipw.persistence.entities.Ticket;
import it.unitn.disi.wp.progettoipw.persistence.entities.PrescrizioneEsame;
import java.util.ArrayList;
/**
 *
 * @author Luigi
 */
public interface ServizioProvincialeDAO extends DAO<ServizioProvinciale, Integer> {
    /**
     * Returns the {@link ServizioProvinciale ssp} with the given {@code id} and
     * {@code password}.
     * @param id the id of the user to get.
     * @param password the password of the user to get.
     * @return the {@link ServizioProvinciale ssp} with the given {@code id} and
     * {@code password}..
     * @throws DAOException if an error occurred during the information
     * retrieving.
     */
    public ServizioProvinciale getByIdAndPassword(Integer id, String password) throws DAOException;
        
    /**
     * Update the ssp passed as parameter and returns it.
     * @param ssp the ServizioSanitarioProvinciale used to update the persistence system.
     * @return the updated ssp.
     * @throws DAOException if an error occurred during the action.
     */
    public ServizioProvinciale update(ServizioProvinciale ssp) throws DAOException;
    public ArrayList<PrescrizioneEsame> getEsamiNonErogati (String cf) throws DAOException;
    public ArrayList<PrescrizioneEsame> getEsamiErogati (String cf) throws DAOException;
    public ArrayList<Ticket> getReportTicketFarmaci(Integer id) throws DAOException;
    public ArrayList<Ticket> getReportTicketEsami(Integer id) throws DAOException;
    public ArrayList<Ticket> getReportTicketVisiteSpec(Integer id) throws DAOException;
    public void erogaEsame (int esame_id , int ssp_id, int prezzo) throws DAOException;
    public void prescriviRichiamoEsame (String min, String max, String provincia, int id_ssp, String tipoesame) throws DAOException;
    public void prescriviRichiamoVisita(String min, String max, String provincia, int id_ssp, String tipovisita) throws DAOException;
    public void resetPsw (Integer id_ssp, String enc_psw) throws DAOException;
}
