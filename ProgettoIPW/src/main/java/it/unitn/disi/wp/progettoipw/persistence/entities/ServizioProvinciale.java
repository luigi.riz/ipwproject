/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.persistence.entities;

import java.io.Serializable;

/**
 * The entity that describe a {@code ssp} entity.
 * 
 * @author Luigi Riz;
 * @since 2019.07.25
 */
public class ServizioProvinciale implements Serializable {
    private Integer id;
    private String userinfo;
    private String password;
    private Boolean isPswTemporary;
    private String email;

    public ServizioProvinciale() {
    }

    public ServizioProvinciale(Integer id, String userinfo, String password, Boolean isPswTemporary, String email) {
        this.id = id;
        this.userinfo = userinfo;
        this.password = password;
        this.isPswTemporary = isPswTemporary;
        this.email = email;
    }    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserinfo() {
        return userinfo;
    }

    public void setUserinfo(String userinfo) {
        this.userinfo = userinfo;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Boolean getIsPswTemporary() {
        return isPswTemporary;
    }

    public void setIsPswTemporary(Boolean isPswTemporary) {
        this.isPswTemporary = isPswTemporary;
    }
}
