/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.servlets.paziente;

import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.progettoipw.persistence.dao.PazienteDAO;
import it.unitn.disi.wp.progettoipw.randomstring.RandomString;
import it.unitn.disi.wp.progettoipw.sha256.SHA256;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import it.unitn.disi.wp.progettoipw.mail.Mail;
import it.unitn.disi.wp.progettoipw.persistence.entities.Paziente;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Luigi
 */
public class PazForgotPsw extends HttpServlet {
    private PazienteDAO pazDao;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            pazDao = daoFactory.getDAO(PazienteDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String new_psw = RandomString.generate(10);
        //String new_psw = "prova";
        String cf = (String)request.getParameter("user");
        Paziente paz = null;
        try {
            paz = pazDao.getByPrimaryKey(cf);
        } catch (DAOException ex) {
            Logger.getLogger(PazForgotPsw.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        if (paz != null) {
            //Invio la mail
            String email = paz.getEmail();
            String subject = "Nuova password temporanea";
            String text = "La nuova password temporanea per "+cf+" è:\n" + new_psw +"\nAl prossimo accesso ti verrà richiesto di cambiarla";
            Mail.sendMail(getServletContext(), email, subject, text);

            //Modifico la psw nel DB
            try {
                pazDao.resetPsw(cf , SHA256.encrypt(new_psw));
            } catch (DAOException ex) {
                throw new ServletException("Impossible to get dao factory for user storage system", ex);
            }
            //Reindirizzo alla home
            request.getSession().setAttribute("CFError", false);
            response.sendRedirect(response.encodeRedirectURL(contextPath + "paziente/login.html"));
        }else{
            request.getSession().setAttribute("CFError", true);
            response.sendRedirect(response.encodeRedirectURL(contextPath + "paziente/forgotpsw.jsp"));
        }
    }
}
