/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.persistence.entities;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * The entity that describe a {@code ssp} entity.
 * 
 * @author Luigi Riz;
 * @since 2019.07.25
 */
public class PrescrizioneVisitaSpec implements Serializable{
    private Integer id;
    private String cf_med;
    private String nome_med;
    private String cf_paz;
    private Timestamp timestamp;
    private String tipovisita;
    private Timestamp erog_time;
    private String cf_medspec;
    private String nome_medspec;
    private String risultati;
    private int id_ssp;

    public PrescrizioneVisitaSpec() {
    }

    public PrescrizioneVisitaSpec(Integer id) {
        this.id = id;
    }

    public PrescrizioneVisitaSpec(Integer id, String cf_med, String nome_med, String cf_paz, Timestamp timestamp, String tipovisita, Timestamp erog_time, String cf_medspec, String nome_medspec, String risultati, int id_ssp) {
        this.id = id;
        this.cf_med = cf_med;
        this.nome_med = nome_med;
        this.cf_paz = cf_paz;
        this.timestamp = timestamp;
        this.tipovisita = tipovisita;
        this.erog_time = erog_time;
        this.cf_medspec = cf_medspec;
        this.nome_medspec = nome_medspec;
        this.risultati = risultati;
        this.id_ssp = id_ssp;
    }    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCf_med() {
        return cf_med;
    }

    public void setCf_med(String cf_med) {
        this.cf_med = cf_med;
    }

    public String getNome_med() {
        return nome_med;
    }

    public void setNome_med(String nome_med) {
        this.nome_med = nome_med;
    }

    public String getCf_paz() {
        return cf_paz;
    }

    public void setCf_paz(String cf_paz) {
        this.cf_paz = cf_paz;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public String getTipovisita() {
        return tipovisita;
    }

    public void setTipovisita(String tipovisita) {
        this.tipovisita = tipovisita;
    }

    public Timestamp getErog_time() {
        return erog_time;
    }

    public void setErog_time(Timestamp erog_time) {
        this.erog_time = erog_time;
    }

    public String getCf_medspec() {
        return cf_medspec;
    }

    public void setCf_medspec(String cf_medspec) {
        this.cf_medspec = cf_medspec;
    }

    public String getNome_medspec() {
        return nome_medspec;
    }

    public void setNome_medspec(String nome_medspec) {
        this.nome_medspec = nome_medspec;
    }

    public String getRisultati() {
        return risultati;
    }

    public void setRisultati(String risultati) {
        this.risultati = risultati;
    }
    
    public int getId_ssp() {
        return id_ssp;
    }

    public void setId_ssp(int id_ssp) {
        this.id_ssp = id_ssp;
    }
}
