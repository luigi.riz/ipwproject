/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.servlets.farmacia;

import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.progettoipw.persistence.dao.FarmaciaDAO;
import it.unitn.disi.wp.progettoipw.persistence.entities.Farmacia;
import it.unitn.disi.wp.progettoipw.sha256.SHA256;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Luigi
 */
public class FarmaciaChangePsw extends HttpServlet {
    private FarmaciaDAO farmDAO;
    
    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            farmDAO = daoFactory.getDAO(FarmaciaDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
    
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String oldPsw=request.getParameter("old_psw");
        String newPsw1=request.getParameter("new_psw_1");
        String newPsw2=request.getParameter("new_psw_2");
        Farmacia farm =  (Farmacia) request.getSession().getAttribute("farm");
        
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        if((Boolean)request.getSession().getAttribute("mustChangePsw") || farm.getPassword().equals(SHA256.encrypt(oldPsw))) {
            if(newPsw1.equals(newPsw2)) {
                farm.setPassword(SHA256.encrypt(newPsw1));
                farm.setIsPswTemporary(false);
                try {
                    request.getSession().setAttribute("farm", farmDAO.update(farm));
                    request.getSession().setAttribute("mustChangePsw", false);
                } catch (DAOException ex) {
                    request.getServletContext().log("Impossible to modify password", ex);
                }
                request.getSession().setAttribute("PSWError1", false);
                request.getSession().setAttribute("PSWError2", false);
                request.getSession().setAttribute("mustShowChangePsw", false);
                response.sendRedirect(response.encodeRedirectURL(contextPath + "farmacia/welcome.jsp"));
            }else{
                //Le due password fornite non coincidono
                //In linea teorica non si dovrebbero mai avere le due password diverse visto che le controllo con il js, però lo metto per sicurezza
                request.getSession().setAttribute("PSWError1", true);
                request.getSession().setAttribute("mustShowChangePsw", true);
                response.sendRedirect(response.encodeRedirectURL(contextPath + "farmacia/welcome.jsp"));
            }
        }else{
            //La password fornita non è quella corretta
            request.getSession().setAttribute("PSWError2", true);
            request.getSession().setAttribute("mustShowChangePsw", true);
            response.sendRedirect(response.encodeRedirectURL(contextPath + "farmacia/welcome.jsp"));
        }
    }

}
