/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.servlets.paziente;


import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.progettoipw.persistence.dao.PazienteDAO;
import be.quodlibet.boxable.BaseTable;
import be.quodlibet.boxable.Row;
import be.quodlibet.boxable.utils.PDStreamUtils;
import it.unitn.disi.wp.progettoipw.persistence.entities.Paziente;
import it.unitn.disi.wp.progettoipw.persistence.entities.Ticket;
import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

/**
 *
 * @author Sean
 */
public class ScaricaTicketServlet extends HttpServlet{
    
     private PazienteDAO pazDao;
    
    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            pazDao = daoFactory.getDAO(PazienteDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
    
        @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        String pdfFolder = getServletContext().getInitParameter("reportFolder");
        if (pdfFolder == null) {
            throw new ServletException("PDFs folder not configured");
        }
        
        pdfFolder = getServletContext().getRealPath(pdfFolder);
        Integer sspId;
        Paziente actual_paz = new Paziente();
        try {
            actual_paz =(Paziente)request.getSession().getAttribute("paziente");
        } catch (NumberFormatException | NullPointerException ex) {
            throw new ServletException("Impossible to retrieve ssp id");
        }
        
        //ticketesami
        ArrayList<Ticket> array_te = actual_paz.getTicketesami();
        
        //ticketfarmaci
        ArrayList<Ticket> array_tf = actual_paz.getTicketfarmaci();
        
        //ticketvisitespec
        ArrayList<Ticket> array_tv = actual_paz.getTicketvisite();
        
        try (PDDocument doc = new PDDocument()) {
            PDPage page = new PDPage();
            doc.addPage(page);
            
            try (PDPageContentStream contents = new PDPageContentStream(doc, page)) {
                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");  
                LocalDateTime now = LocalDateTime.now();  
            
            PDStreamUtils.write(contents, dtf.format(now), PDType1Font.HELVETICA , 9, 500, 730, Color.BLACK);
            PDStreamUtils.write( contents, "- Portale Servizio Sanitario Trentino Alto-Adige -", PDType1Font.HELVETICA_BOLD,
                        20,
                        80,
                        700,
                        Color.BLACK);
                PDStreamUtils.write(
                        contents,
                        "Ticket : visite specialistiche",
                        PDType1Font.HELVETICA_BOLD,
                        14,
                        80,
                        675,
                        Color.BLACK);
                float margin = 80;
                float yStartNewPage = page.getMediaBox().getHeight() - (2 * margin);
                float tableWidth = page.getMediaBox().getWidth() - (2 * margin);
                
                boolean drawContent = true;
                float yStart = yStartNewPage;
                float bottomMargin = 70;
                float yPosition = 660;
                
                BaseTable table = new BaseTable(yPosition, yStartNewPage, bottomMargin, tableWidth, margin, doc, page, true, drawContent);
                Row<PDPage> header = table.createRow(20);
                header.createCell(15, "ID");
                header.createCell(23, "Medico Specialista");
                header.createCell(20, "Tipo visita");
                header.createCell(20, "Data erogazione");
                header.createCell(15, "Prezzo €");
                table.addHeaderRow(header);
               
                for (Ticket ticket : array_tv){
                    String str_time = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss").format(ticket.getErogtime());
                    Row<PDPage> row = table.createRow(12);
                    row.createCell(ticket.getId().toString());
                    row.createCell(ticket.getIdmedspec());
                    row.createCell(ticket.getTipovisita());
                    row.createCell(str_time);
                    row.createCell(ticket.getPrezzo().toString());
                }
                table.draw();
                contents.close();
            //fine tabella 
             
            PDPage page2 = new PDPage();
            doc.addPage(page2);
            
            try (PDPageContentStream contents2 = new PDPageContentStream(doc, page2)) {
                
            
            PDStreamUtils.write(contents2, dtf.format(now), PDType1Font.HELVETICA , 9, 500, 730, Color.BLACK);
            PDStreamUtils.write( contents2, "- Portale Servizio Sanitario Trentino Alto-Adige -", PDType1Font.HELVETICA_BOLD,
                        20,
                        80,
                        700,
                        Color.BLACK);
                PDStreamUtils.write(
                        contents2,
                        "Ticket : ricette farmacologiche",
                       PDType1Font.HELVETICA_BOLD,
                        14,
                        80,
                        675,
                        Color.BLACK);
                float margin2 = 60;
                float yStartNewPage2 = page.getMediaBox().getHeight() - (2 * margin);
                float tableWidth2 = page.getMediaBox().getWidth() - (2 * margin);
                
                boolean drawContent2 = true;
                float yStart2 = yStartNewPage2;
                float bottomMargin2 = 70;
                float yPosition2 = 660;
                
                BaseTable table2 = new BaseTable(yPosition2, yStartNewPage2, bottomMargin2, tableWidth2, margin2, doc, page2, true, drawContent2);
                Row<PDPage> header2 = table2.createRow(20);
                header2.createCell(10, "ID");
                header2.createCell(15, "ID farmacia");
                header2.createCell(25, "Nome farmacia");
                header2.createCell(15, "ID farmaco");
                header2.createCell(20, "Nome farmaco");
                header2.createCell(17, "Data erogazione");
                header2.createCell(10, "Prezzo €");
                table2.addHeaderRow(header2);
               
                
                for (Ticket ticket2 : array_tf){
                    String str_time = new SimpleDateFormat("MM-dd-yyyy").format(ticket2.getData_erog());
                    Row<PDPage> row2 = table2.createRow(12);
                    row2.createCell(ticket2.getId().toString());
                    row2.createCell(ticket2.getIdfarmacia().toString());
                    row2.createCell(ticket2.getNomefarmacia());
                    row2.createCell(ticket2.getIdfarmaco().toString());
                    row2.createCell(ticket2.getNomefarmaco());
                    row2.createCell(str_time);
                    row2.createCell(ticket2.getPrezzo().toString());
                }
                
                table2.draw();
                contents2.close();
            }
            
            //fine tabella 2
            
            PDPage page3 = new PDPage();
            doc.addPage(page3);
            
            try (PDPageContentStream contents3 = new PDPageContentStream(doc, page3)) {
                
            PDStreamUtils.write(contents3, dtf.format(now), PDType1Font.HELVETICA , 9, 500, 730, Color.BLACK);
            PDStreamUtils.write(contents3, "- Portale Servizio Sanitario Trentino Alto-Adige -", PDType1Font.HELVETICA_BOLD,
                        20,
                        80,
                        700,
                        Color.BLACK);
                PDStreamUtils.write(
                        contents3,
                        "Ticket : esami ",
                        PDType1Font.HELVETICA_BOLD,
                        14,
                        80,
                        675,
                        Color.BLACK);
                float margin3 = 80;
                float yStartNewPage3 = page.getMediaBox().getHeight() - (2 * margin3);
                float tableWidth3 = page.getMediaBox().getWidth() - (2 * margin3);
                
                boolean drawContent3 = true;
                float yStart3 = yStartNewPage3;
                float bottomMargin3 = 70;
                float yPosition3= 660;
                
                BaseTable table3 = new BaseTable(yPosition3, yStartNewPage3, bottomMargin3, tableWidth3, margin3, doc, page3, true, drawContent3);
                Row<PDPage> header3 = table3.createRow(20);
                header3.createCell(15, "ID");
                header3.createCell(15, "SSP");
                header3.createCell(20, "Tipo esame");
                header3.createCell(20, "Data erogazione"); 
                header3.createCell(15, "Prezzo €");
                table3.addHeaderRow(header3);
               
                
                for (Ticket ticket3 : array_te){
                    String str_time = new SimpleDateFormat("MM-dd-yyyy").format(ticket3.getData_erog());
                    Row<PDPage> row3 = table3.createRow(12);
                    row3.createCell(ticket3.getId().toString());
                    row3.createCell(ticket3.getIdssp().toString());
                    row3.createCell(ticket3.getNomeesame());
                    row3.createCell(str_time);
                    row3.createCell(ticket3.getPrezzo().toString());
                }
                
                table3.draw();
                contents3.close();
            }

            String nomefile = "tickets_"+Calendar.getInstance().getTimeInMillis()+".pdf";
            doc.save(nomefile);
            response.setContentType("paziente/"+nomefile);
            response.setHeader("Content-disposition", "attachment; filename="+nomefile);
            doc.save(response.getOutputStream());
        }

        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        response.sendRedirect(response.encodeRedirectURL(contextPath + "paziente/welcome.jsp"));
    }
}

    
}
