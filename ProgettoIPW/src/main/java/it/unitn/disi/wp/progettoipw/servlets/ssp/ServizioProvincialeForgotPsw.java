/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.servlets.ssp;

import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.progettoipw.mail.Mail;
import it.unitn.disi.wp.progettoipw.persistence.dao.ServizioProvincialeDAO;
import it.unitn.disi.wp.progettoipw.persistence.entities.ServizioProvinciale;
import it.unitn.disi.wp.progettoipw.randomstring.RandomString;
import it.unitn.disi.wp.progettoipw.sha256.SHA256;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Luigi
 */
public class ServizioProvincialeForgotPsw extends HttpServlet {
    private ServizioProvincialeDAO sspDAO;
    
    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            sspDAO = daoFactory.getDAO(ServizioProvincialeDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String new_psw = RandomString.generate(10);
        Integer id_ssp = Integer.parseInt((String)request.getParameter("id_ssp"));
        
        ServizioProvinciale ssp = null;
        
        try {
            ssp = sspDAO.getByPrimaryKey(id_ssp);
        } catch (DAOException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
        
        
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        if (ssp != null) {
            //Invio la mail
            String email = ssp.getEmail();
            String subject = "Nuova password temporanea";
            String text = "La nuova password temporanea per "+id_ssp+" è:\n" + new_psw +"\nAl prossimo accesso ti verrà richiesto di cambiarla";
            Mail.sendMail(getServletContext(), email, subject, text);

            //Modifico la psw nel DB
            try {
                sspDAO.resetPsw(id_ssp , SHA256.encrypt(new_psw));
            } catch (DAOException ex) {
                throw new ServletException("Impossible to get dao factory for user storage system", ex);
            }
            
            //Reindirizzo alla home
            request.getSession().setAttribute("IDError", false);
            response.sendRedirect(response.encodeRedirectURL(contextPath + "ssp/login.html"));
        }else{
            request.getSession().setAttribute("IDError", true);
            response.sendRedirect(response.encodeRedirectURL(contextPath + "ssp/forgotpsw.jsp"));
        }
        
            
        
        
        
        
    }
}
