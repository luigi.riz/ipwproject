/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.persistence.entities;

/**
 *
 * @author Luigi
 */
public class Esame {
    private Integer idesame;
    private String tipoesame;
    private String descrizione;

    public Esame() {
    }
    
    public Esame(Integer idesame, String tipoesame, String descrizione) {
        this.idesame = idesame;
        this.tipoesame = tipoesame;
        this.descrizione = descrizione;
    }

    public Integer getIdesame() {
        return idesame;
    }

    public void setIdesame(Integer idesame) {
        this.idesame = idesame;
    }

    public String getTipoesame() {
        return tipoesame;
    }

    public void setTipoesame(String tipoesame) {
        this.tipoesame = tipoesame;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }
}
