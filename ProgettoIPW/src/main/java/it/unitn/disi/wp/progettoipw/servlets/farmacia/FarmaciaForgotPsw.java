/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.servlets.farmacia;

import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.progettoipw.mail.Mail;
import it.unitn.disi.wp.progettoipw.persistence.dao.FarmaciaDAO;
import it.unitn.disi.wp.progettoipw.persistence.entities.Farmacia;
import it.unitn.disi.wp.progettoipw.randomstring.RandomString;
import it.unitn.disi.wp.progettoipw.sha256.SHA256;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Luigi
 */
public class FarmaciaForgotPsw extends HttpServlet {
    private FarmaciaDAO farmDAO;
    
    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            farmDAO = daoFactory.getDAO(FarmaciaDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String new_psw = RandomString.generate(10);
        Integer id_farm = Integer.parseInt((String)request.getParameter("id_farm"));
        Farmacia farm = null;
        
        try {
            farm = farmDAO.getByPrimaryKey(id_farm);
        } catch (DAOException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
        
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }

        if (farm != null) {
            //Invio la mail
            String email = farm.getEmail();
            String subject = "Nuova password temporanea";
            String text = "La nuova password temporanea per "+id_farm+" è:\n" + new_psw +"\nAl prossimo accesso ti verrà richiesto di cambiarla";
            Mail.sendMail(getServletContext(), email, subject, text);

            //Modifico la psw nel DB
            try {
                farmDAO.resetPassword(id_farm, SHA256.encrypt(new_psw));
            } catch (DAOException ex) {
                throw new ServletException("Impossible to get dao factory for user storage system", ex);
            }

            //Reindirizzo alla home
            request.getSession().setAttribute("IDError", false);
            response.sendRedirect(response.encodeRedirectURL(contextPath + "farmacia/login.html"));
        }else{
            request.getSession().setAttribute("IDError", true);
            response.sendRedirect(response.encodeRedirectURL(contextPath + "farmacia/forgotpsw.jsp"));
        }
            
    }
}
