/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.persistence.dao.jdbc;

import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.jdbc.JDBCDAO;
import it.unitn.disi.wp.progettoipw.persistence.dao.FarmaciListDAO;
import it.unitn.disi.wp.progettoipw.persistence.entities.Esame;
import it.unitn.disi.wp.progettoipw.persistence.entities.Farmaco;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Luigi
 */
public class JDBCFarmaciListDAO extends JDBCDAO<Farmaco, Integer> implements FarmaciListDAO{

    /**
     * The default constructor of the class.
     *
     * @param con the connection to the persistence system.
     */
    public JDBCFarmaciListDAO(Connection con) {
        super(con);
    }
    
    @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM farmaci");
            if (counter.next()) {
                return counter.getLong(1);
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to count pazienti", ex);
        }

        return 0L;
    }

    @Override
    public Farmaco getByPrimaryKey(Integer p) throws DAOException {
        if (p == null) {
            throw new DAOException("primaryKey is null");
        }
        try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM farmaci WHERE idfarmaco= ?")) {
            stm.setInt(1, p);
            try (ResultSet rs = stm.executeQuery()) {
                Farmaco f = new Farmaco();
                f.setIdfarmaco(rs.getInt("idfarmaco"));
                f.setNomefarmaco(rs.getString("nomefarmaco"));
                return f;
            } catch (SQLException ex) {
            throw new DAOException("Impossible to get the farmaco for the passed primary key", ex);
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to count farmaci", ex);
        }
    }

    @Override
    public List<Farmaco> getAll() throws DAOException {
        try (Statement stmt = CON.createStatement()) {
            ResultSet rs = stmt.executeQuery("SELECT * FROM farmaci");
            ArrayList<Farmaco> Farmaci = new ArrayList();
            while(rs.next()) {
                Farmaco f = new Farmaco();
                f.setIdfarmaco(rs.getInt("idfarmaco"));
                f.setNomefarmaco(rs.getString("nomefarmaco"));
                Farmaci.add(f);
            }
            return Farmaci;
        }catch (SQLException ex) {
            throw new DAOException("Impossible to get farmaci", ex);
        }
    }
    
}
