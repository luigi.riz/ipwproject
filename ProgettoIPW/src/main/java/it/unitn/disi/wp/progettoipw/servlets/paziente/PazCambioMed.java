/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.servlets.paziente;

import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.progettoipw.other.InfoMedico;
import it.unitn.disi.wp.progettoipw.persistence.dao.PazienteDAO;
import it.unitn.disi.wp.progettoipw.persistence.entities.Paziente;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Luigi
 */
public class PazCambioMed extends HttpServlet {

    private PazienteDAO pazDao;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            pazDao = daoFactory.getDAO(PazienteDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
    
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
    }


    /**
     * Handles the HTTP <code>GET</code> method. 
     * Lo utilizziamo per ottenere tutti i medici disponibili per un determinato Paziente.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        ArrayList<InfoMedico> lista = new ArrayList<>();
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        try {
            lista = (ArrayList<InfoMedico>) pazDao.getDispMed((Paziente)request.getSession().getAttribute("paziente"));
            
        } catch (DAOException ex) {
            //TODO: log exception
            request.getServletContext().log("Impossible to retrieve the user", ex);
        }
        request.getSession().setAttribute("listaMedici", lista);
        request.getSession().setAttribute("showChangeMed", true);
        response.sendRedirect(response.encodeRedirectURL(contextPath + "paziente/welcome.jsp"));
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     * Lo utilizziamo per modificare il medico di base del paziente
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        String selected=request.getParameter("medico");
        if(selected!=null) {
            try {
                request.getSession().setAttribute("paziente", pazDao.changeMed((Paziente)request.getSession().getAttribute("paziente"), selected));
            } catch (DAOException ex) {
                request.getServletContext().log("Impossible to change the updated user", ex);
            }    
        }
        request.getSession().setAttribute("listaMedici", null);
        request.getSession().setAttribute("showChangeMed", null);
        response.sendRedirect(response.encodeRedirectURL(contextPath + "paziente/welcome.jsp"));
    }
}
