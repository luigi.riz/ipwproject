/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.servlets.medico;


import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.progettoipw.mail.Mail;
import it.unitn.disi.wp.progettoipw.persistence.dao.MedicoDAO;
import it.unitn.disi.wp.progettoipw.persistence.dao.PazienteDAO;
import it.unitn.disi.wp.progettoipw.persistence.entities.Farmaco;
import java.io.IOException;
import javax.servlet.ServletException;
import it.unitn.disi.wp.progettoipw.persistence.entities.Medico;
import it.unitn.disi.wp.progettoipw.persistence.entities.Paziente;
import java.util.ArrayList;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.text.ParseException;

/**
 *
 * @author Sean
 */

public class PrescrizioneFarmacoServlet extends HttpServlet {
    
    private MedicoDAO medDao;
    private PazienteDAO pazDao;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            medDao = daoFactory.getDAO(MedicoDAO.class);
            pazDao = daoFactory.getDAO(PazienteDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        String idfarm =request.getParameter("idfarm");
        Boolean idValid = false;
        for (Farmaco farm: (ArrayList<Farmaco>)request.getServletContext().getAttribute("FarmaciList")) {
            if (farm.getIdfarmaco().equals(Integer.parseInt(idfarm))) {
                idValid =true;
                break;
            }
        }
        if (idValid) {
            Paziente paz= (Paziente)request.getSession().getAttribute("paziente");
            String cf_paz =paz.getCodicefiscale();
            Medico actual_med =(Medico)request.getSession().getAttribute("medico");
            try{
                medDao.prescriviFarmaco(Integer.valueOf(idfarm), actual_med.getCodicefiscale(), cf_paz);
                //Aggiorno la sessione
                request.getSession().setAttribute("medico", medDao.getByPrimaryKey(actual_med.getCodicefiscale()));
            
                Paziente paz_tmp = pazDao.getByPrimaryKey(paz.getCodicefiscale());
                ArrayList<String> elencofoto_tmp = new ArrayList<>();
                for(String s: paz_tmp.getElencofoto()) {
                    String tmp = "../paziente"+s.substring(1);
                    elencofoto_tmp.add(tmp);
                }
                paz_tmp.setElencofoto(elencofoto_tmp);
                request.getSession().setAttribute("paziente", paz_tmp);
                //Aggiorno la lista dei pazienti
                getServletContext().setAttribute("PazientiList", pazDao.getAll());
                //Invio la mail
                String email = paz.getEmail();
                String subject = "Nuova ricetta";
                String text = "Hai una nuova ricetta da visualizzare: \nFarmaco\nEffettuata da: Dr. "+actual_med.getNome()+" "+actual_med.getCognome();
                Mail.sendMail(getServletContext(), email, subject, text);
                //Redirigo
                request.getSession().setAttribute("FarmError", false);
                response.sendRedirect(response.encodeRedirectURL(contextPath + "medico/schedapaziente.jsp"));
            } catch (DAOException ex){
                throw new ServletException("Impossible to perform action");
            } catch (ParseException e){
                if (e == null) {
                    
                }
            }
        }else{
            request.getSession().setAttribute("FarmError", true);
            response.sendRedirect(response.encodeRedirectURL(contextPath + "medico/schedapaziente.jsp"));
        }   
    }
    
    
}

