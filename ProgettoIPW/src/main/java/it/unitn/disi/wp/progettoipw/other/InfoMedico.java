/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.other;

/**
 *
 * @author Luigi
 */
public class InfoMedico{
    private String nome;
    private String cognome;
    private String cf;
    private String numerotelefono;
    private String citta;

    public InfoMedico() {
    }

    public InfoMedico(String nome, String cognome, String cf, String numerotelefono, String citta) {
        this.nome = nome;
        this.cognome = cognome;
        this.cf = cf;
        this.numerotelefono = numerotelefono;
        this.citta = citta;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCognome() {
        return cognome;
    }

    public void setCognome(String cognome) {
        this.cognome = cognome;
    }

    public String getCf() {
        return cf;
    }

    public void setCf(String cf) {
        this.cf = cf;
    }

    public String getNumerotelefono() {
        return numerotelefono;
    }

    public void setNumerotelefono(String numerotelefono) {
        this.numerotelefono = numerotelefono;
    }

    public String getCitta() {
        return citta;
    }

    public void setCitta(String citta) {
        this.citta = citta;
    }
}
