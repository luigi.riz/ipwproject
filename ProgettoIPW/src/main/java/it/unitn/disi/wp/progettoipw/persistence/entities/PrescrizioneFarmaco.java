/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.persistence.entities;

import java.sql.Timestamp;

/**
 * The entity that describe a {@code PrescrizioneFarmaco} entity.
 * 
 * @author Luigi Riz;
 * @since 2019.07.25
 */
public class PrescrizioneFarmaco {
    private Integer id;
    private String cf_med;
    private String nome_med;
    private String cf_paz;
    private Integer idfarmaco;
    private String nomefarmaco;
    private Timestamp data_prescr;
    private Timestamp data_erog;
    private Integer idfarmacia;
    private String nomefarmacia;

    public PrescrizioneFarmaco() {
    }

    public PrescrizioneFarmaco(Integer id) {
        this.id = id;
    }

    public PrescrizioneFarmaco(Integer id, String cf_med, String nome_med, String cf_paz, Integer idfarmaco, String nomefarmaco, Timestamp data_prescr, Timestamp data_erog, Integer idfarmacia, String nomefarmacia) {
        this.id = id;
        this.cf_med = cf_med;
        this.nome_med = nome_med;
        this.cf_paz = cf_paz;
        this.idfarmaco = idfarmaco;
        this.nomefarmaco = nomefarmaco;
        this.data_prescr = data_prescr;
        this.data_erog = data_erog;
        this.idfarmacia = idfarmacia;
        this.nomefarmacia = nomefarmacia;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCf_med() {
        return cf_med;
    }

    public void setCf_med(String cf_med) {
        this.cf_med = cf_med;
    }

    public String getNome_med() {
        return nome_med;
    }

    public void setNome_med(String nome_med) {
        this.nome_med = nome_med;
    }

    public String getCf_paz() {
        return cf_paz;
    }

    public void setCf_paz(String cf_paz) {
        this.cf_paz = cf_paz;
    }

    public Integer getIdfarmaco() {
        return idfarmaco;
    }

    public void setIdfarmaco(Integer idfarmaco) {
        this.idfarmaco = idfarmaco;
    }

    public String getNomefarmaco() {
        return nomefarmaco;
    }

    public void setNomefarmaco(String nomefarmaco) {
        this.nomefarmaco = nomefarmaco;
    }
    
    

    public Timestamp getData_prescr() {
        return data_prescr;
    }

    public void setData_prescr(Timestamp data_prescr) {
        this.data_prescr = data_prescr;
    }

    public Timestamp getData_erog() {
        return data_erog;
    }

    public void setData_erog(Timestamp data_erog) {
        this.data_erog = data_erog;
    }

    public Integer getIdfarmacia() {
        return idfarmacia;
    }

    public void setIdfarmacia(Integer idfarmacia) {
        this.idfarmacia = idfarmacia;
    }

    public String getNomefarmacia() {
        return nomefarmacia;
    }

    public void setNomefarmacia(String nomefarmacia) {
        this.nomefarmacia = nomefarmacia;
    }
    
    
    
}
