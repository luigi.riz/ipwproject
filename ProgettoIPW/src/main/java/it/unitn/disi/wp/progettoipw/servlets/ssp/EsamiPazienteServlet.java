/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.servlets.ssp;

import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.progettoipw.persistence.dao.ServizioProvincialeDAO;
import it.unitn.disi.wp.progettoipw.persistence.entities.PrescrizioneEsame;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Sean
 */

public class EsamiPazienteServlet extends HttpServlet {
    
    private ServizioProvincialeDAO sspDao;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            sspDao = daoFactory.getDAO(ServizioProvincialeDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        request.getSession().setAttribute("ricetteerog", null);
        request.getSession().setAttribute("ricettenonerog", null);
        request.getSession().setAttribute("actual_paz_cf", null);
        
        response.sendRedirect(response.encodeRedirectURL(contextPath + "ssp/welcome.jsp"));
    }
   
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        String cf_rec=request.getParameter("cf");
        request.getSession().setAttribute("actual_paz_cf", cf_rec);
        ArrayList<PrescrizioneEsame> esamierog = new ArrayList<>();
        ArrayList<PrescrizioneEsame> esaminonerog = new ArrayList<>();
        
        try{
            esamierog = sspDao.getEsamiErogati(cf_rec);
        } catch (DAOException ex){
            throw new ServletException("Impossible to retrieve esami erogati");
        }
        
        try{
            esaminonerog = sspDao.getEsamiNonErogati(cf_rec);
        } catch (DAOException ex){
            throw new ServletException("Impossible to retrieve esami non erogati");
        }
        
        request.getSession().setAttribute("esamierog", esamierog);
        request.getSession().setAttribute("esaminonerog", esaminonerog);
        
        response.sendRedirect(response.encodeRedirectURL(contextPath + "ssp/esamipaziente.jsp"));
    }

}
