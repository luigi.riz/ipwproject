/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.persistence.entities;

import java.sql.Timestamp;

/**
 * The entity that describe a {@code ssp} entity.
 * 
 * @author Luigi Riz;
 * @since 2019.07.25
 */
public class PrescrizioneEsame {
    private Integer id;
    private String cf_med;
    private String nome_med;
    private String cf_paz;
    private Timestamp timestamp;
    private Esame esame;
    private Timestamp data_erog;
    private String risultati;
    private Integer id_ssp;
    private Integer id_ssp_erog;

    public PrescrizioneEsame() {
    }

    public PrescrizioneEsame(Integer id) {
        this.id = id;
    }

    public PrescrizioneEsame(Integer id, String cf_med, String nome_med, String cf_paz, Timestamp timestamp, Esame esame, Timestamp data_erog, String risultati, Integer id_ssp, Integer id_ssp_erog) {
        this.id = id;
        this.cf_med = cf_med;
        this.nome_med = nome_med;
        this.cf_paz = cf_paz;
        this.timestamp = timestamp;
        this.esame = esame;
        this.data_erog = data_erog;
        this.risultati = risultati;
        this.id_ssp = id_ssp;
        this.id_ssp_erog = id_ssp_erog;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCf_med() {
        return cf_med;
    }

    public void setCf_med(String cf_med) {
        this.cf_med = cf_med;
    }

    public String getNome_med() {
        return nome_med;
    }

    public void setNome_med(String nome_med) {
        this.nome_med = nome_med;
    }

    public String getCf_paz() {
        return cf_paz;
    }

    public void setCf_paz(String cf_paz) {
        this.cf_paz = cf_paz;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    public Timestamp getData_erog() {
        return data_erog;
    }

    public void setData_erog(Timestamp data_erog) {
        this.data_erog = data_erog;
    }

    public String getRisultati() {
        return risultati;
    }

    public void setRisultati(String risultati) {
        this.risultati = risultati;
    }

     public Integer getId_ssp() {
        return id_ssp;
    }

    public void setId_ssp(Integer id_ssp) {
        this.id_ssp = id_ssp;
    }

    public Esame getEsame() {
        return esame;
    }

    public void setEsame(Esame esame) {
        this.esame = esame;
    }

    public Integer getId_ssp_erog() {
        return id_ssp_erog;
    }

    public void setId_ssp_erog(Integer id_ssp_erog) {
        this.id_ssp_erog = id_ssp_erog;
    }
}
