/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */

package it.unitn.disi.wp.progettoipw.servlets.farmacia;

import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.progettoipw.persistence.dao.FarmaciaDAO;
import it.unitn.disi.wp.progettoipw.persistence.entities.PrescrizioneFarmaco;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 *
 * @author Sean
 */

public class RicettePazienteServlet extends HttpServlet{
    
    private FarmaciaDAO farmDao;

    @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            farmDao = daoFactory.getDAO(FarmaciaDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
    
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        request.getSession().setAttribute("ricetteerog", null);
        request.getSession().setAttribute("ricettenonerog", null);
        request.getSession().setAttribute("cf_paz", null);
        
        response.sendRedirect(response.encodeRedirectURL(contextPath + "farmacia/welcome.jsp"));
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) 
            throws ServletException, IOException {
        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        
        String cf_rec=request.getParameter("cf");
        System.out.println(cf_rec);
        ArrayList<PrescrizioneFarmaco> ricetteerog = new ArrayList<>();
        ArrayList<PrescrizioneFarmaco> ricettenonerog = new ArrayList<>();
        
        try{
            ricetteerog = farmDao.getRicetteErogate(cf_rec);
        } catch (DAOException ex){
            throw new ServletException("Impossible to retrieve ricette erogate");
        }
        
        try{
            ricettenonerog = farmDao.getRicetteNonErogate(cf_rec);
        } catch (DAOException ex){
            throw new ServletException("Impossible to retrieve ricette non erogate");
        }
        
        request.getSession().setAttribute("ricetteerog", ricetteerog);
        request.getSession().setAttribute("ricettenonerog", ricettenonerog);
        request.getSession().setAttribute("cf_paz", cf_rec);
        response.sendRedirect(response.encodeRedirectURL(contextPath + "farmacia/ricettepaziente.jsp"));
    }
}
