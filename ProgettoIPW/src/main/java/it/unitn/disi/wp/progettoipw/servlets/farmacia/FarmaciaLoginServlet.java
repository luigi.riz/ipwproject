/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.servlets.farmacia;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.progettoipw.persistence.dao.FarmaciaDAO;
import it.unitn.disi.wp.progettoipw.persistence.entities.Farmacia;
import it.unitn.disi.wp.progettoipw.sha256.SHA256;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
/**
 *
 * @author Sean
 */
public class FarmaciaLoginServlet extends HttpServlet {
    
    
    private FarmaciaDAO farmDAO;
    
     @Override
    public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            farmDAO = daoFactory.getDAO(FarmaciaDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("primary_key");
        String password = request.getParameter("password");
        request.getSession().setAttribute("password", password);

        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }

        try {
            Farmacia farm = farmDAO.getByIdAndPassword(Integer.valueOf(id), SHA256.encrypt(password));
            if (farm == null) {
                response.sendRedirect(response.encodeRedirectURL(contextPath + "login.html"));
            } else {
                request.getSession().setAttribute("farm", farm);
                if(!farm.getIsPswTemporary()) {
                    request.getSession().setAttribute("mustChangePsw", false);
                    request.getSession().setAttribute("mustShowChangePsw", false);
                }else{
                    request.getSession().setAttribute("mustChangePsw", true);
                    request.getSession().setAttribute("mustShowChangePsw", true);
                }
                response.sendRedirect(response.encodeRedirectURL(contextPath + "farmacia/welcome.jsp"));
            }
        } catch (DAOException ex) {
            //TODO: log exception
            request.getServletContext().log("Impossible to retrieve the user", ex);
        }
    }
    
}
