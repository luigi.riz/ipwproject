/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.persistence.dao.jdbc;

import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.jdbc.JDBCDAO;
import it.unitn.disi.wp.progettoipw.persistence.dao.FarmaciaDAO;
import it.unitn.disi.wp.progettoipw.persistence.entities.Farmacia;
import it.unitn.disi.wp.progettoipw.persistence.entities.PrescrizioneFarmaco;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.List;
import java.util.ArrayList;
/**
 *
 * @author Sean
 */
public class JDBCFarmaciaDAO extends JDBCDAO<Farmacia, Integer> implements FarmaciaDAO {
    
    public JDBCFarmaciaDAO(Connection con) {
        super(con);
    }
    
     @Override
    public Long getCount() throws DAOException {
        try (Statement stmt = CON.createStatement()) {
            ResultSet counter = stmt.executeQuery("SELECT COUNT(*) FROM farmacie");
            if (counter.next()) {
                return counter.getLong(1);
            }

        } catch (SQLException ex) {
            throw new DAOException("Impossible to count farmacie", ex);
        }

        return 0L;
    }
    
     @Override
    public Farmacia getByIdAndPassword (Integer id, String password) throws DAOException {
        if (id == null || password == null) {
            throw new DAOException("id or password are null", new NullPointerException("id or password are null"));
        }
        
        try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM farmacie WHERE idfarmacia = ? AND password = ?")) {
            stm.setInt(1, id);
            stm.setString(2, password);
            try (ResultSet rs = stm.executeQuery()) {
                int count=0;
                while(rs.next()) {
                    count++;
                    if (count > 1) {
                        throw new DAOException("Unique constraint violated! There are more than one user with the same email! WHY???");
                    }
                    Farmacia farm = new Farmacia();
                    farm.setEmail(rs.getString("email"));
                    farm.setNomefarmacia(rs.getString("nomefarmacia"));
                    farm.setProvincia(rs.getString("provincia"));
                    farm.setIndirizzo(rs.getString("indirizzo"));
                    farm.setIdfarmacia(rs.getInt("idfarmacia"));
                    farm.setPassword(rs.getString("password"));
                    farm.setIsPswTemporary(rs.getBoolean("flag_recuperopassword"));
                    farm.setCitta(rs.getString("citta"));
                    farm.setCap(rs.getInt("cap"));

                    return farm;
                }
                return null;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the user for the passed idfarmacia", ex);
        }
    }
    
    @Override
    public ArrayList<PrescrizioneFarmaco> getRicetteErogate (String cf) throws DAOException{
        ArrayList<PrescrizioneFarmaco> ricetteerog = new ArrayList<>();
        
        try (PreparedStatement stm = CON.prepareStatement("SELECT PF.id, PF.cf_med, M.nome, M.cognome, PF.cf_paz, PF.idfarmaco, F.nomefarmaco, PF.data_prescr, PF.data_erog, T.idfarmacia, F1.nomefarmacia\n" +
                                                            "FROM prescrizione_farmaci PF\n" +
                                                            "LEFT JOIN farmaci F on PF.idfarmaco=F.idfarmaco\n" +
                                                            "LEFT JOIN medici M on PF.cf_med=M.codicefiscale\n" +
                                                            "LEFT JOIN ticket T on PF.id=T.id\n" +
                                                            "LEFT JOIN farmacie F1 on T.idfarmacia=F1.idfarmacia\n" +
                                                            "WHERE cf_paz = ? AND data_erog IS NOT NULL\n" +
                                                            "ORDER BY PF.data_prescr DESC")) {
            stm.setString(1, cf);
            try (ResultSet rs = stm.executeQuery()) {
                while(rs.next()) {
                    PrescrizioneFarmaco pf= new PrescrizioneFarmaco();
                    pf.setId(rs.getInt("id"));
                    pf.setCf_med(rs.getString("cf_med"));
                    pf.setNome_med(rs.getString("nome")+" "+rs.getString("cognome"));
                    pf.setCf_paz(rs.getString("cf_paz"));
                    pf.setIdfarmaco(rs.getInt("idfarmaco"));
                    pf.setNomefarmaco(rs.getString("nomefarmaco"));
                    pf.setData_prescr(rs.getTimestamp("data_prescr"));
                    pf.setData_erog(rs.getTimestamp("data_erog"));
                    pf.setIdfarmacia(rs.getInt("idfarmacia"));
                    pf.setNomefarmacia(rs.getString("nomefarmacia"));
                    ricetteerog.add(pf);
                }
            }
        }catch (SQLException ex) {
            throw new DAOException("Impossible to get ricette for the passed cf", ex);
        }
        
        return  ricetteerog;
    }
    
    @Override
    public ArrayList<PrescrizioneFarmaco> getRicetteNonErogate (String cf) throws DAOException{
        ArrayList<PrescrizioneFarmaco> ricettenonerog = new ArrayList<>();
        
        try (PreparedStatement stm = CON.prepareStatement("SELECT PF.id, PF.cf_med, M.nome, M.cognome, PF.cf_paz, PF.idfarmaco, F.nomefarmaco, PF.data_prescr, PF.data_erog, T.idfarmacia, F1.nomefarmacia\n" +
                                                            "FROM prescrizione_farmaci PF\n" +
                                                            "LEFT JOIN farmaci F on PF.idfarmaco=F.idfarmaco\n" +
                                                            "LEFT JOIN medici M on PF.cf_med=M.codicefiscale\n" +
                                                            "LEFT JOIN ticket T on PF.id=T.id\n" +
                                                            "LEFT JOIN farmacie F1 on T.idfarmacia=F1.idfarmacia\n" +
                                                            "WHERE cf_paz = ? AND data_erog IS NULL\n" +
                                                            "ORDER BY PF.data_prescr DESC")) {
            stm.setString(1, cf);
            try (ResultSet rs = stm.executeQuery()) {
                while(rs.next()) {
                    PrescrizioneFarmaco pf= new PrescrizioneFarmaco();
                    pf.setId(rs.getInt("id"));
                    pf.setCf_med(rs.getString("cf_med"));
                    pf.setNome_med(rs.getString("nome")+" "+rs.getString("cognome"));
                    pf.setCf_paz(rs.getString("cf_paz"));
                    pf.setIdfarmaco(rs.getInt("idfarmaco"));
                    pf.setNomefarmaco(rs.getString("nomefarmaco"));
                    pf.setData_prescr(rs.getTimestamp("data_prescr"));
                    pf.setData_erog(rs.getTimestamp("data_erog"));
                    pf.setIdfarmacia(rs.getInt("idfarmacia"));
                    pf.setNomefarmacia(rs.getString("nomefarmacia"));
                    ricettenonerog.add(pf);
                }
            }
        }catch (SQLException ex) {
            throw new DAOException("Impossible to get ricette for the passed cf", ex);
        }
        return  ricettenonerog;
    }
    
    @Override
    public void erogaRicetta (int ric_id , int farm_id) throws DAOException{
        
        //faccio l'update della data prescrizione
        
        //creo la data
            
        long millis=System.currentTimeMillis();  
        Timestamp time = new Timestamp(millis);
        try (PreparedStatement stm = CON.prepareStatement("UPDATE prescrizione_farmaci\n" +
                                                            "SET data_erog = ?\n" +
                                                            "WHERE id = ?")) {
            stm.setTimestamp(1, time);
            stm.setInt(2, ric_id);
            stm.executeUpdate();
            }
        catch (SQLException ex) {
            throw new DAOException("Impossible to update tuple in database", ex);
        }
         //faccio l'insert nella tabella ticket
         try (PreparedStatement stm2 = CON.prepareStatement("INSERT INTO ticket (id, tipo, prezzo, idfarmacia)\n" +
                                                            "VALUES (?, ? ,?, ?)")) {
            stm2.setInt(1, ric_id);
            stm2.setString(2, "FARMACO");
            stm2.setInt(3, 3);
            stm2.setInt(4, farm_id);
            stm2.execute();
            }
         catch (SQLException ex) {
            throw new DAOException("Impossible to insert tuple in database", ex);
        }
    }
    
    @Override
    public Farmacia getByPrimaryKey(Integer primaryKey) throws DAOException {
        if (primaryKey == null) {
            throw new DAOException("id is null", new NullPointerException("id is null"));
        }
        
        try (PreparedStatement stm = CON.prepareStatement("SELECT * FROM farmacie WHERE idfarmacia = ?")) {
            stm.setInt(1, primaryKey);
            try (ResultSet rs = stm.executeQuery()) {
                int count=0;
                while(rs.next()) {
                    count++;
                    if (count > 1) {
                        throw new DAOException("Unique constraint violated! There are more than one user with the same email! WHY???");
                    }
                    Farmacia farm = new Farmacia();
                    farm.setEmail(rs.getString("email"));
                    farm.setNomefarmacia(rs.getString("nomefarmacia"));
                    farm.setProvincia(rs.getString("provincia"));
                    farm.setIndirizzo(rs.getString("indirizzo"));
                    farm.setIdfarmacia(rs.getInt("idfarmacia"));
                    farm.setPassword(rs.getString("password"));
                    farm.setIsPswTemporary(rs.getBoolean("flag_recuperopassword"));
                    farm.setCitta(rs.getString("citta"));
                    farm.setCap(rs.getInt("cap"));

                    return farm;
                }
                return null;
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to get the user for the passed idfarmacia", ex);
        }
    }

    @Override
    public List<Farmacia> getAll() throws DAOException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Farmacia update(Farmacia farmacia) throws DAOException {
        if (farmacia == null) {
            throw new DAOException("parameter not valid", new IllegalArgumentException("The passed farmacia is null"));
        }

        try (PreparedStatement std = CON.prepareStatement("UPDATE farmacie SET indirizzo = ?, nomefarmacia = ?, cap = ?, citta = ?, provincia = ?, password = ?, flag_recuperopassword = ?,  email = ? WHERE idfarmacia = ?")) {
            
            std.setString(1, farmacia.getIndirizzo());
            std.setString(2, farmacia.getNomefarmacia());
            std.setInt(3, farmacia.getCap());
            std.setString(4, farmacia.getCitta());
            std.setString(5, farmacia.getProvincia());
            std.setString(6, farmacia.getPassword());
            std.setBoolean(7, farmacia.getIsPswTemporary());
            std.setString(8, farmacia.getEmail());
            std.setInt(9, farmacia.getIdfarmacia());
            if (std.executeUpdate() == 1) {
                return farmacia;
            } else {
                throw new DAOException("Impossible to update the user");
            }
        } catch (SQLException ex) {
            throw new DAOException("Impossible to update the user", ex);
        }
    }

    @Override
    public void resetPassword(int farm_id, String enc_psw) throws DAOException{
        try (PreparedStatement stm=CON.prepareStatement("UPDATE farmacie SET password = ?, flag_recuperopassword = ? WHERE idfarmacia = ?")) {
            stm.setString(1, enc_psw);
            stm.setBoolean(2, true);
            stm.setInt(3, farm_id);
            stm.execute();
        }catch (SQLException ex) {
            throw new DAOException("Impossible to insert into fotopazienti", ex);
        }
    }
}
