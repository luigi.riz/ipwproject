/*
 * AA 2018-2019
 * Introduction to Web Programming
 * Progetto IPW
 * UniTN
 */
package it.unitn.disi.wp.progettoipw.servlets.paziente;

/**
 *
 * @author Sean
 */
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.exceptions.DAOFactoryException;
import it.unitn.disi.wp.progettoipw.commons.persistence.dao.factories.DAOFactory;
import it.unitn.disi.wp.progettoipw.persistence.dao.PazienteDAO;
import be.quodlibet.boxable.BaseTable;
import be.quodlibet.boxable.Row;
import be.quodlibet.boxable.utils.PDStreamUtils;
import it.unitn.disi.wp.progettoipw.persistence.entities.Paziente;
import it.unitn.disi.wp.progettoipw.persistence.entities.PrescrizioneFarmaco;
import java.awt.Color;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import java.io.IOException;
import java.awt.image.BufferedImage;
import java.util.HashMap;
import java.util.Map;
import java.time.format.DateTimeFormatter;  
import java.time.LocalDateTime;  
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.graphics.image.JPEGFactory;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.client.j2se.MatrixToImageConfig;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;



public class ScaricaRicettaServlet extends HttpServlet{
        
        private PazienteDAO pazDAO;
        
        @Override
        public void init() throws ServletException {
        DAOFactory daoFactory = (DAOFactory) super.getServletContext().getAttribute("daoFactory");
        if (daoFactory == null) {
            throw new ServletException("Impossible to get dao factory for user storage system");
        }
        try {
            pazDAO = daoFactory.getDAO(PazienteDAO.class);
        } catch (DAOFactoryException ex) {
            throw new ServletException("Impossible to get dao factory for user storage system", ex);
        }
    }
        
        @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
       
        PrescrizioneFarmaco ricetta = new PrescrizioneFarmaco ();
        Integer id_ric = Integer.parseInt(request.getParameter("idricetta"));
        Paziente paziente = (Paziente)request.getSession().getAttribute("paziente");
        
        
        for (PrescrizioneFarmaco pf : paziente.getRicette()){
            if (id_ric.equals(pf.getId())){
                ricetta = pf;
                break;
            }
        }
        
        try (PDDocument doc = new PDDocument()) {
            PDPage page = new PDPage();
            doc.addPage(page);
            
            try (PDPageContentStream contents = new PDPageContentStream(doc, page)) {
            
            DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd-MM-yyyy");  
            LocalDateTime now = LocalDateTime.now();  
            
            PDStreamUtils.write(contents, dtf.format(now), PDType1Font.HELVETICA , 9, 500, 730, Color.BLACK);
            PDStreamUtils.write( contents, "- Portale Servizio Sanitario Trentino Alto-Adige -", PDType1Font.HELVETICA_BOLD,
                        20,
                        80,
                        700,
                        Color.BLACK);
                
            PDStreamUtils.write( contents, "Ricetta Farmacologica "+ricetta.getId().toString(),
                        PDType1Font.HELVETICA_BOLD,
                        15,
                        80,
                        675,
                        Color.BLACK);
               
                
            PDStreamUtils.write(contents, "Scannerizza per visualizzare", PDType1Font.HELVETICA , 9, 255, 200, Color.BLACK);
            PDStreamUtils.write(contents, "il codice del medico di base", PDType1Font.HELVETICA , 9, 255, 190, Color.BLACK);
            
                float margin = 80;
                float yStartNewPage = page.getMediaBox().getHeight() - (2 * margin);
                float tableWidth = page.getMediaBox().getWidth() - (2 * margin);
                
                boolean drawContent = true;
                float yStart = yStartNewPage;
                float bottomMargin = 70;
                float yPosition = 650;
                
                BaseTable table = new BaseTable(yPosition, yStartNewPage, bottomMargin, tableWidth, margin, doc, page, true, drawContent);
                Row<PDPage> header = table.createRow(20);
                header.createCell(20, "ID Prescrizione");
                header.createCell(25, "Codice Fiscale Paziente");
                header.createCell(23, "Data Prescrizione");
                header.createCell(15, "ID Farmaco");
                header.createCell(20, "Descrizione Farmaco");
                table.addHeaderRow(header);
               
                String str_time = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(ricetta.getData_prescr());
                Row<PDPage> row = table.createRow(12);
                row.createCell(ricetta.getId().toString());
                row.createCell(paziente.getCodicefiscale());
                row.createCell(str_time);
                row.createCell(ricetta.getIdfarmaco().toString());
                row.createCell(ricetta.getNomefarmaco());
                
                table.draw();
                contents.close();
            
                
            //fine tabella 
            
            float y = 200;
            addQRCode(doc, page, ricetta.getCf_med(), 270, y);
            
            String nomefile = "ricetta"+id_ric+"_"+Calendar.getInstance().getTimeInMillis()+".pdf";
            doc.save(nomefile);
            response.setContentType("paziente/"+nomefile);
            response.setHeader("Content-disposition", "attachment; filename="+nomefile);
            doc.save(response.getOutputStream());
        }

        String contextPath = getServletContext().getContextPath();
        if (!contextPath.endsWith("/")) {
            contextPath += "/";
        }
        response.sendRedirect(response.encodeRedirectURL(contextPath + "paziente/welcome.jsp"));
    }
}

    private static void addQRCode(PDDocument document, PDPage page, String text, float x, float y) {
 
        try {
         PDPageContentStream contentStream = new PDPageContentStream(document, page, PDPageContentStream.AppendMode.APPEND, true);
         Map hintMap = new HashMap();
         hintMap.put(EncodeHintType.MARGIN, 0);
         hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
         BitMatrix matrix = new MultiFormatWriter().encode(
           new String(text.getBytes("UTF-8"), "UTF-8"),
           BarcodeFormat.QR_CODE, 100, 100, hintMap);
         MatrixToImageConfig config = new MatrixToImageConfig(0xFF000001, 0xFFFFFFFF);
         BufferedImage bImage = MatrixToImageWriter.toBufferedImage(matrix, config);
         PDImageXObject image = JPEGFactory.createFromImage(document, bImage);
         contentStream.drawImage(image, x, y, 75, 75);
         contentStream.close();
        } 
        catch (Exception e) {
         e.printStackTrace();
        }
 
    }
}
