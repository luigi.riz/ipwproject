# Progetto Introduzione alla Programmazione Web

Progetto per lo sviluppo di uno strumento web per la gestione dei servizi sanitari e risorse ad essi collegati a livello regionale.


**Struttura Progetto**

**Sotto-progetti Maven**

Il progetto si compone di 3 sotto-progetti in Maven:
   * persitence-api:&emsp;&emsp;&emsp;gestione accesso DB PosgreSQL, ripreso ed adattato dal progetto sviluppato durante il laboratorio
   * persitence-jdbc-impl:&emsp;&emsp;&emsp;gestione accesso DB PosgreSQL, ripreso ed adattato dal progetto sviluppato durante il laboratorio
   * ProgettoIPW:&emsp;&emsp;&emsp;codice del progetto sviluppato autonomamente e successivamente spiegato

**ProgettoIPW-Source Packages**

   * filters:&emsp;&emsp;&emsp;Filtri per la gestione delle aree riservate del sito
   * listeners:&emsp;&emsp;&emsp;Context Listener per le inizializzazioni da effettuarsi allo startup e al shutdown del server
   * mail:&emsp;&emsp;&emsp;Classe Java per l'invio di mail tramite metodo statico. Semplifica l'utilizzo di JavaMail
   * dao:&emsp;&emsp;&emsp;Interfacce che specificano le funzioni che ogni JDBC_DAO deve implementare
   * dao.jdbc:&emsp;&emsp;&emsp;Implementazioni dei DAO che, tramite queries al DB, popolano oggetti (le "entities") e/o tornano altri dati utili
   * entities:&emsp;&emsp;&emsp;JavaBeans che modellano le entità utilizzate nel sistema
   * randomstring:&emsp;&emsp;&emsp;Classe Java per la creazione di Strighe randomiche, utilizzata per il reset password
   * servlets.[utente]:&emsp;&emsp;&emsp;Servlet per la gestione delle azioni svolte da ciascun utente del sistema
   * sha256:&emsp;&emsp;&emsp;Classe Java per la gestione della crittografia

**ProgettoIPW-Web pages**

   * css:&emsp;&emsp;&emsp;Insieme degli stili sviluppati personalmente
   * [utente]:&emsp;&emsp;&emsp;Insieme delle pagine HTML e JSP relative ad un determinato utente
   * error500.jsp:&emsp;&emsp;&emsp;Pagina a cui si viene dirottati in seguito ad errore 500. Ripresa dal progetto Shopping List di Stefano Chirico
   * login.html/.jsp:&emsp;&emsp;&emsp;Pagina principale del sistema
   * privacy.html:&emsp;&emsp;&emsp;Pagina per la presentazione della Privacy Policy del sito

**Funzionalità implementate** 
    
**Pagina di Login**

   * Accesso tramite Paziente, Medico di base, Medico specialista, Farmacia, SSP

**Paziente**

   * Visualizzare la propria scheda paziente (caricare foto, cambiare password, cambiare mail)
   * Consultare la lista delle proprie prescrizioni (ricette farmacologiche, visite specialistiche ed esami)
   * Cambiare il proprio medico di base (modalità specificate nelle specifiche del progetto)
   * Scaricare le proprie ricette farmacologiche (formato specificato nelle specifiche del progetto)
   * Scaricare il proprio elenco dei ticket (formato specificato nelle specifiche del progetto)
   * Consultare la lista degli esami a scopo informativo

**Medico di base**

   * Visualizzare i propri dati (cambiare password, cambiare mail)
   * Visualizzare la scheda paziente  dei propri pazienti (modalità specificate nelle specifiche del progetto)
   * Effettuare delle prescrizioni (ricette farmacologiche, visite specialistiche ed esami)
   * Consultare la lista degli esami a scopo informativo

**Medico specialista**

   * Visualizzare i propri dati (cambiare password, cambiare mail)
   * Visualizzare la scheda paziente di ogni paziente (modalità specificate nelle specifiche del progetto)
   * Erogare le visite specialistiche con inserimento dei risultati, assieme al ticket (modalità specificate nelle specifiche del progetto)

**Farmacia**

   * Visualizzare i propri dati (cambiare password)
   * Visualizzare la scheda paziente di ogni paziente (modalità specificate nelle specifiche del progetto)
   * Erogare ricette farmacologiche assieme al ticket (modalità specificate nelle specifiche del progetto)
    
**SSP**

   * Visualizzare i propri dati (cambiare password)
   * Visualizzare la scheda paziente di ogni paziente (modalità specificate nelle specifiche del progetto)
   * Erogare esami assieme al ticket (modalità specificate nelle specifiche del progetto)
   * Prescrivere richiami di tipo esame/visita specialistica (modalità specificate nelle specifiche del progetto)
   * Scaricare il report dei ticket (formato specificato nelle specifiche del progetto)



Setting DB (con pgAdmin)
Host name/address: localhost
Port: 5432
Username: postgres
Password: (nessuna)
nome dello schema: progettoipw
Per ogni dubbio, comunque nel web.xml, il primo context-param è quello per la connessione al DB, lì trovate qualche informazione