INSERT INTO med_has_paz (cf_med, cf_paz) VALUES
('DVRDRN73P08D075W', 'PCHLSN60T58A286J'),
('DVRDRN73P08D075W', 'BNNCRN43T59L108I'),
('DVRDRN73P08D075W', 'MTTMLD52P64F176A'),
('DVRDRN73P08D075W', 'FSAMTT79T03B579K'),
('DVRDRN73P08D075W', 'GVNTMS77R07I760K'),
('DVRDRN73P08D075W', 'TZZSRG70R27E481D'),
('DVRDRN73P08D075W', 'PDRNMO72L44H639L'),
('DVRDRN73P08D075W', 'PCHFRZ49P25A116I'),
('DVRDRN73P08D075W', 'GSSMRA22A64L660R'),
('DVRDRN73P08D075W', 'GVNLRA19S63I173B'),
('DVRDRN73P08D075W', 'KFLRRT29A04I949D'),
('DVRDRN73P08D075W', 'MRLMRA02M56D663B'),
('NRDLDI83E50E614G', 'PLLPRI69A28D371I'),
('NRDLDI83E50E614G', 'TZZGZL80A61F307B'),
('NRDLDI83E50E614G', 'ZZLSLV15R62L162L'),
('NRDLDI83E50E614G', 'MNFSMN55B08B203N'),
('NRDLDI83E50E614G', 'FRNMRT70E16L490F'),
('NRDLDI83E50E614G', 'ZCCDVD99S09E334C'),
('NRDLDI83E50E614G', 'BRSLGE75C46E398O'),
('NRDLDI83E50E614G', 'DPLLRA05L44B404J'),
('NRDLDI83E50E614G', 'BLDDMN74T11C756X'),
('NRDLDI83E50E614G', 'BRNNDA05R53E981M'),
('NRDLDI83E50E614G', 'BRSFNC35L17D336O'),
('NRDLDI83E50E614G', 'MYRFNC32D59L527G'),
('RZZSLV81R01E150H', 'FRNDRA76E06I687A'),
('RZZSLV81R01E150H', 'HLZNLS17L59F949S'),
('RZZSLV81R01E150H', 'GRDFLV28R05A178Z'),
('RZZSLV81R01E150H', 'PCHGRL78M15E461T'),
('RZZSLV81R01E150H', 'SCRLDI19H55A839F'),
('RZZSLV81R01E150H', 'RSSLCU31P45I173U'),
('RZZSLV81R01E150H', 'LSLFNC25M63H532S'),
('RZZSLV81R01E150H', 'LSLNMO31T62G173U'),
('RZZSLV81R01E150H', 'GRLNNA47H56M173S'),
('RZZSLV81R01E150H', 'KMEFLV86H22L111K'),
('RZZSLV81R01E150H', 'SDNCMN83T26H858P'),
('RZZSLV81R01E150H', 'GFLDRD49M10F856Q'),
('SMMTMS72M20G443H', 'PRTLNE42S67D663Q'),
('SMMTMS72M20G443H', 'MTTDTL40P55D243Y'),
('SMMTMS72M20G443H', 'CHMCNZ46M41E757B'),
('SMMTMS72M20G443H', 'MRNMNL28C16L137H'),
('SMMTMS72M20G443H', 'GRNMSM47T15H004T'),
('SMMTMS72M20G443H', 'MSTRCC17M41A158X'),
('SMMTMS72M20G443H', 'PRCNGL30L64A022U'),
('SMMTMS72M20G443H', 'MDNMRA01S48C756H'),
('SMMTMS72M20G443H', 'FNTGDU07L21D311U'),
('SMMTMS72M20G443H', 'PRCDNL50L03F835T'),
('SMMTMS72M20G443H', 'NRDLVC68A01E981U'),
('SMMTMS72M20G443H', 'CHMLNE17M45L896F'),
('PRTMRZ80C26A537X', 'PRCNNT81P54L322J'),
('PRTMRZ80C26A537X', 'TMSCRL55H56L915E'),
('PRTMRZ80C26A537X', 'PRPNTN38T09D246K'),
('PRTMRZ80C26A537X', 'MRIPQL73R14A952Z'),
('PRTMRZ80C26A537X', 'GRDLCA81C49L957D'),
('PRTMRZ80C26A537X', 'SCRVNC47D56A967W'),
('PRTMRZ80C26A537X', 'FRNCMN07B16A520T'),
('PRTMRZ80C26A537X', 'TCCMRA11B58H330G'),
('PRTMRZ80C26A537X', 'HLZSRA39T42L137R'),
('PRTMRZ80C26A537X', 'PRCLVC38B49G644S'),
('PRTMRZ80C26A537X', 'BRNHLG96P42A537W'),
('PRTMRZ80C26A537X', 'TCCSLV64H06A968F'),
('MRIGZL85P49E481L', 'BSLMTT14M15F068E'),
('MRIGZL85P49E481L', 'SMNLSS92L59F307L'),
('MRIGZL85P49E481L', 'BSLHLG45P45B160D'),
('MRIGZL85P49E481L', 'BNZLCU57D66I354T'),
('MRIGZL85P49E481L', 'FRTLSN14H19L490X'),
('MRIGZL85P49E481L', 'ZCCLNZ98E09C700J'),
('MRIGZL85P49E481L', 'KFLNCL79H42C727A'),
('MRIGZL85P49E481L', 'RCKFRZ46T60E065G'),
('MRIGZL85P49E481L', 'BRNGRL52H17D457Q'),
('MRIGZL85P49E481L', 'MNTNLS46S52I839M'),
('MRIGZL85P49E481L', 'SCHFRZ37M14C400T'),
('MRIGZL85P49E481L', 'MNPMRA62E19A916J'),
('BRSNCL79A61D484Q', 'GVNMTN72E61E981G'),
('BRSNCL79A61D484Q', 'MSTCST13E58L174Q'),
('BRSNCL79A61D484Q', 'PLTGRZ72P68B158J'),
('BRSNCL79A61D484Q', 'GRNPTR17H14L033Y'),
('BRSNCL79A61D484Q', 'MRIGPL08H02D336D'),
('BRSNCL79A61D484Q', 'BNZMRT74P01L106V'),
('BRSNCL79A61D484Q', 'MSTNMO60E66A635P'),
('BRSNCL79A61D484Q', 'ZCCLNE08T47D631A'),
('BRSNCL79A61D484Q', 'ZZLDRN97B55C994H'),
('BRSNCL79A61D484Q', 'GGRPTR15D05L097I'),
('BRSNCL79A61D484Q', 'GMPPTR66C24D821H'),
('BRSNCL79A61D484Q', 'DVRNLS18R50H152G'),
