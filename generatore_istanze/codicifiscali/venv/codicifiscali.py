import datetime
import time
import random
import os
import shutil
import os.path
import glob
from random import randrange
from codicefiscale import build
from codicefiscale import get_birthday
import hashlib

def encrypt_string(hash_string):
    sha_signature = \
        hashlib.sha256(hash_string.encode()).hexdigest()
    return sha_signature

start_timestamp = time.mktime(time.strptime('2018-06-1  00:00:00', '%Y-%m-%d %H:%M:%S'))
end_timestamp = time.mktime(time.strptime('2019-06-30  00:00:00', '%Y-%m-%d %H:%M:%S'))

#funzione che genera un timestamp casuale tra gli intervalli definiti sopra
def randomize_time(start_timestamp, end_timestamp):
    return time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(randrange(start_timestamp,end_timestamp)))

def delta_time(timestamp):
    delta = random.randint(7200,1296000)
    return time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(timestamp+delta))

#elimino le foto dalla relativa cartella

#apertura dei vari file contenenti i dati in modalita' lettura
r_nomim = open("nomi_m.txt")
r_nomif = open("nomi_f.txt")
r_cognomi = open("cognomi.txt")
r_comuni = open("comuni.txt")
r_farmaci = open("farmaci.txt")
r_farmacie = open("farmacie.txt")
r_esamissr = open("esamissr.txt")
r_visitespec = open("visitespec.txt")
r_cittamedici = open("cittamedici.txt")

#apro i vari file in cui scrivere i dati delle tabelle
output_pazienti = open("tab_pazienti.txt","w")
output_medbase = open("tab_medbase.txt","w")
output_medbpaz = open("tab_medbase_has_paz.txt","w")
output_farmaci = open("tab_farmaci.txt","w")
output_prescrfarmaci = open("tab_prescrizionifarmaci.txt","w")
output_prescresami = open("tab_prescrizioniesami.txt","w")
output_farmacie = open ("tab_farmacie.txt","w")
output_visitespec = open("tab_visitespec.txt","w")
output_ticket = open("tab_ticket.txt","w")
output_foto = open("tab_fotopazienti.txt","w")
queries = open("queries.txt","w")

#splitto i dati dei file txt e li salvo all'interno di vettori
vect_m = r_nomim.read().splitlines()
vect_f = r_nomif.read().splitlines()
vect_c = r_cognomi.read().splitlines()
vect_com = r_comuni.read().splitlines()
vect_farmaci = r_farmaci.read().splitlines()
vect_farmacie = r_farmacie.read().splitlines()
vect_esamissr = r_esamissr.read().splitlines()
vect_visitespec = r_visitespec.read().splitlines()
vect_cittamedici = r_cittamedici.read().splitlines()

#dichiarazione di vettori di supporto per manipolare i dati
vect_ticketpagati = []
#vettori contenenti luogo (l) e rispettivo codice catastale (necessario per la generazione del codice fiscale)
vect_l = []
vect_cat = []

#vettori contententi nome del farmaco e codice ministeriale identificativo
vect_nomefarmaci = []
vect_codicefarmaci = []

#vettori contenenti nomi delle farmacie e un proprio id identificativo
vect_nomefarmacie = []
vect_idfarmacie = []

#vettori che conterranno i codici fiscali (univoci) di pazienti medici (e medici specialisti)
vect_cfpazienti = []
vect_nascitapaz = []
vect_cfmedicibase = []
vect_cfmedicispec = []

i=0
j=0
k=0

#scrivo l'inizio delle query nei vari file
output_pazienti.write("INSERT INTO pazienti (nome, cognome, password, sesso, luogonascita, datanascita, "
                      "numerotelefono, codicefiscale, email, citta, provincia) VALUES\n")
output_farmaci.write("INSERT INTO farmaci (idfarmaco, nomefarmaco) VALUES\n")
output_farmacie.write("INSERT INTO farmacie (idfarmacia, indirizzo, nomefarmacia, cap, citta, provincia) VALUES\n")
output_medbase.write("INSERT INTO medici (nome, cognome, password, sesso, luogonascita, datanascita, numerotelefono, "
                     "codicefiscale, citta, provincia, email, is_spec) VALUES \n")
output_medbpaz.write("INSERT INTO med_has_paz (cf_med, cf_paz) VALUES\n")
output_prescrfarmaci.write("INSERT INTO prescrizione_farmaci (id, cf_med, cf_paz, idfarmaco, "
                           "data_prescr, data_erog) VALUES \n")
output_prescresami.write("INSERT INTO prescrizione_esami (id, cf_med, cf_paz, timestamp, "
                         "tipoesame, data_erog, risultati) VALUES \n")
output_visitespec.write("INSERT INTO prescrizione_visitespec (id, cf_med, cf_paz, timestamp, tipovisita, "
                        "erog_time, cf_medspec, risultati) VALUES \n")
output_ticket.write("INSERT INTO ticket (id, tipo, prezzo) VALUES \n")
output_foto.write("INSERT INTO fotopazienti (cfpaz, url) VALUES \n")
#popolo i vettori per luoghi e codici catastali
while len(vect_com)-1>i:
    dati = vect_com[i].split('#')
    vect_l.append(dati[0].upper())
    vect_cat.append(dati[1])
    i=i+1

#popolo i vettori di nomefarmaco e codice farmaco splittandoli (composto da codice#nome)
while len(vect_farmaci)-1>j:
    dati = vect_farmaci[j].split("#")
    vect_codicefarmaci.append(dati[0].upper())
    vect_nomefarmaci.append(dati[1].upper())
    #intanto scrivo la tabella composta da nomefarmaco e codice
    output_farmaci.write("("+dati[0]+", '"+dati[1]+"'),\n")
    j=j+1

#popolo la tabella farmacie splittando i dati salvati nel vettore (composto da id#via#nome#citta#provincia)
while len(vect_farmacie)-1>k:
    dati = vect_farmacie[k].split("#")
    idfarmacia = dati[0]+"00"
    viafarmacia = dati[1]
    nomefarmacia = dati[2]
    capfarmacia = dati[3]
    cittafarmacia = dati[4]
    provinciafarmacia = dati[5]
    vect_idfarmacie.append(idfarmacia)
    vect_nomefarmacie.append(nomefarmacia)
    output_farmacie.write("("+ idfarmacia+", '"+viafarmacia+"', '"+nomefarmacia+"', '"+capfarmacia+"', '"+cittafarmacia+"', '"+provinciafarmacia+"'),\n")
    k=k+1

#segno un numero di medicibase e pazienti da generare (faccio in modo che ogni medico abbia lo stesso numero di pazienti per comodita)
num_medspec = 10
num_medicibase = 7
num_pazienti = num_medicibase*12

i = num_medicibase
j = num_pazienti
k = num_medspec
#MEDICI SPECIALISTIIIIIIIIIIIII
#genero la tabella dei medici creando man mano i vari dati
while k > 0:
    #sesso del medico
    m_f = random.randint(0,1)
    #mese giorno anno
    mm = random.randint(1,12)
    dd = random.randint(1,28)
    yy = random.randint(1965,1990)
    #prefisso cellulare
    pre_pn = random.randint(320,380)
    #luogo di nascita
    i_bp = random.randint(0,len(vect_l)-1)
    bp = vect_l[i_bp]
    cat = vect_cat[i_bp]
    # resto del numero di cellulare
    pn = random.randint(1000000,9999999)
    # pesco i nomi dal vettore di nomi maschili o femminili a seconda del sesso
    if (m_f == 0):
        name = vect_m[random.randint(0,len(vect_m)-1)]
        sex = "M"
    else:
        name = vect_f[random.randint(0,len(vect_f)-1)]
        sex = "F"
    # pesco un cognome a caso
    surname = vect_c[random.randint(0, len(vect_c) - 1)]
    # password dell' account
    hash_string = "password"
    password = encrypt_string(hash_string)
    # genero il codice fiscale attraverso una libreria
    cf = build(surname, name, datetime.datetime(yy, mm, dd), sex, cat)
    vect_cfmedicispec.append(cf)
    datiloc = vect_cittamedici[random.randint(0,len(vect_cittamedici)-1)].split("#")
    citta = datiloc[0]
    provincia = datiloc[1]
    # email
    email = cf.lower()+"@dayrep.com"
    #scrivo la tupla su un file txt di output
    output_medbase.write("('"+name.upper()+"', '"+surname.upper()+"', '"+password+"', '"+sex+"', '"+bp+"', '"+str(yy)+"-"+str(mm)+"-"+str(dd)+"', '"+str(pre_pn)+"/"+str(pn)+"', '"+cf+"', '"+citta+"', '"+provincia+"', '"+email+"', "+"TRUE"+"),\n")
    k = k-1

#MEDICI BASEEEEEEEEEEEEE
#genero la tabella dei medici creando man mano i vari dati
while i > 0:
    #sesso del medico
    m_f = random.randint(0,1)
    #mese giorno anno
    mm = random.randint(1,12)
    dd = random.randint(1,28)
    yy = random.randint(1965,1990)
    #prefisso cellulare
    pre_pn = random.randint(320,380)
    #luogo di nascita
    i_bp = random.randint(0,len(vect_l)-1)
    bp = vect_l[i_bp]
    cat = vect_cat[i_bp]
    # resto del numero di cellulare
    pn = random.randint(1000000,9999999)
    # pesco i nomi dal vettore di nomi maschili o femminili a seconda del sesso
    if (m_f == 0):
        name = vect_m[random.randint(0,len(vect_m)-1)]
        sex = "M"
    else:
        name = vect_f[random.randint(0,len(vect_f)-1)]
        sex = "F"
    # pesco un cognome a caso
    surname = vect_c[random.randint(0, len(vect_c) - 1)]
    # password dell' account
    hash_string = "password"
    password = encrypt_string(hash_string)
    # genero il codice fiscale attraverso una libreria
    cf = build(surname, name, datetime.datetime(yy, mm, dd), sex, cat)
    # aggiungo il codice al vettore dei codici fiscali dei medici di base
    vect_cfmedicibase.append(cf)
    #citta e provincia
    datiloc = vect_cittamedici[random.randint(0, len(vect_cittamedici) - 1)].split("#")
    citta = datiloc[0]
    provincia = datiloc[1]
    # email
    email = cf.lower()+"@dayrep.com"
    #scrivo la tupla su un file txt di output
    output_medbase.write("('"+name.upper()+"', '"+surname.upper()+"', '"+password+"', '"+sex+"', '"+bp+"', '"+str(yy)+"-"+str(mm)+"-"+str(dd)+"', '"+str(pre_pn)+"/"+str(pn)+"', '"+cf+"', '"+citta+"', '"+provincia+"', '"+email+"', "+"FALSE"+"),\n")
    i = i-1

#PAZIENTIIIIIIIIIII
#ripeto la stessa identica cosa con i pazienti
while j > 0:
    bz_tn = random.randint(0, 1)
    m_f = random.randint(0,1)
    mm = random.randint(1,12)
    dd = random.randint(1,28)
    yy = random.randint(1915,2019)
    vect_nascitapaz.append(yy)
    pre_pn = random.randint(320,380)
    i_bp = random.randint(0,len(vect_l)-1)
    bp = vect_l[i_bp]
    cat = vect_cat[i_bp]
    pn = random.randint(1000000,9999999)
    # citta e provincia
    datiloc = vect_cittamedici[random.randint(0, len(vect_cittamedici) - 1)].split("#")
    citta = datiloc[0]
    provincia = datiloc[1]
    if m_f == 0:
        name = vect_m[random.randint(0,len(vect_m)-1)]
        sex = "M"
        src_dir = 'immagini_profilo\maschi'
    else:
        name = vect_f[random.randint(0,len(vect_f)-1)]
        sex = "F"
        src_dir = 'immagini_profilo\\femmine'
    surname = vect_c[random.randint(0, len(vect_c) - 1)]
    hash_string = "password"
    password = encrypt_string(hash_string)
    cf = build(surname, name, datetime.datetime(yy, mm, dd), sex, cat)
    #aggiungo il codice fiscale del paziente al vettore dei codici fiscali dei pazienti
    vect_cfpazienti.append(cf)

    target_dir = 'fotopazienti\\'+cf
    os.mkdir(target_dir)
    src_files = (os.listdir(src_dir))
    def valid_path(dir_path, filename):
        full_path = os.path.join(dir_path, filename)
        return os.path.isfile(full_path)

    files = [os.path.join(src_dir, f) for f in src_files if valid_path(src_dir, f)]
    choices = random.sample(files, random.randint(0,4))
    for files in choices:
        shutil.copy(files, target_dir)
    i = 0
    for filename in os.listdir(target_dir):
        dst = name.lower()+"_"+surname.lower()+ str(i) + ".jpg"
        src = target_dir + "\\" + filename
        dst = target_dir + "\\" + dst
        os.rename(src, dst)
        output_foto.write("('"+cf+"', '"+dst+"'),\n")
        i += 1

    email = cf.lower()+"@dayrep.com"
    #scrivo la tupla
    output_pazienti.write("('"+name.upper()+"', '"+surname.upper()+"', '"+password+"', '"+sex+"', '"+bp+"', '"+str(yy)+"-"+str(mm)+"-"+str(dd)+"', '"+str(pre_pn)+"/"+str(pn)+"', '"+cf+"', '"+email+"', '"+citta+"', '"+provincia+"'),\n")
    j = j-1

#associo ad ogni paziente un medico e creo le prescrizioni nel mentre
idprescr = 0
idssr = 1
idvis = 2
for med in range (0, len(vect_cfmedicibase)):
    i = num_pazienti / num_medicibase
    #codice fiscale del medico preso dal vettore
    cfmed = vect_cfmedicibase[med]
    while i > 0:
        #codice fiscale del paziente preso dal vettore
        cfpaz = vect_cfpazienti.pop()
        datapaz = get_birthday(cfpaz)
        gg = datapaz[0:2]
        mm = datapaz[3:5]
        annopaz = vect_nascitapaz.pop()
        try:
            nascitasec = time.mktime(time.strptime(str(annopaz)+"-"+str(mm)+"-"+str(gg)+" "+"00:00:00", '%Y-%m-%d %H:%M:%S'))
        except OverflowError:
            nascitasec = start_timestamp
        #scrivo sul file di output
        output_medbpaz.write("('"+cfmed+"', '"+cfpaz+"'),\n")
        #ogni paziente avra un numero random di prescrizioni da 0 a 5
        nvisspec= random.randint(0,8)
        nprescr = random.randint(0,5)
        nesamissr = random.randint(0,6)
        while nesamissr>0:
            timestamp = randomize_time(nascitasec, end_timestamp)
            struct = time.strptime(timestamp, '%Y-%m-%d %H:%M:%S')
            if struct.tm_year < 2019 or (struct.tm_year == 2019 and struct.tm_mon < 5):
                erogtime = delta_time(time.mktime(struct))
                res = random.randint(0, 2)
                if res == 0:
                    risultati = "Valori alti"
                if res == 1:
                    risultati = "Valori bassi"
                if res == 2:
                    risultati = "Valori nella norma"
                vect_ticketpagati.append(idssr)
            else:
                erogtime = "NULL"
                risultati = "NULL"
            esamessr = vect_esamissr[random.randint(0,len(vect_esamissr)-1)]
            output_prescresami.write("("+str(idssr)+", '"+cfmed+"', '"+cfpaz+"', '"+timestamp+"', '"+esamessr+"', '"+erogtime+"', '"+risultati+"'),\n")
            nesamissr = nesamissr - 1
            idssr = idssr + 10
        while nvisspec>0:
            timestamp = randomize_time(nascitasec, end_timestamp)
            struct = time.strptime(timestamp, '%Y-%m-%d %H:%M:%S')
            if struct.tm_year < 2019 or (struct.tm_year == 2019 and struct.tm_mon < 5):
                erogtime = delta_time(time.mktime(struct))
                res = random.randint(0,3)
                if res == 0:
                    risultati = "Tutto a posto"
                if res == 1:
                    vis_random = vect_visitespec[random.randint(0,len(vect_visitespec)-1)]
                    risultati = "Si consiglia un ulteriore "+vis_random.lower()+"."
                if res == 2:
                    vis_random = vect_esamissr[random.randint(0,len(vect_esamissr)-1)]
                    risultati = "Si consiglia un ulteriore esame: "+vis_random.lower()+"."
                if res == 3:
                    vis_random = vect_nomefarmaci[random.randint(0,len(vect_nomefarmaci)-1)]
                    risultati = "Si consiglia di assumere farmaco "+vis_random.upper()+"."
                vect_ticketpagati.append(idvis)
                specvis = vect_visitespec[random.randint(0, len(vect_visitespec)-1)]
                cfmedspec = vect_cfmedicispec[random.randint(0, len(vect_cfmedicispec) - 1)]
            else:
                erogtime = "NULL"
                risultati = "NULL"
                specvis = "NULL"
                cfmedspec = "NULL"

            output_visitespec.write("("+str(idvis)+", '"+cfmed+"', '"+cfpaz+"', '"+timestamp+"', '"+specvis+"', '"+erogtime+"', '"+cfmedspec+"', '"+risultati+"'),\n")
            idvis = idvis+10
            nvisspec = nvisspec - 1
        while nprescr>0:
            timestamp = randomize_time(nascitasec, end_timestamp)
            struct = time.strptime(timestamp, '%Y-%m-%d %H:%M:%S')
            if struct.tm_year<2019 or (struct.tm_year==2019 and struct.tm_mon<5):
                erogtime = delta_time(time.mktime(struct))
                vect_ticketpagati.append(idprescr)
            else:
                erogtime = "NULL"
            #pesco un farmaco a caso
            i_farm = random.randint(0, len(vect_codicefarmaci) - 1)
            cod_f = vect_codicefarmaci[i_farm]
            nome_f = vect_nomefarmaci[i_farm]
            #scrivo sul file di output
            output_prescrfarmaci.write("("+str(idprescr)+", '"+cfmed+"', '"+cfpaz+"', '"+str(cod_f)+"', '"+timestamp+"', '"+erogtime+"'),\n")
            idprescr = idprescr+10
            nprescr = nprescr-1

        i = i-1

for i in range (0, len(vect_ticketpagati)-1 ):
    if vect_ticketpagati[i]%10 == 0:
        output_ticket.write("("+str(vect_ticketpagati[i])+", '"+"FARMACO"+"', "+"3"+"),\n")
    if vect_ticketpagati[i]%10 == 1:
        output_ticket.write("("+str(vect_ticketpagati[i])+", '"+"ESAME"+"', "+"11"+" "+"),\n")
    if vect_ticketpagati[i]%10 == 2:
        output_ticket.write("("+str(vect_ticketpagati[i])+", '"+"VISITA"+"', "+"50"+"),\n")
