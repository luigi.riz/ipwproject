--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5
-- Dumped by pg_dump version 11.5

-- Started on 2020-01-29 12:05:09

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 6 (class 2615 OID 16393)
-- Name: progettoipw; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA progettoipw;


ALTER SCHEMA progettoipw OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 199 (class 1259 OID 19136)
-- Name: esami; Type: TABLE; Schema: progettoipw; Owner: postgres
--

CREATE TABLE progettoipw.esami (
    id integer NOT NULL,
    tipoesame character varying(256) NOT NULL,
    descrizione text NOT NULL
);


ALTER TABLE progettoipw.esami OWNER TO postgres;

--
-- TOC entry 197 (class 1259 OID 19123)
-- Name: farmaci; Type: TABLE; Schema: progettoipw; Owner: postgres
--

CREATE TABLE progettoipw.farmaci (
    idfarmaco integer NOT NULL,
    nomefarmaco character varying(256) NOT NULL
);


ALTER TABLE progettoipw.farmaci OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 19144)
-- Name: farmacie; Type: TABLE; Schema: progettoipw; Owner: postgres
--

CREATE TABLE progettoipw.farmacie (
    idfarmacia integer NOT NULL,
    indirizzo character varying(256) NOT NULL,
    nomefarmacia character varying(256) NOT NULL,
    cap integer NOT NULL,
    citta character varying(256) NOT NULL,
    provincia character varying(2) NOT NULL,
    password character varying(256) NOT NULL,
    flag_recuperopassword boolean,
    email character varying(256) NOT NULL
);


ALTER TABLE progettoipw.farmacie OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 19286)
-- Name: fotopazienti; Type: TABLE; Schema: progettoipw; Owner: postgres
--

CREATE TABLE progettoipw.fotopazienti (
    cf_paz character varying(256) NOT NULL,
    url character varying(256) NOT NULL,
    datafoto date
);


ALTER TABLE progettoipw.fotopazienti OWNER TO postgres;

--
-- TOC entry 203 (class 1259 OID 19168)
-- Name: med_has_paz; Type: TABLE; Schema: progettoipw; Owner: postgres
--

CREATE TABLE progettoipw.med_has_paz (
    cf_med character varying(256) NOT NULL,
    cf_paz character varying(256) NOT NULL
);


ALTER TABLE progettoipw.med_has_paz OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 19152)
-- Name: medici; Type: TABLE; Schema: progettoipw; Owner: postgres
--

CREATE TABLE progettoipw.medici (
    nome character varying(256) NOT NULL,
    cognome character varying(256) NOT NULL,
    password character varying(256) NOT NULL,
    flag_recuperopassword boolean,
    sesso character varying(1) NOT NULL,
    luogonascita character varying(256) NOT NULL,
    datanascita date NOT NULL,
    numerotelefono character varying(11) NOT NULL,
    codicefiscale character varying(16) NOT NULL,
    citta character varying(256) NOT NULL,
    provincia character varying(2) NOT NULL,
    email character varying(256) NOT NULL,
    is_spec boolean
);


ALTER TABLE progettoipw.medici OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 19160)
-- Name: pazienti; Type: TABLE; Schema: progettoipw; Owner: postgres
--

CREATE TABLE progettoipw.pazienti (
    nome character varying(256) NOT NULL,
    cognome character varying(256) NOT NULL,
    password character varying(256) NOT NULL,
    flag_recuperopassword boolean,
    sesso character varying(1) NOT NULL,
    luogonascita character varying(256) NOT NULL,
    datanascita date NOT NULL,
    dataultimavisita date,
    numerotelefono character varying(11) NOT NULL,
    codicefiscale character varying(16) NOT NULL,
    email character varying(256) NOT NULL,
    citta character varying(256) NOT NULL,
    provincia character varying(2) NOT NULL
);


ALTER TABLE progettoipw.pazienti OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 19207)
-- Name: prescrizione_esami; Type: TABLE; Schema: progettoipw; Owner: postgres
--

CREATE TABLE progettoipw.prescrizione_esami (
    id integer NOT NULL,
    cf_med character varying(256),
    id_ssp integer,
    cf_paz character varying(256) NOT NULL,
    id_esame integer NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    data_erog timestamp without time zone,
    risultati text
);


ALTER TABLE progettoipw.prescrizione_esami OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 19184)
-- Name: prescrizione_farmaci; Type: TABLE; Schema: progettoipw; Owner: postgres
--

CREATE TABLE progettoipw.prescrizione_farmaci (
    id integer NOT NULL,
    cf_med character varying(256) NOT NULL,
    cf_paz character varying(256) NOT NULL,
    idfarmaco integer NOT NULL,
    data_prescr timestamp without time zone NOT NULL,
    data_erog timestamp without time zone
);


ALTER TABLE progettoipw.prescrizione_farmaci OWNER TO postgres;

--
-- TOC entry 206 (class 1259 OID 19235)
-- Name: prescrizione_visitespec; Type: TABLE; Schema: progettoipw; Owner: postgres
--

CREATE TABLE progettoipw.prescrizione_visitespec (
    id integer NOT NULL,
    cf_med character varying(256),
    id_ssp integer,
    cf_paz character varying(256) NOT NULL,
    "timestamp" timestamp without time zone NOT NULL,
    tipovisita character varying(256),
    erog_time timestamp without time zone,
    cf_medspec character varying(256),
    risultati text
);


ALTER TABLE progettoipw.prescrizione_visitespec OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 19128)
-- Name: serviziprovinciali; Type: TABLE; Schema: progettoipw; Owner: postgres
--

CREATE TABLE progettoipw.serviziprovinciali (
    id integer NOT NULL,
    userinfo character varying(256) NOT NULL,
    password character varying(256) NOT NULL,
    flag_recuperopassword boolean,
    email character varying(256)
);


ALTER TABLE progettoipw.serviziprovinciali OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 19263)
-- Name: ticket; Type: TABLE; Schema: progettoipw; Owner: postgres
--

CREATE TABLE progettoipw.ticket (
    id integer NOT NULL,
    tipo character varying(256) NOT NULL,
    prezzo integer NOT NULL,
    idfarmacia integer,
    idmedspec character varying(256),
    idssp integer
);


ALTER TABLE progettoipw.ticket OWNER TO postgres;

--
-- TOC entry 2899 (class 0 OID 19136)
-- Dependencies: 199
-- Data for Name: esami; Type: TABLE DATA; Schema: progettoipw; Owner: postgres
--

COPY progettoipw.esami (id, tipoesame, descrizione) FROM stdin;
70001	ACE (ANGIOTENSIN CONVERTING ENZYME)	L’ACE (Enzima di conversione dell’Angiotensina) è un enzima prodotto dalle cellule endoteliali dei vasi sanguigni di tutti gli organi, ma in particolare dal polmone. L’azione dell’enzima ACE è di catalizzare la trasformazione dell’Angiotensina I in Angiotensina II, intervenendo così nella regolazione del sistema Renina-Angiotensina-Aldosterone. L’esame viene eseguito attraverso un prelievo di sangue.
70003	ADENOVIRUS FECI COLTURALE	Gli adenovirus sono virus a DNA comunemente acquisiti in seguito al contatto con le secrezioni di una persona infetta (inclusa la trasmissione attraverso le mani) o con un oggetto contaminato. Fino al 5% delle infezioni respiratorie e fino al 15% delle diarree acute sono causate da Adenovirus, ma la maggior parte di esse sono lievi e di durata limitata. L’esame viene eseguito attraverso un prelievo di sangue
70005	ADH Ormone antidiuretico (PLASMA)	L’esame per L’ADH misura la quantità di vasopressina presente nel corpo.L’esame per L’ADH si esegue su un campione di sangue, prelevato dalla vena.
70007	ALANINA AMINOTRANSFERASI ALT (GPT)	Il valore delL’alanina aminotransferasi (ALT) è utilizzato spesso assieme ai risultati del test di aspartato aminotransferasi (AST), o GOT (transaminasi glutammico ossalacetica) per ottenere il cosiddetto rapporto AST ALT. Questo valore spesso può essere utile per determinare se è presente un danno al fegato connesso a un abuso di alcool. L’esame viene eseguito attraverso un prelievo di sangue
70009	ALBUMINEMIA	L’esame serve a conoscere la concentrazione dell’albumina, la proteina più abbondante presente nella parte liquida (plasma) del sangue.
70011	ALFA 1 GLICOPROTEINA ACIDA	L´ alfa 1 glicoproteina acida è una mucoproteina sintetizzata dal fegato. Rappresenta un indice di infiammazione acuta. Un aumento dei valori normali di alfa 1 glicoproteina acida si può riscontrare in caso di infezioni (reumatismo articolare acuto, tbc, ecc.), neoplasie metastatizzanti, necrosi tessutale (infarto miocardico, ecc.) e intervento chirurgico. Una diminuzione dei valori normali di alfa 1 glicoproteina acida, invece, si osserva in caso di insufficienza epatica, malnutrizione e enteropatie protido-disperdenti.
70013	ALFA 1 MICROGLOBULINA	L’alfa 1 microglobulina è una proteina plasmatica sintetizzata dal fegato. La sua funzione è quella di rimuovere radicali liberi e ossidanti (in particolare il gruppo eme) dai tessuti, proteggendoli dallo stress ossidativo.
70015	ALFA-1-ANTITRIPSINA	La alfa-1 antitripsina (AAT) è una proteina del sangue che protegge i polmoni dal danneggiamento causato dagli enzimi attivati. Questo esame misura la concentrazione di AAT nel sangue. Alcuni esami associati possono determinare quali forme anormali di AAT il soggetto ha ereditato. L’esame viene eseguito attraverso un prelievo di sangue
70017	ALPHA – FETO PROTEINA	Questo esame misura la concentrazione di alfa- fetoproteina (AFP) nel sangue. AFP è una proteina che viene principalmente prodotta dal fegato del feto e dalla porzione dell’embrione in via di sviluppo che è simile alla cavità del tuorlo nelle uova degli uccelli (tessuti del sacco vitellino).
70019	AMA 	Questo esame rileva e misura la quantità (titolo) degli anticorpi anti-nucleo (AMA). Gli AMA sono autoanticorpi fortemente associati alla presenza di colangite biliare primitiva, nota anche come cirrosi biliare primitiva.
70021	AMILASEMIA	Questo esame misura la quantità di amilasi nel sangue, nelL’urina o, qualche volta, nel liquido peritoneale, ossia nel liquido presente tra le membrane che ricoprono la cavità addominale e intorno agli organi.
70023	AMILASI PANCREATICA	L’amilasi (isoenzima pancreatico) viene riversata nelL’intestino tenue attraverso il dotto pancreatico, dove interviene nella digestione dei carboidrati assunti con la dieta. L’esame delle amilasi ne valuta la presenza e la quantità nel sangue e/o nelle urine (rappresentando quest’ultime la via di escrezione).
70025	AMMONIEMIA	L’ammoniemia è un valore che indica la concentrazione ematica di ammoniaca, un prodotto azotato che deriva principalmente dal metabolismo delle proteine alimentari e dalle fermentazioni batteriche intestinali. L’esame viene effettuato attraverso prelievo del sangue
70027	ANA ( Anticorpi anti-nucleo)	Gli anticorpi anti-nucleo (ANA) sono un gruppo di anticorpi prodotti dal sistema immunitario in grado di riconoscere erroneamente delle strutture delL’organismo di appartenenza (autoanticorpi). Il test ANA identifica la presenza di questi autoanticorpi nel sangue.
70029	ANDROSTENEDIONE	L’esame serve per valutare la funzionalità delle ghiandole surrenali, per rilevare la presenza di tumori alle ghiandole surrenali, per determinare le cause di virilizzazione (comparsa di caratteri secondari maschili) nelle femmine e di pubertà precoce nei bambini maschi, per valutare la produzione di androgeni e le funzionalità testicolare e ovarica, per monitorare il trattamento per l’iperplasia surrenale congenita  (CAH).
70031	ANTICOAGULANTE LUPUS	L’esame serve come supporto alla diagnosi delle cause di un episodio trombotico; in seguito al riscontro di un aPTT allungato; per determinare le cause di aborti spontanei ricorrenti; nel contesto di approfondimenti effettuati in presenza di una sindrome da antifosfolipidi.
70033	ANTICORPI ANTI CITOPLASMA 	Gli Anticorpi anti-citoplasma (ANCA) sono autoanticorpi prodotti dal sistema immunitario che erroneamente riconosce come estranee alcune proteine presenti nei neutrofili. Il test ANCA misura la quantità di questi autoanticorpi nel sangue.
70035	ANTICORPI ANTI PAROTITE	Gli anticorpi anti-parotite sono degli anticorpi, misurabili nel sangue, che servono a fare diagnosi di parotite.
70037	ANTICORPI ANTI PIASTRINE	Gli anticorpi antipiastrine possono avere un’origine alloimmune o autoimmune. Nel primo caso possono riscontrarsi in corso di piastrinopenia neonatale, con anticorpi rivolti prevalentemente verso l’antigene piastrinico di superficie HPA-1°, oppure nelle forme post-trasfusionali, con presenza di anticorpi rivolti principalmente verso il sistema HLA di classe I. Analisi effettuata su un campione di sangue
70039	ANTICORPI ANTI dsDNA	Gli anticorpi anti-DNA a doppia elica (anti-dsDNA) fanno parte di un gruppo di autoanticorpi chiamati anticorpi antinucleo (ANA). Questo esame rileva la presenza degli anti-dsDNA
70041	ANTIMICOGRAMMA MICETI	L’antimicogramma è un’indagine che viene eseguita sui miceti isolati dai vari materiali patologici al fine di testare la sensibilità di tali miceti verso degli antimicotici.
70043	ANTINUCLEO ANTI ISTONE	Gli anticorpi anti-istone sono autoanticorpi, ossia anticorpi prodotti dal sistema immunitario in grado di riconoscere ed attaccare gli istoni appartenenti all’organismo di appartenenza. Gli istoni sono delle proteine facenti parte della cromatina, il materiale genetico presente nel nucleo di tutte le cellule dell’organismo. Questo test rileva la presenza di autoanticorpi anti-istoni nel circolo ematico.
70199	ETOSUCCIMIDE	L’etosuccimide è un anticonvulsivante. Agisce sul cervello riducendo L’attività elettrica anomala che porta alla comparsa delle convulsioni.
70045	ANTITROMBINA	L’antitrombina è una proteina prodotta dal fegato che aiuta a regolare la formazione del coagulo nel sangue (è un blando fluidificante del sangue). Il test dell’antitrombina misura l’attività (funzionalità) e la concentrazione (quantità) di antitrombina nel sangue di un individuo ed è richiesto per approfondimenti diagnostici in caso di eccessiva formazione di coaguli nel sangue.
70047	APC resistance (COAGULAZIONE)	La proteina C è una proteina che possiede attività anticoagulante ed assieme alla proteina S e antitrombina III, ha il compito di contrastare l’eccessiva funzione dell’attività coagulativa mantenendo fluido il sangue. In alcuni pazienti è presente una variazione geneticamente determinata dal fattore V della coagulazione (fattore V di Leiden) che ostacola l’attività anticoagulante della proteina C; questo fenomeno è evidenziato dal test della Resistenza alla Proteina C Attivata (aPCR). L’aPCR è associata ad un aumentato rischio trombo embolico.
70049	ASCA 	Gli anticorpi anti-Saccharomyces cerevisiae (ASCA) sono proteine prodotte dal sistema immunitario presenti frequentemente nelle persone affette da malattie infiammatorie intestinali. Questo test rileva gli ASCA nel sangue.
70051	ASMA (Ab-ANTIMUSCOLOLISCIO)	Gli anticorpi anti-muscolo liscio (ASMA) sono autoanticorpi, ossia proteine prodotte dal sistema immunitario in grado di riconoscere ed attaccare l’actina presente nella muscolatura liscia e in altri tessuti dell’organismo di appartenenza, specialmente nel fegato. Questo esame rileva e misura la quantità (titolo anticorpale) degli ASMA (o anticorpi anti-actina) nel sangue.
70053	ASPARTATO AMINOTRANSFERASI AST (GOT)	L’aspartato amino transferasi (AST) è un enzima che si trova nelle cellule dell’organismo ma più che altro nel cuore e nel fegato, e in minor concentrazione, in reni e muscoli. In individui sani, i livelli di AST nel sangue sono bassi. Quando il fegato o i muscoli sono danneggiati, L’AST viene rilasciata nel sangue. Ciò rende l’AST un utile marcatore di danno epatico.
70055	AZOTEMIA	Questo esame misura la concentrazione di urea nel sangue. L’urea è prodotta nel fegato quando le proteine sono frammentate nelle loro componenti (amminoacidi) e metabolizzate. Questo processo produce ammoniaca, che è convertita in un prodotto di scarto meno tossico, l’urea.
70057	Alfa-2-macroglobulina	Le alfa 2 globuline appartengono al gruppo delle globuline. Sono proteine diffuse nelle cellule che svolgono la funzione di trasporto di lipidi e ormoni. L’esame per controllare i livelli di alfa 2 globulina presente nel sangue viene effettuato attraverso L’elettroforesi delle proteine del sangue.
70059	Anti Cardiolipina 	Gli anticorpi anti-cardiolipina sono autoanticorpi prodotti dal sistema immunitario, in grado di attaccare la cardiolipina, una molecola espressa sulla superficie di numerose cellule e piastrine, del suo stesso organismo. Questi autoanticorpi possono influenzare le capacità dell’organismo di regolare la coagulazione sanguigna con modalità attualmente non del tutto chiarite. Questo test rileva la presenza degli anticorpi anti-cardiolipina nel circolo ematico.
70061	Anti insula pancreatica	L’autoimmunità insulare è caratterizzata dalla comparsa di autoanticorpi anti-insulari diretti contro l’insulina (IAA), la decarbossilasi dell’acido glutammico (GADA), la proteina tirosin-fosfatasi IA-2 (IA-2A) ed altri autoantigeni. Gli autoanticorpi anti-insula pancreatica non sono patogenetici, ma sono marcatori utili per l’identificazione del processo autoimmune anti-insulare. IAA ed IA-2A sono più frequentemente riscontrati in bambini con diabete di tipo 1 (DMT1), mentre la frequenza dei GADA in pazienti diabetici non è influenzata dall’età all’esordio della malattia
70063	BACILLO DI KOCH ESPETT.MICOBATTERI	Il Mycobacterium tuberculosis (chiamato anche bacillo di Koch) appartenente alla famiglia delle Mycobacteriacee, famiglia di batteri gram-variabili. È il bacillo responsabile della tubercolosi nelL’uomo.
70065	BACILLO DI KOCH LIQUIDO SEMINALE	Il Mycobacterium tuberculosis (chiamato anche bacillo di Koch) appartenente alla famiglia delle Mycobacteriacee, famiglia di batteri gram-variabili. È il bacillo responsabile della tubercolosi nelL’uomo. Analisi effettuata su un campione di liquido seminale
70067	BACILLO DI KOCH URINE (MICOBATTERI)	Il Mycobacterium tuberculosis (chiamato anche bacillo di Koch) appartenente alla famiglia delle Mycobacteriacee, famiglia di batteri gram-variabili. È il bacillo responsabile della tubercolosi nelL’uomo. Analisi effettuata su un campione di urine
70069	BACILLO DI KOCH URINE PCR (DNA)	Il Mycobacterium tuberculosis (chiamato anche bacillo di Koch) appartenente alla famiglia delle Mycobacteriacee, famiglia di batteri gram-variabili. È il bacillo responsabile della tubercolosi nelL’uomo. Analisi effettuata su DNA prelevato da campione di urine
70071	BATTERI ANAEROBI COLTURA (MICOPL E UREAPL)	Un tampone cutaneo (esame colturale della ferita) è un test che permette di rilevare ed identificare i batteri patogeni (responsabili dell’infezione) presenti in una ferita. Qualsiasi ferita può essere infettata da una grande varietà di batteri. L’esame colturale permette di rilevare un’eventuale infezione, identificare il tipo di batterio responsabile e guidare le scelte terapeutiche verso trattamenti mirati.
70073	BENZODIAZEPINE URINARIE (URINE SEMPLICI)	Le benzodiazepine potenziano nel sistema nervoso centrale (SNC) L’attività del GABA, un neurotrasmettitore inibitorio, legandosi ad uno specifico recettore sulla superficie dei neuroni. L’azione ansiolitica delle benzodiazepine è dovuta alla inibizione delle reazioni negative (es. paura) provocate dalL’attivazione di gruppi di neuroni che contengono monoamine specifiche. Le manifestazioni tossiche che seguono alL’assunzione di dosi elevate di benzodiazepine sono L’esacerbazione degli effetti terapeutici che porta alla depressione del SNC. Analisi effettuata su campione di urine
70075	BETA 2 MICROGLOBULINA URINE 24H	La beta-2 microglobulina (B2M) è una proteina che si trova sulla superficie della maggior parte delle cellule ed è rilasciata nel circolo sanguigno, in modo particolare dai linfociti B e dalle cellule tumorali. Questo esame misura la B2M nel sangue, nelle urine, o raramente nel liquido cerebrospinale (CSF).
70077	BILIRUBINA TOTALE	La bilirubina è un pigmento giallo- arancio, prodotto di scarto della degradazione del gruppo eme. Il gruppo eme è un componente dell’emoglobina, che si trova nei globuli rossi (GR). La bilirubina viene processata dal fegato nello stadio finale, per permettere la sua eliminazione dall’organismo. Questo esame misura la concentrazione di bilirubina nel sangue per valutare la funzionalità epatica o come supporto nel diagnosticare l’anemia causata dalla distruzione dei globuli rossi (anemia emolitica).
70079	BORRELIA BURGDORFERI	Borrelia burgdorferi è l’agente etiologico della borreliosi di Lyme, malattia trasmessa da diverse specie di zecche del genere Ixodes. La borreliosi di Lyme è un’affezione multi sistemica che può colpire diversi organi, come la pelle, il sistema nervoso, le articolazioni maggiori e il sistema cardiovascolare.
70517	TRIGLICERIDI	I trigliceridi sono un tipo di grassi e la maggior fonte di energia per l’organismo. Questo esame misura la concentrazione di trigliceridi nel sangue.
70081	Beta-HCG	La gonadotropina corionica umana (hCG) è un ormone prodotto dalla placenta nelle donne in gravidanza. I livelli di hCG aumentano precocemente in gravidanza ed essa viene eliminata con L’urina. Il test di gravidanza rileva l’hCG nel sangue e nelL’urina per la conferma o l’esclusione di una gravidanza.
70083	Beta-due-macroglobulina	La beta-2 microglobulina (B2M) è una proteina che si trova sulla superficie della maggior parte delle cellule ed è rilasciata nel circolo sanguigno, in modo particolare dai linfociti B e dalle cellule tumorali. E’ presente nella maggior parte dei fluidi corporei e la sua concentrazione aumenta nelle patologie in cui la produzione e/o la distruzione delle cellule è incrementata, o nelle quali si attiva il sistema immunitario. Questo esame misura la B2M nel sangue, nelle urine, o raramente nel liquido cerebrospinale (CSF).
70085	C-PEPTIDE	Il peptide C è una molecola di 31 amminoacidi rilasciata dalle cellule beta del pancreas durante il clivaggio della pro-insulina (precursore dell’ormone) in insulina. Rispetto a quest’ultima, il C-peptide è rilasciato in un rapporto di 1:1 (per ogni molecola di insulina, viene messa in circolo una molecola di C-peptide). L’analisi viene effettuata su un campione di urine o sangue
70087	CALCIO	Il calcio è uno degli elementi più abbondanti e importanti dell’organismo. E’ essenziale per la comunicazione tra le cellule delL’organismo e per l’appropriato funzionamento di muscoli, nervi e cuore. Il calcio è necessario per la coagulazione del sangue ed è importante per la formazione, la densità e il mantenimento delle ossa. Questo esame misura la concentrazione di calcio nel sangue e nell’urina, valore che riflette la quantità totale di calcio ione nelL’organismo.
70089	CALCITONINA	La calcitonina è un ormone prodotto dalle cellule C della tiroide. La tiroide è una piccola ghiandola di forma simile ad una farfalla posta sulla trachea alla base della gola. Oltre alla calcitonina, la tiroide è responsabile  della produzione di altri ormoni in grado di regolare il metabolismo, come la tiroxina (T4) e la triiodotironina (T3). Il test della calcitonina misura la quantità di calcitonina nel sangue.
70091	CALPROTECTINA	Questo esame misura la concentrazione di calprotectina nelle feci, utile marcatore di infiammazione intestinale. La calprotectina è una proteina rilasciata da un particolare tipo di leucociti, chiamati neutrofili. In caso di infiammazione, i neutrofili vengono richiamati nell’intestino e rilasciano calprotectina, la cui concentrazione nelle feci aumenta.
70093	CANDIDA TAMPONE URETRALE	La candidosi, detta anche candidiasi o moniliasi, è un’infezione da funghi del genere Candida, di cui Candida albicans è il più comune. La candidosi comprende le infezioni che vanno dal livello superficiale, come ad esempio il mughetto orale e le vaginiti, a quelle sistemiche potenzialmente mortali. Analisi effettuata attraverso tampone uretrale
70095	CANDIDA TAMPONE VAGINALE	La candidosi, detta anche candidiasi o moniliasi, è un’infezione da funghi del genere Candida, di cui Candida albicans è il più comune. La candidosi comprende le infezioni che vanno dal livello superficiale, come ad esempio il mughetto orale e le vaginiti, a quelle sistemiche potenzialmente mortali. Analisi effettuata attraverso tampone vaginale
70097	CANDIDA URINE	La candidosi, detta anche candidiasi o moniliasi, è un’infezione da funghi del genere Candida, di cui Candida albicans è il più comune. La candidosi comprende le infezioni che vanno dal livello superficiale, come ad esempio il mughetto orale e le vaginiti, a quelle sistemiche potenzialmente mortali. Analisi effettuata su campione di urine
70099	CERULOPLASMINA	Il test misura la quantità di ceruloplasmina nel sangue. La ceruloplasmina è un enzima contenente rame che gioca un ruolo nel metabolismo del ferro. Il rame è un minerale essenziale che viene assunto con la dieta.
70101	CICLOSPORINA	Questo test misura la concentrazione di ciclosporina nel sangue. Si tratta di un farmaco immunosoppressore, in grado di diminuire le difese immunitarie del paziente.
70103	CISTATINA C	La Cistatina C è una proteina relativamente piccola che è prodotta dalle cellule dell’organismo che contengono una nucleo, e si ritrova in vari liquidi biologici, incluso il sangue. E’ prodotta, filtrata dal sangue attraverso i reni e metabolizzata a velocità costante. Questo esame misura la concentrazione di cistatina C nel sangue per valutare la funzionalità renale.
70105	CITO IGG AVIDITY	Il Citomegalovirus, virus della famiglia degli Herpesviridae, è un agente infettivo molto comune. Il virus può essere contratto a seguito di un’infezione cosiddetta ’primaria’ (insorge quando L’individuo entra a contatto con il virus per la prima volta) oppure a seguito di trasmissione verticale (ovvero da madre a feto)
70107	CITOMEGALOVIRUS IMMUNOBLOTTING	Il test per il CMV riguarda la misura degli anticorpi anti-CMV (proteine del sistema immunitario prodotte in seguito all’infezione da CMV), o la ricerca del virus. Il virus può essere identificato durante un’infezione attiva tramite l’esame colturale del CMV o ricercando il suo materiale genetico (DNA) nei tessuti o fluidi corporei.
70109	CLAMIDIA DNA	La Clamidia è una infezione sessualmente trasmissibile tra le più comuni, causata da un batterio chiamato Chlamydia trachomatis. L’esecuzione del test di screening e la diagnosi dell’infezione da Clamidia è molto importante per la prevenzione della comparsa delle complicanze a lungo termine e della diffusione dell’infezione.
70111	CLEARANCE DELLA CREATININA	Questo esame misura le concentrazioni di creatinina sia nel sangue che nel campione di urina raccolta nelle 24 ore. I risultati sono usati per calcolare la concentrazione di creatinina che passa dal sangue all’urina. Questo calcolo permette una valutazione generale della quantità di sangue che viene filtrato dai reni in un periodo di tempo di 24 ore.
70113	CLORO	Il test viene effettuato per determinare se il paziente ha problemi di equilibrio elettrolitico o acido-base e per monitorare il trattamento. Viene eseguito attraverso L’analisi di un campione di sangue prelevato da una vena del braccio, talvolta un campione di urina random o un campione di urine raccolte nelle 24 ore.
70115	COLESTEROLO HDL	Le lipoproteine ad alta densità (colesterolo HDL, HDL-C) è una classe di lipoproteine che trasporta il colesterolo nel sangue. L’HDL-C consiste principalmente in una proteina con una piccola quantità di colesterolo. E’ considerato benefico perché rimuove l’eccesso di colesterolo dai tessuti e lo trasporta al fegato per depositarlo. Per questo motivo il colesterolo HDL è spesso chiamato “buono”. Questo esame misura le HDL-C nel sangue.
70117	COLORAZIONE BAND.G.ALTA RISOLUZIONE	La colorazione di Gram è una procedura utilizzata per rilevare la presenza di batteri e talvolta di funghi in campioni provenienti dai siti di sospetta infezione. Questo esame fornisce dei risultati in tempi brevi e, oltre a rilevare la presenza dei microrganismi, fornisce anche una prima classificazione.
70119	COLORAZIONE BANDEGGIO G.	La colorazione di Gram è una procedura utilizzata per rilevare la presenza di batteri e talvolta di funghi in campioni provenienti dai siti di sospetta infezione. Questo esame fornisce dei risultati in tempi brevi e, oltre a rilevare la presenza dei microrganismi, fornisce anche una prima classificazione.
70121	COPROCOLTURA	La coprocoltura è finalizzata all’identificazione di batteri patogeni eventualmente presenti nel campione di feci. Questo test è in grado di distinguere tra i batteri normalmente presenti nel tratto gastrointestinale (flora batterica normale) e quelli in grado di causare una malattia (patogeni). Pertanto il test viene eseguito per identificare i batteri patogeni presenti nel tratto gastrointestinale di un paziente con sintomi di gastroenterite.
70123	CORTISOLO	Il cortisolo è un ormone che prende parte al metabolismo delle proteine, dei lipidi e dei carboidrati. Influenza la concentrazione di glucosio nel sangue, aiuta a mantenere costante la pressione arteriosa, ha una funzione regolatoria sul sistema immunitario. La maggior parte del cortisolo nel sangue è legato a una proteina; solo una piccola percentuale è “libera” e biologicamente attiva. Il cortisolo libero è secreto nell’urina ed è presente nella saliva. Questo esame misura la quantità di cortisolo nel sangue, nell’urina, nella saliva.
70125	CREATININEMIA	L’esame misura i livelli di creatinina nel sangue. È un esame di routine ma può essere anche prescritto in caso di sospetta alterazione della funzionalità dei reni, nel caso di patologie renali o di pazienti sottoposti a dialisi.
70127	CROMOGRANINA	Tramite questo esame viene misurata la concentrazione di Cromogranina A (CgA) nel sangue. La CgA è una proteina rilasciata nel circolo ematico dalle cellule neuroendocrine, in risposta a stimoli del sistema nervoso centrale. Tali cellule sono presenti in pressoché tutti gli organi.
70129	CUPREMIA	Analisi effettuata su campione di sangue, il cui scopo è quantificare il rame totale al suo interno
70131	CYFRA 21	CYFRA-21 è un frammento del filamento intermedio della citocheratina 19, normalmente espresso nelle cellule di origine epiteliali ed anche nelle loro derivazioni maligne.
70133	DIGOSSINA	La digossina è un farmaco usato per trattare l’insufficienza cardiaca e le aritmie cardiache. Alcune malattie cardiache, inclusa l’insufficienza cardiaca congestizia, comportano la riduzione della funzionalità cardiaca. Di conseguenza, il sangue ristagna negli arti inferiori, nelle mani, nei piedi, nei polmoni e nel fegato, causando gonfiore, affanno, e affaticamento. Questo test misura la quantità di digossina nel sangue.
70135	DOSAGGIO CATENE Kappa/Lambda nel siero	Le catene leggere (light chains) sono catene proteiche prodotte da un tipo di cellule immunitarie chiamate plasmacellule. Questo esame consiste nella misura della concentrazione di FLC kappa e lambda (κFLC e λFLC) nel sangue e nel calcolo del rapporto tra catene kappa/lambda. L’alterazione di questi parametri fornisce indicazioni diagnostiche, prognostiche e nel monitoraggio delle discrasie plasmacellulari associate all’aumentata produzione di FLC.
70137	DOSAGGIO LEVETIRACETAM	Il levetiracetam è un farmaco utilizzato per il trattamento di alcune forme di epilessia, come L’epilessia mioclonica giovanile, L’epilessia lobo temporale, le crisi parziali complesse, le convulsioni miocloniche, le convulsioni refrattarie, le convulsioni secondarie generalizzate, le convulsioni semplici parziali, le convulsioni tonico-cloniche. Il farmaco viene di solito prescritto come farmaco secondario da assumere in combinazione con altri antiepilettici.
70139	DRUG TEST	Il test delle droghe d’abuso consiste nella rivelazione di una o più sostanze legali e/o illegali nelL’urina o, più raramente, nel sangue, nella saliva, nei capelli o nel sudore. Questo test identifica delle sostanze che, di norma, non sono presenti nell’organismo umano, fatta eccezione per alcuni ormoni o steroidi misurati nell’ambito dei protocolli antidoping negli atleti.
70141	Dosaggio FENITOINA	Con questo esame viene misurata la quantità di fenitoina nel sangue. La fenitoina è un farmaco utilizzato per trattare alcuni tipi di convulsioni (vedi la pagina Epilessia), tra le quali le crisi parziali complesse (alterazioni dello stato di coscienza precedute, accompagnate o seguite da sintomi psicologici) o le convulsioni tonico-croniche (perdita improvvisa di coscienza, arresto respiratorio e rigidità muscolare).
70143	Dosaggio FENOBARBITAL	Con questo esame viene misurata la quantità di fenitoina nel sangue. La fenitoina è un farmaco utilizzato per trattare alcuni tipi di convulsioni (vedi la pagina Epilessia), tra le quali le crisi parziali complesse (alterazioni dello stato di coscienza precedute, accompagnate o seguite da sintomi psicologici) o le convulsioni tonico-croniche (perdita improvvisa di coscienza, arresto respiratorio e rigidità muscolare).
70145	ECHINOCOCCO IGG	L’ echinococcosi (idatidosi) è una infezione causata dallo stadio larvale di varie specie di tenie del genere Echinococcus. La più comune infezione umana è data da Echinococcus granulosus, mentre che utilizza canidi come ospiti definitivi e erbivori o onnivori vari come ospiti intermedi.
70147	ELETTROFORESI DELLE PROTEINE URINARIE	Il test delle proteine urinarie determina e/o misura le proteine escrete nell’urina. Le proteine normalmente non si trovano nell’urina, a parte in alcuni momenti a causa di infezioni o stress. Se la loro presenza è persistente è possibile che ci sia danno renale.
70149	ELETTROFORESI PROTEICA	L’elettroforesi è una tecnica utilizzata nelL’ambito delle analisi di laboratorio che sfrutta la massa molecolare e la carica elettrica delle proteine, per valutarne la quantità e la qualità. In particolare, quest’esame consente di separare le proteine in cinque frazioni: albumina, alfa 1 globuline, alfa 2 globuline, beta globuline e gamma globuline. Quest’esame è realizzato adottando un metodo molto particolare: al campione è applicato un campo elettrico, grazie al quale le proteine si "raggruppano" per tipologia.
70151	EMA ANTIENDOMISIO	Gli anticorpi anti-endomisio (classe IgA) hanno una specificità molto elevata. In caso di positività degli anticorpi anti-transglutaminasi IgA (tTG), la ricerca degli anticorpi anti-endomisio (EMA) è il test di secondo livello più importante per escludere o confermare l’intolleranza al glutine e formare dunque una diagnosi di celiachia.
70153	EMOCOLTURA	Le emocolture (colture di sangue) hanno lo scopo di individuare ed identificare i microrganismi presenti nel sangue. Le infezioni del circolo ematico sono più comunemente di origine batterica (batteriemia), ma possono anche essere causate da lieviti, altri funghi o da virus (viremia).
70155	EMOCROMO completo	L’emocromo è una delle analisi del sangue più richieste, poiché i suoi risultati contribuiscono a tenere sotto controllo lo stato di salute generale di una persona. Detto anche esame emocromocitometrico, questo test consiste nella valutazione dei diversi parametri che si riferiscono ai principali componenti del sangue
70157	EMOGLOBINA GLICOSILATA	Questo esame misura la concentrazione della emoglobina glicata A1c (HbA1c) nel sangue, un’emoglobina con attaccato il glucosio. La misura della percentuale di HbA1c, consente di effettuare una stima della quantità di glucosio presente nel sangue (glicemia) negli ultimi 2-3 mesi.
70159	ENA (Antigeni nucleari estraibili)	Gli ENA (Extractable Nuclear Antigen) sono antigeni nucleari estratti dal nucleo cellulare. Nelle patologie su base autoimmunitaria vengono prodotti diversi tipi di anticorpi diretti contro specifiche componenti  nucleari che non sono più riconosciute come proprie (perdita della tolleranza immunitaria).
70161	ENTEROBIUS VERMICULARISSCOTHC TEST	L’Enterobius vermicularis è un parassita intestinale cosmopolita e frequente soprattutto nelle comunità infantili. In seguito alL’infestazione (ingestione di uova) si ha maturazione del verme che si localizza a livello delL’intestino cieco.
70163	EPSTEIN-BARR-VCA	Il virus di Epstein-Barr (EBV) è di solito la causa un malessere caratterizzato da sintomi abbastanza lievi. Questo esame rileva la presenza di anticorpi diretti contro l’EBV nel sangue, come supporto alla diagnosi dell’infezione.
70165	ERITROPOIETINA	L’Eritropoietina (EPO) è un ormone prodotto perlopiù a livello renale. La sua azione è di fondamentale importanza per la produzione dei globuli rossi (eritrociti), responsabili del trasporto dell’ossigeno dai polmoni verso tutto l’organismo. Questo test misura la quantità di EPO nel circolo ematico.
70167	ES.PARASSIT.FECI MACRO-MICROSCOPICO	L’esame parassitologico delle feci è utile per la diagnosi di parassitosi intestinale, che si manifesta con diarrea, crampi, occlusione intestinale, dimagrimento e anemia. L’esame viene effettuato attraverso la ricerca nelle feci dei parassiti e delle loro uova: elminti (uova e/o larve), protozoi come Giardia intestinalis, coccidi (Cryptosporidium parvum, Isospora belli, Cyclospora cayetanensis).
70169	ESAME CHIMICO FISICO DELLE FECI	L’esame chimico-fisico delle feci si basa sull’osservazione microscopica delle caratteristiche fisiche delle feci e sulla determinazione del pH. È di normale riscontro la pre­senza nelle feci di residui di tessuto connettivo, di fibre muscolari, grassi, amidi, residui vegetali, cristalli, leucociti, globuli ros­si, cellule epiteliali e batteri. La presenza di questi elementi ha significato patologico solo in caso di eccessiva quantità.
70171	ESAME COLT.LIQUIDO SEMINALE MICETI	Gli esami colturali sono normalmente utilizzati in medicina per identificare L’eventuale presenza di agenti patogeni nei fluidi corporei. Analisi effettuata su campione di liquido seminale
70173	ESAME COLTURALE BRONCOASPIRATO	Gli esami colturali sono normalmente utilizzati in medicina per identificare L’eventuale presenza di agenti patogeni nei fluidi corporei. Analisi effettuata tramite raccolta delle secrezioni bronchiali
70175	ESAME COLTURALE LIQUIDO DI DRENAGGIO	Gli esami colturali sono normalmente utilizzati in medicina per identificare L’eventuale presenza di agenti patogeni nei fluidi corporei. Analisi effettuata tramite liquido di drenaggio
70177	ESAME COLTURALE SECRETO NASALE	Gli esami colturali sono normalmente utilizzati in medicina per identificare L’eventuale presenza di agenti patogeni nei fluidi corporei. Analisi effettuata tramite tampone nasale
70179	ESAME COLTURALE TAMPONE FERITA SETTICA	Gli esami colturali sono normalmente utilizzati in medicina per identificare L’eventuale presenza di agenti patogeni nei fluidi corporei. Analisi effettuata tramite tampone su ferita settica
70181	ESAME DEL CALCOLO SALIVARE	Per calcolosi salivare o scialolitiasi, in campo medico, si intende la presenza di concrezione (calcolo), composta quasi esclusivamente da carbonato di calcio, nelle ghiandole salivari che ne comportano L’ostruzione.
70183	ESAME DEL CALCOLO URINARIO	La calcolosi urinaria, detta anche litiasi o calcoli renali, è una delle più comuni e antiche malattie delle vie urinarie. Tale patologia è caratterizzata dalla presenza di piccoli sassolini, calcoli appunto, lungo il decorso delle vie urinarie.
70185	ESAME DI URINOCOLTURA	L’urinocoltura consente di rilevare – nel campione di urina – la presenza di batteri responsabili di infezioni a carico delle vie urinarie e isolarli. Uno dei più comuni è l’Escherichia Coli. Grazie all’antibiogramma poi è possibile valutare la sensibilità e la resistenza dei microrganismi presenti nelle urine agli antibiotici. Analisi effettuata su un campione di urina.
70187	ESAME MICOLOGICO FECI	L’esame delle feci consiste nelL’analisi di un campione fecale raccolto dal paziente secondo le modalità prescritte dal medico. Tale esame comprende valutazioni macroscopiche (aspetto, colore, odore, consistenza), chimiche (pH, acidi grassi, sangue ed altri contenuti) e microbiologiche (coprocoltura e ricerca di parassiti).
70189	ESAME MICROSCOPICO ESPETTORATO	L’espettorato è un muco denso o catarro espulso dalle vie aeree inferiori (bronchi e polmoni) tramite un colpo di tosse profonda; non è saliva o sputo. È importante assicurarsi che il campione raccolto provenga dalle vie aeree inferiori e non dalle vie aeree superiori (faringe, laringe). La raccolta del campione può avvenire tramite espulsione spontanea o può essere indotta.
70191	ESAME PARASSITOLOGICO DELLE FECI	L’esame parassitologico delle feci è utile per la diagnosi di parassitosi intestinale, che si manifesta con diarrea, crampi, occlusione intestinale, dimagrimento e anemia. L’esame viene effettuato attraverso la ricerca nelle feci dei parassiti e delle loro uova: elminti (uova e/o larve), protozoi come Giardia intestinalis, coccidi (Cryptosporidium parvum, Isospora belli, Cyclospora cayetanensis).
70193	ESTRADIOLO	Gli estrogeni sono un gruppo di steroidi responsabili dello sviluppo e della funzionalità degli organi riproduttivi e della formazione dei caratteri secondari nelle donne. Insieme all’ormone progesterone, aiutano a regolare il ciclo mestruale, sono coinvolti nella crescita del seno e dell’utero e aiutano a mantenere una gravidanza sana. Sebbene siano considerati gli ormoni femminili per eccellenza, essi sono presenti anche negli uomini e giocano un ruolo nel metabolismo osseo e nella crescita di entrambi i sessi. Il test degli estrogeni misura una delle tre componenti: estrone (E1), estradiolo (E2) o estriolo (E3) nel sangue o nelL’urina.
70195	ESTRAZIONE DNA	Macromolecola biologica costituita da una catena di nucleotidi organizzati nello spazio a formare una doppia elica; il DNA è il costituente dei cromosomi ed è portatore dell’informazione genetica; tale informazione è codificata dalla sequenza di nucleotidi di cui il DNA è costituito.
70197	ETANOLO	L’etanolo è un ingrediente tossico contenuto in alcune bevande come birra, vino e liquori. Questo esame misura la quantità di etanolo presente nel sangue, nelL’urina, nel respiro o nella saliva. L’alcol consumato viene assorbito dal tratto gastrointestinale e trasportato nell’organismo dal circolo sanguigno. Piccole quantità di etanolo sono eliminate con L’urina o dai polmoni tramite la respirazione ma la maggior parte è metabolizzata dal fegato.
70201	FERRITINA	Questo test misura la quantità di ferritina nel sangue.La ferritina è una proteina contenente ferro e rappresenta la forma principale di deposito di ferro delL’organismo. Le piccole quantità di ferritina rilasciate alL’interno del circolo ematico riflettono direttamente l’entità dei depositi presenti.
70203	FIBRINOGENO	Il fibrinogeno è prodotto dal fegato e viene rilasciato in circolo insieme ad altri fattori della coagulazione. Di solito, in seguito al danneggiamento di un tessuto o di un vaso, viene innescato un processo coagulativo, che culmina nella formazione di un coagulo in grado di interrompere il sanguinamento nella sede della lesione. Piccoli elementi cellulari, detti piastrine, aderiscono e si aggregano in corrispondenza della sede della lesione, innescando poi la cascata coagulativa e L’attivazione progressiva dei fattori della coagulazione.
70205	FIBROSI CISTICA I LIVELLO	La Fibrosi Cistica (FC) è una patologia ereditaria multisistemica che può interessare i polmoni, il pancreas e le ghiandole sudoripare. Le persone affette da FC infatti producono un muco denso e appiccicoso che determina la presenza frequente di infezioni respiratorie, il blocco del rilascio degli enzimi pancreatici e quindi l’inibizione della digestione di lipidi e proteine. Il test di laboratorio per la Fibrosi Cistica (FC) possono essere utilizzati come sostegno alla diagnosi, per determinare lo stato di portatore del gene CTFR mutato e/o per il monitoraggio dello stato di pazienti affetti da FC.
70207	FOSFATASI ALCALINA	La fosfatasi alcalina (ALP) è un enzima che si trova in diversi tessuti dell’organismo, inclusi fegato, ossa, reni, intestino e placenta delle donne in gravidanza. Nelle cellule ossee ed epatiche c’è la quantità più alta di ALP. Questo esame misura la concentrazione di ALP nel sangue.
70209	FOSFORO	Il fosforo è un elemento che si combina con altre sostanze per formare composti fosfati organici ed inorganici. Il termine fosforo e fosfato sono spesso usati come sinonimi, ma in realtà il test misura la concentrazione del fosfato inorganico nel sangue.
70211	FRUTTOSAMINA	La fruttosamina è un composto che si forma quando il glucosio si lega alle proteine del siero. Il test misura la quantità totale di fruttosamina (proteina glicosilata) nel sangue.
70213	GASTRINA	La gastrina è un ormone prodotto dalle “cellule G”, presenti in un’area dello stomaco chiamata antro. Questo ormone regola la produzione di acidi nello stomaco durante il processo digestivo. Questo esame misura la concentrazione di gastrina nel sangue ed è utile nella valutazione di soggetti che presentano ulcera peptica e/o altri sintomi addominali gravi.
70215	GH (ORMONE SOMATOTROPO)	L’ormone della crescita (GH) è essenziale per la normale crescita e sviluppo dei bambini. Esso promuove un’appropriata crescita delle ossa dalla nascita alla pubertà. Sia nei bambini che negli adulti, l’ormone della crescita è responsabile della regolazione del metabolismo, della produzione di grassi, proteine e glucosio. Inoltre contribuisce  a regolare la produzione della massa muscolare e dei globuli rossi.
70217	GLICEMIA	Il dosaggio del glucosio ematico (o glicemia) viene richiesto per valutare e monitorare la presenza di ipoglicemia e iperglicemia (rispettivamente basso e alto livello di glucosio nel sangue), per fare diagnosi di diabete (in questo caso si valuta anche la presenza di glucosio nelle urine), per evidenziare una situazione di pre-diabete (detta anche intolleranza glucidica) e per monitorare la terapia farmacologica e dietetica nei pazienti affetti da diabete.
70219	GONOCOCCO LIQUIDO SEMINALE	Il Gonococco (Neisseria gonorrhoeae) è un batterio trasmesso sessualmente che causa nell’uomo, dopo un breve periodo di incubazione di 1 -7 gioni , un’uretrite acuta caratterizzata da un’ abbondante secrezione muco -purulenta. Nelle donne l’infezione si manifesta a livello cervicale con sintomi meno evidenti. Nella maggior parte dei casi è infatti la diagnosi dell’infezione nel partner che spinge la donna ad eseguire un controllo. Analisi effettuata su liquido seminale
70221	GONOCOCCO TAMPONE CERVICALE	Il Gonococco (Neisseria gonorrhoeae) è un batterio trasmesso sessualmente che causa nell’uomo, dopo un breve periodo di incubazione di 1 -7 gioni , un’uretrite acuta caratterizzata da un’ abbondante secrezione muco -purulenta. Nelle donne l’infezione si manifesta a livello cervicale con sintomi meno evidenti. Nella maggior parte dei casi è infatti la diagnosi dell’infezione nel partner che spinge la donna ad eseguire un controllo. Analisi effettuata attraverso tampone cervicale
70223	GONOCOCCO TAMPONE URETRALE	Il Gonococco (Neisseria gonorrhoeae) è un batterio trasmesso sessualmente che causa nell’uomo, dopo un breve periodo di incubazione di 1 -7 gioni , un’uretrite acuta caratterizzata da un’ abbondante secrezione muco -purulenta. Nelle donne l’infezione si manifesta a livello cervicale con sintomi meno evidenti. Nella maggior parte dei casi è infatti la diagnosi dell’infezione nel partner che spinge la donna ad eseguire un controllo. Analisi effettuata attraverso tampone uretrale
70225	GRUPPO SANGUIGNO	La tipizzazione del gruppo sanguigno è basata sulla presenza di marcatori (carboidrati o proteine) o antigeni presenti sulla superficie dei globuli rossi (eritrociti). I due antigeni principali identificati sulla superficie eritrocitaria sono chiamati antigeni A e B, insieme al fattore Rh. La determinazione del gruppo sanguigno avviene tramite la ricerca della presenza/assenza sulla superficie eritrocitaria di questi antigeni, per determinare il gruppo sanguigno AB0 e il fattore Rh.
70227	HCV GENOTIPO	L’epatite C (HCV) è un virus che causa infezione al fegato, caratterizzata da infiammazione e danno all’organo stesso. I test dell’epatite C sono un gruppo di analisi usato per determinare, diagnosticare e monitorare il trattamento per l’epatite C. Il test più frequentemente utilizzato ricerca nel sangue gli anticorpi prodotti in risposta all’infezione. Altri esami rilevano la presenza di RNA virale, la concentrazione di RNA virale presente oppure la specifica sottospecie del virus.
70229	HDV Ag VIRUS EPATITE DELTA	L’epatite D, o epatite Delta, è una malattia causata dalL’omonimo virus RNA a singola elica (HDV), che necessita della presenza di HBV per replicarsi; di conseguenza, possono essere colpiti da epatite D solamente quei soggetti che hanno precedentemente (super-infezione) o simultaneamente (co-infezione) contratto L’epatite di tipo B.
70231	HPV DNA SCREENING	Del gruppo del papilloma virus (HPV) fanno parte più di 100 virus. Alcuni tipi di HPV sono considerati ad alto rischio perché associati a tumore. I test dell’HPV determinano la presenza di materiale genetico (DNA o RNA) del virus.
70233	IMMUNOFISSAZIONE SIERO	L’immunofissazione è un esame di laboratorio che consente di individuare e tipizzare le gamma globuline presenti in un campione biologico. Nel dettaglio, L’analisi permette di studiare - nel sangue o nelle urine del paziente - le classi di immunoglobuline (IgA, IgG, IgM, IgE o IgD) ed il tipo di catena leggera kappa o lambda, in funzione della loro specifica mobilità elettroforetica.
70235	IMMUNOGLOBULINE	Particolari proteine prodotte dalL’organismo in risposta a sostanze estranee, inclusi batteri e virus. Esistono cinque diverse classi di immunoglobuline prodotte da plasma cellule nel midollo osseo  ed altri tessuti linfoidi che legano e neutralizzano le sostanze estranee (antigeni). Le cinque principali classi di immunoglobuline sono: A, D, E, G ed M.
70237	INIBINA B	L’Inibina B è un ormone proteico prodotto, nella donna, da cellule dell’ovaio e, nell’uomo, da cellule del testicolo. Agisce attraverso l’inibizione dell’ormone Follicolo stimolante (FSH) e, più precisamente: nella donna stimola la capacità di maturazione dei follicoli ovarici (ed è ritenuto un marcatore di riserva follicolare), nell’uomo invece, agisce indirettamente sullo sviluppo dei gameti e controlla la spermatogenesi con un meccanismo di feedback sulla secrezione dell’ormone FSH.  La sua concentrazione ematica si correla con il volume  e la maturità dei testicoli e con la concentrazione degli spermatozoi.
70239	INSULINA	L’insulina è un ormone prodotto ed immagazzinato dalle cellule beta del pancreas. Viene secreta in risposta alL’innalzamento dei livelli di glucosio ematico in seguito ai pasti ed è vitale per il suo trasporto e l’immagazzinamento. L’insulina favorisce l’ingresso del glucosio nelle cellule e ha un ruolo importante nella regolazione dei suoi livelli nel sangue e del metabolismo dei lipidi. Questo test misura la quantità di insulina nel sangue.
70241	IgF1 (SOMATOMEDINA-C)	La Somatomedina C o IGF 1 (fattore di crescita insulino simile) è un ormone con una struttura simile a quella delL’insulina. La secrezione di GH (ormone della crescita) stimola la sintesi di IGF-1 nel fegato e negli altri tessuti. La somatomedina viene comunque rilasciata dalL’organismo anche indipendentemente dal GH, ed è prodotta localmente nei muscoli.
70243	LAMOTRIGINA	La lamotrigina è un principio attivo appartenente al gruppo degli antiepilettici e abitualmente impiegato nel trattamento delL’epilessia. Nonostante ciò, la lamotrigina si è mostrata efficace anche nel trattamento del disturbo bipolare e, in particolar modo, nel trattamento degli episodi acuti depressivi.
70245	LEPTINA	La leptina è un’adipochina, un ormone prodotto dalle cellule del tessuto adiposo (adipociti) e, in misura minore, da altri tessuti come la placenta nelle donne in gravidanza. Questo test misura la quantità di leptina nel sangue.
70247	LIPASI	La lipasi è uno degli enzimi prodotti dal pancreas, coinvolti nella digestione dei grassi introdotti con la dieta. Questo esame misura la quantità di lipasi nel sangue.
70249	LIQUIDO PERITONEALE	Il liquido peritoneale ha la funzione di lubrificare la cavità addominale. Si trova in piccole quantità tra gli strati del peritoneo. Il liquido peritoneale è prodotto dalle cellule mesoteliali nelle membrane addominali e agisce come lubrificante all’esterno degli organi allo scopo di ridurre la frizione tra di essi durante i movimenti legati alla digestione.
70251	LIQUIDO SINOVIALE	 Il liquido sinoviale svolge un ruolo di protezione nei confronti delle estremità delle ossa riducendo la frizione delle articolazioni durante il movimento, in particolare nelle articolazioni delle ginocchia, spalle, fianchi, mani e piedi. L’analisi del liquido sinoviale riguarda un gruppo di esami volti alla rilevazione di variazioni dell’aspetto e composizione del liquido sinoviale indicative della presenza di patologie riguardanti sia la struttura che la funzionalità articolare.
70253	LIQUOR CEFALO-RACHIDIANO	Il liquor o liquido cefalorachidiano (LCR) è un liquido incolore e limpido (come l’acqua) che scorre intorno all’encefalo ed al midollo spinale sorreggendoli e proteggendoli. L’analisi del LCR comprende una serie di test necessari alla valutazione dell’aspetto e alla misura delle sostanze in esso presenti, al fine di diagnosticare varie possibili patologie/condizioni a carico del sistema nervoso centrale.
70255	LITIEMIA	Il litio (per esempio Carbolithium®) è un farmaco usato per il trattamento di malattie psichiatriche come il disturbo bipolare, mania acuta e altri disturbi dell’umore. Questo esame del sangue viene utilizzato per trovare la dose adatta al paziente, soprattutto all’inizio della terapia e periodicamente durante il trattamento.
70257	MAGNESIO	Il magnesio è un minerale presente in tutte le cellule dell’organismo ed è coinvolto in molti processi vitali quali la produzione di energia a livello cellulare, la contrazione muscolare, la trasmissione dell’impulso nervoso, la mineralizzazione e lo sviluppo delle ossa. Il magnesio viene assunto essenzialmente con la dieta; viene assorbito nelL’intestino tenue e nel colon e immagazzinato nelle ossa, cellule e tessuti. Tuttavia, questo test è utile per la valutazione dello stato complessivo di salute di una persona
70259	MAGNESIO URINARIO	Il magnesio è un minerale presente in tutte le cellule dell’organismo ed è coinvolto in molti processi vitali quali la produzione di energia a livello cellulare, la contrazione muscolare, la trasmissione dell’impulso nervoso, la mineralizzazione e lo sviluppo delle ossa. Il magnesio viene assunto essenzialmente con la dieta; viene assorbito nelL’intestino tenue e nel colon e immagazzinato nelle ossa, cellule e tessuti. Tuttavia, questo test è utile per la valutazione dello stato complessivo di salute di una persona. Effettuato attraverso L’analisi di un campione urinario.
70261	METANEFRINE URINARIE (24H)	L’esame misura la concentrazione di metanefrine e normetanfrina escrete nell’urina in un periodo di 24 ore. Le metanefrine e la normetanefrina sono metaboliti inattivi delle catecolamine epinefrina (adrenalina) e norepinefrina (noradrenalina). Le catecolamine sono un gruppo di ormoni prodotti dalla midollare (porzione interna) delle ghiandole surrenali, piccoli organi di forma triangolare localizzati sopra i reni.
70263	MICETI ESPETTORATO	Uno dei quattro maggiori gruppi di microorganismi (batteri, virus, parassiti e funghi) presenti in natura, cui appartengono i lieviti (piccole strutture unicellulari simili ai batteri) o le muffe (larghe forme filamentose visibili ad occhio nudo).
70265	MICETI FECI	Uno dei quattro maggiori gruppi di microorganismi (batteri, virus, parassiti e funghi) presenti in natura, cui appartengono i lieviti (piccole strutture unicellulari simili ai batteri) o le muffe (larghe forme filamentose visibili ad occhio nudo). Analisi effettuata su un campione di feci.
70267	MICETI LIQUIDO SEMINALE	Uno dei quattro maggiori gruppi di microorganismi (batteri, virus, parassiti e funghi) presenti in natura, cui appartengono i lieviti (piccole strutture unicellulari simili ai batteri) o le muffe (larghe forme filamentose visibili ad occhio nudo). Analisi effettuata su un campione di liquido seminale
70269	MICETI TAMPONE	Uno dei quattro maggiori gruppi di microorganismi (batteri, virus, parassiti e funghi) presenti in natura, cui appartengono i lieviti (piccole strutture unicellulari simili ai batteri) o le muffe (larghe forme filamentose visibili ad occhio nudo). Analisi effettuata tramite tampone.
70435	SCOTCH – TEST	Lo "Scotch test" deriva il suo nome dal caratteristico modo di “prelievo del campione”, attraverso un nastro adesivo che va fatta aderire alla zona interessata (perianale) per poi trasferirlo su un vetrino da laboratorio per la successiva lettura al microscopio.
70271	MICETI TAMPONE RETTALE	Uno dei quattro maggiori gruppi di microorganismi (batteri, virus, parassiti e funghi) presenti in natura, cui appartengono i lieviti (piccole strutture unicellulari simili ai batteri) o le muffe (larghe forme filamentose visibili ad occhio nudo). Analisi effettuata tramite tampone rettale.
70273	MICETI TAMPONE VAGINALE	Uno dei quattro maggiori gruppi di microorganismi (batteri, virus, parassiti e funghi) presenti in natura, cui appartengono i lieviti (piccole strutture unicellulari simili ai batteri) o le muffe (larghe forme filamentose visibili ad occhio nudo). Analisi effettuata tramite tampone vaginale.
70275	MICETI URINE	Uno dei quattro maggiori gruppi di microorganismi (batteri, virus, parassiti e funghi) presenti in natura, cui appartengono i lieviti (piccole strutture unicellulari simili ai batteri) o le muffe (larghe forme filamentose visibili ad occhio nudo). Analisi effettuata su un campione di urine.
70277	MICOBATTERI (B. KOCH) ESAME COLTURALE	Gruppo di batteri di forma di bastoncino (bacilli) che includono il Mycobacterium tuberculosis (responsabile della tubercolosi) e il Mycobacterium leprae (responsabile della lebbra), oltre che di più di 100 specie presenti nell’ambiente. I micobatteri possono essere distinti in “non tubercolari”, “tubercolari” e/o atipici.
70279	MICOPLASMI LIQ. SEMINALE	I micoplasmi sono i più piccoli microrganismi liberi viventi conosciuti. Possono essere presenti insieme alla normale flora batterica residente nella gola, nelle vie aeree superiori e nel tratto urogenitale. I micoplasmi sono diversi da altri tipi di batteri per molti aspetti e possono essere difficili da crescere in coltura e da identificare. Il test del micoplasma è utilizzato per determinare la presenza di un’infezione recente o attiva operata da questo microrganismo.
70281	MICROALBUMINURIA	La presenza di piccole quantità di albumina nelle urine (microalbuminuria) può comparire negli stadi molto precoci della nefropatia diabetica e può fungere quindi da campanello d’allarme per questa temibile complicanza. La microalbuminuria viene rilevata attraverso un apposito test che ne dosa la quantità.
70283	MIOGLOBINA	La mioglobina è una piccola proteina presente nel muscolo scheletrico e cardiaco, in grado di legare l’ossigeno. La sua funzione consiste nell’intrappolare all’interno del tessuto muscolare l’ossigeno che viene utilizzato dalle cellule per la produzione dell’energia necessaria alla contrazione. Il danneggiamento del tessuto muscolare cardiaco o scheletrico, comporta il rilascio della mioglobina nel circolo ematico. Dopo poche ore da un trauma di questo tipo, è possibile quindi rilevare nel sangue grandi quantità di mioglobina.
70285	MORBILLO ANTICORPI	Il virus del morbillo (rubeola) e il virus della parotite sono virus appartenenti alla famiglia dei Paramyxoviridae. Entrambi causano delle infezioni che di solito si risolvono spontaneamente entro pochi giorni; tuttavia possono causare complicanze gravi, motivo per il quale sono inseriti all’interno dei protocolli di vaccinazione. Gli esami diagnostici per il morbillo e la parotite comprendono: i metodi molecolari (PCR, polymerase chain reaction) per la ricerca del materiale genetico virale (RNA) e i test sierologici per la ricerca di anticorpi anti-virus del morbillo o della parotite.
70287	MYCOBATTERIUM TUBERCOLOSI DNA	Il Mycobacterium tuberculosis (chiamato anche bacillo di Koch) appartenente alla famiglia delle Mycobacteriacee, famiglia di batteri gram-variabili. È il bacillo responsabile della tubercolosi nelL’uomo. Analisi effettuata su campione di DNA.
70289	MYCOPLASMA PNEUMONIAE	I micoplasmi sono i più piccoli microrganismi liberi viventi conosciuti. Possono essere presenti insieme alla normale flora batterica residente nella gola, nelle vie aeree superiori e nel tratto urogenitale. I micoplasmi sono diversi da altri tipi di batteri per molti aspetti e possono essere difficili da crescere in coltura e da identificare. Il test del micoplasma è utilizzato per determinare la presenza di un’infezione recente o attiva operata da questo microrganismo.
70291	NORADRENALINA PLASMATICA	La noradrenalina, o norepinefrina che dir si voglia, è un ormone sintetizzato nella porzione interna (midollare) del surrene, nonché un neurotrasmettitore prodotto dal sistema nervoso centrale e periferico. La maggior parte della noradrenalina circolante proviene proprio dalle terminazioni nervose, mentre a livello surrenale viene sintetizzata prevalentemente adrenalina (il 90% delle cellule della midollare del surrene sono specializzate nella sua sintesi). La noradrenalina viene rilasciata in maniera importante e sovrafisiologica in risposta ad un severo stress fisico o psicologico, come un’importante emorragia, una riduzione delle concentrazioni ematiche di glucosio, ferite traumatiche, interventi chirurgici o esperienze paurose.
70293	NUMERO di DIBUCAINA	La colinesterasi (pseudocolinesterasi) è presente nel fegato, pancreas, cuore, sangue e cervello.In ambito clinico viene utilizzata per monitorare la funzionalità epatica e in quello preoperatorio può consentire di individuare i pazienti portatori di forme atipiche. Le forme atipiche vengono distinte in base alla resistenza all’inibizione da dibucaina, da fluoruro e alla completa assenza di attività della colinesterasi. Nel caso della dibucaina viene misurata l’attività della colinesterasi totale nel siero e si determina il NUMERO DI DIBUCAINA che rappresenta la percentuale di inibizione dell’ attività dell’enzima in presenza del farmaco
70295	OMOCISTEINA	L’omocisteina è un aminoacido presente normalmente in quantità piccolissime nelle cellule dell’organismo. Questo perché l’omocisteina prodotta all’interno delle cellule viene rapidamente metabolizzata e convertita in altri prodotti. Le vitamine B6, B12 e i folati sono essenziali per questi processi di conversione perciò un deficit di queste vitamine può essere evidenziato da un incremento dell’omocisteina plasmatica. Questo esame misura la quantità di omocisteina a livello plasmatico o urinario.
70297	ORMONE ANTIMULLERIANO	L’ormone anti-Mülleriano (AMH) è prodotto dai tessuti riproduttivi, ovvero i testicoli nei maschi e le ovaie nelle femmine. Il ruolo dell’AMH e la sua concentrazione variano in relazione al sesso e all’età. Questo test misura la concentrazione dell’AMH nel sangue.
70299	OSTEOCALCINA	L’osso è una struttura rigida composta perlopiù da tessuto connettivo duro, e costituisce la maggior parte dello scheletro umano. Si tratta di un tessuto vivo e continuamente in crescita e rimodellamento, con un tasso di ricambio di circa il 10% annuo. I marcatori ossei presenti nel sangue e nelle urine rilevano i prodotti del rimodellamento osseo, fornendo un’indicazione riguardo la velocità di riassorbimento e neoformazione ossea, e riguardo la sua eventuale alterazione, indice di una malattia ossea.
70301	PARASSITI INTESTINALI	L’esame parassitologico delle feci è utile per la diagnosi di parassitosi intestinale, che si manifesta con diarrea, crampi, occlusione intestinale, dimagrimento e anemia. L’esame viene effettuato attraverso la ricerca nelle feci dei parassiti e delle loro uova: elminti (uova e/o larve), protozoi come Giardia intestinalis, coccidi (Cryptosporidium parvum, Isospora belli, Cyclospora cayetanensis).
70303	PARATORMONE	Questo test misura la quantità di PTH nel sangue. L’ormone paratiroideo (PTH) è implicato nel mantenimento del corretto livello di calcio nel sangue. È parte di un sistema a feedback che comprende calcio, PTH, vitamina D, e, in una certa misura, fosforo (fosfato) e magnesio. Qualsiasi condizione clinica o patologia in grado di sconvolgere questo sistema, può causare aumenti o diminuzioni inappropriate dei livelli di calcio e del PTH e quindi determinare la comparsa di sintomi di ipercalcemia o ipocalcemia.
70305	PARVOVIRUS B19 IGG	Il Parvovirus B19 è un virus in grado di causare uno stato di malessere tipico dell’età pediatrica, noto con il nome di quinta malattia o eritema infettivo. Il virus può essere facilmente trasmesso tramite le gocce di saliva e quindi tramite lo stretto contatto con persone portatrici. La trasmissione può anche avvenire per via materno fetale da madre infetta o tramite l’esposizione a sangue o emoderivati infetti.
70307	PARVOVIRUS B19 IGM	Il Parvovirus B19 è un virus in grado di causare uno stato di malessere tipico dell’età pediatrica, noto con il nome di quinta malattia o eritema infettivo. Il virus può essere facilmente trasmesso tramite le gocce di saliva e quindi tramite lo stretto contatto con persone portatrici. La trasmissione può anche avvenire per via materno fetale da madre infetta o tramite l’esposizione a sangue o emoderivati infetti.
70309	PERTOSSE IGG	La pertosse è un’infezione delle vie respiratorie causata dal batterio Bordetella pertussis. Questo batterio è altamente contagioso e può essere trasmesso da una persona all’altra tramite gocce di saliva o per contatto ravvicinato. Il test della pertosse viene effettuato per rilevare e diagnosticare un’infezione operata da B. pertussis.
70311	PERTOSSE IGM	La pertosse è un’infezione delle vie respiratorie causata dal batterio Bordetella pertussis. Questo batterio è altamente contagioso e può essere trasmesso da una persona all’altra tramite gocce di saliva o per contatto ravvicinato. Il test della pertosse viene effettuato per rilevare e diagnosticare un’infezione operata da B. pertussis.
70313	PIASTRINE	Le piastrine, anche chiamate trombociti, sono piccoli frammenti di cellule essenziali per la normale coagulazione del sangue. Essi sono formate a partire da cellule molto grandi chiamate megacariociti, nel midollo osseo e sono rilasciate nel circolo sanguigno. La conta piastrinica è il test che determina il numero di piastrine del sangue del paziente.
70315	POTASSIO EMATICO	Questo esame misura la concentrazione di potassio nel sangue. Il potassio è un elettrolita di vitale importanza per il metabolismo cellulare e la funzionalità muscolare. Il potassio, insieme ad altri elettroliti come sodio, cloro e bicarbonato (o CO2 totale), partecipa alla regolazione della quantità di liquidi nell’organismo, stimola la contrazione muscolare e contribuisce al mantenimento dell’equilibrio acido- base. Il potassio è presente in tutti i liquidi biologici, ma è maggiormente concentrato all’interno delle cellule.
70317	POTASSIO URINARIO	Questo esame misura la concentrazione di potassio nelle urine. Il potassio è un elettrolita di vitale importanza per il metabolismo cellulare e la funzionalità muscolare. Il potassio, insieme ad altri elettroliti come sodio, cloro e bicarbonato (o CO2 totale), partecipa alla regolazione della quantità di liquidi nell’organismo, stimola la contrazione muscolare e contribuisce al mantenimento dell’equilibrio acido- base. Il potassio è presente in tutti i liquidi biologici, ma è maggiormente concentrato all’interno delle cellule.
70319	PRELIEVO DI SANGUE VENOSO	Analisi del sangue effettuata su un campione di sangue venoso.
70421	PROCALCITONINA	l test misura la quantità di procalcitonina nel sangue. La procalcitonina è una proteina prodotta da molti tipi di cellule dell’organismo, spesso in risposta ad infezioni batteriche ma anche in seguito a danno tissutale. I livelli di procalcitonina nel sangue aumentano rapidamente e considerevolmente in corso d’infezioni batteriche sistemiche e sepsi.
70423	PROFILO AUTOANTICORPI	Gli autoanticorpi sono anticorpi (proteine) in grado di riconoscere e reagire erroneamente con tessuti e organi dell’organismo di appartenenza, cosiddetti “self”. Il sistema immunitario è normalmente in grado di discriminare tra gli elementi estranei all’organismo (non-self) e quelli appartenenti allo stesso (self), innescando una risposta immunitaria solamente in caso di esposizione a elementi “non-self”, come batteri o virus. Nel caso in cui il meccanismo di discriminazione tra elementi “self” e “non-self” cessi di funzionare correttamente, il sistema immunitario produce anticorpi diretti contro le cellule e i tessuti dell’organismo di appartenenza. L’azione degli autoanticorpi scatena delle risposte infiammatorie inappropriate, con conseguente danneggiamento e/o disfunzione d’organo o sistemica e comparsa dei caratteristici segni e sintomi delle patologie autoimmuni.
70425	PROGESTERONE	Il progesterone è un ormone  implicato nella preparazione dell’organismo femminile alla gravidanza ed esplica il suo ruolo in associazione ad altri ormoni femminili. Questo esame misura la concentrazione di progesterone nel sangue.
70427	PROLATTINA	La prolattina è un ormone che svolge un ruolo fondamentale nella promozione della produzione di latte da parte delle ghiandole mammarie ed è normalmente elevato in gravidanza e durante il puerperio. Nelle donne non in gravidanza e negli uomini è normalmente presente in quantità limitata. Questo esame misura la quantità di prolattina nel sangue.
70429	PROTEINA C COAGULATIVA	La proteina C coagulativa è un fattore che partecipa alla formazione dei coaguli, limitandone L’estensione. Questo enzima è normalmente presente nel sangue, ma la sua attività o la sua quantità possono risultare carenti per vari motivi. Un deficit della proteina C può derivare, ad esempio, da fattori congeniti, iperconsumo, carenza di vitamina K, assunzione di estroprogestinici o alti livelli di estradiolo per induzione delL’ovulazione.
70431	PROTEINA C REATTIVA	La proteina C reattiva (PCR) è una proteina di fase acuta, prodotta dal fegato e rilasciata in circolo entro poche ore dal danno tissutale, dall’inizio dell’infezione o da altre cause di infiammazione. Un aumento importante si osserva, ad esempio, dopo un trauma o un infarto, a seguito di patologie autoimmuni attive o fuori controllo, e con un’infezione batterica grave come la sepsi. La concentrazione di PCR può aumentare anche di centinaia di volte in risposta ad una patologia infiammatoria. L’aumento della proteina nel sangue può precedere dolore, febbre o altri indicatori clinici. Questo esame misura la quantità di PCR nel sangue e può essere di supporto nella determinazione dell’infiammazione dovuta ad una condizione acuta o nel monitoraggio del decorso di una patologia cronica.
70433	Proteina S Coagulativa	La proteina S è un importante fattore anticoagulante, normalmente presente nel sangue. Quest’elemento coopera con la proteina C coagulativa durante il processo di formazione dei coaguli, controllandone L’estensione e contrastando la tendenza a sviluppare fenomeni tromboembolici. Analisi effettuata su campione di sangue
70437	SCOTCH TEST PER MICETI	Lo "Scotch test" deriva il suo nome dal caratteristico modo di “prelievo del campione”, attraverso un nastro adesivo che va fatta aderire alla zona interessata (perianale) per poi trasferirlo su un vetrino da laboratorio per la successiva lettura al microscopio.
70439	SCRAPING UNGUEALE (MICETI)	Prelevamento di campione ungueale
70441	SCREENING INALANTI PHADIATOP	L’esame allergologico per inalanti (prick test) è un test compiuto sulla cute che ha il fine di verificare la reazione ad alcune sostanze che possano provocare allergie respiratorie (asma e rinite allergica) come polline, polvere o acari, farmaci.
70443	SELENIO	L’importanza del selenio risiede nelL’essere un insostituibile costituente delL’enzima glutatione perossidasi ,attivo nel ridurre lo stress ossidativo.Il deficit di selenio si riscontra in casi di malnutrizione o inadeguata nutrizione parenterale. Analisi effettuata attraverso un campione di sangue
70445	SHBG	La globulina legante gli ormoni sessuali (SHBG) è una proteina prodotta nel fegato in grado di legare il testosterone, il diidrotestosterone (DHT) e l’estradiolo (estrogeno) e di trasportarli in forma inattiva nel circolo sanguigno. Le variazioni dei livelli di SHBG possono quindi influenzare la quantità di ormoni biodisponibili. Il test misura la quantità di SHBG nel sangue ed è spesso utilizzato per valutare la carenza o L’eccesso di testosterone.
70448	SIEROGLUTINAZIONE DI WIDAL WRIGHT	IL test di Widal è un utile sostegno diagnostico nel caso in cui sia sospettata la presenza di una malattia infettiva caratterizzata da febbre persistente, nota come febbre enterica o tifoide, causata dal batterio Salmonella typhi. Questa malattia è maggiormente diffusa nelle aree del mondo nelle quali le condizioni igieniche sono più scarse, e può essere contratta tramite L’ingestione di cibi e bevande contaminate.
70451	SODIO	Lo scopo del dosaggio del Sodio è quello di valutare l’equilibrio liquidi-elettroliti ed acido base e le funzioni correlate, renale, surrenale e neuromuscolare.
70453	SPERMIOGRAMMA	Lo spermiogramma misura la quantità e la qualità del liquido rilasciato durante l’eiaculazione. Valuta sia la porzione liquida, chiamata liquido seminale, e al microscopio, le cellule mobili chiamate spermatozoi. Questo esame è spesso utilizzato nella valutazione dell’infertilità maschile.
70455	STREPTOC.PYOGENES TAMPONE FARINGEO	Questo test identifica la presenza del batterio Streptococcus pyogenes, noto anche come Streptococco β-emolitico di gruppo A (SBEA) o Streptococco di gruppo A. Questo streptococco è la causa batterica più frequente dell’infiammazione e/o dolore della parte posteriore della gola (faringite), chiamato comunemente “mal di gola”. Analisi effettuata attraverso tampone faringeo
70457	STREPTOC.PYOGENES TAMPONE RAPIDO	Questo test identifica la presenza del batterio Streptococcus pyogenes, noto anche come Streptococco β-emolitico di gruppo A (SBEA) o Streptococco di gruppo A. Questo streptococco è la causa batterica più frequente dell’infiammazione e/o dolore della parte posteriore della gola (faringite), chiamato comunemente “mal di gola”. Analisi effettuata attraverso tampone
70459	STREPTOCOCCO AGALACTIAE TAMP. VAG.	Lo Streptococcus agalactiae, noto anche come Streptococco β-emolitico di gruppo B (SGB), è un batterio molto comune che colonizza il tratto intestinale e genitale. Raramente causa problemi negli adulti sani ma può causarne nei bambini appena nati. Il test rileva la presenza di questi batteri nel tratto genitale/rettale delle donne in gravidanza. Analisi effettuata attraverso tampone vaginale.
70461	STREPTOCOCCO GRUPPO B TAMP. RETTALE	Lo Streptococcus agalactiae, noto anche come Streptococco β-emolitico di gruppo B (SGB), è un batterio molto comune che colonizza il tratto intestinale e genitale. Raramente causa problemi negli adulti sani ma può causarne nei bambini appena nati. Il test rileva la presenza di questi batteri nel tratto genitale/rettale delle donne in gravidanza. Analisi effettuata attraverso tampone rettale.
70463	STREPTOZYME	Lo Streptozyme test rivela la presenza di anticorpi contro alcuni antigeni esocellulari (esotossine ed esoenzimi) dello streptococco beta-emolitico A: - streptolisina - ialuronidasi - streptochinasi - DNA-asi - NAD-asi La positività >300 è indice di infezione streptococcica recente e richiede la titolazione dei singoli anticorpi. La negatività (<100) esclude un´ infezione streptococcica recente.
70465	STRONGYLOIDES NELLE FECI	La strongiloidiasi è un’infezione causata dal nematode Strongyloides stercoralis, che penetra nel corpo quando la pelle nuda entra in contatto con il terreno contaminato dal verme. Analisi effettuata su campione di feci
70467	TAMPONE AURICOLARE	Un tampone auricolare(esame colturale della ferita) è un test che permette di rilevare ed identificare i batteri patogeni (responsabili dell’infezione) presenti in una ferita. Qualsiasi ferita può essere infettata da una grande varietà di batteri. L’esame colturale permette di rilevare un’eventuale infezione, identificare il tipo di batterio responsabile e guidare le scelte terapeutiche verso trattamenti mirati.
70469	TAMPONE CHIRURGICO	Un tampone chirurgico (esame colturale della ferita) è un test che permette di rilevare ed identificare i batteri patogeni (responsabili dell’infezione) presenti in una ferita. Qualsiasi ferita può essere infettata da una grande varietà di batteri. L’esame colturale permette di rilevare un’eventuale infezione, identificare il tipo di batterio responsabile e guidare le scelte terapeutiche verso trattamenti mirati.
70471	TAMPONE CUTANEO	Un tampone cutaneo (esame colturale della ferita) è un test che permette di rilevare ed identificare i batteri patogeni (responsabili dell’infezione) presenti in una ferita. Qualsiasi ferita può essere infettata da una grande varietà di batteri. L’esame colturale permette di rilevare un’eventuale infezione, identificare il tipo di batterio responsabile e guidare le scelte terapeutiche verso trattamenti mirati.
70473	TAMPONE DEL CAVO ORALE	Un tampone del cavo orale (esame colturale della ferita) è un test che permette di rilevare ed identificare i batteri patogeni (responsabili dell’infezione) presenti in una ferita. Qualsiasi ferita può essere infettata da una grande varietà di batteri. L’esame colturale permette di rilevare un’eventuale infezione, identificare il tipo di batterio responsabile e guidare le scelte terapeutiche verso trattamenti mirati.
70475	TAMPONE FARINGEO	Un tampone cutaneo (esame colturale della ferita) è un test che permette di rilevare ed identificare i batteri patogeni (responsabili dell’infezione) presenti in una ferita. Qualsiasi ferita può essere infettata da una grande varietà di batteri. L’esame colturale permette di rilevare un’eventuale infezione, identificare il tipo di batterio responsabile e guidare le scelte terapeutiche verso trattamenti mirati.
70515	TREPONEMA	Il termine "Treponema" indica un genere di batteri a cui appartengono due specie patogene differenti, Treponema pallidum e Treponema carateum, microorganismi coinvolti rispettivamente nelL’insorgenza della sifilide e della pinta.
70477	TAMPONE LINGUALE	Un tampone linguale (esame colturale della ferita) è un test che permette di rilevare ed identificare i batteri patogeni (responsabili dell’infezione) presenti in una ferita. Qualsiasi ferita può essere infettata da una grande varietà di batteri. L’esame colturale permette di rilevare un’eventuale infezione, identificare il tipo di batterio responsabile e guidare le scelte terapeutiche verso trattamenti mirati.
70479	TAMPONE NASALE	Un tampone nasale (esame colturale della ferita) è un test che permette di rilevare ed identificare i batteri patogeni (responsabili dell’infezione) presenti in una ferita. Qualsiasi ferita può essere infettata da una grande varietà di batteri. L’esame colturale permette di rilevare un’eventuale infezione, identificare il tipo di batterio responsabile e guidare le scelte terapeutiche verso trattamenti mirati.
70481	TAMPONE OCULARE	Un tampone oculare (esame colturale della ferita) è un test che permette di rilevare ed identificare i batteri patogeni (responsabili dell’infezione) presenti in una ferita. Qualsiasi ferita può essere infettata da una grande varietà di batteri. L’esame colturale permette di rilevare un’eventuale infezione, identificare il tipo di batterio responsabile e guidare le scelte terapeutiche verso trattamenti mirati.
70483	TAMPONE RETTALE	Un tampone rettale (esame colturale della ferita) è un test che permette di rilevare ed identificare i batteri patogeni (responsabili dell’infezione) presenti in una ferita. Qualsiasi ferita può essere infettata da una grande varietà di batteri. L’esame colturale permette di rilevare un’eventuale infezione, identificare il tipo di batterio responsabile e guidare le scelte terapeutiche verso trattamenti mirati.
70485	TAMPONE TONSILLARE	Un tampone tonsillare (esame colturale della ferita) è un test che permette di rilevare ed identificare i batteri patogeni (responsabili dell’infezione) presenti in una ferita. Qualsiasi ferita può essere infettata da una grande varietà di batteri. L’esame colturale permette di rilevare un’eventuale infezione, identificare il tipo di batterio responsabile e guidare le scelte terapeutiche verso trattamenti mirati.
70487	TAMPONE URETRALE	Un tampone uretrale (esame colturale della ferita) è un test che permette di rilevare ed identificare i batteri patogeni (responsabili dell’infezione) presenti in una ferita. Qualsiasi ferita può essere infettata da una grande varietà di batteri. L’esame colturale permette di rilevare un’eventuale infezione, identificare il tipo di batterio responsabile e guidare le scelte terapeutiche verso trattamenti mirati.
70489	TAMPONE VAGINALE	Un tampone vaginale(esame colturale della ferita) è un test che permette di rilevare ed identificare i batteri patogeni (responsabili dell’infezione) presenti in una ferita. Qualsiasi ferita può essere infettata da una grande varietà di batteri. L’esame colturale permette di rilevare un’eventuale infezione, identificare il tipo di batterio responsabile e guidare le scelte terapeutiche verso trattamenti mirati.
70491	TELOPEPTIDE C (CTX)	Il test viene effettuato per la valutazione ed il monitoraggio del tasso di riassorbimento e formazione ossea; per il monitoraggio di alcune malattie metaboliche ossee come l’osteoporosi; per rilevare alcuni disordini metabolici ossei come la malattia di Paget
70493	TEMPO DI PROTROMBINA	Il tempo di protrombina (PT) è la misura del tempo necessario alla formazione di un coagulo, in un campione di sangue. L’INR (International Normalized Ratio) è una modalità di calcolo utilizzata per esprimere il PT in maniera normalizzata, a prescindere dallo strumento e dal reagente utilizzato e viene utilizzato nel monitoraggio dei pazienti in terapia con alcuni farmaci anticoagulanti orali ad azione anti-vitamina K (ad es. il warfarin).
70495	TEST DI GRAVIDANZA	Il test di gravidanza è uno strumento diagnostico in grado di accertare L’avvenuto concepimento; questa valutazione può essere eseguita qualora vi sia un ritardo nella comparsa delle mestruazioni.
70497	TEST PER MONONUCLEOSI	La mononucleosi infettiva, comunemente chiamata mononucleosi, è un’infezione causata dal virus di Epstein- Barr (EBV). Questo esame determina la presenza di proteine nel sangue chiamate anticorpi eterofili che sono prodotti dal sistema immunitario in risposta all’infezione da EBV.
70499	TESTOSTERONE FREE	Il testosterone è un ormone steroideo (androgeno) prodotto da particolari tessuti endocrini (cellule di Leydig) nei testicoli maschili. E’ inoltre prodotto dalle ghiandole surrenali sia nei maschi che nelle femmine e, in piccola quantità, dalle ovaie nelle donne.
70501	TESTOSTERONE PLASMATICO	Il testosterone è un ormone steroideo (androgeno) prodotto da particolari tessuti endocrini (cellule di Leydig) nei testicoli maschili. E’ inoltre prodotto dalle ghiandole surrenali sia nei maschi che nelle femmine e, in piccola quantità, dalle ovaie nelle donne. Questo esame misura la concentrazione di testosterone nel sangue.
70503	TIREOGLOBULINA	Questo esame misura la concentrazione di tireoglobulina nel sangue. La tireoglobulina è una proteina prodotta dalla tiroide, la ghiandola che regola il metabolismo dell’organismo.
70505	TOXOCARA CANIS	Il genere Toxocara raggruppa alcune specie di parassiti responsabili di zoonosi (in quanto gli ospiti definitivi di questi nematodi sono rappresentati da animali come il cane e gatto); possono anche infettare L’uomo per ingestione di uova che si ritrovano in acqua o cibo contaminato da feci di animali infetti.
70507	TOXOPLASMA IGG	La toxoplasmosi è un’infezione causata da un parassita chiamato Toxoplasma gondii. Gli esami possono essere di tipo sierologico o molecolare. Il test sierologico rileva gli anticorpi prodotti in risposta all’infezione e, sulla base del tipo di anticorpi presenti (IgM o IgG), L’infezione può essere identificata come attiva o pregressa. Analisi effettuata su campione di sangue
70509	TOXOPLASMA IMMUNOBL. IGA	Le immunoglobuline sono una parte fondamentale del sistema immunitario. Sono proteine prodotte da specifiche cellule immunitarie, chiamate plasmacellule, in risposta a batteri, virus, microrganismi e altre sostanze riconosciute dall’organismo come antigeni estranei (“non- self”) e pericolosi. Questo esame misura la concentrazione delle immunoglobuline G, A ed M (IgG, IgA, IgM) nel sangue e, in alcune circostanze, nel liquor o nella saliva.
70511	TRANSGLUTAMINASI	La transglutaminasi è un enzima che viene attivato dalla trombina. Questo forma legami covalenti tra le molecole di fibrina. NelL’organismo umano si riscontrano otto diverse transglutaminasi, le cui funzioni vanno dalla coagulazione del sangue alla costruzione di capelli e pelle.
70513	TRASFERRINA DESIALATA (CDT)	La transferrina desialata (CDT) è un test di abuso alcolico di medio-lungo periodo; non deve perciò essere confuso con il test alcolemico eseguito con l’etilometro (il cosiddetto test del palloncino) e utilizzato dalle Forze dell’Ordine per verificare se il conducente di un autoveicolo è in stato di ebbrezza; con l’etilometro infatti si misura il valore dell’alcolemia, ovvero la concentrazione di alcol (etanolo) presente nel sangue.
70519	TRIPTASI	La triptasi è un enzima rilasciato dai mastociti, insieme all’istamina e ad altre sostanze chimiche, in seguito alla normale attivazione immunitaria e alle risposte allergiche da iperesensibilità. Questo test misura la quantità di triptasi nel sangue.
70521	TROPONINA	Le troponine sono una famiglia di proteine presenti nei muscoli scheletrico e cardiaco (miocardio), implicate nel meccanismo di contrazione muscolare. I test delle troponine misurano la concentrazione nel sangue delle troponine cardiache (specifiche del cuore) come supporto alla diagnosi di danno cardiaco.
70523	TSH	L’ormone tireostimolante (TSH), o tireotropina, è prodotto dall’ipofisi, un piccolo organo situato alla base delL’encefalo. Il TSH stimola il rilascio nel sangue da parte della tiroide degli ormoni tiroxina (T4) e triiodotironina (T3). Questo esame misura la concentrazione di TSH nel sangue.
70525	URINOCOLTURA	Il test colturale delle urine determina e identifica batteri e lieviti nelle urine.
70527	VARICELLA HERPES ZOSTER	L’esame viene fatto er la diagnosi, se necessario, di varicella o Herpes Zoster attiva, recente o pregressa; per verificare la presenza di immunizzazione al Virus della Varicella Zoster (VZV) o per verificare il rischio di riattivazione del VZV prima della somministrazione di farmaci immunosoppressivi.
70529	VIRUS EPATITE B ANTIGENE HBsAg IMMUNOBL.	L’epatite B è una patologia causata dalL’infezione operata dal virus HBV. I test per l’epatite B rilevano la presenza proteine virali (antigeni),di  anticorpi prodotti in risposta all’infezione o del materiale genetico (DNA) del virus. La valutazione complessiva delL’insieme dei risultati dei test consente di discriminare tra un’infezione attiva e l’immunità risultante da una precedente esposizione o vaccinazione.
70531	VITAMINA B 12	La vitamina B12 (o cianocobalamina), è una vitamina idrosolubile, indispensabile per sintetizzare il DNA e quindi necessaria per la crescita e lo sviluppo dell’intero organismo, dal sistema nervoso alla produzione di globuli rossi nel midollo osseo. Si trova solo nelle proteine di origine animale. L’esame della vitamina B12 si effettua attraverso un prelievo di sangue da una vena del braccio.
70533	VITAMINA D TOTALE	Vitamina D è un termine generico che comprende diverse molecole liposolubili coinvolte sull’assorbimento del calcio ed è fondamentale per la crescita e la mineralizzazione delle ossa. Una carenza della vitamina D (25 OH) si rileva nel rachitismo infantile, nell’osteoporosi, nella menopausa, nella osteomalacia, in gravidanza, nella fase della crescita, nelle persone anziane, nei malassorbimenti, nell’insufficienza epatica. L’esame si effettua attraverso un prelievo di sangue da una vena del braccio.
\.


--
-- TOC entry 2897 (class 0 OID 19123)
-- Dependencies: 197
-- Data for Name: farmaci; Type: TABLE DATA; Schema: progettoipw; Owner: postgres
--

COPY progettoipw.farmaci (idfarmaco, nomefarmaco) FROM stdin;
45795	TIBOLONE ARISTO
45595	CEFTRIAXONE QILU
46666	LECROSINE
46248	SITAGLIPTIN BIOGARAN
46616	PALIPERIDONE SANDOZ
46709	HLAUPNEF
45579	LIDOCAINA AGUETTANT
45592	LOPERAMIDE AUROBINDO
45914	PANTOPRAZOLO ALTAN
46055	TOCANTRI
46463	BRONCHIPRET
45969	PALIPERIDONE KRKA
45374	VILDAGLIPTIN E METFORMINA SANDOZ
44044	PEMETREXED SUN
44577	ROSUVASTATINA SUN
44636	BORTEZOMIB TEVA
8783	ESAFOSFINA
42279	TELMISARTAN RANBAXY
41693	ENALAPRIL RANBAXY ITALIA
40538	DONEPEZIL RANBAXY
40842	VALSARTAN E IDROCLOROTIAZIDE RANBAXY
40880	OMEPRAZOLO RANBAXY ITALIA
40938	RABEPRAZOLO RANBAXY
40154	SILDENAFIL RANBAXY
39449	IMIPENEM E CILASTATINA RANBAXY
28850	TIOREDOX
36600	SERTRALINA RANBAXY
37034	AMOXICILLINA E ACIDO CLAVULANICO RANBAXY
37134	SIMVASTATINA RANBAXY
37245	DOXAZOSINA RANBAXY
37379	LUMINITY
37527	CLARITROMICINA RANBAXY
41535	LABIRIAD
40822	LATANOPROST E TIMOLOLO ZENTIVA
40952	FINASTERIDE TEVA GENERICS
33984	SODIO VALPROATO SANOFI
33070	PARACETAMOLO ZENTIVA ITALIA
33178	TAREG
17389	PREPARAZIONE H
20465	LASIX FIALE
25275	DILADEL
25278	TILDIEM
26922	ROSSITROL
23105	DEPAMIDE
23635	PARACETAMOLO ZENTIVA
23770	LASITONE
46104	EFAVIRENZ/EMTRICITABINA/TENOFOVIR DISOPROXIL KRKA
43571	PREGABALIN RANBAXY
35932	ACTRAPID
26667	SYSCOR
27857	CLARILAX STITICHEZZA
24507	TETRIZOLINA BOUTY
44932	ONGENTYS
36196	REYATAZ
46130	HEMLIBRA
45445	MAVIRET
39729	VISUCORTEX
36515	TICOVAC
37028	EDEVEN C.M.
37994	FOSFOMICINA MYLAN
33656	ENANTYUM
33913	DOLAUT
36218	FOROTAN
36497	DOLAUT GOLA
38092	AMLODIPINA DOC GENERICI
44211	DARUNAVIR ZENTIVA
45833	MICOFENOLATO MOFETILE TILLOMED
45055	DAPTOMICINA XELLIA
43323	OCTILIA
42637	OMEGA 3 BOUTY
42324	IBUPROFENE ZENTIVA ITALIA
41259	CANDESARTAN ZENTIVA
29021	PENTAGLOBIN
35783	PARACETAMOLO ZENTIVA LAB
24744	FEIBA
45687	TADALAFIL CIPLA
44495	ROSUVASTATINA ALMUS
3671	DIFTETALL
36458	FRAXODI
22441	ALLOPURINOLO MOLTENI
22593	BRUFEN
45673	COPEMYLTRI
33791	SUBUTEX
32318	CEROXTERIL
23001	LORANS
44977	CLARICYCLIC
45386	IBUPROFENE SANOFI
44322	NEBIVOLOLO E IDROCLOROTIAZIDE DOC GENERICI
43751	PEMETREXED TEVA
43463	EFACTI
42930	SIBILLETTE
42995	IBUPROFENE MYLAN ITALIA
41788	PERINDOPRIL TEVA ITALIA
41599	RABEPRAZOLO ZENTIVA
41340	FOSINOPRIL AUROBINDO
39688	CLARITROMICINA ALMUS
39903	ZORENDOL
37720	CLARITROMICINA SANDOZ
37804	ENALAPRIL IDROCLOROTIAZIDE RATIOPHARM
37805	PERINDOPRIL TEVA GENERICS
36061	FROBENKIDS FEBBRE E DOLORE
20582	FLUIMUCIL
45832	OLMESARTAN E AMLODIPINA DOC
43527	PLEGRIDY
40913	CANDESARTAN MYLAN PHARMA
40969	CANDESARTAN MYLAN GENERICS
36850	LEVEMIR
37045	ROTARIX
37221	BARACLUDE
37631	ENALAPRIL E IDROCLOROTIAZIDE ZENTIVA
37932	LISINOPRIL ZENTIVA
33161	LOETTE
33232	GADRAL
33315	MABTHERA
18610	CARNITENE
20711	PENSTAPHO
24994	IBIFEN
21894	RITMODAN
23673	IMODIUM
43256	MICOFENOLATO MOFETILE TECNIGEN
45834	SUNITINIB TEVA
44269	GHEMAXAN
44039	ENOXAPARINA ROVI
45427	QARZIBA
43903	OCTILIA ALLERGIA E INFIAMMAZIONE
42892	DUTASTERIDE CIPLA
41836	SOLFLU
13046	ENTEROGERMINA
26736	FRAXIPARINA
45246	KOLEROS
43275	ERIOFERT
35410	YVOXID
20157	CERULISINA
42978	ATORVASTATINA AUROBINDO
34949	HERCEPTIN
35494	INOMAX
44500	PRALUENT
40480	ALVAND
29543	VERELAIT
36679	INEGY
38555	CORTIVIS
34525	FERRIPROX
35563	NOVOMIX
41271	ESBRIET
41275	YERVOY
29008	ALPHA D3
35219	XELODA
45495	EFAVIRENZ / EMTRICITABINA / TENOFOVIR DISOPROXIL ZENTIVA
46343	SEGLUROMET
44953	ENDOLUCINBETA
44291	OPDIVO
42682	PERJETA
41876	LAMIVUDINA/ZIDOVUDINA TEVA
30758	HARMONET
20558	ALFATEX
45936	ADYNOVI
45677	LUTATHERA
44559	GENVOYA
44318	EVOTAZ
27830	LACIPIL
45266	TRUXIMA
46342	STEGLUJAN
46339	STEGLATRO
44233	ZYKADIA
43533	GAZYVARO
43447	SYLVANT
43367	DELTYBA
42640	CAPECITABINA ACCORD
39398	AFINITOR
37914	DAFIRO
37920	COPALIA
38008	ESTREVA
25447	DISEON
44290	LUMARK
42647	BETMIGA
37369	QUINAPRIL IDROCLOROTIAZIDE ZENTIVA
37621	RAMIPRIL DOC GENERICI
37684	GESTODIOL
38118	RAMIPRIL E IDROCLOROTIAZIDE PENSA
38470	OLANZAPINA ANGELINI
38650	AZITROERRE
35256	DELAMAN
35433	ESOPRAL
35849	MYOVIEW
35859	SOTALOLO TEVA
36222	ADAPTUS
32944	SEROQUEL
21257	ETAPIAM
26979	VERAX BLU
43544	FLENDIVA
43503	IRBEDIUR
43836	PREGABALIN  EG
43719	PREGABALIN TECNIGEN
43485	CANDESARTAN E IDROCLOROTIAZIDE MYLAN
42545	VALGANCICLOVIR TEVA
42825	OMEGA 3 DOC GENERICI
41993	CANDESARTAN HCS
42564	TORASEMIDE TEVA ITALIA
41715	LUSINELLE
41716	LUSINE
41105	VALSARTAN E IDROCLOROTIAZIDE DOC GENERICI
41230	LEFLUNOMIDE MYLAN
41316	LUTIZ
41399	ATORVASTATINA F.I.R.M.A.
40440	EXITELEV
40508	CANDESARTAN E IDROCLOROTIAZIDE DOC GENERICI
37301	QUINAPRIL ZENTIVA
24665	NEO NEVRAL
25238	CINOBAC
22572	EQUILID
23749	SPIROFUR
24139	DIDROGYL
44718	TRAMADOLO S.A.L.F.
45912	PADOVIEW
40772	LEVOFLOXACINA CLARIS
45034	PREGABALIN LABORATORI EUROGENERICI
44608	FLECTORGO
44744	AMLODIPINA E ATORVASTATINA DOC GENERICI
45301	ROSUVASTATINA PENSA
43508	FENECOX GOLA
44164	DOLSTIP
43690	PREGABALIN HCS
43379	EFAVIRENZ AUROBINDO
28900	CEPIMEX
37867	LENOXE
38474	VASOKINOX
34208	CAMPRAL
34602	IBUFIZZ
35487	DELIPRAMIL
35512	FENEXTRA
35550	BABY RINOLO C.M.
35613	DELTACORTENESOL
36072	FENKID
36478	BABY RINOLO FEBBRE E DOLORE
33486	IDRACAL
10089	DELTACORTENE
12475	MERANKOL
17758	GLUCOPHAGE
\.


--
-- TOC entry 2900 (class 0 OID 19144)
-- Dependencies: 200
-- Data for Name: farmacie; Type: TABLE DATA; Schema: progettoipw; Owner: postgres
--

COPY progettoipw.farmacie (idfarmacia, indirizzo, nomefarmacia, cap, citta, provincia, password, flag_recuperopassword, email) FROM stdin;
404600	Via Noldin 23	PIETRALBA DI GUARDA IGINIO E FIGLIE SNC	39055	LAIVES	BZ	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	bzlvfrm@dayrep.com
409800	Via Portici 46 Laubengasse	AQUILA NERA DI BERTOLINI PAOLO E C. SAS	39100	BOLZANO	BZ	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	bzbzfrm@dayrep.com
410000	Via Dante 10	SAN GIORGIO	39012	MERANO	BZ	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	bzmrfrm@dayrep.com
413100	Via Rio Scaleres 22	APOTHEKE AM ROSSLAUF DES DANIEL NAGLER & C. O.H.G.	39042	BRESSANONE	BZ	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	bzbrfrm@dayrep.com
413200	Piazza Citta 10	MAIR WALTER	39049	VIPITENO	BZ	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	bzvpfrm@dayrep.com
422600	Via Malfatti 6	SANTO STEFANO DI CANDIOLI ROBERTO & C. SNC	38065	MORI	TN	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	tnmrfrm@dayrep.com
422800	Via Cesare Battisti 26	DE VARDA	38057	PERGINE VALSUGANA	TN	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	tnpvfrm@dayrep.com
423400	Via Maffei8	ACCORSI MASSIMO & C.  SNC	38066	RIVA DEL GARDA	TN	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	tnrdgfrm@dayrep.com
424100	Via Dante 1/BIS	THALER	38068	ROVERETO	TN	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	tnrvfrm@dayrep.com
424900	Viale Verona 92	S. BARTOLAMEO DOTT. C. BERTOLINI & C. SNC	38123	TRENTO	TN	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	tntnfrm@dayrep.com
\.


--
-- TOC entry 2908 (class 0 OID 19286)
-- Dependencies: 208
-- Data for Name: fotopazienti; Type: TABLE DATA; Schema: progettoipw; Owner: postgres
--

COPY progettoipw.fotopazienti (cf_paz, url, datafoto) FROM stdin;
DVRNLS18R50H152G	fotopazienti\\DVRNLS18R50H152G\\annalisa_davarda0.jpg	2019-07-10
DVRNLS18R50H152G	fotopazienti\\DVRNLS18R50H152G\\annalisa_davarda1.jpg	2019-10-01
GGRPTR15D05L097I	fotopazienti\\GGRPTR15D05L097I\\pietro_egger0.jpg	2019-08-23
GGRPTR15D05L097I	fotopazienti\\GGRPTR15D05L097I\\pietro_egger1.jpg	2018-12-24
GGRPTR15D05L097I	fotopazienti\\GGRPTR15D05L097I\\pietro_egger2.jpg	2019-06-17
GGRPTR15D05L097I	fotopazienti\\GGRPTR15D05L097I\\pietro_egger3.jpg	2019-01-31
ZZLDRN97B55C994H	fotopazienti\\ZZLDRN97B55C994H\\adriana_azzolini0.jpg	2018-12-12
ZZLDRN97B55C994H	fotopazienti\\ZZLDRN97B55C994H\\adriana_azzolini1.jpg	2019-03-13
ZZLDRN97B55C994H	fotopazienti\\ZZLDRN97B55C994H\\adriana_azzolini2.jpg	2019-02-18
ZZLDRN97B55C994H	fotopazienti\\ZZLDRN97B55C994H\\adriana_azzolini3.jpg	2019-04-19
MSTNMO60E66A635P	fotopazienti\\MSTNMO60E66A635P\\noemi_amistadi0.jpg	2019-06-07
MSTNMO60E66A635P	fotopazienti\\MSTNMO60E66A635P\\noemi_amistadi1.jpg	2019-12-30
MSTNMO60E66A635P	fotopazienti\\MSTNMO60E66A635P\\noemi_amistadi2.jpg	2019-09-12
BNZMRT74P01L106V	fotopazienti\\BNZMRT74P01L106V\\umberto_benuzzi0.jpg	2019-11-02
BNZMRT74P01L106V	fotopazienti\\BNZMRT74P01L106V\\umberto_benuzzi1.jpg	2019-09-24
BNZMRT74P01L106V	fotopazienti\\BNZMRT74P01L106V\\umberto_benuzzi2.jpg	2019-06-02
BNZMRT74P01L106V	fotopazienti\\BNZMRT74P01L106V\\umberto_benuzzi3.jpg	2019-09-01
MRIGPL08H02D336D	fotopazienti\\MRIGPL08H02D336D\\gianpaolo_miori0.jpg	2019-06-05
MRIGPL08H02D336D	fotopazienti\\MRIGPL08H02D336D\\gianpaolo_miori1.jpg	2019-05-27
MRIGPL08H02D336D	fotopazienti\\MRIGPL08H02D336D\\gianpaolo_miori2.jpg	2019-09-06
GRNPTR17H14L033Y	fotopazienti\\GRNPTR17H14L033Y\\pietro_grandi0.jpg	2019-04-22
PLTGRZ72P68B158J	fotopazienti\\PLTGRZ72P68B158J\\grazia_plattner0.jpg	2019-12-06
PLTGRZ72P68B158J	fotopazienti\\PLTGRZ72P68B158J\\grazia_plattner1.jpg	2019-03-07
PLTGRZ72P68B158J	fotopazienti\\PLTGRZ72P68B158J\\grazia_plattner2.jpg	2019-12-24
MSTCST13E58L174Q	fotopazienti\\MSTCST13E58L174Q\\cristina_amistadi0.jpg	2019-11-20
GVNMTN72E61E981G	fotopazienti\\GVNMTN72E61E981G\\martina_giovannini0.jpg	2019-06-27
MNPMRA62E19A916J	fotopazienti\\MNPMRA62E19A916J\\mario_menapace0.jpg	2019-06-17
MNPMRA62E19A916J	fotopazienti\\MNPMRA62E19A916J\\mario_menapace1.jpg	2019-07-12
MNPMRA62E19A916J	fotopazienti\\MNPMRA62E19A916J\\mario_menapace2.jpg	2018-12-09
MNPMRA62E19A916J	fotopazienti\\MNPMRA62E19A916J\\mario_menapace3.jpg	2019-12-03
SCHFRZ37M14C400T	fotopazienti\\SCHFRZ37M14C400T\\fabrizio_schnitzer0.jpg	2019-04-01
SCHFRZ37M14C400T	fotopazienti\\SCHFRZ37M14C400T\\fabrizio_schnitzer1.jpg	2019-04-08
SCHFRZ37M14C400T	fotopazienti\\SCHFRZ37M14C400T\\fabrizio_schnitzer2.jpg	2019-03-11
SCHFRZ37M14C400T	fotopazienti\\SCHFRZ37M14C400T\\fabrizio_schnitzer3.jpg	2019-02-23
MNTNLS46S52I839M	fotopazienti\\MNTNLS46S52I839M\\annalisa_montagni0.jpg	2019-12-21
MNTNLS46S52I839M	fotopazienti\\MNTNLS46S52I839M\\annalisa_montagni1.jpg	2019-05-30
KFLNCL79H42C727A	fotopazienti\\KFLNCL79H42C727A\\nicole_kofler0.jpg	2019-09-25
KFLNCL79H42C727A	fotopazienti\\KFLNCL79H42C727A\\nicole_kofler1.jpg	2019-07-13
KFLNCL79H42C727A	fotopazienti\\KFLNCL79H42C727A\\nicole_kofler2.jpg	2019-04-19
KFLNCL79H42C727A	fotopazienti\\KFLNCL79H42C727A\\nicole_kofler3.jpg	2019-06-27
ZCCLNZ98E09C700J	fotopazienti\\ZCCLNZ98E09C700J\\lorenzo_zuccatti0.jpg	2019-08-09
FRTLSN14H19L490X	fotopazienti\\FRTLSN14H19L490X\\alessandro_forti0.jpg	2018-12-16
FRTLSN14H19L490X	fotopazienti\\FRTLSN14H19L490X\\alessandro_forti1.jpg	2019-12-30
FRTLSN14H19L490X	fotopazienti\\FRTLSN14H19L490X\\alessandro_forti2.jpg	2018-12-13
FRTLSN14H19L490X	fotopazienti\\FRTLSN14H19L490X\\alessandro_forti3.jpg	2019-05-16
BNZLCU57D66I354T	fotopazienti\\BNZLCU57D66I354T\\lucia_benuzzi0.jpg	2019-03-19
BNZLCU57D66I354T	fotopazienti\\BNZLCU57D66I354T\\lucia_benuzzi1.jpg	2019-01-04
BSLHLG45P45B160D	fotopazienti\\BSLHLG45P45B160D\\helga_biasolli0.jpg	2019-11-06
BSLHLG45P45B160D	fotopazienti\\BSLHLG45P45B160D\\helga_biasolli1.jpg	2019-06-20
BSLHLG45P45B160D	fotopazienti\\BSLHLG45P45B160D\\helga_biasolli2.jpg	2019-05-13
SMNLSS92L59F307L	fotopazienti\\SMNLSS92L59F307L\\alessia_simoncelli0.jpg	2019-08-14
SMNLSS92L59F307L	fotopazienti\\SMNLSS92L59F307L\\alessia_simoncelli1.jpg	2019-01-23
SMNLSS92L59F307L	fotopazienti\\SMNLSS92L59F307L\\alessia_simoncelli2.jpg	2019-03-15
SMNLSS92L59F307L	fotopazienti\\SMNLSS92L59F307L\\alessia_simoncelli3.jpg	2019-11-27
BSLMTT14M15F068E	fotopazienti\\BSLMTT14M15F068E\\matteo_biasolli0.jpg	2019-11-23
BSLMTT14M15F068E	fotopazienti\\BSLMTT14M15F068E\\matteo_biasolli1.jpg	2019-03-10
BSLMTT14M15F068E	fotopazienti\\BSLMTT14M15F068E\\matteo_biasolli2.jpg	2019-08-21
BSLMTT14M15F068E	fotopazienti\\BSLMTT14M15F068E\\matteo_biasolli3.jpg	2019-03-26
TCCSLV64H06A968F	fotopazienti\\TCCSLV64H06A968F\\silvio_toccoli0.jpg	2018-12-02
TCCSLV64H06A968F	fotopazienti\\TCCSLV64H06A968F\\silvio_toccoli1.jpg	2019-10-25
TCCSLV64H06A968F	fotopazienti\\TCCSLV64H06A968F\\silvio_toccoli2.jpg	2019-05-29
PRCLVC38B49G644S	fotopazienti\\PRCLVC38B49G644S\\ludovica_pircher0.jpg	2019-03-13
PRCLVC38B49G644S	fotopazienti\\PRCLVC38B49G644S\\ludovica_pircher1.jpg	2019-04-15
PRCLVC38B49G644S	fotopazienti\\PRCLVC38B49G644S\\ludovica_pircher2.jpg	2019-09-15
TCCMRA11B58H330G	fotopazienti\\TCCMRA11B58H330G\\mara_toccoli0.jpg	2019-03-30
TCCMRA11B58H330G	fotopazienti\\TCCMRA11B58H330G\\mara_toccoli1.jpg	2019-03-10
SCRVNC47D56A967W	fotopazienti\\SCRVNC47D56A967W\\veronica_scrinzi0.jpg	2019-12-27
SCRVNC47D56A967W	fotopazienti\\SCRVNC47D56A967W\\veronica_scrinzi1.jpg	2019-06-16
SCRVNC47D56A967W	fotopazienti\\SCRVNC47D56A967W\\veronica_scrinzi2.jpg	2019-05-01
PRPNTN38T09D246K	fotopazienti\\PRPNTN38T09D246K\\antonio_pirpamer0.jpg	2019-06-27
TMSCRL55H56L915E	fotopazienti\\TMSCRL55H56L915E\\carla_tomasi0.jpg	2018-12-09
TMSCRL55H56L915E	fotopazienti\\TMSCRL55H56L915E\\carla_tomasi1.jpg	2019-10-18
TMSCRL55H56L915E	fotopazienti\\TMSCRL55H56L915E\\carla_tomasi2.jpg	2019-04-28
PRCNNT81P54L322J	fotopazienti\\PRCNNT81P54L322J\\antonietta_pircher0.jpg	2018-12-01
CHMLNE17M45L896F	fotopazienti\\CHMLNE17M45L896F\\elena_chemolli0.jpg	2019-02-24
NRDLVC68A01E981U	fotopazienti\\NRDLVC68A01E981U\\ludovico_nardelli0.jpg	2019-08-13
PRCDNL50L03F835T	fotopazienti\\PRCDNL50L03F835T\\daniele_pircher0.jpg	2019-03-22
FNTGDU07L21D311U	fotopazienti\\FNTGDU07L21D311U\\guido_fontana0.jpg	2019-07-10
FNTGDU07L21D311U	fotopazienti\\FNTGDU07L21D311U\\guido_fontana1.jpg	2019-04-18
MDNMRA01S48C756H	fotopazienti\\MDNMRA01S48C756H\\mara_modena0.jpg	2019-02-23
PRCNGL30L64A022U	fotopazienti\\PRCNGL30L64A022U\\angela_pircher0.jpg	2019-06-10
MSTRCC17M41A158X	fotopazienti\\MSTRCC17M41A158X\\rebecca_amistadi0.jpg	2019-01-15
GRNMSM47T15H004T	fotopazienti\\GRNMSM47T15H004T\\massimo_grandi0.jpg	2019-12-30
GRNMSM47T15H004T	fotopazienti\\GRNMSM47T15H004T\\massimo_grandi1.jpg	2019-05-23
MRNMNL28C16L137H	fotopazienti\\MRNMNL28C16L137H\\manuel_morandi0.jpg	2019-05-23
MRNMNL28C16L137H	fotopazienti\\MRNMNL28C16L137H\\manuel_morandi1.jpg	2019-02-14
MRNMNL28C16L137H	fotopazienti\\MRNMNL28C16L137H\\manuel_morandi2.jpg	2019-07-15
CHMCNZ46M41E757B	fotopazienti\\CHMCNZ46M41E757B\\cinzia_chemolli0.jpg	2019-05-02
CHMCNZ46M41E757B	fotopazienti\\CHMCNZ46M41E757B\\cinzia_chemolli1.jpg	2019-03-14
MTTDTL40P55D243Y	fotopazienti\\MTTDTL40P55D243Y\\donatella_matteotti0.jpg	2019-10-23
MTTDTL40P55D243Y	fotopazienti\\MTTDTL40P55D243Y\\donatella_matteotti1.jpg	2019-02-14
MTTDTL40P55D243Y	fotopazienti\\MTTDTL40P55D243Y\\donatella_matteotti2.jpg	2019-08-23
PRTLNE42S67D663Q	fotopazienti\\PRTLNE42S67D663Q\\elena_portrich0.jpg	2019-10-21
GFLDRD49M10F856Q	fotopazienti\\GFLDRD49M10F856Q\\edoardo_gufler0.jpg	2019-05-10
GFLDRD49M10F856Q	fotopazienti\\GFLDRD49M10F856Q\\edoardo_gufler1.jpg	2019-09-04
GFLDRD49M10F856Q	fotopazienti\\GFLDRD49M10F856Q\\edoardo_gufler2.jpg	2019-03-19
GFLDRD49M10F856Q	fotopazienti\\GFLDRD49M10F856Q\\edoardo_gufler3.jpg	2019-04-09
KMEFLV86H22L111K	fotopazienti\\KMEFLV86H22L111K\\flavio_keim0.jpg	2019-06-08
KMEFLV86H22L111K	fotopazienti\\KMEFLV86H22L111K\\flavio_keim1.jpg	2019-09-07
KMEFLV86H22L111K	fotopazienti\\KMEFLV86H22L111K\\flavio_keim2.jpg	2019-08-06
KMEFLV86H22L111K	fotopazienti\\KMEFLV86H22L111K\\flavio_keim3.jpg	2019-02-26
GRLNNA47H56M173S	fotopazienti\\GRLNNA47H56M173S\\anna_gerola0.jpg	2019-11-19
GRLNNA47H56M173S	fotopazienti\\GRLNNA47H56M173S\\anna_gerola1.jpg	2019-05-12
GRLNNA47H56M173S	fotopazienti\\GRLNNA47H56M173S\\anna_gerola2.jpg	2019-03-03
GRLNNA47H56M173S	fotopazienti\\GRLNNA47H56M173S\\anna_gerola3.jpg	2019-03-13
LSLFNC25M63H532S	fotopazienti\\LSLFNC25M63H532S\\francesca_elsler0.jpg	2019-03-05
SCRLDI19H55A839F	fotopazienti\\SCRLDI19H55A839F\\lidia_scrinzi0.jpg	2019-01-22
GRDFLV28R05A178Z	fotopazienti\\GRDFLV28R05A178Z\\flavio_giordani0.jpg	2019-05-25
HLZNLS17L59F949S	fotopazienti\\HLZNLS17L59F949S\\annalisa_holzer0.jpg	2019-04-04
FRNDRA76E06I687A	fotopazienti\\FRNDRA76E06I687A\\dario_franceschini0.jpg	2019-06-21
MYRFNC32D59L527G	fotopazienti\\MYRFNC32D59L527G\\francesca_mayr0.jpg	2019-01-21
BRNNDA05R53E981M	fotopazienti\\BRNNDA05R53E981M\\nadia_baroni0.jpg	2019-04-06
BRNNDA05R53E981M	fotopazienti\\BRNNDA05R53E981M\\nadia_baroni1.jpg	2019-10-20
BRNNDA05R53E981M	fotopazienti\\BRNNDA05R53E981M\\nadia_baroni2.jpg	2019-03-14
BLDDMN74T11C756X	fotopazienti\\BLDDMN74T11C756X\\damiano_baldessari0.jpg	2019-04-27
BLDDMN74T11C756X	fotopazienti\\BLDDMN74T11C756X\\damiano_baldessari1.jpg	2019-10-07
BLDDMN74T11C756X	fotopazienti\\BLDDMN74T11C756X\\damiano_baldessari2.jpg	2018-12-03
DPLLRA05L44B404J	fotopazienti\\DPLLRA05L44B404J\\lara_depaul0.jpg	2019-02-10
DPLLRA05L44B404J	fotopazienti\\DPLLRA05L44B404J\\lara_depaul1.jpg	2019-03-10
BRSLGE75C46E398O	fotopazienti\\BRSLGE75C46E398O\\elga_bressan0.jpg	2019-03-21
BRSLGE75C46E398O	fotopazienti\\BRSLGE75C46E398O\\elga_bressan1.jpg	2019-08-30
BRSLGE75C46E398O	fotopazienti\\BRSLGE75C46E398O\\elga_bressan2.jpg	2019-07-19
BRSLGE75C46E398O	fotopazienti\\BRSLGE75C46E398O\\elga_bressan3.jpg	2019-12-20
ZCCDVD99S09E334C	fotopazienti\\ZCCDVD99S09E334C\\davide_zucchelli0.jpg	2019-03-05
ZCCDVD99S09E334C	fotopazienti\\ZCCDVD99S09E334C\\davide_zucchelli1.jpg	2019-04-28
ZCCDVD99S09E334C	fotopazienti\\ZCCDVD99S09E334C\\davide_zucchelli2.jpg	2019-07-04
ZCCDVD99S09E334C	fotopazienti\\ZCCDVD99S09E334C\\davide_zucchelli3.jpg	2019-09-29
MNFSMN55B08B203N	fotopazienti\\MNFSMN55B08B203N\\simone_manfrini0.jpg	2019-01-27
MNFSMN55B08B203N	fotopazienti\\MNFSMN55B08B203N\\simone_manfrini1.jpg	2019-07-01
ZZLSLV15R62L162L	fotopazienti\\ZZLSLV15R62L162L\\silvia_azzolini0.jpg	2019-06-07
ZZLSLV15R62L162L	fotopazienti\\ZZLSLV15R62L162L\\silvia_azzolini1.jpg	2019-04-25
ZZLSLV15R62L162L	fotopazienti\\ZZLSLV15R62L162L\\silvia_azzolini2.jpg	2019-05-15
ZZLSLV15R62L162L	fotopazienti\\ZZLSLV15R62L162L\\silvia_azzolini3.jpg	2019-03-22
TZZGZL80A61F307B	fotopazienti\\TZZGZL80A61F307B\\graziella_tezzele0.jpg	2019-08-20
MRLMRA02M56D663B	fotopazienti\\MRLMRA02M56D663B\\mara_miorelli0.jpg	2019-06-10
MRLMRA02M56D663B	fotopazienti\\MRLMRA02M56D663B\\mara_miorelli1.jpg	2019-12-22
KFLRRT29A04I949D	fotopazienti\\KFLRRT29A04I949D\\roberto_kofler0.jpg	2019-11-16
KFLRRT29A04I949D	fotopazienti\\KFLRRT29A04I949D\\roberto_kofler1.jpg	2019-12-20
KFLRRT29A04I949D	fotopazienti\\KFLRRT29A04I949D\\roberto_kofler2.jpg	2019-04-15
KFLRRT29A04I949D	fotopazienti\\KFLRRT29A04I949D\\roberto_kofler3.jpg	2019-11-02
GSSMRA22A64L660R	fotopazienti\\GSSMRA22A64L660R\\mara_gasser0.jpg	2019-02-14
GSSMRA22A64L660R	fotopazienti\\GSSMRA22A64L660R\\mara_gasser1.jpg	2019-10-10
GSSMRA22A64L660R	fotopazienti\\GSSMRA22A64L660R\\mara_gasser2.jpg	2019-07-05
PDRNMO72L44H639L	fotopazienti\\PDRNMO72L44H639L\\noemi_pedrotti0.jpg	2019-03-17
PDRNMO72L44H639L	fotopazienti\\PDRNMO72L44H639L\\noemi_pedrotti1.jpg	2019-06-09
PDRNMO72L44H639L	fotopazienti\\PDRNMO72L44H639L\\noemi_pedrotti2.jpg	2019-09-23
FSAMTT79T03B579K	fotopazienti\\FSAMTT79T03B579K\\matteo_faes0.jpg	2019-09-19
FSAMTT79T03B579K	fotopazienti\\FSAMTT79T03B579K\\matteo_faes1.jpg	2019-09-22
FSAMTT79T03B579K	fotopazienti\\FSAMTT79T03B579K\\matteo_faes2.jpg	2019-04-06
MTTMLD52P64F176A	fotopazienti\\MTTMLD52P64F176A\\matilde_matteotti0.jpg	2019-02-02
BNNCRN43T59L108I	fotopazienti\\BNNCRN43T59L108I\\caren_boninsegna0.jpg	2018-12-19
BNNCRN43T59L108I	fotopazienti\\BNNCRN43T59L108I\\caren_boninsegna1.jpg	2019-07-27
PCHLSN60T58A286J	fotopazienti\\PCHLSN60T58A286J\\alessandra_pichler0.jpg	2019-03-24
PCHLSN60T58A286J	fotopazienti\\PCHLSN60T58A286J\\alessandra_pichler1.jpg	2019-06-05
PCHLSN60T58A286J	fotopazienti\\PCHLSN60T58A286J\\alessandra_pichler2.jpg	2019-07-28
PCHLSN60T58A286J	fotopazienti\\PCHLSN60T58A286J\\alessandra_pichler3.jpg	2019-03-05
\.


--
-- TOC entry 2903 (class 0 OID 19168)
-- Dependencies: 203
-- Data for Name: med_has_paz; Type: TABLE DATA; Schema: progettoipw; Owner: postgres
--

COPY progettoipw.med_has_paz (cf_med, cf_paz) FROM stdin;
DVRDRN73P08D075W	PCHLSN60T58A286J
DVRDRN73P08D075W	BNNCRN43T59L108I
DVRDRN73P08D075W	MTTMLD52P64F176A
DVRDRN73P08D075W	FSAMTT79T03B579K
DVRDRN73P08D075W	GVNTMS77R07I760K
DVRDRN73P08D075W	TZZSRG70R27E481D
DVRDRN73P08D075W	PDRNMO72L44H639L
DVRDRN73P08D075W	PCHFRZ49P25A116I
DVRDRN73P08D075W	GSSMRA22A64L660R
DVRDRN73P08D075W	GVNLRA19S63I173B
DVRDRN73P08D075W	KFLRRT29A04I949D
DVRDRN73P08D075W	MRLMRA02M56D663B
NRDLDI83E50E614G	PLLPRI69A28D371I
NRDLDI83E50E614G	TZZGZL80A61F307B
NRDLDI83E50E614G	ZZLSLV15R62L162L
NRDLDI83E50E614G	MNFSMN55B08B203N
NRDLDI83E50E614G	FRNMRT70E16L490F
NRDLDI83E50E614G	ZCCDVD99S09E334C
NRDLDI83E50E614G	BRSLGE75C46E398O
NRDLDI83E50E614G	DPLLRA05L44B404J
NRDLDI83E50E614G	BLDDMN74T11C756X
NRDLDI83E50E614G	BRNNDA05R53E981M
NRDLDI83E50E614G	BRSFNC35L17D336O
NRDLDI83E50E614G	MYRFNC32D59L527G
RZZSLV81R01E150H	FRNDRA76E06I687A
RZZSLV81R01E150H	HLZNLS17L59F949S
RZZSLV81R01E150H	GRDFLV28R05A178Z
RZZSLV81R01E150H	PCHGRL78M15E461T
RZZSLV81R01E150H	SCRLDI19H55A839F
RZZSLV81R01E150H	RSSLCU31P45I173U
RZZSLV81R01E150H	LSLFNC25M63H532S
RZZSLV81R01E150H	LSLNMO31T62G173U
RZZSLV81R01E150H	GRLNNA47H56M173S
RZZSLV81R01E150H	KMEFLV86H22L111K
RZZSLV81R01E150H	SDNCMN83T26H858P
RZZSLV81R01E150H	GFLDRD49M10F856Q
SMMTMS72M20G443H	PRTLNE42S67D663Q
SMMTMS72M20G443H	MTTDTL40P55D243Y
SMMTMS72M20G443H	CHMCNZ46M41E757B
SMMTMS72M20G443H	MRNMNL28C16L137H
SMMTMS72M20G443H	GRNMSM47T15H004T
SMMTMS72M20G443H	MSTRCC17M41A158X
SMMTMS72M20G443H	PRCNGL30L64A022U
SMMTMS72M20G443H	MDNMRA01S48C756H
SMMTMS72M20G443H	FNTGDU07L21D311U
SMMTMS72M20G443H	PRCDNL50L03F835T
SMMTMS72M20G443H	NRDLVC68A01E981U
SMMTMS72M20G443H	CHMLNE17M45L896F
PRTMRZ80C26A537X	PRCNNT81P54L322J
PRTMRZ80C26A537X	TMSCRL55H56L915E
PRTMRZ80C26A537X	PRPNTN38T09D246K
PRTMRZ80C26A537X	MRIPQL73R14A952Z
PRTMRZ80C26A537X	GRDLCA81C49L957D
PRTMRZ80C26A537X	SCRVNC47D56A967W
PRTMRZ80C26A537X	FRNCMN07B16A520T
PRTMRZ80C26A537X	TCCMRA11B58H330G
PRTMRZ80C26A537X	HLZSRA39T42L137R
PRTMRZ80C26A537X	PRCLVC38B49G644S
PRTMRZ80C26A537X	BRNHLG96P42A537W
PRTMRZ80C26A537X	TCCSLV64H06A968F
MRIGZL85P49E481L	BSLMTT14M15F068E
MRIGZL85P49E481L	SMNLSS92L59F307L
MRIGZL85P49E481L	BSLHLG45P45B160D
MRIGZL85P49E481L	BNZLCU57D66I354T
MRIGZL85P49E481L	FRTLSN14H19L490X
MRIGZL85P49E481L	ZCCLNZ98E09C700J
MRIGZL85P49E481L	KFLNCL79H42C727A
MRIGZL85P49E481L	RCKFRZ46T60E065G
MRIGZL85P49E481L	BRNGRL52H17D457Q
MRIGZL85P49E481L	MNTNLS46S52I839M
MRIGZL85P49E481L	SCHFRZ37M14C400T
MRIGZL85P49E481L	MNPMRA62E19A916J
BRSNCL79A61D484Q	GVNMTN72E61E981G
BRSNCL79A61D484Q	MSTCST13E58L174Q
BRSNCL79A61D484Q	PLTGRZ72P68B158J
BRSNCL79A61D484Q	GRNPTR17H14L033Y
BRSNCL79A61D484Q	MRIGPL08H02D336D
BRSNCL79A61D484Q	BNZMRT74P01L106V
BRSNCL79A61D484Q	MSTNMO60E66A635P
BRSNCL79A61D484Q	ZCCLNE08T47D631A
BRSNCL79A61D484Q	ZZLDRN97B55C994H
BRSNCL79A61D484Q	GGRPTR15D05L097I
BRSNCL79A61D484Q	GMPPTR66C24D821H
BRSNCL79A61D484Q	DVRNLS18R50H152G
\.


--
-- TOC entry 2901 (class 0 OID 19152)
-- Dependencies: 201
-- Data for Name: medici; Type: TABLE DATA; Schema: progettoipw; Owner: postgres
--

COPY progettoipw.medici (nome, cognome, password, flag_recuperopassword, sesso, luogonascita, datanascita, numerotelefono, codicefiscale, citta, provincia, email, is_spec) FROM stdin;
ALAN	ZUCCHELLI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	PRATO ALLO STELVIO	1985-08-05	358/7614318	ZCCLNA85M05H004D	ROVERETO	TN	zcclna85m05h004d@dayrep.com	t
SANDRA	BRESCIANI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	FORTEZZA	1986-04-22	355/8389784	BRSSDR86D62D731H	RIVA DEL GARDA	TN	brssdr86d62d731h@dayrep.com	t
LOREDANA	BETTA	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	BRONZOLO	1971-03-05	323/2153027	BTTLDN71C45B203N	ROVERETO	TN	bttldn71c45b203n@dayrep.com	t
CARLO	BRESCIANI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	TONADICO	1982-02-24	361/9709829	BRSCRL82B24L201U	BRESSANONE	BZ	brscrl82b24l201u@dayrep.com	t
ALESSIO	RIZ	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	AVELENGO	1983-11-27	333/6527272	RZILSS83S27A507U	BOLZANO	BZ	rzilss83s27a507u@dayrep.com	t
MIRTA	RASOM	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	PERCA	1981-11-18	370/6762870	RSMMRT81S58G443J	RIVA DEL GARDA	TN	rsmmrt81s58g443j@dayrep.com	t
CHIARA	BARONI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	RIO DI PUSTERIA	1983-05-26	329/5704442	BRNCHR83E66H299L	MERANO	BZ	brnchr83e66h299l@dayrep.com	t
RICCARDO	KEIM	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	BREZ	1969-10-01	331/1868687	KMERCR69R01B165K	MERANO	BZ	kmercr69r01b165k@dayrep.com	t
GIUSEPPE	BENUZZI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	RODENGO	1974-07-13	325/3695037	BNZGPP74L13H475Z	TRENTO	TN	bnzgpp74l13h475z@dayrep.com	t
BARBARA	GAMPER	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	TERRES	1968-08-23	337/3644980	GMPBBR68M63L137D	MERANO	BZ	gmpbbr68m63l137d@dayrep.com	t
ADRIANO	DAVARDA	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	CORTINA SULLA STRADA DEL VINO	1973-09-08	324/1666101	DVRDRN73P08D075W	BOLZANO	BZ	dvrdrn73p08d075w@dayrep.com	f
LIDIA	NARDELLI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	LISIGNAGO	1983-05-10	337/7946063	NRDLDI83E50E614G	RIVA DEL GARDA	TN	nrdldi83e50e614g@dayrep.com	f
SILVIO	RIZZI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	GRAUNO	1981-10-01	338/3155462	RZZSLV81R01E150H	MERANO	BZ	rzzslv81r01e150h@dayrep.com	f
TOMMASO	SOMMAVILLA	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	PERCA	1972-08-20	363/1531508	SMMTMS72M20G443H	RIVA DEL GARDA	TN	smmtms72m20g443h@dayrep.com	f
MAURIZIO	PORTRICH	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	BADIA	1980-03-26	350/4936694	PRTMRZ80C26A537X	RIVA DEL GARDA	TN	prtmrz80c26a537x@dayrep.com	f
GRAZIELLA	MIORI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	LAUREGNO	1985-09-09	329/1313917	MRIGZL85P49E481L	BRESSANONE	BZ	mrigzl85p49e481l@dayrep.com	f
NICOLE	BRESCIANI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	FALZES	1979-01-21	359/2300266	BRSNCL79A61D484Q	ROVERETO	TN	brsncl79a61d484q@dayrep.com	f
\.


--
-- TOC entry 2902 (class 0 OID 19160)
-- Dependencies: 202
-- Data for Name: pazienti; Type: TABLE DATA; Schema: progettoipw; Owner: postgres
--

COPY progettoipw.pazienti (nome, cognome, password, flag_recuperopassword, sesso, luogonascita, datanascita, dataultimavisita, numerotelefono, codicefiscale, email, citta, provincia) FROM stdin;
ANNALISA	DAVARDA	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	RACINES	2018-10-10	2019-12-04	330/8486197	DVRNLS18R50H152G	dvrnls18r50h152g@dayrep.com	DRO	TN
PIETRO	GAMPER	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	FUNES	1966-03-24	2019-11-04	355/6321643	GMPPTR66C24D821H	gmpptr66c24d821h@dayrep.com	RIVA DEL GARDA	TN
PIETRO	EGGER	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	TENNO	2015-04-05	2019-12-07	341/6584271	GGRPTR15D05L097I	ggrptr15d05l097i@dayrep.com	RIVA DEL GARDA	TN
ADRIANA	AZZOLINI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	COREDO	1997-02-15	2019-12-22	338/8674104	ZZLDRN97B55C994H	zzldrn97b55c994h@dayrep.com	TRENTO	TN
ELENA	ZUCCHELLI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	FLAVON	2008-12-07	2019-12-23	334/7884195	ZCCLNE08T47D631A	zcclne08t47d631a@dayrep.com	DRO	TN
NOEMI	AMISTADI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	BARBIANO	1960-05-26	2019-12-23	370/1478390	MSTNMO60E66A635P	mstnmo60e66a635p@dayrep.com	RIVA DEL GARDA	TN
UMBERTO	BENUZZI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	TERENTO	1974-09-01	2019-11-15	342/8084894	BNZMRT74P01L106V	bnzmrt74p01l106v@dayrep.com	RIVA DEL GARDA	TN
GIANPAOLO	MIORI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	DON	2008-06-02	2019-12-19	379/4267620	MRIGPL08H02D336D	mrigpl08h02d336d@dayrep.com	MORI	TN
PIETRO	GRANDI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	TAIO	1917-06-14	2019-11-19	353/8726077	GRNPTR17H14L033Y	grnptr17h14l033y@dayrep.com	TRENTO	TN
GRAZIA	PLATTNER	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	BRESIMO	1972-09-28	2019-12-20	372/1077988	PLTGRZ72P68B158J	pltgrz72p68b158j@dayrep.com	MORI	TN
CRISTINA	AMISTADI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	TIONE DI TRENTO	2013-05-18	2019-11-14	352/8248157	MSTCST13E58L174Q	mstcst13e58l174q@dayrep.com	TRENTO	TN
MARTINA	GIOVANNINI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	MARTELLO	1972-05-21	2019-12-17	323/9367294	GVNMTN72E61E981G	gvnmtn72e61e981g@dayrep.com	DRO	TN
MARIO	MENAPACE	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	BOCENAGO	1962-05-19	2019-11-22	348/7924517	MNPMRA62E19A916J	mnpmra62e19a916j@dayrep.com	BOLZANO	BZ
FABRIZIO	SCHNITZER	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	CAVIZZANA	1937-08-14	2019-11-16	334/4019270	SCHFRZ37M14C400T	schfrz37m14c400t@dayrep.com	LAIVES	BZ
ANNALISA	MONTAGNI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	SORAGA	1946-11-12	2019-12-27	341/2899616	MNTNLS46S52I839M	mntnls46s52i839m@dayrep.com	BRUNICO	BZ
GABRIELE	BERNARDI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	FAEDO	1952-06-17	2019-11-28	355/9192855	BRNGRL52H17D457Q	brngrl52h17d457q@dayrep.com	LAIVES	BZ
FABRIZIA	RICK	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	GIUSTINO	1946-12-20	2019-11-13	336/6117959	RCKFRZ46T60E065G	rckfrz46t60e065g@dayrep.com	BRUNICO	BZ
NICOLE	KOFLER	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	CIS	1979-06-02	2019-11-23	324/9304563	KFLNCL79H42C727A	kflncl79h42c727a@dayrep.com	LAIVES	BZ
LORENZO	ZUCCATTI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	CIMONE	1998-05-09	2019-11-11	354/9851903	ZCCLNZ98E09C700J	zcclnz98e09c700j@dayrep.com	BOLZANO	BZ
ALESSANDRO	FORTI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	ULTIMO	2014-06-19	2019-11-08	379/9792699	FRTLSN14H19L490X	frtlsn14h19l490x@dayrep.com	BRUNICO	BZ
LUCIA	BENUZZI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	SANTORSOLA TERME	1957-04-26	2019-12-27	358/6548750	BNZLCU57D66I354T	bnzlcu57d66i354t@dayrep.com	MERANO	BZ
HELGA	BIASOLLI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	BRESSANONE	1945-09-05	2019-11-15	335/4228437	BSLHLG45P45B160D	bslhlg45p45b160d@dayrep.com	LAIVES	BZ
ALESSIA	SIMONCELLI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	MOLVENO	1992-07-19	2019-11-02	345/5353360	SMNLSS92L59F307L	smnlss92l59f307l@dayrep.com	BRESSANONE	BZ
MATTEO	BIASOLLI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	MAZZIN	2014-08-15	2019-11-09	320/1148672	BSLMTT14M15F068E	bslmtt14m15f068e@dayrep.com	BRESSANONE	BZ
SILVIO	TOCCOLI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	BONDONE	1964-06-06	2019-11-14	356/7080354	TCCSLV64H06A968F	tccslv64h06a968f@dayrep.com	DRO	TN
HELGA	BERNARD	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	BADIA	1996-09-02	2019-12-09	344/4698655	BRNHLG96P42A537W	brnhlg96p42a537w@dayrep.com	MORI	TN
LUDOVICA	PIRCHER	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	PIEVE DI LEDRO	1938-02-09	2019-12-09	352/9314732	PRCLVC38B49G644S	prclvc38b49g644s@dayrep.com	TRENTO	TN
SARA	HOLZER	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	TERRES	1939-12-02	2019-11-06	351/4308855	HLZSRA39T42L137R	hlzsra39t42l137r@dayrep.com	DRO	TN
MARA	TOCCOLI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	RIVA DEL GARDA	2011-02-18	2019-12-16	370/6574776	TCCMRA11B58H330G	tccmra11b58h330g@dayrep.com	RIVA DEL GARDA	TN
CLEMENTE	FRANCESCHINI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	AVIO	2007-02-16	2019-11-28	353/1611172	FRNCMN07B16A520T	frncmn07b16a520t@dayrep.com	DRO	TN
VERONICA	SCRINZI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	BONDO	1947-04-16	2019-11-09	343/8000869	SCRVNC47D56A967W	scrvnc47d56a967w@dayrep.com	DRO	TN
ALICE	GIORDANI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	VILLA LAGARINA	1981-03-09	2019-12-06	363/3670037	GRDLCA81C49L957D	grdlca81c49l957d@dayrep.com	TRENTO	TN
PASQUALE	MIORI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	BOLZANO	1973-10-14	2019-12-12	380/5012721	MRIPQL73R14A952Z	mripql73r14a952z@dayrep.com	MORI	TN
ANTONIO	PIRPAMER	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	DAMBEL	1938-12-09	2019-11-28	378/7354851	PRPNTN38T09D246K	prpntn38t09d246k@dayrep.com	MORI	TN
CARLA	TOMASI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	VILLABASSA	1955-06-16	2019-11-02	379/4711263	TMSCRL55H56L915E	tmscrl55h56l915e@dayrep.com	DRO	TN
ANTONIETTA	PIRCHER	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	TRAMBILENO	1981-09-14	2019-11-03	322/6841419	PRCNNT81P54L322J	prcnnt81p54l322j@dayrep.com	TRENTO	TN
ELENA	CHEMOLLI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	VIGOLO VATTARO	1917-08-05	2019-11-27	331/7088747	CHMLNE17M45L896F	chmlne17m45l896f@dayrep.com	MORI	TN
LUDOVICO	NARDELLI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	MARTELLO	1968-01-01	2019-11-23	347/5266491	NRDLVC68A01E981U	nrdlvc68a01e981u@dayrep.com	DRO	TN
DANIELE	PIRCHER	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	NAGO-TORBOLE	1950-07-03	2019-12-01	341/2161596	PRCDNL50L03F835T	prcdnl50l03f835t@dayrep.com	ROVERETO	TN
GUIDO	FONTANA	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	DOBBIACO	2007-07-21	2019-12-26	331/6910197	FNTGDU07L21D311U	fntgdu07l21d311u@dayrep.com	MORI	TN
MARA	MODENA	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	CIVEZZANO	2001-11-08	2019-12-16	335/2817197	MDNMRA01S48C756H	mdnmra01s48c756h@dayrep.com	TRENTO	TN
ANGELA	PIRCHER	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	CERMES	1930-07-24	2019-12-19	377/4234115	PRCNGL30L64A022U	prcngl30l64a022u@dayrep.com	DRO	TN
REBECCA	AMISTADI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	ALBIANO	1917-08-01	2019-12-11	358/2132638	MSTRCC17M41A158X	mstrcc17m41a158x@dayrep.com	TRENTO	TN
MASSIMO	GRANDI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	PRATO ALLO STELVIO	1947-12-15	2019-11-05	345/7658305	GRNMSM47T15H004T	grnmsm47t15h004t@dayrep.com	TRENTO	TN
MANUEL	MORANDI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	TERRES	1928-03-16	2019-11-01	378/1215232	MRNMNL28C16L137H	mrnmnl28c16l137h@dayrep.com	TRENTO	TN
CINZIA	CHEMOLLI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	LUSERNA	1946-08-01	2019-12-13	376/2185366	CHMCNZ46M41E757B	chmcnz46m41e757b@dayrep.com	MORI	TN
DONATELLA	MATTEOTTI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	DAIANO	1940-09-15	2019-12-14	331/8567048	MTTDTL40P55D243Y	mttdtl40p55d243y@dayrep.com	DRO	TN
ELENA	PORTRICH	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	FONDO	1942-11-27	2019-12-09	346/5613120	PRTLNE42S67D663Q	prtlne42s67d663q@dayrep.com	RIVA DEL GARDA	TN
EDOARDO	GUFLER	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	NAZ-SCIAVES	1949-08-10	2019-12-07	330/6416473	GFLDRD49M10F856Q	gfldrd49m10f856q@dayrep.com	BOLZANO	BZ
CLEMENTE	SEIDNER	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	SAN GENESIO ATESINO	1983-12-26	2019-12-02	373/9064205	SDNCMN83T26H858P	sdncmn83t26h858p@dayrep.com	BOLZANO	BZ
FLAVIO	KEIM	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	TERMENO SULLA STRADA DEL VINO	1986-06-22	2019-11-19	367/5610357	KMEFLV86H22L111K	kmeflv86h22l111k@dayrep.com	BRUNICO	BZ
ANNA	GEROLA	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	ZIANO DI FIEMME	1947-06-16	2019-11-29	350/2240025	GRLNNA47H56M173S	grlnna47h56m173s@dayrep.com	BRUNICO	BZ
NOEMI	ELSLER	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	OSSANA	1931-12-22	2019-12-28	379/4674132	LSLNMO31T62G173U	lslnmo31t62g173u@dayrep.com	MERANO	BZ
FRANCESCA	ELSLER	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	RONCHI VALSUGANA	1925-08-23	2019-11-13	366/1613142	LSLFNC25M63H532S	lslfnc25m63h532s@dayrep.com	LAIVES	BZ
LUCIA	ROSSI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	SANTA CRISTINA VALGARDENA	1931-09-05	2019-12-03	321/4127445	RSSLCU31P45I173U	rsslcu31p45i173u@dayrep.com	MERANO	BZ
LIDIA	SCRINZI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	BEZZECCA	2019-06-15	2019-12-21	360/5075405	SCRLDI19H55A839F	scrldi19h55a839f@dayrep.com	BRESSANONE	BZ
GABRIELE	PICHLER	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	LASINO	1978-08-15	2019-12-06	359/2505740	PCHGRL78M15E461T	pchgrl78m15e461t@dayrep.com	BOLZANO	BZ
FLAVIO	GIORDANI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	ALDENO	1928-10-05	2019-11-18	370/4237989	GRDFLV28R05A178Z	grdflv28r05a178z@dayrep.com	BOLZANO	BZ
ANNALISA	HOLZER	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	NOVA LEVANTE	1917-07-19	2019-11-24	345/4721293	HLZNLS17L59F949S	hlznls17l59f949s@dayrep.com	BOLZANO	BZ
DARIO	FRANCESCHINI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	SESTO	1976-05-06	2019-12-24	338/7986363	FRNDRA76E06I687A	frndra76e06i687a@dayrep.com	BOLZANO	BZ
FRANCESCA	MAYR	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	VADENA	1932-04-19	2019-12-01	372/8690857	MYRFNC32D59L527G	myrfnc32d59l527g@dayrep.com	TRENTO	TN
FRANCESCO	BRESCIANI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	DON	1935-07-17	2019-11-01	325/4246335	BRSFNC35L17D336O	brsfnc35l17d336o@dayrep.com	RIVA DEL GARDA	TN
NADIA	BARONI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	MARTELLO	2005-10-13	2019-11-25	353/7577042	BRNNDA05R53E981M	brnnda05r53e981m@dayrep.com	ROVERETO	TN
DAMIANO	BALDESSARI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	CIVEZZANO	1974-12-11	2019-12-20	366/1577434	BLDDMN74T11C756X	blddmn74t11c756x@dayrep.com	ROVERETO	TN
LARA	DEPAUL	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	CALDONAZZO	2005-07-04	2019-12-25	363/2244257	DPLLRA05L44B404J	dpllra05l44b404j@dayrep.com	DRO	TN
ELGA	BRESSAN	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	LACES	1975-03-06	2019-12-10	374/2332930	BRSLGE75C46E398O	brslge75c46e398o@dayrep.com	RIVA DEL GARDA	TN
DAVIDE	ZUCCHELLI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	ISERA	1999-11-09	2019-11-13	358/8324897	ZCCDVD99S09E334C	zccdvd99s09e334c@dayrep.com	MORI	TN
UMBERTO	FRANCESCHINI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	ULTIMO	1970-05-16	2019-12-26	361/5177986	FRNMRT70E16L490F	frnmrt70e16l490f@dayrep.com	DRO	TN
SIMONE	MANFRINI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	BRONZOLO	1955-02-08	2019-12-02	356/7657929	MNFSMN55B08B203N	mnfsmn55b08b203n@dayrep.com	RIVA DEL GARDA	TN
SILVIA	AZZOLINI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	TIARNO DI SOPRA	1915-10-22	2019-11-08	340/6472044	ZZLSLV15R62L162L	zzlslv15r62l162l@dayrep.com	RIVA DEL GARDA	TN
GRAZIELLA	TEZZELE	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	MOLVENO	1980-01-21	2019-12-07	327/1704331	TZZGZL80A61F307B	tzzgzl80a61f307b@dayrep.com	RIVA DEL GARDA	TN
PIERO	PELLEGRINI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	DRO	1969-01-28	2019-11-28	358/5486872	PLLPRI69A28D371I	pllpri69a28d371i@dayrep.com	MORI	TN
MARA	MIORELLI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	FONDO	2002-08-16	2019-12-03	377/5221223	MRLMRA02M56D663B	mrlmra02m56d663b@dayrep.com	BOLZANO	BZ
ROBERTO	KOFLER	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	STENICO	1929-01-04	2019-12-08	345/3524349	KFLRRT29A04I949D	kflrrt29a04i949d@dayrep.com	BRESSANONE	BZ
LARA	GIOVANNINI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	SANTA CRISTINA VALGARDENA	1919-11-23	2019-12-17	340/4408100	GVNLRA19S63I173B	gvnlra19s63i173b@dayrep.com	LAIVES	BZ
MARA	GASSER	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	VANDOIES	1922-01-24	2019-12-09	349/7393375	GSSMRA22A64L660R	gssmra22a64l660r@dayrep.com	BOLZANO	BZ
FABRIZIO	PICHLER	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	ALA	1949-09-25	2019-11-15	358/9140365	PCHFRZ49P25A116I	pchfrz49p25a116i@dayrep.com	BRUNICO	BZ
NOEMI	PEDROTTI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	RUMO	1972-07-04	2019-12-19	327/4585998	PDRNMO72L44H639L	pdrnmo72l44h639l@dayrep.com	LAIVES	BZ
SERGIO	TEZZELE	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	LAUREGNO	1970-10-27	2019-12-24	378/4722072	TZZSRG70R27E481D	tzzsrg70r27e481d@dayrep.com	BOLZANO	BZ
THOMAS	GIOVANNINI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	SIROR	1977-10-07	2019-11-09	341/8036367	GVNTMS77R07I760K	gvntms77r07i760k@dayrep.com	BRESSANONE	BZ
MATTEO	FAES	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	M	CANAZEI	1979-12-03	2019-12-15	373/3076479	FSAMTT79T03B579K	fsamtt79t03b579k@dayrep.com	MERANO	BZ
MATILDE	MATTEOTTI	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	MEZZANO	1952-09-24	2019-11-18	353/1142772	MTTMLD52P64F176A	mttmld52p64f176a@dayrep.com	LAIVES	BZ
CAREN	BONINSEGNA	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	TERLANO	1943-12-19	2019-11-11	340/3603679	BNNCRN43T59L108I	bnncrn43t59l108i@dayrep.com	MERANO	BZ
ALESSANDRA	PICHLER	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	F	ANDRIANO	1960-12-18	2019-11-11	370/5908359	PCHLSN60T58A286J	pchlsn60t58a286j@dayrep.com	BRUNICO	BZ
\.


--
-- TOC entry 2905 (class 0 OID 19207)
-- Dependencies: 205
-- Data for Name: prescrizione_esami; Type: TABLE DATA; Schema: progettoipw; Owner: postgres
--

COPY progettoipw.prescrizione_esami (id, cf_med, id_ssp, cf_paz, id_esame, "timestamp", data_erog, risultati) FROM stdin;
71	DVRDRN73P08D075W	\N	FSAMTT79T03B579K	70235	1995-06-07 08:26:42	1995-06-08 17:02:21	Valori bassi
51	DVRDRN73P08D075W	\N	BNNCRN43T59L108I	70103	2018-07-31 15:55:32	2018-08-13 20:40:26	Valori bassi
181	DVRDRN73P08D075W	\N	PDRNMO72L44H639L	70053	1976-03-14 20:02:08	1976-03-27 19:14:26	Valori nella norma
681	RZZSLV81R01E150H	\N	HLZNLS17L59F949S	70221	2018-12-02 08:29:01	2018-12-15 02:56:41	Valori nella norma
201	DVRDRN73P08D075W	\N	GSSMRA22A64L660R	70061	2018-12-23 12:08:26	2018-12-27 00:00:49	Valori alti
701	RZZSLV81R01E150H	\N	HLZNLS17L59F949S	70515	2018-12-29 06:44:33	2019-01-02 04:08:58	Valori alti
101	DVRDRN73P08D075W	\N	TZZSRG70R27E481D	70421	2001-11-09 00:51:41	2001-11-19 12:04:10	Valori nella norma
241	DVRDRN73P08D075W	\N	GVNLRA19S63I173B	70035	2019-04-19 10:34:21	2019-04-26 05:17:56	Valori bassi
31	DVRDRN73P08D075W	\N	PCHLSN60T58A286J	70001	2018-07-22 06:31:23	2018-07-28 05:07:29	Valori bassi
221	DVRDRN73P08D075W	\N	GSSMRA22A64L660R	70069	2019-05-07 14:28:05	\N	\N
231	DVRDRN73P08D075W	\N	GVNLRA19S63I173B	70033	2019-06-15 09:49:37	\N	\N
271	DVRDRN73P08D075W	\N	GVNLRA19S63I173B	70201	2018-06-20 17:44:22	2018-07-02 22:04:05	Valori alti
111	DVRDRN73P08D075W	\N	TZZSRG70R27E481D	70101	2010-01-30 05:03:26	2010-01-30 15:59:03	Valori bassi
711	RZZSLV81R01E150H	\N	HLZNLS17L59F949S	70517	2018-10-01 00:48:08	2018-10-02 23:58:09	Valori nella norma
311	DVRDRN73P08D075W	\N	KFLRRT29A04I949D	70245	2019-02-27 06:00:33	2019-03-02 12:32:36	Valori bassi
261	DVRDRN73P08D075W	\N	GVNLRA19S63I173B	70089	2019-02-28 00:08:51	2019-03-06 06:01:47	Valori nella norma
11	DVRDRN73P08D075W	\N	PCHLSN60T58A286J	70491	2019-03-21 05:52:03	2019-03-25 13:07:18	Valori bassi
161	DVRDRN73P08D075W	\N	PDRNMO72L44H639L	70117	2012-12-25 16:02:39	2012-12-30 06:50:28	Valori nella norma
121	DVRDRN73P08D075W	\N	TZZSRG70R27E481D	70157	2011-05-10 17:13:15	2011-05-17 03:54:12	Valori nella norma
41	DVRDRN73P08D075W	\N	PCHLSN60T58A286J	70003	2018-07-15 19:44:55	2018-07-27 01:36:03	Valori nella norma
301	DVRDRN73P08D075W	\N	KFLRRT29A04I949D	70303	2019-06-02 15:55:43	\N	\N
741	RZZSLV81R01E150H	\N	RSSLCU31P45I173U	70421	2019-04-14 14:33:32	2019-04-16 22:15:47	Valori bassi
291	DVRDRN73P08D075W	\N	KFLRRT29A04I949D	70301	2019-04-11 18:18:29	2019-04-22 23:34:46	Valori nella norma
281	DVRDRN73P08D075W	\N	GVNLRA19S63I173B	70199	2019-01-28 05:29:08	2019-02-08 08:27:50	Valori nella norma
191	DVRDRN73P08D075W	\N	PCHFRZ49P25A116I	70067	2019-03-07 12:30:48	2019-03-09 14:37:40	Valori bassi
81	DVRDRN73P08D075W	\N	GVNTMS77R07I760K	70287	1983-08-25 10:12:01	1983-08-26 03:32:07	Valori nella norma
61	DVRDRN73P08D075W	\N	MTTMLD52P64F176A	70125	2019-02-15 17:02:26	2019-02-18 16:59:43	Valori bassi
141	DVRDRN73P08D075W	\N	TZZSRG70R27E481D	70313	1979-01-04 02:42:40	1979-01-11 13:41:22	Valori bassi
691	RZZSLV81R01E150H	\N	HLZNLS17L59F949S	70271	2019-05-28 22:00:46	\N	\N
21	DVRDRN73P08D075W	\N	PCHLSN60T58A286J	70483	2019-01-25 14:10:56	2019-02-08 18:05:28	Valori nella norma
211	DVRDRN73P08D075W	\N	GSSMRA22A64L660R	70063	2019-03-02 19:55:07	2019-03-05 04:53:51	Valori bassi
131	DVRDRN73P08D075W	\N	TZZSRG70R27E481D	70159	1997-02-18 02:35:15	1997-02-22 21:57:52	Valori alti
1	DVRDRN73P08D075W	\N	PCHLSN60T58A286J	70503	2019-06-20 20:44:58	\N	\N
781	RZZSLV81R01E150H	\N	LSLNMO31T62G173U	70065	2018-06-05 15:04:18	2018-06-08 11:03:11	Valori nella norma
761	RZZSLV81R01E150H	\N	LSLNMO31T62G173U	70427	2019-01-29 13:10:01	2019-02-06 17:37:25	Valori alti
861	RZZSLV81R01E150H	\N	KMEFLV86H22L111K	70175	2016-10-29 11:04:04	2016-11-12 21:12:06	Valori bassi
891	RZZSLV81R01E150H	\N	GFLDRD49M10F856Q	70125	2018-07-29 17:55:30	2018-08-08 00:37:05	Valori alti
911	RZZSLV81R01E150H	\N	GFLDRD49M10F856Q	70199	2019-01-02 07:13:14	2019-01-14 10:10:04	Valori alti
801	RZZSLV81R01E150H	\N	LSLNMO31T62G173U	70009	2018-10-10 14:07:21	2018-10-11 00:42:32	Valori nella norma
751	RZZSLV81R01E150H	\N	RSSLCU31P45I173U	70423	2019-04-09 01:03:42	2019-04-14 13:54:51	Valori nella norma
851	RZZSLV81R01E150H	\N	KMEFLV86H22L111K	70173	2002-10-09 14:36:03	2002-10-13 17:02:59	Valori nella norma
791	RZZSLV81R01E150H	\N	LSLNMO31T62G173U	70013	2019-04-01 03:50:36	2019-04-04 05:57:28	Valori bassi
871	RZZSLV81R01E150H	\N	KMEFLV86H22L111K	70177	2009-12-10 01:02:56	2009-12-18 07:59:00	Valori alti
881	RZZSLV81R01E150H	\N	GFLDRD49M10F856Q	70131	2019-05-28 15:49:52	\N	\N
821	RZZSLV81R01E150H	\N	GRLNNA47H56M173S	70037	2018-12-08 07:56:48	2018-12-12 17:57:02	Valori nella norma
771	RZZSLV81R01E150H	\N	LSLNMO31T62G173U	70441	2018-11-19 23:22:18	2018-12-01 07:17:03	Valori alti
901	RZZSLV81R01E150H	\N	GFLDRD49M10F856Q	70143	2018-12-20 04:01:05	2018-12-26 22:24:38	Valori alti
921	RZZSLV81R01E150H	\N	GFLDRD49M10F856Q	70193	2019-01-10 00:19:59	2019-01-12 15:55:48	Valori nella norma
931	RZZSLV81R01E150H	\N	GFLDRD49M10F856Q	70101	2018-10-11 21:29:45	2018-10-13 18:58:15	Valori bassi
831	RZZSLV81R01E150H	\N	KMEFLV86H22L111K	70051	2003-01-31 08:20:59	2003-02-10 00:44:03	Valori alti
811	RZZSLV81R01E150H	\N	GRLNNA47H56M173S	70007	2018-12-04 23:17:14	2018-12-16 08:10:02	Valori alti
841	RZZSLV81R01E150H	\N	KMEFLV86H22L111K	70171	2011-07-28 18:22:54	2011-07-31 08:31:30	Valori bassi
1791	MRIGZL85P49E481L	\N	SMNLSS92L59F307L	70027	2013-11-14 15:01:36	2013-11-26 06:42:38	Valori alti
1991	MRIGZL85P49E481L	\N	RCKFRZ46T60E065G	70433	2019-05-17 22:42:18	\N	\N
1971	MRIGZL85P49E481L	\N	RCKFRZ46T60E065G	70217	2018-07-02 20:09:42	2018-07-09 15:11:15	Valori nella norma
1861	MRIGZL85P49E481L	\N	BSLHLG45P45B160D	70199	2019-06-27 12:25:24	\N	\N
1891	MRIGZL85P49E481L	\N	FRTLSN14H19L490X	70311	2014-08-05 12:24:05	2014-08-18 18:59:50	Valori bassi
1831	MRIGZL85P49E481L	\N	SMNLSS92L59F307L	70239	2000-04-28 22:08:09	2000-05-06 16:33:05	Valori alti
1811	MRIGZL85P49E481L	\N	SMNLSS92L59F307L	70065	2017-01-30 11:38:47	2017-02-07 21:20:10	Valori alti
1871	MRIGZL85P49E481L	\N	BSLHLG45P45B160D	70177	2018-09-05 18:29:34	2018-09-07 08:29:48	Valori nella norma
1921	MRIGZL85P49E481L	\N	ZCCLNZ98E09C700J	70223	2002-01-13 19:15:37	2002-01-15 23:54:16	Valori nella norma
2051	MRIGZL85P49E481L	\N	MNTNLS46S52I839M	70243	2018-08-21 20:55:41	2018-08-26 13:15:25	Valori nella norma
1901	MRIGZL85P49E481L	\N	FRTLSN14H19L490X	70311	2017-01-31 05:33:26	2017-02-06 10:02:06	Valori bassi
2041	MRIGZL85P49E481L	\N	BRNGRL52H17D457Q	70241	2018-06-26 21:16:51	2018-07-02 00:53:57	Valori nella norma
2061	MRIGZL85P49E481L	\N	SCHFRZ37M14C400T	70245	2018-08-11 00:12:32	2018-08-23 14:03:05	Valori bassi
2091	MRIGZL85P49E481L	\N	SCHFRZ37M14C400T	70279	2018-08-07 18:55:13	2018-08-13 21:55:43	Valori nella norma
1931	MRIGZL85P49E481L	\N	KFLNCL79H42C727A	70111	1992-02-08 13:36:39	1992-02-13 19:50:32	Valori nella norma
2111	MRIGZL85P49E481L	\N	SCHFRZ37M14C400T	70215	2018-07-16 10:50:08	2018-07-28 20:05:30	Valori bassi
2011	MRIGZL85P49E481L	\N	BRNGRL52H17D457Q	70429	2019-01-28 14:38:38	2019-01-31 08:03:37	Valori nella norma
1801	MRIGZL85P49E481L	\N	SMNLSS92L59F307L	70041	1995-10-31 13:39:04	1995-11-04 12:50:33	Valori nella norma
1941	MRIGZL85P49E481L	\N	KFLNCL79H42C727A	70303	2016-11-16 09:43:10	2016-11-29 10:40:09	Valori nella norma
1851	MRIGZL85P49E481L	\N	BSLHLG45P45B160D	70297	2019-03-05 05:19:58	2019-03-13 06:20:17	Valori bassi
1881	MRIGZL85P49E481L	\N	BSLHLG45P45B160D	70309	2019-05-06 12:29:09	\N	\N
2031	MRIGZL85P49E481L	\N	BRNGRL52H17D457Q	70211	2019-03-28 20:34:39	2019-04-11 20:00:27	Valori nella norma
1911	MRIGZL85P49E481L	\N	ZCCLNZ98E09C700J	70421	2013-07-06 00:17:46	2013-07-08 18:16:55	Valori nella norma
2081	MRIGZL85P49E481L	\N	SCHFRZ37M14C400T	70281	2019-03-11 06:22:02	2019-03-17 10:01:07	Valori alti
1951	MRIGZL85P49E481L	\N	RCKFRZ46T60E065G	70057	2019-03-16 07:38:37	2019-03-23 10:38:35	Valori bassi
1981	MRIGZL85P49E481L	\N	RCKFRZ46T60E065G	70435	2019-02-05 10:28:07	2019-02-16 00:35:17	Valori bassi
1961	MRIGZL85P49E481L	\N	RCKFRZ46T60E065G	70255	2018-08-16 11:03:36	2018-08-25 14:08:55	Valori bassi
2001	MRIGZL85P49E481L	\N	RCKFRZ46T60E065G	70431	2018-10-23 13:43:41	2018-10-31 18:48:20	Valori nella norma
2101	MRIGZL85P49E481L	\N	SCHFRZ37M14C400T	70267	2019-01-14 22:04:49	2019-01-17 18:06:39	Valori bassi
1821	MRIGZL85P49E481L	\N	SMNLSS92L59F307L	70117	1997-08-11 22:02:27	1997-08-20 15:02:04	Valori nella norma
2071	MRIGZL85P49E481L	\N	SCHFRZ37M14C400T	70283	2018-12-07 08:15:52	2018-12-21 13:58:48	Valori alti
1841	MRIGZL85P49E481L	\N	SMNLSS92L59F307L	70241	2001-07-28 05:40:32	2001-08-08 06:37:17	Valori alti
2021	MRIGZL85P49E481L	\N	BRNGRL52H17D457Q	70421	2018-10-30 18:43:43	2018-11-09 01:49:27	Valori alti
2341	BRSNCL79A61D484Q	\N	MSTNMO60E66A635P	70273	2019-06-16 01:29:39	\N	\N
351	NRDLDI83E50E614G	\N	ZZLSLV15R62L162L	70151	2018-12-13 17:45:51	2018-12-14 11:20:44	Valori bassi
1521	PRTMRZ80C26A537X	\N	MRIPQL73R14A952Z	70305	1991-03-23 01:47:13	1991-04-05 06:41:33	Valori nella norma
2551	BRSNCL79A61D484Q	\N	GMPPTR66C24D821H	70429	2019-03-15 15:17:45	2019-03-17 17:13:12	Valori nella norma
1181	SMMTMS72M20G443H	\N	PRCNGL30L64A022U	70093	2018-12-23 10:37:20	2019-01-05 14:32:35	Valori alti
1081	SMMTMS72M20G443H	\N	GRNMSM47T15H004T	70499	2018-09-09 06:02:20	2018-09-22 05:08:28	Valori nella norma
481	NRDLDI83E50E614G	\N	BRSLGE75C46E398O	70045	1988-03-28 08:51:13	1988-04-05 07:25:01	Valori alti
1491	PRTMRZ80C26A537X	\N	PRPNTN38T09D246K	70501	2018-10-22 09:05:58	2018-11-01 01:54:36	Valori bassi
1091	SMMTMS72M20G443H	\N	GRNMSM47T15H004T	70501	2019-05-12 16:26:35	\N	\N
1541	PRTMRZ80C26A537X	\N	MRIPQL73R14A952Z	70311	2017-11-21 11:31:54	2017-11-26 19:54:02	Valori alti
951	SMMTMS72M20G443H	\N	PRTLNE42S67D663Q	70207	2018-10-16 17:35:13	2018-10-21 11:56:23	Valori nella norma
2431	BRSNCL79A61D484Q	\N	ZCCLNE08T47D631A	70007	2012-10-22 19:36:50	2012-10-29 09:44:24	Valori bassi
1531	PRTMRZ80C26A537X	\N	MRIPQL73R14A952Z	70307	1976-01-01 15:18:16	1976-01-14 18:33:22	Valori alti
1711	PRTMRZ80C26A537X	\N	PRCLVC38B49G644S	70451	2019-04-10 09:30:21	2019-04-11 03:42:13	Valori alti
511	NRDLDI83E50E614G	\N	DPLLRA05L44B404J	70113	2013-08-11 09:54:04	2013-08-25 03:28:14	Valori alti
1671	PRTMRZ80C26A537X	\N	HLZSRA39T42L137R	70307	2019-05-08 21:12:26	\N	\N
1351	SMMTMS72M20G443H	\N	CHMLNE17M45L896F	70055	2019-05-21 09:59:00	\N	\N
341	NRDLDI83E50E614G	\N	PLLPRI69A28D371I	70311	2018-07-22 00:42:10	2018-08-04 11:03:00	Valori nella norma
2491	BRSNCL79A61D484Q	\N	GGRPTR15D05L097I	70273	2017-07-15 03:34:23	2017-07-22 23:42:20	Valori bassi
431	NRDLDI83E50E614G	\N	FRNMRT70E16L490F	70085	1993-05-16 02:54:10	1993-05-25 00:39:37	Valori bassi
1691	PRTMRZ80C26A537X	\N	PRCLVC38B49G644S	70489	2018-10-19 20:56:33	2018-10-31 22:03:06	Valori nella norma
2241	BRSNCL79A61D484Q	\N	MRIGPL08H02D336D	70211	2016-11-25 20:33:37	2016-12-07 22:52:54	Valori nella norma
2361	BRSNCL79A61D484Q	\N	MSTNMO60E66A635P	70125	2018-07-11 11:31:08	2018-07-18 11:24:20	Valori nella norma
491	NRDLDI83E50E614G	\N	DPLLRA05L44B404J	70047	2008-04-02 04:38:26	2008-04-10 12:47:13	Valori bassi
2331	BRSNCL79A61D484Q	\N	MSTNMO60E66A635P	70291	2018-09-27 14:16:35	2018-10-06 21:13:06	Valori bassi
2561	BRSNCL79A61D484Q	\N	DVRNLS18R50H152G	70431	2019-01-17 19:15:21	2019-01-27 23:47:08	Valori bassi
2471	BRSNCL79A61D484Q	\N	GGRPTR15D05L097I	70249	2018-01-07 02:27:34	2018-01-08 02:28:59	Valori alti
541	NRDLDI83E50E614G	\N	BLDDMN74T11C756X	70119	2005-05-27 09:34:33	2005-06-04 09:20:38	Valori alti
2501	BRSNCL79A61D484Q	\N	GGRPTR15D05L097I	70271	2018-07-30 18:34:50	2018-08-03 03:35:50	Valori bassi
2141	BRSNCL79A61D484Q	\N	GVNMTN72E61E981G	70045	2006-06-07 20:31:50	2006-06-14 20:08:51	Valori nella norma
451	NRDLDI83E50E614G	\N	ZCCDVD99S09E334C	70089	2007-06-01 03:11:22	2007-06-12 17:42:10	Valori alti
1471	PRTMRZ80C26A537X	\N	TMSCRL55H56L915E	70451	2019-02-28 12:44:26	2019-03-13 12:33:16	Valori nella norma
971	SMMTMS72M20G443H	\N	MTTDTL40P55D243Y	70267	2018-08-18 20:49:57	2018-08-20 01:58:31	Valori nella norma
1331	SMMTMS72M20G443H	\N	PRCDNL50L03F835T	70011	2019-04-17 00:16:08	2019-04-21 15:41:14	Valori alti
941	SMMTMS72M20G443H	\N	PRTLNE42S67D663Q	70103	2019-01-12 12:53:04	2019-01-14 08:15:11	Valori bassi
2271	BRSNCL79A61D484Q	\N	BNZMRT74P01L106V	70159	1989-04-15 04:40:51	1989-04-17 19:44:10	Valori bassi
2131	BRSNCL79A61D484Q	\N	GVNMTN72E61E981G	70165	1983-09-07 19:25:12	1983-09-17 01:43:47	Valori bassi
991	SMMTMS72M20G443H	\N	CHMCNZ46M41E757B	70243	2019-06-01 13:01:49	\N	\N
2541	BRSNCL79A61D484Q	\N	GMPPTR66C24D821H	70421	2019-02-07 23:44:21	2019-02-21 21:48:09	Valori nella norma
2371	BRSNCL79A61D484Q	\N	MSTNMO60E66A635P	70077	2018-07-22 01:42:01	2018-08-01 16:50:22	Valori nella norma
2401	BRSNCL79A61D484Q	\N	ZCCLNE08T47D631A	70013	2012-07-01 12:36:36	2012-07-02 18:06:16	Valori nella norma
641	NRDLDI83E50E614G	\N	BRNNDA05R53E981M	70245	2007-01-02 14:48:37	2007-01-07 00:26:11	Valori bassi
1571	PRTMRZ80C26A537X	\N	SCRVNC47D56A967W	70233	2018-10-24 00:53:42	2018-11-03 19:02:17	Valori alti
2571	BRSNCL79A61D484Q	\N	DVRNLS18R50H152G	70501	2019-05-12 12:57:37	\N	\N
1751	PRTMRZ80C26A537X	\N	BRNHLG96P42A537W	70113	2012-10-29 17:44:28	2012-11-01 12:28:58	Valori alti
961	SMMTMS72M20G443H	\N	PRTLNE42S67D663Q	70211	2019-03-07 21:01:47	2019-03-17 12:57:44	Valori alti
1781	PRTMRZ80C26A537X	\N	BRNHLG96P42A537W	70145	2001-06-05 08:37:53	2001-06-10 13:44:34	Valori bassi
1581	PRTMRZ80C26A537X	\N	SCRVNC47D56A967W	70231	2019-02-22 12:07:16	2019-02-24 18:52:07	Valori nella norma
1301	SMMTMS72M20G443H	\N	FNTGDU07L21D311U	70115	2017-06-25 02:22:31	2017-07-03 21:20:45	Valori alti
2311	BRSNCL79A61D484Q	\N	BNZMRT74P01L106V	70177	1976-03-29 20:20:39	1976-04-12 05:39:37	Valori nella norma
2171	BRSNCL79A61D484Q	\N	GRNPTR17H14L033Y	70087	2019-05-20 14:28:56	\N	\N
2121	BRSNCL79A61D484Q	\N	GVNMTN72E61E981G	70099	1983-08-01 22:51:35	1983-08-05 15:15:25	Valori nella norma
1171	SMMTMS72M20G443H	\N	PRCNGL30L64A022U	70091	2019-04-18 18:53:22	2019-05-03 00:15:06	Valori bassi
401	NRDLDI83E50E614G	\N	FRNMRT70E16L490F	70005	2013-07-27 13:05:47	2013-07-29 06:11:22	Valori alti
2191	BRSNCL79A61D484Q	\N	GRNPTR17H14L033Y	70083	2019-03-17 21:19:12	2019-03-27 21:45:21	Valori alti
2161	BRSNCL79A61D484Q	\N	GRNPTR17H14L033Y	70041	2019-04-30 00:46:14	2019-05-07 14:11:11	Valori alti
601	NRDLDI83E50E614G	\N	BRNNDA05R53E981M	70531	2014-04-17 04:58:03	2014-04-26 13:20:33	Valori bassi
1701	PRTMRZ80C26A537X	\N	PRCLVC38B49G644S	70455	2018-09-18 15:54:42	2018-09-26 12:32:31	Valori nella norma
2391	BRSNCL79A61D484Q	\N	ZCCLNE08T47D631A	70057	2013-10-17 21:19:07	2013-10-22 02:41:44	Valori bassi
1651	PRTMRZ80C26A537X	\N	HLZSRA39T42L137R	70117	2018-09-01 00:32:50	2018-09-08 03:16:15	Valori alti
1141	SMMTMS72M20G443H	\N	MSTRCC17M41A158X	70001	2019-03-18 09:40:45	2019-03-26 09:47:16	Valori bassi
461	NRDLDI83E50E614G	\N	BRSLGE75C46E398O	70041	1991-03-31 19:34:36	1991-04-02 09:49:34	Valori bassi
561	NRDLDI83E50E614G	\N	BLDDMN74T11C756X	70523	1983-03-29 20:47:38	1983-04-04 07:18:22	Valori bassi
1431	PRTMRZ80C26A537X	\N	PRCNNT81P54L322J	70433	1995-03-06 18:41:29	1995-03-16 18:31:41	Valori alti
1481	PRTMRZ80C26A537X	\N	PRPNTN38T09D246K	70499	2019-04-22 14:30:50	2019-04-23 20:35:39	Valori alti
1031	SMMTMS72M20G443H	\N	CHMCNZ46M41E757B	70151	2019-03-28 11:17:10	2019-03-29 18:07:18	Valori nella norma
1761	PRTMRZ80C26A537X	\N	BRNHLG96P42A537W	70101	2012-08-10 02:42:39	2012-08-19 21:06:44	Valori nella norma
1771	PRTMRZ80C26A537X	\N	BRNHLG96P42A537W	70125	2017-09-24 14:26:18	2017-10-01 07:24:10	Valori alti
551	NRDLDI83E50E614G	\N	BLDDMN74T11C756X	70521	2015-01-31 23:03:18	2015-02-13 00:17:52	Valori nella norma
1161	SMMTMS72M20G443H	\N	PRCNGL30L64A022U	70143	2018-07-23 18:16:01	2018-08-07 10:08:45	Valori alti
2461	BRSNCL79A61D484Q	\N	GGRPTR15D05L097I	70211	2015-08-07 03:03:59	2015-08-17 00:13:13	Valori bassi
1361	SMMTMS72M20G443H	\N	CHMLNE17M45L896F	70033	2018-11-22 23:19:22	2018-11-26 18:14:15	Valori bassi
1371	SMMTMS72M20G443H	\N	CHMLNE17M45L896F	70001	2018-11-16 09:38:05	2018-11-19 00:55:56	Valori bassi
1041	SMMTMS72M20G443H	\N	CHMCNZ46M41E757B	70443	2018-06-28 16:12:08	2018-07-12 22:57:06	Valori bassi
1661	PRTMRZ80C26A537X	\N	HLZSRA39T42L137R	70091	2018-08-29 08:14:00	2018-09-02 06:24:45	Valori alti
521	NRDLDI83E50E614G	\N	DPLLRA05L44B404J	70115	2007-04-05 08:19:51	2007-04-09 04:11:47	Valori bassi
1501	PRTMRZ80C26A537X	\N	PRPNTN38T09D246K	70201	2019-03-08 19:46:36	2019-03-11 05:59:40	Valori bassi
331	NRDLDI83E50E614G	\N	PLLPRI69A28D371I	70249	2018-07-05 11:58:39	2018-07-16 19:57:08	Valori bassi
1561	PRTMRZ80C26A537X	\N	GRDLCA81C49L957D	70245	1995-04-02 04:13:15	1995-04-07 09:15:03	Valori bassi
981	SMMTMS72M20G443H	\N	MTTDTL40P55D243Y	70269	2019-05-06 10:04:33	\N	\N
2181	BRSNCL79A61D484Q	\N	GRNPTR17H14L033Y	70085	2018-12-16 18:28:02	2018-12-23 23:17:34	Valori nella norma
1021	SMMTMS72M20G443H	\N	CHMCNZ46M41E757B	70049	2018-06-03 02:13:12	2018-06-04 06:32:33	Valori bassi
1001	SMMTMS72M20G443H	\N	CHMCNZ46M41E757B	70145	2018-09-25 16:22:52	2018-09-29 04:57:04	Valori nella norma
1221	SMMTMS72M20G443H	\N	MDNMRA01S48C756H	70305	2008-06-09 18:56:56	2008-06-10 20:06:43	Valori bassi
2451	BRSNCL79A61D484Q	\N	ZZLDRN97B55C994H	70111	2011-01-03 02:40:27	2011-01-10 08:37:48	Valori bassi
2291	BRSNCL79A61D484Q	\N	BNZMRT74P01L106V	70143	1983-02-21 18:45:15	1983-02-22 17:44:05	Valori alti
1591	PRTMRZ80C26A537X	\N	FRNCMN07B16A520T	70209	2013-05-08 03:52:55	2013-05-09 14:30:33	Valori nella norma
2321	BRSNCL79A61D484Q	\N	MSTNMO60E66A635P	70295	2018-06-07 17:38:04	2018-06-16 22:24:46	Valori alti
1281	SMMTMS72M20G443H	\N	FNTGDU07L21D311U	70111	2011-05-10 09:58:48	2011-05-18 16:32:05	Valori bassi
2381	BRSNCL79A61D484Q	\N	ZCCLNE08T47D631A	70061	2010-12-28 01:51:20	2010-12-31 17:21:02	Valori bassi
1461	PRTMRZ80C26A537X	\N	TMSCRL55H56L915E	70491	2018-07-08 23:12:05	2018-07-19 03:05:37	Valori bassi
531	NRDLDI83E50E614G	\N	BLDDMN74T11C756X	70117	1990-08-07 23:39:19	1990-08-16 15:53:33	Valori alti
2151	BRSNCL79A61D484Q	\N	GVNMTN72E61E981G	70031	2002-08-01 13:20:07	2002-08-09 13:08:44	Valori bassi
2231	BRSNCL79A61D484Q	\N	MRIGPL08H02D336D	70203	2011-10-18 15:31:22	2011-10-19 13:40:40	Valori alti
1211	SMMTMS72M20G443H	\N	MDNMRA01S48C756H	70119	2015-02-10 04:04:03	2015-02-24 21:19:09	Valori bassi
361	NRDLDI83E50E614G	\N	ZZLSLV15R62L162L	70149	2019-06-05 04:12:44	\N	\N
321	NRDLDI83E50E614G	\N	PLLPRI69A28D371I	70247	2018-06-22 16:55:25	2018-06-27 04:35:57	Valori bassi
1411	PRTMRZ80C26A537X	\N	PRCNNT81P54L322J	70497	2018-04-29 02:12:58	2018-05-13 12:20:55	Valori alti
1261	SMMTMS72M20G443H	\N	FNTGDU07L21D311U	70105	2016-09-29 21:11:04	2016-10-10 04:12:48	Valori bassi
2301	BRSNCL79A61D484Q	\N	BNZMRT74P01L106V	70165	1990-02-18 19:51:57	1990-02-25 12:43:27	Valori alti
1681	PRTMRZ80C26A537X	\N	HLZSRA39T42L137R	70491	2018-08-14 19:28:57	2018-08-20 21:40:18	Valori nella norma
571	NRDLDI83E50E614G	\N	BLDDMN74T11C756X	70525	1998-05-01 07:12:28	1998-05-02 11:50:03	Valori bassi
1201	SMMTMS72M20G443H	\N	PRCNGL30L64A022U	70113	2018-12-27 22:49:57	2019-01-11 22:14:24	Valori nella norma
631	NRDLDI83E50E614G	\N	BRNNDA05R53E981M	70243	2017-08-15 16:58:07	2017-08-25 13:29:27	Valori nella norma
1741	PRTMRZ80C26A537X	\N	BRNHLG96P42A537W	70311	2012-12-11 10:55:04	2012-12-17 15:32:39	Valori bassi
1551	PRTMRZ80C26A537X	\N	GRDLCA81C49L957D	70301	1992-10-05 03:31:14	1992-10-16 23:11:13	Valori alti
1071	SMMTMS72M20G443H	\N	GRNMSM47T15H004T	70481	2018-11-11 03:51:51	2018-11-22 22:42:40	Valori nella norma
1641	PRTMRZ80C26A537X	\N	HLZSRA39T42L137R	70113	2018-12-29 15:03:31	2019-01-10 16:30:19	Valori alti
1241	SMMTMS72M20G443H	\N	MDNMRA01S48C756H	70041	2009-07-11 05:20:38	2009-07-16 11:10:34	Valori nella norma
471	NRDLDI83E50E614G	\N	BRSLGE75C46E398O	70043	2018-05-04 05:29:14	2018-05-17 13:05:40	Valori alti
2221	BRSNCL79A61D484Q	\N	MRIGPL08H02D336D	70105	2011-12-27 22:34:53	2012-01-01 02:48:21	Valori alti
1611	PRTMRZ80C26A537X	\N	TCCMRA11B58H330G	70203	2017-05-21 22:24:47	2017-06-05 02:12:14	Valori nella norma
1401	PRTMRZ80C26A537X	\N	PRCNNT81P54L322J	70507	2007-05-14 09:39:06	2007-05-18 23:22:45	Valori nella norma
2531	BRSNCL79A61D484Q	\N	GMPPTR66C24D821H	70299	2018-08-12 05:58:08	2018-08-13 09:40:45	Valori alti
1391	PRTMRZ80C26A537X	\N	PRCNNT81P54L322J	70509	2011-04-28 11:29:48	2011-05-11 07:09:13	Valori alti
1051	SMMTMS72M20G443H	\N	GRNMSM47T15H004T	70445	2019-02-18 23:28:53	2019-02-19 05:28:36	Valori bassi
2511	BRSNCL79A61D484Q	\N	GGRPTR15D05L097I	70233	2018-11-16 07:06:42	2018-11-21 05:55:39	Valori bassi
2251	BRSNCL79A61D484Q	\N	MRIGPL08H02D336D	70231	2019-02-11 08:40:26	2019-02-22 02:03:17	Valori nella norma
581	NRDLDI83E50E614G	\N	BLDDMN74T11C756X	70527	1994-05-16 05:06:50	1994-05-18 20:45:45	Valori alti
2441	BRSNCL79A61D484Q	\N	ZZLDRN97B55C994H	70113	2011-02-13 07:19:41	2011-02-22 09:53:36	Valori nella norma
2351	BRSNCL79A61D484Q	\N	MSTNMO60E66A635P	70271	2019-02-18 10:06:07	2019-02-18 19:35:57	Valori alti
1151	SMMTMS72M20G443H	\N	PRCNGL30L64A022U	70151	2019-05-14 09:30:44	\N	\N
1121	SMMTMS72M20G443H	\N	MSTRCC17M41A158X	70501	2019-05-06 01:56:32	\N	\N
661	NRDLDI83E50E614G	\N	BRSFNC35L17D336O	70233	2018-06-21 05:54:53	2018-07-03 21:16:17	Valori nella norma
2411	BRSNCL79A61D484Q	\N	ZCCLNE08T47D631A	70011	2017-11-23 04:25:57	2017-11-23 13:48:41	Valori alti
2261	BRSNCL79A61D484Q	\N	BNZMRT74P01L106V	70229	1986-07-17 06:59:44	1986-07-21 07:54:08	Valori nella norma
2201	BRSNCL79A61D484Q	\N	MRIGPL08H02D336D	70081	2008-11-22 03:33:45	2008-11-27 20:59:46	Valori nella norma
391	NRDLDI83E50E614G	\N	FRNMRT70E16L490F	70079	2009-09-18 01:53:46	2009-09-21 09:06:33	Valori alti
1311	SMMTMS72M20G443H	\N	FNTGDU07L21D311U	70245	2017-09-22 17:12:56	2017-10-04 15:50:00	Valori bassi
2281	BRSNCL79A61D484Q	\N	BNZMRT74P01L106V	70139	1981-02-28 06:09:02	1981-03-03 20:01:52	Valori bassi
1441	PRTMRZ80C26A537X	\N	PRCNNT81P54L322J	70431	1994-09-05 13:22:37	1994-09-05 17:03:39	Valori alti
1101	SMMTMS72M20G443H	\N	GRNMSM47T15H004T	70505	2018-06-01 13:38:25	2018-06-01 19:52:03	Valori nella norma
441	NRDLDI83E50E614G	\N	FRNMRT70E16L490F	70087	1972-01-17 04:49:47	1972-01-28 23:25:31	Valori alti
1271	SMMTMS72M20G443H	\N	FNTGDU07L21D311U	70109	2016-03-04 00:13:43	2016-03-08 08:04:45	Valori nella norma
1511	PRTMRZ80C26A537X	\N	PRPNTN38T09D246K	70301	2019-01-26 08:08:40	2019-01-28 21:24:28	Valori alti
1321	SMMTMS72M20G443H	\N	PRCDNL50L03F835T	70279	2018-11-01 00:25:29	2018-11-07 04:12:52	Valori nella norma
371	NRDLDI83E50E614G	\N	ZZLSLV15R62L162L	70147	2019-02-20 17:25:29	2019-03-04 14:00:48	Valori alti
2481	BRSNCL79A61D484Q	\N	GGRPTR15D05L097I	70261	2016-05-09 15:46:59	2016-05-18 04:15:50	Valori alti
1631	PRTMRZ80C26A537X	\N	HLZSRA39T42L137R	70199	2018-11-19 06:55:00	2018-11-24 15:16:49	Valori nella norma
2521	BRSNCL79A61D484Q	\N	GMPPTR66C24D821H	70251	2019-05-03 14:24:36	\N	\N
591	NRDLDI83E50E614G	\N	BRNNDA05R53E981M	70529	2016-02-06 00:01:09	2016-02-12 03:17:38	Valori nella norma
2211	BRSNCL79A61D484Q	\N	MRIGPL08H02D336D	70087	2013-04-04 04:36:14	2013-04-11 09:28:20	Valori nella norma
2421	BRSNCL79A61D484Q	\N	ZCCLNE08T47D631A	70009	2016-11-22 02:47:49	2016-11-26 06:13:51	Valori nella norma
2581	BRSNCL79A61D484Q	\N	DVRNLS18R50H152G	70505	2019-03-23 12:57:18	2019-04-02 07:43:12	Valori bassi
1381	SMMTMS72M20G443H	\N	CHMLNE17M45L896F	70511	2019-06-09 03:49:51	\N	\N
1011	SMMTMS72M20G443H	\N	CHMCNZ46M41E757B	70047	2019-06-17 02:05:02	\N	\N
651	NRDLDI83E50E614G	\N	BRSFNC35L17D336O	70241	2018-07-08 16:15:17	2018-07-20 01:11:13	Valori alti
381	NRDLDI83E50E614G	\N	MNFSMN55B08B203N	70077	2019-03-06 06:36:14	2019-03-10 05:58:57	Valori nella norma
1291	SMMTMS72M20G443H	\N	FNTGDU07L21D311U	70113	2011-11-17 04:28:12	2011-11-27 15:43:03	Valori alti
1621	PRTMRZ80C26A537X	\N	TCCMRA11B58H330G	70201	2012-12-15 18:14:25	2012-12-19 06:35:09	Valori bassi
421	NRDLDI83E50E614G	\N	FRNMRT70E16L490F	70005	1977-02-13 11:59:52	1977-02-26 08:34:29	Valori alti
1731	PRTMRZ80C26A537X	\N	PRCLVC38B49G644S	70317	2018-06-07 22:01:44	2018-06-17 01:02:06	Valori alti
411	NRDLDI83E50E614G	\N	FRNMRT70E16L490F	70003	2004-02-13 07:35:52	2004-02-24 06:48:49	Valori alti
1451	PRTMRZ80C26A537X	\N	TMSCRL55H56L915E	70461	2019-06-26 12:52:20	\N	\N
611	NRDLDI83E50E614G	\N	BRNNDA05R53E981M	70299	2006-01-04 23:56:12	2006-01-05 14:13:20	Valori alti
501	NRDLDI83E50E614G	\N	DPLLRA05L44B404J	70111	2012-01-26 00:27:18	2012-02-08 17:47:36	Valori nella norma
1191	SMMTMS72M20G443H	\N	PRCNGL30L64A022U	70095	2018-10-23 18:14:01	2018-10-29 23:14:20	Valori nella norma
671	NRDLDI83E50E614G	\N	MYRFNC32D59L527G	70219	2019-06-11 03:40:36	\N	\N
621	NRDLDI83E50E614G	\N	BRNNDA05R53E981M	70251	2008-08-30 19:28:16	2008-09-01 19:47:29	Valori alti
1341	SMMTMS72M20G443H	\N	PRCDNL50L03F835T	70099	2019-02-18 15:02:59	2019-02-23 01:14:04	Valori alti
1061	SMMTMS72M20G443H	\N	GRNMSM47T15H004T	70479	2018-07-22 07:52:21	2018-07-24 12:08:23	Valori alti
1131	SMMTMS72M20G443H	\N	MSTRCC17M41A158X	70451	2018-12-19 21:31:32	2018-12-26 06:19:48	Valori alti
1601	PRTMRZ80C26A537X	\N	FRNCMN07B16A520T	70205	2008-12-10 09:07:49	2008-12-16 09:12:23	Valori bassi
1231	SMMTMS72M20G443H	\N	MDNMRA01S48C756H	70487	2001-11-12 09:00:26	2001-11-21 06:11:16	Valori bassi
1421	PRTMRZ80C26A537X	\N	PRCNNT81P54L322J	70495	2007-05-31 17:55:30	2007-06-06 06:07:08	Valori bassi
1111	SMMTMS72M20G443H	\N	MSTRCC17M41A158X	70521	2018-12-03 01:46:23	2018-12-06 21:14:04	Valori nella norma
1721	PRTMRZ80C26A537X	\N	PRCLVC38B49G644S	70319	2018-11-18 07:58:42	2018-11-26 21:43:12	Valori nella norma
1251	SMMTMS72M20G443H	\N	MDNMRA01S48C756H	70211	2008-08-10 07:12:43	2008-08-16 13:30:16	Valori nella norma
731	RZZSLV81R01E150H	\N	PCHGRL78M15E461T	70501	2007-12-05 06:59:04	2007-12-20 00:06:28	Valori nella norma
721	RZZSLV81R01E150H	\N	HLZNLS17L59F949S	70519	2018-12-13 03:05:55	2018-12-21 11:23:27	Valori alti
251	DVRDRN73P08D075W	\N	GVNLRA19S63I173B	70037	2018-10-13 01:27:34	2018-10-26 06:21:57	Valori alti
171	DVRDRN73P08D075W	\N	PDRNMO72L44H639L	70067	2003-03-09 11:42:15	2003-03-14 18:15:14	Valori nella norma
151	DVRDRN73P08D075W	\N	TZZSRG70R27E481D	70097	1972-04-17 20:15:43	1972-04-18 17:17:09	Valori alti
91	DVRDRN73P08D075W	\N	GVNTMS77R07I760K	70529	2007-04-27 13:22:39	2007-04-30 09:47:21	Valori nella norma
\.


--
-- TOC entry 2904 (class 0 OID 19184)
-- Dependencies: 204
-- Data for Name: prescrizione_farmaci; Type: TABLE DATA; Schema: progettoipw; Owner: postgres
--

COPY progettoipw.prescrizione_farmaci (id, cf_med, cf_paz, idfarmaco, data_prescr, data_erog) FROM stdin;
0	DVRDRN73P08D075W	PCHLSN60T58A286J	41271	2018-11-18 16:31:12	2018-11-30 04:27:34
10	DVRDRN73P08D075W	PCHLSN60T58A286J	36497	2018-12-24 03:18:45	2019-01-03 04:32:05
20	DVRDRN73P08D075W	PCHLSN60T58A286J	20465	2019-02-22 10:08:15	2019-02-22 19:55:03
30	DVRDRN73P08D075W	PCHLSN60T58A286J	41788	2018-08-24 23:27:18	2018-08-27 23:41:55
40	DVRDRN73P08D075W	MTTMLD52P64F176A	33315	2018-08-10 15:04:20	2018-08-13 05:00:25
50	DVRDRN73P08D075W	TZZSRG70R27E481D	41715	1982-06-18 08:38:22	1982-07-03 04:48:50
60	DVRDRN73P08D075W	TZZSRG70R27E481D	17389	2014-06-02 21:21:45	2014-06-04 13:07:21
70	DVRDRN73P08D075W	PCHFRZ49P25A116I	41316	2019-06-02 12:21:28	\N
80	DVRDRN73P08D075W	PCHFRZ49P25A116I	32318	2018-06-13 02:49:11	2018-06-22 04:07:50
90	DVRDRN73P08D075W	GSSMRA22A64L660R	43719	2018-07-01 03:23:01	2018-07-05 20:18:27
100	DVRDRN73P08D075W	GVNLRA19S63I173B	42892	2019-03-03 11:41:21	2019-03-10 07:47:40
110	DVRDRN73P08D075W	GVNLRA19S63I173B	46463	2018-11-04 14:48:52	2018-11-14 07:06:54
120	DVRDRN73P08D075W	GVNLRA19S63I173B	42978	2018-11-29 19:43:40	2018-12-14 03:09:10
130	DVRDRN73P08D075W	MRLMRA02M56D663B	33656	2007-09-10 11:34:03	2007-09-25 02:59:05
140	DVRDRN73P08D075W	MRLMRA02M56D663B	37045	2014-02-19 01:07:15	2014-03-01 11:57:12
150	DVRDRN73P08D075W	MRLMRA02M56D663B	43903	2006-03-05 02:20:20	2006-03-10 18:40:03
160	NRDLDI83E50E614G	TZZGZL80A61F307B	42545	1981-06-26 15:20:55	1981-07-10 06:21:32
170	NRDLDI83E50E614G	TZZGZL80A61F307B	45034	1990-03-13 19:29:53	1990-03-28 11:03:17
180	NRDLDI83E50E614G	TZZGZL80A61F307B	36061	2014-12-22 16:14:53	2015-01-03 08:37:48
190	NRDLDI83E50E614G	TZZGZL80A61F307B	12475	1989-11-24 12:05:00	1989-11-28 22:48:18
200	NRDLDI83E50E614G	TZZGZL80A61F307B	24507	1990-07-22 02:35:28	1990-07-24 23:12:33
210	NRDLDI83E50E614G	ZZLSLV15R62L162L	28850	2019-06-22 19:02:31	\N
220	NRDLDI83E50E614G	ZZLSLV15R62L162L	37045	2019-01-08 15:53:44	2019-01-09 12:20:24
230	NRDLDI83E50E614G	ZZLSLV15R62L162L	41993	2018-11-27 19:49:46	2018-12-08 21:59:14
240	NRDLDI83E50E614G	MNFSMN55B08B203N	26667	2019-04-30 16:52:07	2019-05-08 12:43:29
250	NRDLDI83E50E614G	ZCCDVD99S09E334C	41788	2008-03-21 09:09:18	2008-03-23 22:15:18
260	NRDLDI83E50E614G	DPLLRA05L44B404J	45969	2011-08-27 01:51:36	2011-09-06 08:20:31
270	NRDLDI83E50E614G	DPLLRA05L44B404J	40440	2012-02-15 00:25:51	2012-02-24 14:09:54
280	NRDLDI83E50E614G	DPLLRA05L44B404J	29543	2014-10-13 00:55:18	2014-10-23 05:56:14
290	NRDLDI83E50E614G	DPLLRA05L44B404J	36478	2018-11-25 20:55:53	2018-12-09 04:38:44
300	NRDLDI83E50E614G	DPLLRA05L44B404J	41316	2011-12-06 09:13:55	2011-12-19 07:46:15
310	NRDLDI83E50E614G	BLDDMN74T11C756X	45595	2013-11-19 13:30:54	2013-11-21 11:57:37
320	NRDLDI83E50E614G	BLDDMN74T11C756X	45266	1984-01-24 04:58:04	1984-02-01 04:30:02
330	NRDLDI83E50E614G	BLDDMN74T11C756X	43527	2005-09-08 12:20:35	2005-09-19 11:24:35
340	NRDLDI83E50E614G	BRNNDA05R53E981M	42978	2017-12-18 23:56:03	2017-12-26 19:27:12
350	NRDLDI83E50E614G	BRNNDA05R53E981M	20582	2013-11-23 03:53:40	2013-11-24 05:34:01
360	NRDLDI83E50E614G	BRNNDA05R53E981M	45034	2006-09-12 15:00:31	2006-09-25 16:18:18
370	NRDLDI83E50E614G	BRNNDA05R53E981M	44269	2015-07-26 13:57:55	2015-07-26 17:03:45
380	NRDLDI83E50E614G	BRNNDA05R53E981M	20711	2019-05-08 03:20:40	\N
390	NRDLDI83E50E614G	BRSFNC35L17D336O	37720	2018-08-09 12:39:24	2018-08-24 04:01:42
400	NRDLDI83E50E614G	MYRFNC32D59L527G	34949	2018-11-12 19:04:48	2018-11-18 21:52:52
410	NRDLDI83E50E614G	MYRFNC32D59L527G	35433	2019-03-13 08:58:35	2019-03-25 01:03:35
420	NRDLDI83E50E614G	MYRFNC32D59L527G	20711	2018-09-04 14:14:26	2018-09-18 15:46:24
430	RZZSLV81R01E150H	HLZNLS17L59F949S	44977	2018-07-11 03:12:08	2018-07-14 21:47:01
440	RZZSLV81R01E150H	HLZNLS17L59F949S	45034	2018-07-29 11:58:18	2018-07-31 18:41:39
450	RZZSLV81R01E150H	GRDFLV28R05A178Z	41693	2018-06-13 02:23:42	2018-06-27 16:02:10
460	RZZSLV81R01E150H	GRDFLV28R05A178Z	27830	2019-02-06 09:54:16	2019-02-10 16:02:07
470	RZZSLV81R01E150H	PCHGRL78M15E461T	45246	1985-09-05 22:48:15	1985-09-12 15:38:18
480	RZZSLV81R01E150H	SCRLDI19H55A839F	38555	2019-06-16 08:34:44	\N
490	RZZSLV81R01E150H	SCRLDI19H55A839F	35859	2019-06-26 23:33:30	\N
500	RZZSLV81R01E150H	SCRLDI19H55A839F	42892	2019-06-15 21:34:50	\N
510	RZZSLV81R01E150H	RSSLCU31P45I173U	24994	2018-10-17 20:06:02	2018-10-19 18:50:32
520	RZZSLV81R01E150H	RSSLCU31P45I173U	37369	2018-07-13 13:54:44	2018-07-22 22:07:49
530	RZZSLV81R01E150H	RSSLCU31P45I173U	23673	2018-08-18 00:34:09	2018-08-23 02:13:57
540	RZZSLV81R01E150H	RSSLCU31P45I173U	24994	2018-11-25 17:39:04	2018-11-25 22:53:07
550	RZZSLV81R01E150H	RSSLCU31P45I173U	22572	2018-10-03 08:15:12	2018-10-04 13:43:13
560	RZZSLV81R01E150H	LSLNMO31T62G173U	45914	2018-12-12 22:16:58	2018-12-20 15:12:22
570	RZZSLV81R01E150H	LSLNMO31T62G173U	25447	2019-01-26 19:02:24	2019-02-08 18:05:19
580	RZZSLV81R01E150H	LSLNMO31T62G173U	30758	2018-08-28 15:37:04	2018-09-08 03:31:03
590	RZZSLV81R01E150H	LSLNMO31T62G173U	41788	2018-08-10 22:48:53	2018-08-24 23:05:45
600	RZZSLV81R01E150H	GRLNNA47H56M173S	42640	2018-12-12 16:23:55	2018-12-22 10:00:17
610	RZZSLV81R01E150H	GRLNNA47H56M173S	36497	2019-04-21 02:16:20	2019-05-01 07:18:48
620	RZZSLV81R01E150H	GRLNNA47H56M173S	29008	2018-06-08 12:28:39	2018-06-21 08:13:03
630	RZZSLV81R01E150H	KMEFLV86H22L111K	44559	2019-04-02 02:37:24	2019-04-10 01:51:27
640	RZZSLV81R01E150H	KMEFLV86H22L111K	26979	2016-09-13 18:23:14	2016-09-25 07:38:36
650	RZZSLV81R01E150H	SDNCMN83T26H858P	46343	2011-08-17 21:48:06	2011-09-01 07:43:51
660	RZZSLV81R01E150H	SDNCMN83T26H858P	40440	2011-02-22 06:47:49	2011-03-05 19:46:38
670	RZZSLV81R01E150H	SDNCMN83T26H858P	20558	2009-06-10 05:07:51	2009-06-12 06:40:47
680	RZZSLV81R01E150H	SDNCMN83T26H858P	39449	1988-05-23 07:39:13	1988-05-28 09:32:37
690	RZZSLV81R01E150H	SDNCMN83T26H858P	33161	2011-03-29 06:37:56	2011-04-04 10:52:53
700	RZZSLV81R01E150H	GFLDRD49M10F856Q	10089	2018-08-07 06:58:33	2018-08-20 10:14:36
710	RZZSLV81R01E150H	GFLDRD49M10F856Q	46130	2018-06-13 11:02:59	2018-06-20 07:20:31
720	RZZSLV81R01E150H	GFLDRD49M10F856Q	21894	2018-06-08 10:59:57	2018-06-19 07:27:34
730	RZZSLV81R01E150H	GFLDRD49M10F856Q	45795	2018-12-14 21:41:02	2018-12-23 10:48:26
740	RZZSLV81R01E150H	GFLDRD49M10F856Q	23749	2018-10-02 06:05:51	2018-10-05 20:32:11
750	SMMTMS72M20G443H	PRTLNE42S67D663Q	43527	2019-05-15 16:42:47	\N
760	SMMTMS72M20G443H	PRTLNE42S67D663Q	35932	2018-09-22 11:56:38	2018-09-30 13:14:06
770	SMMTMS72M20G443H	PRTLNE42S67D663Q	43571	2018-12-17 21:22:48	2018-12-21 06:29:36
780	SMMTMS72M20G443H	PRTLNE42S67D663Q	33656	2019-06-14 15:58:00	\N
790	SMMTMS72M20G443H	MTTDTL40P55D243Y	45374	2019-06-10 08:08:31	\N
800	SMMTMS72M20G443H	MTTDTL40P55D243Y	36218	2019-04-22 05:13:38	2019-04-28 02:29:51
810	SMMTMS72M20G443H	MTTDTL40P55D243Y	33315	2019-06-20 03:50:34	\N
820	SMMTMS72M20G443H	MTTDTL40P55D243Y	43503	2019-02-03 22:48:13	2019-02-10 01:17:31
830	SMMTMS72M20G443H	MRNMNL28C16L137H	42564	2019-06-12 12:18:46	\N
840	SMMTMS72M20G443H	GRNMSM47T15H004T	41993	2018-07-23 03:34:42	2018-07-27 05:32:08
850	SMMTMS72M20G443H	MSTRCC17M41A158X	43447	2019-01-04 19:09:26	2019-01-18 13:41:04
860	SMMTMS72M20G443H	MSTRCC17M41A158X	41693	2018-07-17 20:59:07	2018-07-30 10:41:09
870	SMMTMS72M20G443H	MSTRCC17M41A158X	45246	2018-08-18 17:04:31	2018-08-23 06:29:31
880	SMMTMS72M20G443H	MSTRCC17M41A158X	12475	2019-03-13 16:19:19	2019-03-28 12:58:35
890	SMMTMS72M20G443H	PRCNGL30L64A022U	35849	2019-04-20 14:53:22	2019-04-21 02:26:49
900	SMMTMS72M20G443H	MDNMRA01S48C756H	41836	2018-06-10 20:46:31	2018-06-16 06:02:57
910	SMMTMS72M20G443H	MDNMRA01S48C756H	36515	2009-12-08 17:21:44	2009-12-17 03:43:00
920	SMMTMS72M20G443H	MDNMRA01S48C756H	44500	2015-11-16 21:46:43	2015-11-18 07:29:27
930	SMMTMS72M20G443H	FNTGDU07L21D311U	43751	2016-07-11 14:25:49	2016-07-16 14:47:04
940	SMMTMS72M20G443H	FNTGDU07L21D311U	24744	2013-07-17 16:24:05	2013-07-26 20:36:57
950	SMMTMS72M20G443H	NRDLVC68A01E981U	45914	2019-06-26 00:57:48	\N
960	SMMTMS72M20G443H	NRDLVC68A01E981U	46339	2018-07-19 07:33:45	2018-07-30 15:28:45
970	SMMTMS72M20G443H	NRDLVC68A01E981U	44164	2018-06-01 18:18:35	2018-06-08 09:34:57
980	SMMTMS72M20G443H	NRDLVC68A01E981U	41271	2018-09-20 00:02:12	2018-09-24 18:01:24
990	SMMTMS72M20G443H	CHMLNE17M45L896F	44953	2018-08-28 20:55:59	2018-09-04 18:35:45
1000	SMMTMS72M20G443H	CHMLNE17M45L896F	43527	2019-01-14 06:38:46	2019-01-25 20:13:27
1010	SMMTMS72M20G443H	CHMLNE17M45L896F	44318	2019-05-01 21:22:50	\N
1020	PRTMRZ80C26A537X	TMSCRL55H56L915E	41105	2019-02-24 05:45:30	2019-03-06 00:38:26
1030	PRTMRZ80C26A537X	TMSCRL55H56L915E	37804	2018-10-22 13:16:31	2018-10-27 03:12:37
1040	PRTMRZ80C26A537X	TMSCRL55H56L915E	44322	2019-06-03 02:30:58	\N
1050	PRTMRZ80C26A537X	PRPNTN38T09D246K	45833	2019-04-25 15:14:44	2019-05-05 00:37:49
1060	PRTMRZ80C26A537X	PRPNTN38T09D246K	46709	2018-11-26 09:42:46	2018-12-02 17:17:25
1070	PRTMRZ80C26A537X	GRDLCA81C49L957D	41105	1993-07-05 18:34:51	1993-07-19 03:15:36
1080	PRTMRZ80C26A537X	SCRVNC47D56A967W	32318	2018-07-11 20:16:45	2018-07-12 09:23:48
1090	PRTMRZ80C26A537X	FRNCMN07B16A520T	37914	2014-10-22 10:09:19	2014-10-31 08:46:27
1100	PRTMRZ80C26A537X	FRNCMN07B16A520T	45055	2018-11-29 16:11:23	2018-12-03 12:18:56
1110	PRTMRZ80C26A537X	FRNCMN07B16A520T	45374	2019-04-29 17:29:02	2019-05-09 12:33:55
1120	PRTMRZ80C26A537X	TCCMRA11B58H330G	21894	2019-06-22 23:23:26	\N
1130	PRTMRZ80C26A537X	TCCMRA11B58H330G	22441	2017-08-06 09:48:22	2017-08-18 16:49:44
1140	PRTMRZ80C26A537X	TCCMRA11B58H330G	38092	2017-09-01 06:33:29	2017-09-04 05:33:28
1150	PRTMRZ80C26A537X	HLZSRA39T42L137R	36218	2019-04-13 11:01:27	2019-04-26 03:59:40
1160	PRTMRZ80C26A537X	PRCLVC38B49G644S	45914	2019-03-12 16:37:17	2019-03-24 01:19:40
1170	PRTMRZ80C26A537X	PRCLVC38B49G644S	35256	2019-04-26 01:00:04	2019-05-02 21:28:39
1180	PRTMRZ80C26A537X	BRNHLG96P42A537W	40508	2001-05-12 17:53:04	2001-05-24 16:46:16
1190	PRTMRZ80C26A537X	BRNHLG96P42A537W	43544	2016-10-18 14:14:30	2016-10-23 08:24:44
1200	PRTMRZ80C26A537X	BRNHLG96P42A537W	45386	2005-02-13 05:19:36	2005-02-23 09:17:16
1210	PRTMRZ80C26A537X	BRNHLG96P42A537W	44953	2017-09-06 05:32:03	2017-09-12 00:59:23
1220	MRIGZL85P49E481L	BSLMTT14M15F068E	36679	2018-03-06 05:35:42	2018-03-12 13:32:10
1230	MRIGZL85P49E481L	BSLMTT14M15F068E	37804	2019-04-03 07:43:53	2019-04-04 12:42:49
1240	MRIGZL85P49E481L	SMNLSS92L59F307L	37684	2006-11-30 22:41:06	2006-12-14 03:02:51
1250	MRIGZL85P49E481L	SMNLSS92L59F307L	36218	2012-11-09 07:50:34	2012-11-15 03:25:45
1260	MRIGZL85P49E481L	BSLHLG45P45B160D	45592	2019-04-25 06:39:44	2019-05-05 20:00:43
1270	MRIGZL85P49E481L	BSLHLG45P45B160D	42930	2018-12-24 12:24:41	2018-12-27 00:38:06
1280	MRIGZL85P49E481L	BNZLCU57D66I354T	37621	2018-06-14 00:48:10	2018-06-16 10:30:59
1290	MRIGZL85P49E481L	BNZLCU57D66I354T	45936	2019-03-07 06:47:50	2019-03-11 11:43:17
1300	MRIGZL85P49E481L	FRTLSN14H19L490X	42647	2016-11-02 23:55:19	2016-11-14 05:53:05
1310	MRIGZL85P49E481L	ZCCLNZ98E09C700J	40952	2003-01-23 06:52:49	2003-02-01 19:05:25
1320	MRIGZL85P49E481L	ZCCLNZ98E09C700J	35410	2005-06-06 10:08:35	2005-06-17 22:41:57
1330	MRIGZL85P49E481L	RCKFRZ46T60E065G	45246	2019-06-18 21:08:44	\N
1340	MRIGZL85P49E481L	RCKFRZ46T60E065G	39729	2018-06-02 01:39:06	2018-06-03 13:32:48
1350	MRIGZL85P49E481L	RCKFRZ46T60E065G	42647	2019-03-19 14:23:39	2019-04-03 07:59:58
1360	MRIGZL85P49E481L	RCKFRZ46T60E065G	45912	2018-11-02 22:02:03	2018-11-16 05:38:49
1370	MRIGZL85P49E481L	BRNGRL52H17D457Q	43463	2018-12-03 03:01:35	2018-12-17 03:35:45
1380	MRIGZL85P49E481L	BRNGRL52H17D457Q	45386	2018-11-08 03:08:50	2018-11-08 13:08:04
1390	MRIGZL85P49E481L	BRNGRL52H17D457Q	28850	2018-07-16 14:00:27	2018-07-16 16:16:08
1400	MRIGZL85P49E481L	BRNGRL52H17D457Q	45969	2018-09-18 01:02:23	2018-09-24 20:33:44
1410	MRIGZL85P49E481L	MNTNLS46S52I839M	43379	2018-12-24 01:37:08	2018-12-24 10:33:48
1420	MRIGZL85P49E481L	MNTNLS46S52I839M	43503	2018-08-26 11:33:00	2018-09-05 18:17:34
1430	MRIGZL85P49E481L	MNTNLS46S52I839M	45246	2019-03-10 03:38:29	2019-03-10 21:25:45
1440	MRIGZL85P49E481L	MNPMRA62E19A916J	41993	2018-07-12 06:36:29	2018-07-22 18:19:27
1450	MRIGZL85P49E481L	MNPMRA62E19A916J	38474	2018-06-11 13:34:24	2018-06-12 03:38:25
1460	BRSNCL79A61D484Q	GVNMTN72E61E981G	41788	2005-03-25 00:51:40	2005-03-26 20:49:59
1470	BRSNCL79A61D484Q	MSTCST13E58L174Q	40880	2013-08-19 01:22:17	2013-08-29 10:37:52
1480	BRSNCL79A61D484Q	MSTCST13E58L174Q	44269	2018-04-26 09:38:01	2018-05-04 19:20:17
1490	BRSNCL79A61D484Q	PLTGRZ72P68B158J	44269	1997-12-29 22:01:04	1998-01-06 01:49:20
1500	BRSNCL79A61D484Q	PLTGRZ72P68B158J	33232	2012-08-13 15:22:18	2012-08-23 04:27:37
1510	BRSNCL79A61D484Q	GRNPTR17H14L033Y	41599	2019-04-24 01:08:54	2019-05-03 21:37:24
1520	BRSNCL79A61D484Q	GRNPTR17H14L033Y	45427	2018-09-17 18:04:16	2018-09-22 09:52:27
1530	BRSNCL79A61D484Q	GRNPTR17H14L033Y	41271	2019-04-16 01:19:19	2019-04-16 14:06:57
1540	BRSNCL79A61D484Q	GRNPTR17H14L033Y	36478	2019-06-22 15:10:53	\N
1550	BRSNCL79A61D484Q	GRNPTR17H14L033Y	35219	2018-09-13 16:27:00	2018-09-21 15:47:53
1560	BRSNCL79A61D484Q	MRIGPL08H02D336D	35932	2013-03-17 01:15:40	2013-03-25 02:40:06
1570	BRSNCL79A61D484Q	MRIGPL08H02D336D	36222	2012-09-05 19:28:30	2012-09-08 01:15:55
1580	BRSNCL79A61D484Q	MRIGPL08H02D336D	42640	2008-11-12 02:15:34	2008-11-13 23:24:47
1590	BRSNCL79A61D484Q	MRIGPL08H02D336D	37720	2010-01-27 19:53:40	2010-01-30 16:20:28
1600	BRSNCL79A61D484Q	BNZMRT74P01L106V	44500	2000-07-22 22:42:15	2000-07-28 14:23:10
1610	BRSNCL79A61D484Q	BNZMRT74P01L106V	38470	2019-06-27 17:12:18	\N
1620	BRSNCL79A61D484Q	BNZMRT74P01L106V	44495	1997-05-26 16:21:33	1997-05-27 10:15:57
1630	BRSNCL79A61D484Q	BNZMRT74P01L106V	44953	1987-06-05 02:56:35	1987-06-08 10:46:59
1640	BRSNCL79A61D484Q	MSTNMO60E66A635P	44559	2018-09-28 04:59:51	2018-10-02 17:41:20
1650	BRSNCL79A61D484Q	MSTNMO60E66A635P	45936	2018-09-05 19:33:44	2018-09-09 14:48:10
1660	BRSNCL79A61D484Q	MSTNMO60E66A635P	10089	2019-05-21 04:00:50	\N
1670	BRSNCL79A61D484Q	MSTNMO60E66A635P	44291	2018-10-29 05:36:26	2018-11-10 06:17:10
1680	BRSNCL79A61D484Q	MSTNMO60E66A635P	43503	2018-06-14 22:30:09	2018-06-25 12:25:45
1690	BRSNCL79A61D484Q	ZCCLNE08T47D631A	41836	2011-08-09 23:50:57	2011-08-22 18:30:35
1700	BRSNCL79A61D484Q	ZZLDRN97B55C994H	42825	2002-08-21 08:53:47	2002-08-24 00:34:36
1710	BRSNCL79A61D484Q	GGRPTR15D05L097I	43533	2015-10-01 01:01:36	2015-10-11 02:20:22
1720	BRSNCL79A61D484Q	GGRPTR15D05L097I	43903	2017-12-08 05:01:01	2017-12-12 16:02:32
1730	BRSNCL79A61D484Q	GMPPTR66C24D821H	44636	2019-02-23 20:18:27	2019-02-24 21:01:10
1740	BRSNCL79A61D484Q	GMPPTR66C24D821H	41230	2018-11-25 01:55:17	2018-12-03 10:43:43
1750	BRSNCL79A61D484Q	GMPPTR66C24D821H	35932	2018-09-25 03:54:00	2018-09-28 17:23:31
1760	BRSNCL79A61D484Q	GMPPTR66C24D821H	45673	2019-03-24 13:18:31	2019-03-25 11:03:07
1770	BRSNCL79A61D484Q	DVRNLS18R50H152G	41271	2018-11-22 23:17:26	2018-12-05 04:03:35
\.


--
-- TOC entry 2906 (class 0 OID 19235)
-- Dependencies: 206
-- Data for Name: prescrizione_visitespec; Type: TABLE DATA; Schema: progettoipw; Owner: postgres
--

COPY progettoipw.prescrizione_visitespec (id, cf_med, id_ssp, cf_paz, "timestamp", tipovisita, erog_time, cf_medspec, risultati) FROM stdin;
2	DVRDRN73P08D075W	\N	PCHLSN60T58A286J	2019-04-15 15:39:45	Visita senologica	2019-04-22 05:51:22	KMERCR69R01B165K	Tutto a posto
12	DVRDRN73P08D075W	\N	PCHLSN60T58A286J	2018-10-20 14:57:39	Visita pneumologica	2018-10-22 15:42:49	GMPBBR68M63L137D	Tutto a posto
22	DVRDRN73P08D075W	\N	PCHLSN60T58A286J	2019-01-12 06:40:42	Visita chirurgia maxillo facciale	2019-01-13 07:04:47	GMPBBR68M63L137D	Si consiglia un ulteriore esame: chlamydie ricerca diretta (eia).
32	DVRDRN73P08D075W	\N	BNNCRN43T59L108I	2018-09-16 16:31:01	Visita oncologica	2018-09-23 05:31:20	BRSCRL82B24L201U	Tutto a posto
52	DVRDRN73P08D075W	\N	BNNCRN43T59L108I	2019-03-12 02:41:42	Visita di radiologia interventistica	2019-03-13 15:44:29	GMPBBR68M63L137D	Si consiglia un ulteriore esame: tomografia computerizzata dell arto superiore.
62	DVRDRN73P08D075W	\N	MTTMLD52P64F176A	2018-10-13 19:22:43	Visita anestesiologica/algologica	2018-10-15 07:43:00	ZCCLNA85M05H004D	Si consiglia un ulteriore visita oncoematologica pediatrica.
72	DVRDRN73P08D075W	\N	MTTMLD52P64F176A	2018-08-01 04:34:33	Visita chirurgia plastica	2018-08-07 04:44:36	KMERCR69R01B165K	Si consiglia di assumere farmaco STEGLUJAN.
82	DVRDRN73P08D075W	\N	MTTMLD52P64F176A	2018-10-04 04:15:30	Visita pediatrica	2018-10-13 01:55:37	BNZGPP74L13H475Z	Si consiglia un ulteriore esame: tempo di protrombina.
92	DVRDRN73P08D075W	\N	MTTMLD52P64F176A	2018-10-31 01:53:49	Visita neuropsichiatrica infantile	2018-11-10 19:25:26	ZCCLNA85M05H004D	Si consiglia di assumere farmaco SOLFLU.
102	DVRDRN73P08D075W	\N	FSAMTT79T03B579K	2010-04-08 08:17:17	Visita chirurgia di controllo	2010-04-19 12:53:17	BRSSDR86D62D731H	Si consiglia un ulteriore relazione per organi giudiziari.
112	DVRDRN73P08D075W	\N	FSAMTT79T03B579K	2017-05-18 19:04:33	Visita oculistica pediatrica	2017-05-25 13:08:04	ZCCLNA85M05H004D	Tutto a posto
122	DVRDRN73P08D075W	\N	FSAMTT79T03B579K	2013-05-01 08:24:57	Visita urologica/andrologica	2013-05-10 19:40:01	RZILSS83S27A507U	Si consiglia un ulteriore esame: ricerca mutazione (dgge).
132	DVRDRN73P08D075W	\N	FSAMTT79T03B579K	1999-04-19 07:30:54	Visita chirurgia generale	1999-04-21 04:02:56	BTTLDN71C45B203N	Si consiglia un ulteriore visita ortopedica.
142	DVRDRN73P08D075W	\N	FSAMTT79T03B579K	1983-08-07 06:29:27	Visita dermatologica	1983-08-16 14:49:13	BTTLDN71C45B203N	Tutto a posto
152	DVRDRN73P08D075W	\N	FSAMTT79T03B579K	1987-08-24 20:29:55	Visita cardiochirurgica	1987-08-26 01:54:18	GMPBBR68M63L137D	Si consiglia un ulteriore visita dietologica.
162	DVRDRN73P08D075W	\N	FSAMTT79T03B579K	2013-11-03 09:20:42	Controllo/programmazione di neurostimolatore encefalico	2013-11-04 04:46:12	BNZGPP74L13H475Z	Si consiglia un ulteriore visita di riabilitazione neurologica.
172	DVRDRN73P08D075W	\N	GVNTMS77R07I760K	1989-10-27 07:51:42	Relazione per organi giudiziari	1989-11-09 04:00:38	BRNCHR83E66H299L	Si consiglia un ulteriore esame: immunizzazione malattia autoimmune.
182	DVRDRN73P08D075W	\N	GVNTMS77R07I760K	2005-06-10 13:02:03	Visita chirurgia di controllo	2005-06-18 23:46:16	ZCCLNA85M05H004D	Si consiglia di assumere farmaco PARACETAMOLO ZENTIVA ITALIA.
192	DVRDRN73P08D075W	\N	TZZSRG70R27E481D	1982-11-26 15:32:39	Visita dietologica	1982-12-02 14:45:40	BRNCHR83E66H299L	Tutto a posto
202	DVRDRN73P08D075W	\N	TZZSRG70R27E481D	2003-12-27 23:34:02	Visita ortopedica	2004-01-09 14:45:07	BTTLDN71C45B203N	Si consiglia un ulteriore visita ostetrica.
212	DVRDRN73P08D075W	\N	TZZSRG70R27E481D	1972-12-10 05:40:50	Visita cardiochirurgica pediatrica	1972-12-12 12:45:21	ZCCLNA85M05H004D	Si consiglia un ulteriore visita chirurgia generale.
222	DVRDRN73P08D075W	\N	TZZSRG70R27E481D	2009-10-22 06:22:39	Visita di medicina estetica	2009-10-24 15:34:41	BTTLDN71C45B203N	Tutto a posto
232	DVRDRN73P08D075W	\N	PDRNMO72L44H639L	1985-10-26 00:27:57	Visita di radioterapia	1985-11-01 21:49:17	BRNCHR83E66H299L	Si consiglia di assumere farmaco RITMODAN.
242	DVRDRN73P08D075W	\N	PDRNMO72L44H639L	2004-01-03 06:03:43	Relazione per organi giudiziari	2004-01-13 09:41:52	ZCCLNA85M05H004D	Si consiglia un ulteriore esame: gruppo sanguigno abo e rh (d).
252	DVRDRN73P08D075W	\N	PDRNMO72L44H639L	1973-05-12 21:33:24	Visita di medicina estetica	1973-05-25 07:04:22	BRSCRL82B24L201U	Tutto a posto
262	DVRDRN73P08D075W	\N	PDRNMO72L44H639L	1997-06-20 09:22:19	Visita ostetrica	1997-06-28 10:05:49	BRSCRL82B24L201U	Tutto a posto
272	DVRDRN73P08D075W	\N	PDRNMO72L44H639L	1981-07-23 04:34:53	Visita reumatologica	1981-08-05 23:11:32	BRSSDR86D62D731H	Tutto a posto
282	DVRDRN73P08D075W	\N	PDRNMO72L44H639L	2010-05-30 02:58:24	Visita reumatologica	2010-06-04 10:29:57	KMERCR69R01B165K	Si consiglia un ulteriore visita fisiatrica .
292	DVRDRN73P08D075W	\N	PDRNMO72L44H639L	2018-07-08 09:56:25	Visita epatologica	2018-07-16 11:49:25	RZILSS83S27A507U	Si consiglia un ulteriore visita cardiochirurgica.
302	DVRDRN73P08D075W	\N	PDRNMO72L44H639L	2009-01-04 16:42:37	Visita geriatrica	2009-01-11 09:09:00	ZCCLNA85M05H004D	Tutto a posto
322	DVRDRN73P08D075W	\N	GVNLRA19S63I173B	2018-12-30 10:11:59	Visita di medicina nucleare	2019-01-05 01:18:10	ZCCLNA85M05H004D	Si consiglia di assumere farmaco ESOPRAL.
332	DVRDRN73P08D075W	\N	GVNLRA19S63I173B	2019-01-14 04:49:16	Visita oncologica	2019-01-21 11:06:45	RSMMRT81S58G443J	Tutto a posto
342	DVRDRN73P08D075W	\N	GVNLRA19S63I173B	2018-12-10 20:44:25	Visita fisiatrica 	2018-12-12 05:17:16	BRNCHR83E66H299L	Si consiglia un ulteriore esame: splintaggio per gruppi di 4 denti.
352	DVRDRN73P08D075W	\N	GVNLRA19S63I173B	2018-07-09 05:53:01	Visita epatologica	2018-07-14 14:14:32	RZILSS83S27A507U	Tutto a posto
362	DVRDRN73P08D075W	\N	GVNLRA19S63I173B	2018-11-05 12:13:00	Visita ematologica	2018-11-10 16:04:43	KMERCR69R01B165K	Tutto a posto
372	DVRDRN73P08D075W	\N	GVNLRA19S63I173B	2018-08-03 14:17:21	Visita di medicina nucleare	2018-08-05 10:09:05	BRNCHR83E66H299L	Si consiglia un ulteriore esame: tempo di tromboplastina.
382	DVRDRN73P08D075W	\N	GVNLRA19S63I173B	2018-09-27 03:23:28	Visita otorinolaringoiatrica	2018-09-29 18:53:01	BRSSDR86D62D731H	Tutto a posto
392	DVRDRN73P08D075W	\N	KFLRRT29A04I949D	2018-09-11 14:15:03	Visita di medicina nucleare	2018-09-24 17:59:04	RSMMRT81S58G443J	Si consiglia un ulteriore esame: risonanza magnetica nucleare (rm) muscoloscheletrica.
402	DVRDRN73P08D075W	\N	KFLRRT29A04I949D	2018-12-03 09:23:08	Controllo periodico per terapia anticoagulante orale (TAO)	2018-12-11 08:24:19	KMERCR69R01B165K	Si consiglia un ulteriore esame: tomografia computerizzata dell arto superiore.
412	DVRDRN73P08D075W	\N	KFLRRT29A04I949D	2018-12-31 11:02:52	Visita ginecologica	2019-01-06 14:39:26	RZILSS83S27A507U	Tutto a posto
432	DVRDRN73P08D075W	\N	MRLMRA02M56D663B	2006-05-09 18:32:53	Visita epatologica	2006-05-24 05:02:42	GMPBBR68M63L137D	Tutto a posto
442	DVRDRN73P08D075W	\N	MRLMRA02M56D663B	2017-07-13 13:38:59	Visita multidisciplinare per cure palliative	2017-07-25 13:37:56	KMERCR69R01B165K	Si consiglia un ulteriore visita audiologica: visita foniatrica.
452	NRDLDI83E50E614G	\N	PLLPRI69A28D371I	2019-04-15 23:13:33	Visita reumatologica	2019-04-20 20:57:39	BRNCHR83E66H299L	Tutto a posto
462	NRDLDI83E50E614G	\N	PLLPRI69A28D371I	2018-12-17 14:07:34	Visita neurologica	2018-12-20 18:23:02	RSMMRT81S58G443J	Si consiglia un ulteriore visita chirurgia maxillo facciale.
472	NRDLDI83E50E614G	\N	TZZGZL80A61F307B	2003-04-06 18:44:26	Visita genetica medica	2003-04-20 21:31:24	RSMMRT81S58G443J	Si consiglia di assumere farmaco VALGANCICLOVIR TEVA.
482	NRDLDI83E50E614G	\N	TZZGZL80A61F307B	2014-01-17 06:07:07	Relazione per organi giudiziari	2014-01-25 01:55:05	BRSCRL82B24L201U	Si consiglia un ulteriore visita fisiatrica .
492	NRDLDI83E50E614G	\N	TZZGZL80A61F307B	2001-10-09 11:32:07	Visita pneumologica	2001-10-15 23:17:08	BNZGPP74L13H475Z	Si consiglia di assumere farmaco IMODIUM.
502	NRDLDI83E50E614G	\N	TZZGZL80A61F307B	1984-09-18 12:35:01	Visita audiologica: visita foniatrica	1984-10-03 08:58:32	BRSSDR86D62D731H	Tutto a posto
512	NRDLDI83E50E614G	\N	TZZGZL80A61F307B	1999-03-05 06:37:54	Visita medico-sportiva	1999-03-07 07:33:59	ZCCLNA85M05H004D	Tutto a posto
522	NRDLDI83E50E614G	\N	TZZGZL80A61F307B	1981-05-28 04:21:09	Visita reumatologica	1981-06-02 18:22:53	BRSSDR86D62D731H	Si consiglia di assumere farmaco FEIBA.
532	NRDLDI83E50E614G	\N	TZZGZL80A61F307B	2001-05-31 18:18:01	Visita oculistica di controllo	2001-06-13 02:54:23	BRSSDR86D62D731H	Si consiglia di assumere farmaco DELTACORTENESOL.
542	NRDLDI83E50E614G	\N	TZZGZL80A61F307B	2010-08-19 15:52:44	Visita chirurgia generale	2010-08-31 13:12:45	RZILSS83S27A507U	Si consiglia un ulteriore visita radioterapica.
552	NRDLDI83E50E614G	\N	ZZLSLV15R62L162L	2018-08-28 05:45:41	Visita neurologica pediatrica	2018-08-31 07:57:52	RSMMRT81S58G443J	Si consiglia un ulteriore visita urologica pediatrica.
572	NRDLDI83E50E614G	\N	ZZLSLV15R62L162L	2018-12-07 23:15:13	Controllo/programmazione di neurostimolatore encefalico	2018-12-14 01:41:52	BRSSDR86D62D731H	Si consiglia un ulteriore visita reumatologica.
582	NRDLDI83E50E614G	\N	ZZLSLV15R62L162L	2019-04-27 01:31:32	Visita neurologica pediatrica	2019-04-28 00:12:02	BRSSDR86D62D731H	Si consiglia di assumere farmaco SEGLUROMET.
592	NRDLDI83E50E614G	\N	ZZLSLV15R62L162L	2018-11-07 23:31:53	Visita chirurgia maxillo facciale	2018-11-11 00:14:10	BNZGPP74L13H475Z	Si consiglia un ulteriore esame: impianto di dente.
602	NRDLDI83E50E614G	\N	ZZLSLV15R62L162L	2018-07-30 04:07:08	Visita medico-sportiva	2018-08-02 07:40:37	RZILSS83S27A507U	Tutto a posto
612	NRDLDI83E50E614G	\N	MNFSMN55B08B203N	2018-06-04 00:12:44	Visita chirurgia generale	2018-06-11 14:00:30	BRSSDR86D62D731H	Si consiglia di assumere farmaco SUNITINIB TEVA.
622	NRDLDI83E50E614G	\N	MNFSMN55B08B203N	2018-11-16 22:38:44	Visita oncoematologica pediatrica	2018-12-01 17:23:08	KMERCR69R01B165K	Si consiglia un ulteriore visita di medicina nucleare.
632	NRDLDI83E50E614G	\N	FRNMRT70E16L490F	2004-07-28 22:38:57	Visita reumatologica	2004-07-30 09:57:36	BRSCRL82B24L201U	Si consiglia un ulteriore visita genetica medica.
642	NRDLDI83E50E614G	\N	ZCCDVD99S09E334C	2015-06-10 15:26:35	Visita pneumologica	2015-06-20 19:54:43	GMPBBR68M63L137D	Si consiglia un ulteriore esame: tipizzazione genomica hla - dp alta risoluzione.
652	NRDLDI83E50E614G	\N	ZCCDVD99S09E334C	2007-12-02 09:11:28	Visita procreazione (fecondazione) assistita	2007-12-15 22:16:30	RSMMRT81S58G443J	Si consiglia un ulteriore visita genetica medica.
662	NRDLDI83E50E614G	\N	ZCCDVD99S09E334C	2009-06-26 01:37:44	Visita pneumologica	2009-07-01 00:07:13	KMERCR69R01B165K	Si consiglia di assumere farmaco TADALAFIL CIPLA.
672	NRDLDI83E50E614G	\N	BRSLGE75C46E398O	2018-09-21 04:26:08	Visita di radiologia interventistica	2018-09-25 20:37:29	ZCCLNA85M05H004D	Si consiglia un ulteriore visita dentistica.
682	NRDLDI83E50E614G	\N	BRSLGE75C46E398O	2005-08-16 05:00:59	Visita neurochirurgica	2005-08-26 10:51:06	ZCCLNA85M05H004D	Si consiglia un ulteriore visita neuropsichiatrica infantile.
692	NRDLDI83E50E614G	\N	BRSLGE75C46E398O	1997-08-21 02:45:32	Visita angiologica	1997-08-25 07:00:08	GMPBBR68M63L137D	Si consiglia un ulteriore esame: analisi citogenetica per fragilit� cromosomica.
702	NRDLDI83E50E614G	\N	DPLLRA05L44B404J	2010-09-07 07:18:14	Visita per cure palliative	2010-09-19 00:20:55	BTTLDN71C45B203N	Tutto a posto
712	NRDLDI83E50E614G	\N	DPLLRA05L44B404J	2014-05-12 05:29:26	Visita oculistica di controllo	2014-05-24 04:15:13	BNZGPP74L13H475Z	Tutto a posto
722	NRDLDI83E50E614G	\N	DPLLRA05L44B404J	2018-01-16 05:56:04	Visita cardiologica. Incluso: ECG	2018-01-28 12:15:17	RSMMRT81S58G443J	Tutto a posto
732	NRDLDI83E50E614G	\N	DPLLRA05L44B404J	2013-03-05 03:27:18	Visita cardiochirurgica pediatrica	2013-03-19 09:11:53	BTTLDN71C45B203N	Si consiglia un ulteriore esame: intervento chirurgico preprotesico.
742	NRDLDI83E50E614G	\N	DPLLRA05L44B404J	2015-02-25 18:28:27	Visita oculistica di controllo	2015-02-28 08:49:24	BRNCHR83E66H299L	Tutto a posto
752	NRDLDI83E50E614G	\N	DPLLRA05L44B404J	2009-02-11 06:22:12	Visita oculistica	2009-02-16 05:21:32	GMPBBR68M63L137D	Tutto a posto
762	NRDLDI83E50E614G	\N	BLDDMN74T11C756X	1985-04-02 21:28:22	Visita cardiologica. Incluso: ECG	1985-04-03 21:18:46	BNZGPP74L13H475Z	Si consiglia di assumere farmaco MICOFENOLATO MOFETILE TECNIGEN.
772	NRDLDI83E50E614G	\N	BLDDMN74T11C756X	2007-06-02 14:50:27	Controllo/programmazione di neurostimolatore spinale	2007-06-13 22:43:10	RSMMRT81S58G443J	Si consiglia un ulteriore visita di medicina fisica e riabilitazione.
782	NRDLDI83E50E614G	\N	BLDDMN74T11C756X	2018-10-07 13:00:27	Visita vulnologica	2018-10-19 22:42:36	RZILSS83S27A507U	Tutto a posto
792	NRDLDI83E50E614G	\N	BLDDMN74T11C756X	1976-02-12 04:00:30	Visita pneumologica	1976-02-22 03:34:31	BRSCRL82B24L201U	Si consiglia un ulteriore esame: campylobacter esame colturale.
802	NRDLDI83E50E614G	\N	BLDDMN74T11C756X	1997-08-05 16:42:46	Visita chirurgia vascolare	1997-08-09 18:37:43	BRSSDR86D62D731H	Si consiglia un ulteriore esame: coltura di linee linfocitarie stabilizzate con virus o interleuchina.
812	NRDLDI83E50E614G	\N	BLDDMN74T11C756X	1987-08-04 05:20:49	Visita ginecologica	1987-08-04 16:35:23	KMERCR69R01B165K	Si consiglia un ulteriore esame: ibridazione in situ (fish).
822	NRDLDI83E50E614G	\N	BLDDMN74T11C756X	2012-03-26 08:30:56	Visita dietologica	2012-03-29 19:55:09	BRSSDR86D62D731H	Si consiglia un ulteriore esame: densitometria ossera.
832	NRDLDI83E50E614G	\N	BRNNDA05R53E981M	2010-01-24 07:55:15	Visita chirurgia generale	2010-02-06 12:42:43	RSMMRT81S58G443J	Si consiglia di assumere farmaco ATORVASTATINA AUROBINDO.
842	NRDLDI83E50E614G	\N	BRNNDA05R53E981M	2006-10-09 10:39:49	Visita di radiologia interventistica	2006-10-10 20:54:26	BRNCHR83E66H299L	Si consiglia un ulteriore esame: colesterolo hdl.
852	NRDLDI83E50E614G	\N	BRNNDA05R53E981M	2007-10-06 23:48:42	Controllo e programmazione pace-maker	2007-10-21 14:44:53	GMPBBR68M63L137D	Tutto a posto
862	NRDLDI83E50E614G	\N	BRNNDA05R53E981M	2012-11-10 02:24:07	Visita audiologica: visita foniatrica	2012-11-18 07:54:33	GMPBBR68M63L137D	Tutto a posto
872	NRDLDI83E50E614G	\N	BRSFNC35L17D336O	2019-04-10 15:56:08	Controllo protesico elettroacustico	2019-04-12 04:34:58	RZILSS83S27A507U	Si consiglia di assumere farmaco RAMIPRIL E IDROCLOROTIAZIDE PENSA.
1352	RZZSLV81R01E150H	\N	SDNCMN83T26H858P	2003-09-21 23:14:00	Visita medico-sportiva	2003-10-04 21:50:59	BRSSDR86D62D731H	Si consiglia un ulteriore visita nefrologica.
892	NRDLDI83E50E614G	\N	BRSFNC35L17D336O	2019-03-03 05:53:11	Visita psicodiagnostica	2019-03-07 07:30:51	RSMMRT81S58G443J	Si consiglia un ulteriore visita di riabilitazione neurologica.
902	NRDLDI83E50E614G	\N	MYRFNC32D59L527G	2019-04-18 08:57:17	Visita infettivologica o delle malattie tropicali	2019-04-22 21:36:01	BRSSDR86D62D731H	Si consiglia un ulteriore esame: urato urea.
912	NRDLDI83E50E614G	\N	MYRFNC32D59L527G	2019-04-26 15:12:12	Visita proctologica	2019-05-03 13:04:15	RSMMRT81S58G443J	Tutto a posto
922	NRDLDI83E50E614G	\N	MYRFNC32D59L527G	2018-09-09 19:02:07	Visita chirurgia plastica	2018-09-11 16:00:49	BRNCHR83E66H299L	Tutto a posto
932	NRDLDI83E50E614G	\N	MYRFNC32D59L527G	2018-11-24 19:27:59	Visita oncoematologica	2018-12-07 06:40:29	BRNCHR83E66H299L	Si consiglia un ulteriore esame: anticorpi anti microsomi.
942	RZZSLV81R01E150H	\N	FRNDRA76E06I687A	1996-09-05 10:26:50	Visita infettivologica o delle malattie tropicali	1996-09-16 07:53:51	BNZGPP74L13H475Z	Si consiglia di assumere farmaco PALIPERIDONE SANDOZ.
952	RZZSLV81R01E150H	\N	HLZNLS17L59F949S	2018-10-19 03:02:00	Visita oncoematologica pediatrica	2018-11-02 16:35:04	RSMMRT81S58G443J	Si consiglia di assumere farmaco OMEPRAZOLO RANBAXY ITALIA.
962	RZZSLV81R01E150H	\N	HLZNLS17L59F949S	2018-08-18 00:30:13	Visita psichiatrica	2018-08-19 04:54:23	BTTLDN71C45B203N	Si consiglia di assumere farmaco INOMAX.
972	RZZSLV81R01E150H	\N	HLZNLS17L59F949S	2018-08-02 15:59:24	Visita infettivologica o delle malattie tropicali	2018-08-16 15:03:21	BRNCHR83E66H299L	Si consiglia un ulteriore visita pediatrica.
982	RZZSLV81R01E150H	\N	HLZNLS17L59F949S	2018-11-03 10:23:44	Visita dentistica	2018-11-10 16:20:59	RZILSS83S27A507U	Si consiglia un ulteriore esame: colorazione aggiuntiva in bande: bandeggio t.
992	RZZSLV81R01E150H	\N	HLZNLS17L59F949S	2018-12-15 05:06:43	Visita chirurgia di controllo	2018-12-26 03:08:28	ZCCLNA85M05H004D	Si consiglia un ulteriore esame: trattamento applicazioni protesi semovibili.
1012	RZZSLV81R01E150H	\N	HLZNLS17L59F949S	2018-11-22 14:32:39	Visita pediatrica	2018-12-07 07:44:17	RSMMRT81S58G443J	Si consiglia un ulteriore visita di medicina estetica.
1022	RZZSLV81R01E150H	\N	GRDFLV28R05A178Z	2018-10-15 21:43:23	Visita endocrinologica e diabetologica	2018-10-23 17:07:29	BRNCHR83E66H299L	Si consiglia di assumere farmaco CANDESARTAN E IDROCLOROTIAZIDE MYLAN.
1052	RZZSLV81R01E150H	\N	RSSLCU31P45I173U	2018-12-06 21:41:52	Visita di medicina nucleare	2018-12-21 00:31:29	BRNCHR83E66H299L	Tutto a posto
1062	RZZSLV81R01E150H	\N	RSSLCU31P45I173U	2019-04-25 10:05:10	Controllo protesico elettroacustico	2019-04-28 13:49:56	ZCCLNA85M05H004D	Tutto a posto
1072	RZZSLV81R01E150H	\N	RSSLCU31P45I173U	2018-09-07 11:51:34	Visita dermatologica	2018-09-16 18:34:39	BTTLDN71C45B203N	Tutto a posto
1082	RZZSLV81R01E150H	\N	RSSLCU31P45I173U	2019-03-12 19:11:19	Visita fisiatrica 	2019-03-19 07:53:16	BRNCHR83E66H299L	Si consiglia un ulteriore esame: tipizzazione genomica hla - c seq. diretto.
1092	RZZSLV81R01E150H	\N	RSSLCU31P45I173U	2018-10-24 12:24:50	Visita oculistica di controllo	2018-11-01 08:44:55	RSMMRT81S58G443J	Tutto a posto
1122	RZZSLV81R01E150H	\N	LSLNMO31T62G173U	2018-11-08 18:13:53	Visita angiologica	2018-11-20 04:19:43	GMPBBR68M63L137D	Si consiglia un ulteriore esame: chlamydie ricerca diretta (eia).
1132	RZZSLV81R01E150H	\N	LSLNMO31T62G173U	2018-09-24 12:03:41	Visita angiologica	2018-09-25 08:00:34	BTTLDN71C45B203N	Si consiglia di assumere farmaco ALVAND.
1142	RZZSLV81R01E150H	\N	LSLNMO31T62G173U	2018-10-18 12:57:52	Visita dentistica	2018-11-02 09:11:23	BRNCHR83E66H299L	Tutto a posto
1152	RZZSLV81R01E150H	\N	LSLNMO31T62G173U	2018-08-12 20:18:43	Visita dermatologica	2018-08-26 14:56:28	BNZGPP74L13H475Z	Si consiglia un ulteriore visita endocrinologica pediatrica.
1162	RZZSLV81R01E150H	\N	LSLNMO31T62G173U	2018-06-24 01:38:13	Visita allergologica	2018-06-28 03:44:50	BRNCHR83E66H299L	Si consiglia un ulteriore esame: beta tromboglobulina.
1172	RZZSLV81R01E150H	\N	LSLNMO31T62G173U	2019-04-20 23:40:59	Visita audiologica: visita foniatrica	2019-05-02 04:35:37	BRSSDR86D62D731H	Si consiglia di assumere farmaco ATORVASTATINA AUROBINDO.
1182	RZZSLV81R01E150H	\N	GRLNNA47H56M173S	2018-08-27 10:04:00	Visita endocrinologica e diabetologica	2018-08-28 22:34:21	BRNCHR83E66H299L	Si consiglia un ulteriore visita geriatrica.
1192	RZZSLV81R01E150H	\N	GRLNNA47H56M173S	2018-10-12 14:51:23	Visita oncoematologica pediatrica	2018-10-24 10:27:16	BRSSDR86D62D731H	Tutto a posto
1202	RZZSLV81R01E150H	\N	GRLNNA47H56M173S	2018-06-26 12:16:22	Visita oculistica pediatrica	2018-07-10 11:23:23	RSMMRT81S58G443J	Tutto a posto
1212	RZZSLV81R01E150H	\N	GRLNNA47H56M173S	2019-04-16 04:54:58	Visita chirurgia plastica	2019-04-23 14:05:17	GMPBBR68M63L137D	Si consiglia un ulteriore esame: ricostruzione dente mediante otturazione a tre o pi� superfici.
1222	RZZSLV81R01E150H	\N	KMEFLV86H22L111K	1993-03-31 22:55:38	Visita chirurgia toracica	1993-04-14 00:55:13	BTTLDN71C45B203N	Tutto a posto
1232	RZZSLV81R01E150H	\N	KMEFLV86H22L111K	2007-10-12 23:08:16	Visita oculistica di controllo	2007-10-20 07:12:29	RSMMRT81S58G443J	Si consiglia un ulteriore esame: coltura di villi coriali a breve termine.
1242	RZZSLV81R01E150H	\N	KMEFLV86H22L111K	2008-03-03 22:49:40	Visita angiologica	2008-03-04 20:51:07	RSMMRT81S58G443J	Si consiglia di assumere farmaco ROSUVASTATINA PENSA.
1252	RZZSLV81R01E150H	\N	KMEFLV86H22L111K	2002-03-06 10:16:12	Visita senologica	2002-03-14 13:57:37	BNZGPP74L13H475Z	Tutto a posto
1262	RZZSLV81R01E150H	\N	KMEFLV86H22L111K	2015-12-05 00:58:23	Visita per cure palliative	2015-12-18 15:24:39	ZCCLNA85M05H004D	Si consiglia un ulteriore esame: tipizzazione sierologica hla classe ii.
1272	RZZSLV81R01E150H	\N	KMEFLV86H22L111K	2014-10-30 16:10:34	Visita cardiologica. Incluso: ECG	2014-11-14 09:20:25	RZILSS83S27A507U	Si consiglia di assumere farmaco VILDAGLIPTIN E METFORMINA SANDOZ.
1282	RZZSLV81R01E150H	\N	SDNCMN83T26H858P	1996-08-08 06:46:14	Visita audiologica: visita foniatrica	1996-08-22 11:41:53	BTTLDN71C45B203N	Si consiglia un ulteriore visita di medicina nucleare.
1292	RZZSLV81R01E150H	\N	SDNCMN83T26H858P	1993-03-22 01:25:10	Visita ortopedica	1993-03-28 13:29:02	KMERCR69R01B165K	Si consiglia un ulteriore esame: impianto di protesi dentaria.
1302	RZZSLV81R01E150H	\N	SDNCMN83T26H858P	1997-12-21 12:18:24	Visita dermatologica	1997-12-29 11:10:26	BNZGPP74L13H475Z	Tutto a posto
1312	RZZSLV81R01E150H	\N	SDNCMN83T26H858P	2018-09-03 05:07:19	Visita radioterapica	2018-09-08 00:37:55	BRNCHR83E66H299L	Si consiglia di assumere farmaco NOVOMIX.
1322	RZZSLV81R01E150H	\N	SDNCMN83T26H858P	2015-06-09 01:00:03	Visita nefrologica	2015-06-10 19:12:38	RSMMRT81S58G443J	Si consiglia di assumere farmaco DIDROGYL.
1332	RZZSLV81R01E150H	\N	SDNCMN83T26H858P	1995-03-18 17:50:23	Visita oculistica	1995-03-21 22:47:03	RZILSS83S27A507U	Tutto a posto
1342	RZZSLV81R01E150H	\N	SDNCMN83T26H858P	2014-12-01 00:09:48	Controllo/programmazione di neurostimolatore encefalico	2014-12-13 00:58:16	BRSCRL82B24L201U	Si consiglia un ulteriore esame: trigliceridi.
1372	RZZSLV81R01E150H	\N	GFLDRD49M10F856Q	2018-09-17 18:27:03	Visita cardiochirurgica	2018-10-02 09:40:19	BTTLDN71C45B203N	Tutto a posto
1382	RZZSLV81R01E150H	\N	GFLDRD49M10F856Q	2018-08-24 19:53:41	Visita reumatologica	2018-09-06 18:48:42	BTTLDN71C45B203N	Si consiglia di assumere farmaco PREGABALIN TECNIGEN.
1392	RZZSLV81R01E150H	\N	GFLDRD49M10F856Q	2019-02-05 02:33:07	Visita dietologica	2019-02-10 22:10:05	BRNCHR83E66H299L	Si consiglia un ulteriore esame: tomografia computerizzata dell arto superiore senza e con contrasto.
1402	RZZSLV81R01E150H	\N	GFLDRD49M10F856Q	2019-02-28 17:23:43	Controllo/programmazione di neurostimolatore spinale	2019-03-09 01:04:55	KMERCR69R01B165K	Tutto a posto
1412	RZZSLV81R01E150H	\N	GFLDRD49M10F856Q	2018-11-15 02:30:45	Visita pediatrica	2018-11-27 21:05:30	BTTLDN71C45B203N	Si consiglia un ulteriore esame: densitometria ossera.
1422	SMMTMS72M20G443H	\N	PRTLNE42S67D663Q	2018-09-23 10:21:11	Visita geriatrica	2018-09-26 21:54:47	BRNCHR83E66H299L	Si consiglia un ulteriore esame: risonanza magnetica nucleare (rm) muscoloscheletrica senza e con contrasto.
1432	SMMTMS72M20G443H	\N	PRTLNE42S67D663Q	2018-07-31 23:56:43	Visita gastroenterologica	2018-08-07 02:25:21	RZILSS83S27A507U	Tutto a posto
1442	SMMTMS72M20G443H	\N	PRTLNE42S67D663Q	2019-03-18 22:19:41	Visita neurologica	2019-03-19 06:49:00	BTTLDN71C45B203N	Si consiglia un ulteriore visita cardiochirurgica.
1452	SMMTMS72M20G443H	\N	PRTLNE42S67D663Q	2019-01-19 16:40:09	Visita allergologica	2019-01-30 00:30:33	RZILSS83S27A507U	Tutto a posto
1462	SMMTMS72M20G443H	\N	PRTLNE42S67D663Q	2018-12-16 00:39:50	Visita radioterapica	2018-12-19 07:00:16	BRNCHR83E66H299L	Tutto a posto
1472	SMMTMS72M20G443H	\N	MTTDTL40P55D243Y	2018-07-11 15:26:08	Visita urologica pediatrica	2018-07-17 21:50:52	GMPBBR68M63L137D	Si consiglia un ulteriore visita chirurgia pediatrica.
1482	SMMTMS72M20G443H	\N	MTTDTL40P55D243Y	2019-01-02 00:23:18	Visita audiologica: visita foniatrica	2019-01-09 09:00:12	KMERCR69R01B165K	Si consiglia un ulteriore visita procreazione (fecondazione) assistita.
1492	SMMTMS72M20G443H	\N	MTTDTL40P55D243Y	2019-02-16 14:20:50	Visita chirurgia generale	2019-02-28 17:27:24	BRSSDR86D62D731H	Si consiglia di assumere farmaco CANDESARTAN E IDROCLOROTIAZIDE MYLAN.
1502	SMMTMS72M20G443H	\N	MRNMNL28C16L137H	2019-01-15 00:00:12	Visita endocrinologica e diabetologica	2019-01-18 05:18:11	GMPBBR68M63L137D	Tutto a posto
1512	SMMTMS72M20G443H	\N	MRNMNL28C16L137H	2018-08-18 22:32:52	Relazione per organi giudiziari	2018-08-26 20:02:27	RSMMRT81S58G443J	Si consiglia di assumere farmaco IBUPROFENE SANOFI.
1522	SMMTMS72M20G443H	\N	MSTRCC17M41A158X	2019-02-26 09:14:56	Visita nefrologica	2019-03-12 10:21:27	BRSCRL82B24L201U	Si consiglia di assumere farmaco TRAMADOLO S.A.L.F..
1532	SMMTMS72M20G443H	\N	MSTRCC17M41A158X	2018-08-27 07:25:20	Relazione per organi giudiziari	2018-09-06 00:25:39	RSMMRT81S58G443J	Si consiglia di assumere farmaco TADALAFIL CIPLA.
1542	SMMTMS72M20G443H	\N	MSTRCC17M41A158X	2019-03-26 12:43:25	Visita cardiologica. Incluso: ECG	2019-04-09 15:13:50	BRSCRL82B24L201U	Si consiglia un ulteriore esame: magnesio.
1552	SMMTMS72M20G443H	\N	MSTRCC17M41A158X	2018-08-05 13:09:24	Relazione per organi giudiziari	2018-08-18 18:06:53	BTTLDN71C45B203N	Si consiglia un ulteriore visita neurochirurgica pediatrica.
1572	SMMTMS72M20G443H	\N	MSTRCC17M41A158X	2018-09-23 18:43:36	Visita endocrinologica e diabetologica	2018-10-07 12:19:42	BNZGPP74L13H475Z	Si consiglia di assumere farmaco ESBRIET.
1582	SMMTMS72M20G443H	\N	MSTRCC17M41A158X	2018-10-25 00:36:46	Visita chirurgia generale	2018-10-28 23:58:56	GMPBBR68M63L137D	Si consiglia di assumere farmaco CARNITENE.
1602	SMMTMS72M20G443H	\N	PRCNGL30L64A022U	2019-03-01 02:43:09	Visita neurologica	2019-03-11 20:55:19	GMPBBR68M63L137D	Tutto a posto
1612	SMMTMS72M20G443H	\N	PRCNGL30L64A022U	2018-08-22 02:48:07	Visita allergologica	2018-08-24 16:18:53	GMPBBR68M63L137D	Si consiglia di assumere farmaco IBUFIZZ.
1622	SMMTMS72M20G443H	\N	PRCNGL30L64A022U	2018-07-28 10:05:41	Controllo periodico per terapia anticoagulante orale (TAO)	2018-08-10 11:54:42	KMERCR69R01B165K	Si consiglia di assumere farmaco PREGABALIN TECNIGEN.
1632	SMMTMS72M20G443H	\N	PRCNGL30L64A022U	2019-03-09 06:05:17	Visita reumatologica	2019-03-18 15:32:20	BRNCHR83E66H299L	Si consiglia di assumere farmaco BETMIGA.
1642	SMMTMS72M20G443H	\N	PRCNGL30L64A022U	2018-06-19 10:34:39	Visita ginecologica	2018-06-27 11:17:43	BRSSDR86D62D731H	Si consiglia di assumere farmaco SEGLUROMET.
1652	SMMTMS72M20G443H	\N	PRCNGL30L64A022U	2018-09-21 07:46:09	Controllo/programmazione di neurostimolatore encefalico	2018-10-02 16:11:09	BRSCRL82B24L201U	Tutto a posto
1662	SMMTMS72M20G443H	\N	FNTGDU07L21D311U	2011-09-28 15:01:37	Visita dermatologica	2011-10-08 22:20:48	GMPBBR68M63L137D	Si consiglia di assumere farmaco PENTAGLOBIN.
1672	SMMTMS72M20G443H	\N	FNTGDU07L21D311U	2011-12-14 05:42:29	Visita di medicina nucleare	2011-12-20 02:30:06	BNZGPP74L13H475Z	Tutto a posto
1682	SMMTMS72M20G443H	\N	FNTGDU07L21D311U	2008-02-08 07:21:33	Visita chirurgia generale	2008-02-21 17:33:50	RSMMRT81S58G443J	Si consiglia un ulteriore esame: inserzione di ponte fisso.
1692	SMMTMS72M20G443H	\N	FNTGDU07L21D311U	2011-05-28 02:56:11	Controllo e programmazione pace-maker	2011-05-30 23:30:45	BTTLDN71C45B203N	Si consiglia di assumere farmaco STEGLUJAN.
1702	SMMTMS72M20G443H	\N	FNTGDU07L21D311U	2016-10-16 23:13:36	Visita dietologica	2016-10-27 00:09:29	BRSCRL82B24L201U	Si consiglia un ulteriore esame: esercizi respiratori per seduta collettiva.
1722	SMMTMS72M20G443H	\N	PRCDNL50L03F835T	2019-03-17 04:16:20	Controllo/programmazione di neurostimolatore encefalico	2019-03-27 23:09:50	RSMMRT81S58G443J	Si consiglia un ulteriore visita endocrinologica e diabetologica.
1732	SMMTMS72M20G443H	\N	PRCDNL50L03F835T	2018-10-04 14:31:08	Visita vulnologica	2018-10-09 02:58:36	ZCCLNA85M05H004D	Si consiglia un ulteriore controllo protesico elettroacustico.
1742	SMMTMS72M20G443H	\N	PRCDNL50L03F835T	2018-09-21 08:50:11	Visita allergologica	2018-10-06 01:04:04	BRSSDR86D62D731H	Si consiglia un ulteriore visita urologica/andrologica.
1752	SMMTMS72M20G443H	\N	NRDLVC68A01E981U	2019-03-30 13:09:21	Visita pneumologica	2019-03-31 05:49:07	BNZGPP74L13H475Z	Si consiglia di assumere farmaco OMEPRAZOLO RANBAXY ITALIA.
1772	SMMTMS72M20G443H	\N	CHMLNE17M45L896F	2018-06-05 02:46:26	Controllo e programmazione pace-maker	2018-06-18 15:53:16	BRSCRL82B24L201U	Si consiglia un ulteriore esame: tipizzazione genomica hla - b.
1782	SMMTMS72M20G443H	\N	CHMLNE17M45L896F	2018-08-28 08:07:50	Visita audiologica: visita foniatrica	2018-09-01 03:30:01	ZCCLNA85M05H004D	Si consiglia di assumere farmaco VERELAIT.
1792	SMMTMS72M20G443H	\N	CHMLNE17M45L896F	2018-07-15 20:42:28	Visita di medicina estetica	2018-07-28 21:01:52	BRSCRL82B24L201U	Si consiglia di assumere farmaco MYOVIEW.
1802	SMMTMS72M20G443H	\N	CHMLNE17M45L896F	2018-11-08 01:46:48	Visita allergologica	2018-11-09 11:54:14	BRSSDR86D62D731H	Tutto a posto
1812	SMMTMS72M20G443H	\N	CHMLNE17M45L896F	2018-06-07 10:41:01	Visita oculistica pediatrica	2018-06-21 11:45:41	RZILSS83S27A507U	Tutto a posto
1822	PRTMRZ80C26A537X	\N	PRCNNT81P54L322J	1999-02-18 04:11:40	Visita ortopedica	1999-02-27 10:29:39	ZCCLNA85M05H004D	Tutto a posto
1832	PRTMRZ80C26A537X	\N	PRCNNT81P54L322J	1982-09-06 16:05:03	Visita oncoematologica pediatrica	1982-09-17 18:55:29	BTTLDN71C45B203N	Si consiglia un ulteriore controllo e programmazione pace-maker.
1842	PRTMRZ80C26A537X	\N	PRCNNT81P54L322J	1992-11-21 19:28:12	Visita ematologica	1992-12-03 21:22:47	RZILSS83S27A507U	Tutto a posto
1852	PRTMRZ80C26A537X	\N	PRCNNT81P54L322J	1988-10-21 11:36:54	Visita chirurgia plastica	1988-10-22 11:52:45	GMPBBR68M63L137D	Si consiglia un ulteriore visita multidisciplinare per cure palliative.
1862	PRTMRZ80C26A537X	\N	TMSCRL55H56L915E	2018-06-19 18:55:41	Visita di radioterapia	2018-06-21 09:20:52	BRSSDR86D62D731H	Si consiglia di assumere farmaco SUBUTEX.
1872	PRTMRZ80C26A537X	\N	TMSCRL55H56L915E	2018-06-28 02:00:47	Controllo e programmazione pace-maker	2018-06-30 18:48:08	RSMMRT81S58G443J	Tutto a posto
1882	PRTMRZ80C26A537X	\N	TMSCRL55H56L915E	2019-02-01 23:25:00	Visita multidisciplinare per cure palliative	2019-02-05 19:38:01	KMERCR69R01B165K	Tutto a posto
1902	PRTMRZ80C26A537X	\N	TMSCRL55H56L915E	2018-08-23 07:59:23	Visita cardiologica. Incluso: ECG	2018-08-31 02:46:31	BRSCRL82B24L201U	Si consiglia di assumere farmaco PREGABALIN TECNIGEN.
1912	PRTMRZ80C26A537X	\N	TMSCRL55H56L915E	2018-07-17 01:17:54	Visita di medicina estetica	2018-07-30 17:51:36	RZILSS83S27A507U	Si consiglia di assumere farmaco LUTATHERA.
1922	PRTMRZ80C26A537X	\N	TMSCRL55H56L915E	2019-01-01 00:09:01	Controllo/programmazione di neurostimolatore spinale	2019-01-07 00:52:29	ZCCLNA85M05H004D	Si consiglia un ulteriore esame: risonanza magnetica nucleare (rm) muscoloscheletrica.
1932	PRTMRZ80C26A537X	\N	TMSCRL55H56L915E	2018-08-13 08:39:21	Visita oculistica	2018-08-19 20:31:07	BNZGPP74L13H475Z	Si consiglia un ulteriore esame: fenotipo rh.
1942	PRTMRZ80C26A537X	\N	PRPNTN38T09D246K	2018-10-19 15:26:18	Visita neurologica	2018-10-30 00:17:44	RSMMRT81S58G443J	Si consiglia di assumere farmaco DISEON.
1952	PRTMRZ80C26A537X	\N	PRPNTN38T09D246K	2019-02-26 19:51:46	Visita neuropsichiatrica infantile	2019-03-02 17:34:09	BRSSDR86D62D731H	Si consiglia di assumere farmaco FROBENKIDS FEBBRE E DOLORE.
1962	PRTMRZ80C26A537X	\N	PRPNTN38T09D246K	2018-08-26 07:53:00	Visita psichiatrica	2018-09-04 13:45:59	RSMMRT81S58G443J	Tutto a posto
1972	PRTMRZ80C26A537X	\N	PRPNTN38T09D246K	2018-11-22 15:31:20	Visita angiologica	2018-11-26 18:04:00	GMPBBR68M63L137D	Si consiglia un ulteriore esame: tipizzazione genomica hla - c.
1982	PRTMRZ80C26A537X	\N	PRPNTN38T09D246K	2018-07-26 08:37:18	Visita di medicina fisica e riabilitazione	2018-07-29 07:12:24	BRSSDR86D62D731H	Si consiglia un ulteriore esame: cariotipo da metafasi di midollo osseo.
1992	PRTMRZ80C26A537X	\N	PRPNTN38T09D246K	2019-03-05 19:53:38	Controllo periodico per terapia anticoagulante orale (TAO)	2019-03-12 02:25:50	GMPBBR68M63L137D	Si consiglia un ulteriore visita oncoematologica.
2002	PRTMRZ80C26A537X	\N	PRPNTN38T09D246K	2018-10-13 11:03:43	Visita proctologica	2018-10-18 18:16:30	BRNCHR83E66H299L	Si consiglia di assumere farmaco INOMAX.
2012	PRTMRZ80C26A537X	\N	PRPNTN38T09D246K	2018-09-10 11:30:57	Visita senologica	2018-09-11 12:03:36	BNZGPP74L13H475Z	Si consiglia di assumere farmaco OMEGA 3 BOUTY.
2022	PRTMRZ80C26A537X	\N	MRIPQL73R14A952Z	1982-01-16 23:46:55	Visita oculistica di controllo	1982-01-20 19:02:53	RZILSS83S27A507U	Tutto a posto
2032	PRTMRZ80C26A537X	\N	MRIPQL73R14A952Z	1974-01-08 19:53:52	Visita di medicina estetica	1974-01-13 18:47:15	BRSCRL82B24L201U	Si consiglia di assumere farmaco EXITELEV.
2042	PRTMRZ80C26A537X	\N	MRIPQL73R14A952Z	1989-08-29 18:02:33	Plicometria: valutazione dello stato nutrizionale	1989-08-30 10:30:15	ZCCLNA85M05H004D	Si consiglia un ulteriore controllo e programmazione pace-maker.
2052	PRTMRZ80C26A537X	\N	MRIPQL73R14A952Z	1992-12-04 21:39:50	Visita endocrinologica e diabetologica	1992-12-07 06:27:26	BNZGPP74L13H475Z	Si consiglia un ulteriore visita dietologica.
2062	PRTMRZ80C26A537X	\N	MRIPQL73R14A952Z	1986-03-19 03:52:12	Visita reumatologica	1986-03-31 04:28:23	ZCCLNA85M05H004D	Tutto a posto
2072	PRTMRZ80C26A537X	\N	MRIPQL73R14A952Z	1987-01-23 08:57:50	Visita chirurgia di controllo	1987-01-26 18:47:55	RZILSS83S27A507U	Tutto a posto
2082	PRTMRZ80C26A537X	\N	MRIPQL73R14A952Z	2004-11-16 05:28:52	Visita per cure palliative	2004-11-20 00:33:42	KMERCR69R01B165K	Si consiglia di assumere farmaco DIFTETALL.
2092	PRTMRZ80C26A537X	\N	GRDLCA81C49L957D	1983-01-21 07:19:12	Visita dietologica	1983-01-22 09:10:37	RSMMRT81S58G443J	Si consiglia un ulteriore controllo e programmazione pace-maker.
2102	PRTMRZ80C26A537X	\N	GRDLCA81C49L957D	2018-02-27 17:11:14	Visita pediatrica	2018-02-28 18:50:11	KMERCR69R01B165K	Tutto a posto
2112	PRTMRZ80C26A537X	\N	GRDLCA81C49L957D	1996-04-28 17:51:43	Visita chirurgia toracica	1996-05-11 11:21:51	KMERCR69R01B165K	Si consiglia di assumere farmaco LUSINELLE.
2122	PRTMRZ80C26A537X	\N	GRDLCA81C49L957D	1981-06-18 12:58:01	Controllo protesico elettroacustico	1981-06-22 08:31:15	BTTLDN71C45B203N	Tutto a posto
2132	PRTMRZ80C26A537X	\N	GRDLCA81C49L957D	1987-05-30 01:20:10	Visita psicodiagnostica	1987-06-12 05:12:30	ZCCLNA85M05H004D	Si consiglia un ulteriore esame: cariotipo da metafasi di fibroblasti.
2142	PRTMRZ80C26A537X	\N	FRNCMN07B16A520T	2007-02-25 15:05:13	Controllo protesico elettroacustico	2007-02-27 22:10:14	BRNCHR83E66H299L	Tutto a posto
2152	PRTMRZ80C26A537X	\N	FRNCMN07B16A520T	2008-07-21 13:58:36	Controllo periodico per terapia anticoagulante orale (TAO)	2008-07-24 16:21:35	BNZGPP74L13H475Z	Si consiglia un ulteriore esame: tipizzazione genomica hla - dp seq. diretto.
2162	PRTMRZ80C26A537X	\N	FRNCMN07B16A520T	2007-09-20 02:48:00	Visita di medicina estetica	2007-10-03 16:42:21	RZILSS83S27A507U	Si consiglia un ulteriore visita dermatologica.
2172	PRTMRZ80C26A537X	\N	FRNCMN07B16A520T	2018-06-01 21:44:20	Visita psichiatrica	2018-06-10 19:08:08	BRNCHR83E66H299L	Tutto a posto
2182	PRTMRZ80C26A537X	\N	FRNCMN07B16A520T	2010-11-07 20:28:24	Visita reumatologica	2010-11-11 05:46:01	GMPBBR68M63L137D	Si consiglia di assumere farmaco CEPIMEX.
2192	PRTMRZ80C26A537X	\N	FRNCMN07B16A520T	2013-02-09 03:36:17	Visita multidisciplinare per cure palliative	2013-02-15 02:15:26	RSMMRT81S58G443J	Si consiglia di assumere farmaco LATANOPROST E TIMOLOLO ZENTIVA.
2202	PRTMRZ80C26A537X	\N	HLZSRA39T42L137R	2019-03-29 20:08:59	Visita neurochirurgica	2019-03-30 23:15:11	BNZGPP74L13H475Z	Si consiglia un ulteriore visita urologica pediatrica.
2212	PRTMRZ80C26A537X	\N	HLZSRA39T42L137R	2018-08-14 04:34:28	Controllo protesico elettroacustico	2018-08-14 13:40:22	BNZGPP74L13H475Z	Si consiglia di assumere farmaco ATORVASTATINA F.I.R.M.A..
2222	PRTMRZ80C26A537X	\N	HLZSRA39T42L137R	2018-08-07 03:49:49	Visita radioterapica	2018-08-14 14:36:33	BRSCRL82B24L201U	Si consiglia un ulteriore esame: tipizzazione genomica hla - dq seq. diretto.
2232	PRTMRZ80C26A537X	\N	HLZSRA39T42L137R	2018-07-03 11:06:16	Visita chirurgia generale	2018-07-17 20:15:38	BNZGPP74L13H475Z	Si consiglia di assumere farmaco CANDESARTAN MYLAN GENERICS.
1892	PRTMRZ80C26A537X	\N	TMSCRL55H56L915E	2019-05-25 04:39:26	Visita reumatologica	\N	\N	\N
2242	PRTMRZ80C26A537X	\N	PRCLVC38B49G644S	2019-04-19 17:18:24	Visita psicodiagnostica	2019-04-29 16:53:18	GMPBBR68M63L137D	Si consiglia un ulteriore visita neuropsichiatrica infantile.
2252	PRTMRZ80C26A537X	\N	PRCLVC38B49G644S	2018-10-07 17:01:31	Visita oculistica di controllo	2018-10-18 06:51:43	ZCCLNA85M05H004D	Si consiglia un ulteriore visita chirurgia vascolare.
2262	PRTMRZ80C26A537X	\N	PRCLVC38B49G644S	2018-07-09 04:59:44	Visita fisiatrica 	2018-07-22 22:25:29	RZILSS83S27A507U	Si consiglia un ulteriore visita geriatrica.
2272	PRTMRZ80C26A537X	\N	PRCLVC38B49G644S	2018-07-26 01:20:44	Visita chirurgia di controllo	2018-07-29 13:56:33	BRNCHR83E66H299L	Si consiglia un ulteriore esame: colorazione aggiuntiva in bande: bandeggio t.
2282	PRTMRZ80C26A537X	\N	PRCLVC38B49G644S	2018-10-19 21:22:12	Visita radioterapica	2018-10-26 23:05:59	BTTLDN71C45B203N	Si consiglia un ulteriore esame: miceti anticorpi.
2292	PRTMRZ80C26A537X	\N	PRCLVC38B49G644S	2018-12-09 16:25:20	Visita neurochirurgica	2018-12-23 13:38:16	GMPBBR68M63L137D	Tutto a posto
2322	PRTMRZ80C26A537X	\N	BRNHLG96P42A537W	2010-12-19 21:38:08	Visita cardiologica. Incluso: ECG	2010-12-26 09:33:33	BRNCHR83E66H299L	Si consiglia un ulteriore esame: inserzione di protesi provvisoria.
2332	PRTMRZ80C26A537X	\N	BRNHLG96P42A537W	2008-03-06 09:03:06	Visita cardiochirurgica pediatrica	2008-03-08 14:15:22	GMPBBR68M63L137D	Si consiglia un ulteriore esame: analisi mutazione del dna con ibridazione sonde non radiomarcate.
2342	PRTMRZ80C26A537X	\N	BRNHLG96P42A537W	2006-11-23 22:17:18	Visita genetica medica	2006-11-30 22:24:55	ZCCLNA85M05H004D	Si consiglia un ulteriore esame: anticorpi anti microsomi.
2352	PRTMRZ80C26A537X	\N	BRNHLG96P42A537W	1998-02-24 18:03:34	Visita chirurgia plastica	1998-03-06 01:22:49	BNZGPP74L13H475Z	Si consiglia un ulteriore visita audiologica: visita foniatrica.
2362	PRTMRZ80C26A537X	\N	BRNHLG96P42A537W	1999-08-28 11:44:46	Visita reumatologica	1999-09-10 10:53:58	RZILSS83S27A507U	Si consiglia un ulteriore esame: irradiazione cutanea.
2372	PRTMRZ80C26A537X	\N	TCCSLV64H06A968F	2018-12-27 05:20:08	Visita vulnologica	2018-12-30 23:28:31	BRNCHR83E66H299L	Tutto a posto
2382	PRTMRZ80C26A537X	\N	TCCSLV64H06A968F	2018-10-19 07:04:04	Visita chirurgia plastica	2018-10-25 16:41:34	BRNCHR83E66H299L	Si consiglia un ulteriore esame: coltura di linfociti fetali.
2392	PRTMRZ80C26A537X	\N	TCCSLV64H06A968F	2019-02-25 13:54:30	Plicometria: valutazione dello stato nutrizionale	2019-03-06 09:26:38	ZCCLNA85M05H004D	Si consiglia di assumere farmaco IMIPENEM E CILASTATINA RANBAXY.
2402	MRIGZL85P49E481L	\N	BSLMTT14M15F068E	2015-10-04 01:23:47	Relazione per organi giudiziari	2015-10-10 17:32:56	RSMMRT81S58G443J	Si consiglia di assumere farmaco VALSARTAN E IDROCLOROTIAZIDE RANBAXY.
2412	MRIGZL85P49E481L	\N	BSLMTT14M15F068E	2018-09-18 09:46:12	Visita ematologica	2018-10-01 17:22:02	GMPBBR68M63L137D	Si consiglia un ulteriore visita otorinolaringoiatrica.
2422	MRIGZL85P49E481L	\N	BSLMTT14M15F068E	2015-07-22 08:19:22	Visita radioterapica	2015-07-30 04:54:58	GMPBBR68M63L137D	Si consiglia di assumere farmaco CANDESARTAN MYLAN PHARMA.
2432	MRIGZL85P49E481L	\N	BSLMTT14M15F068E	2015-06-25 21:58:08	Visita di radioterapia	2015-07-07 22:24:40	GMPBBR68M63L137D	Si consiglia di assumere farmaco LUSINELLE.
2442	MRIGZL85P49E481L	\N	BSLMTT14M15F068E	2018-02-06 16:07:15	Visita audiologica: visita foniatrica	2018-02-15 08:34:37	BNZGPP74L13H475Z	Tutto a posto
2452	MRIGZL85P49E481L	\N	BSLMTT14M15F068E	2017-11-12 17:38:23	Visita per cure palliative	2017-11-26 04:45:48	BNZGPP74L13H475Z	Si consiglia di assumere farmaco TETRIZOLINA BOUTY.
2462	MRIGZL85P49E481L	\N	SMNLSS92L59F307L	2014-12-03 05:38:18	Visita chirurgia maxillo facciale	2014-12-15 20:28:12	ZCCLNA85M05H004D	Si consiglia un ulteriore visita angiologica.
2472	MRIGZL85P49E481L	\N	SMNLSS92L59F307L	1997-06-12 13:59:23	Visita psicodiagnostica	1997-06-22 05:49:55	BRNCHR83E66H299L	Tutto a posto
2482	MRIGZL85P49E481L	\N	SMNLSS92L59F307L	1995-06-25 09:17:43	Visita proctologica	1995-07-03 12:39:51	RZILSS83S27A507U	Si consiglia un ulteriore visita ortopedica.
2492	MRIGZL85P49E481L	\N	SMNLSS92L59F307L	2005-09-07 12:15:59	Visita immunologica	2005-09-12 14:59:47	GMPBBR68M63L137D	Si consiglia un ulteriore esame: estrazione dna o rna.
2502	MRIGZL85P49E481L	\N	SMNLSS92L59F307L	2001-03-14 09:28:44	Visita endocrinologica e diabetologica	2001-03-26 05:50:03	BTTLDN71C45B203N	Si consiglia un ulteriore visita anestesiologica/algologica.
2512	MRIGZL85P49E481L	\N	SMNLSS92L59F307L	2007-01-27 23:21:07	Visita oncologica	2007-02-03 22:41:21	RZILSS83S27A507U	Tutto a posto
2522	MRIGZL85P49E481L	\N	SMNLSS92L59F307L	2018-01-02 10:04:50	Visita dermatologica	2018-01-15 03:37:19	BNZGPP74L13H475Z	Si consiglia un ulteriore esame: cariotipo da metafasi di fibroblasti.
2542	MRIGZL85P49E481L	\N	BSLHLG45P45B160D	2019-03-07 16:05:27	Visita reumatologica	2019-03-18 20:14:48	KMERCR69R01B165K	Tutto a posto
2552	MRIGZL85P49E481L	\N	BSLHLG45P45B160D	2018-08-21 08:39:41	Visita otorinolaringoiatrica	2018-08-30 07:37:58	GMPBBR68M63L137D	Si consiglia un ulteriore esame: colorazione aggiuntiva in bande: bandeggio t.
2562	MRIGZL85P49E481L	\N	BSLHLG45P45B160D	2019-01-13 16:24:48	Visita ematologica	2019-01-22 03:31:30	BRSSDR86D62D731H	Si consiglia un ulteriore esame: tomografia computerizzata dell arto superiore senza e con contrasto.
2572	MRIGZL85P49E481L	\N	BSLHLG45P45B160D	2018-09-13 20:07:00	Visita dentistica	2018-09-26 18:01:23	RZILSS83S27A507U	Si consiglia un ulteriore esame: terapia del dolore da metastasi ossee.
2582	MRIGZL85P49E481L	\N	FRTLSN14H19L490X	2015-06-07 02:51:59	Visita oculistica	2015-06-09 12:48:08	BRSCRL82B24L201U	Si consiglia di assumere farmaco SOLFLU.
2592	MRIGZL85P49E481L	\N	FRTLSN14H19L490X	2016-05-28 19:50:56	Visita dietologica	2016-06-02 02:40:48	RSMMRT81S58G443J	Si consiglia un ulteriore visita otorinolaringoiatrica.
2602	MRIGZL85P49E481L	\N	FRTLSN14H19L490X	2018-05-09 10:08:27	Visita di medicina nucleare	2018-05-14 15:26:27	ZCCLNA85M05H004D	Si consiglia di assumere farmaco DISEON.
2612	MRIGZL85P49E481L	\N	FRTLSN14H19L490X	2018-01-06 10:36:39	Visita oculistica	2018-01-20 10:06:16	KMERCR69R01B165K	Si consiglia un ulteriore controllo protesico elettroacustico.
2622	MRIGZL85P49E481L	\N	ZCCLNZ98E09C700J	2005-01-02 00:52:34	Visita nefrologica	2005-01-09 00:06:15	KMERCR69R01B165K	Si consiglia di assumere farmaco ENOXAPARINA ROVI.
2632	MRIGZL85P49E481L	\N	ZCCLNZ98E09C700J	2005-11-01 09:09:53	Visita di radiologia interventistica	2005-11-09 16:56:57	ZCCLNA85M05H004D	Si consiglia un ulteriore esame: tipizzazione genomica hla - dqb1 bassa risoluzione.
2642	MRIGZL85P49E481L	\N	ZCCLNZ98E09C700J	2015-11-06 10:34:55	Visita audiologica: visita foniatrica	2015-11-09 06:13:22	ZCCLNA85M05H004D	Si consiglia di assumere farmaco IBUPROFENE MYLAN ITALIA.
2652	MRIGZL85P49E481L	\N	ZCCLNZ98E09C700J	2015-06-11 21:44:37	Visita di radiologia interventistica	2015-06-19 15:14:07	BRSCRL82B24L201U	Si consiglia di assumere farmaco CARNITENE.
2662	MRIGZL85P49E481L	\N	ZCCLNZ98E09C700J	2009-05-29 20:03:53	Visita oculistica	2009-06-10 00:31:31	BRSCRL82B24L201U	Si consiglia un ulteriore visita anestesiologica/algologica.
2302	PRTMRZ80C26A537X	\N	PRCLVC38B49G644S	2019-05-24 12:53:44	Visita cardiochirurgica	\N	\N	\N
2672	MRIGZL85P49E481L	\N	KFLNCL79H42C727A	2001-12-17 06:56:44	Visita psicodiagnostica	2002-01-01 05:41:40	BRSSDR86D62D731H	Si consiglia un ulteriore esame: trattamento con apparecchi fissi.
2682	MRIGZL85P49E481L	\N	KFLNCL79H42C727A	2018-08-26 01:00:18	Visita oncoematologica	2018-08-29 22:54:29	GMPBBR68M63L137D	Si consiglia di assumere farmaco FRAXODI.
2692	MRIGZL85P49E481L	\N	KFLNCL79H42C727A	1988-06-11 17:21:09	Visita oncoematologica	1988-06-23 04:31:12	KMERCR69R01B165K	Tutto a posto
2702	MRIGZL85P49E481L	\N	KFLNCL79H42C727A	1983-10-12 12:40:36	Visita gastroenterologica	1983-10-19 14:52:27	KMERCR69R01B165K	Si consiglia di assumere farmaco CLARITROMICINA SANDOZ.
2712	MRIGZL85P49E481L	\N	KFLNCL79H42C727A	2013-06-14 02:55:08	Visita neuropsichiatrica infantile	2013-06-22 16:32:12	BRNCHR83E66H299L	Tutto a posto
2722	MRIGZL85P49E481L	\N	KFLNCL79H42C727A	1981-03-19 17:06:01	Visita pneumologica	1981-03-30 23:43:47	BRSCRL82B24L201U	Tutto a posto
2732	MRIGZL85P49E481L	\N	RCKFRZ46T60E065G	2018-08-28 20:54:06	Visita oncoematologica pediatrica	2018-09-02 14:30:05	BRSSDR86D62D731H	Si consiglia di assumere farmaco VERELAIT.
2742	MRIGZL85P49E481L	\N	RCKFRZ46T60E065G	2019-03-03 18:37:17	Visita oculistica	2019-03-09 19:14:33	BTTLDN71C45B203N	Si consiglia di assumere farmaco CARNITENE.
2752	MRIGZL85P49E481L	\N	RCKFRZ46T60E065G	2019-02-27 16:23:43	Visita chirurgia plastica	2019-03-11 03:14:22	RZILSS83S27A507U	Si consiglia un ulteriore visita oculistica.
2782	MRIGZL85P49E481L	\N	MNTNLS46S52I839M	2019-04-08 20:09:02	Visita ostetrica	2019-04-19 04:27:40	KMERCR69R01B165K	Si consiglia un ulteriore controllo periodico per terapia anticoagulante orale (tao).
2792	MRIGZL85P49E481L	\N	MNTNLS46S52I839M	2018-12-29 18:20:54	Visita audiologica: visita foniatrica	2019-01-04 18:41:33	BNZGPP74L13H475Z	Si consiglia di assumere farmaco VASOKINOX.
2802	MRIGZL85P49E481L	\N	MNTNLS46S52I839M	2019-04-03 09:35:25	Visita senologica	2019-04-16 05:32:36	BRSCRL82B24L201U	Si consiglia un ulteriore visita endocrinologica e diabetologica.
2812	MRIGZL85P49E481L	\N	MNTNLS46S52I839M	2018-11-30 19:34:15	Controllo periodico per terapia anticoagulante orale (TAO)	2018-12-13 13:17:29	RZILSS83S27A507U	Tutto a posto
2822	MRIGZL85P49E481L	\N	MNTNLS46S52I839M	2018-11-08 12:39:44	Controllo periodico per terapia anticoagulante orale (TAO)	2018-11-10 19:57:59	KMERCR69R01B165K	Si consiglia un ulteriore esame: tipizzazione genomica hla - dq seq. diretto.
2832	MRIGZL85P49E481L	\N	MNTNLS46S52I839M	2018-06-10 02:44:09	Visita multidisciplinare per cure palliative	2018-06-20 16:57:38	BTTLDN71C45B203N	Si consiglia un ulteriore visita cardiochirurgica pediatrica.
2842	MRIGZL85P49E481L	\N	MNPMRA62E19A916J	2018-08-17 07:19:18	Visita dentistica	2018-08-24 18:52:22	RSMMRT81S58G443J	Si consiglia un ulteriore visita oncologica.
2862	MRIGZL85P49E481L	\N	MNPMRA62E19A916J	2018-07-23 10:54:03	Controllo/programmazione di neurostimolatore spinale	2018-08-05 23:40:43	RZILSS83S27A507U	Tutto a posto
2872	BRSNCL79A61D484Q	\N	GVNMTN72E61E981G	1978-06-17 03:05:23	Visita epatologica	1978-06-30 20:55:16	ZCCLNA85M05H004D	Si consiglia di assumere farmaco VERAX BLU.
2882	BRSNCL79A61D484Q	\N	GVNMTN72E61E981G	2001-02-06 10:14:32	Visita fisiatrica 	2001-02-19 04:10:04	RSMMRT81S58G443J	Tutto a posto
2892	BRSNCL79A61D484Q	\N	GVNMTN72E61E981G	2012-08-15 03:47:21	Visita neurochirurgica	2012-08-17 12:52:50	RZILSS83S27A507U	Si consiglia un ulteriore visita anestesiologica/algologica.
2902	BRSNCL79A61D484Q	\N	GVNMTN72E61E981G	1998-01-27 02:47:13	Visita chirurgia toracica	1998-02-10 19:17:12	KMERCR69R01B165K	Tutto a posto
2912	BRSNCL79A61D484Q	\N	GVNMTN72E61E981G	2008-09-09 14:45:25	Visita proctologica	2008-09-19 09:37:07	RSMMRT81S58G443J	Si consiglia di assumere farmaco LATANOPROST E TIMOLOLO ZENTIVA.
2922	BRSNCL79A61D484Q	\N	MSTCST13E58L174Q	2018-11-07 10:12:57	Controllo e programmazione pace-maker	2018-11-11 04:44:30	BRSSDR86D62D731H	Si consiglia di assumere farmaco ROSUVASTATINA SUN.
2932	BRSNCL79A61D484Q	\N	MSTCST13E58L174Q	2017-03-20 14:40:35	Visita urologica pediatrica	2017-03-27 04:49:30	BRNCHR83E66H299L	Si consiglia di assumere farmaco CEROXTERIL.
2942	BRSNCL79A61D484Q	\N	MSTCST13E58L174Q	2017-01-30 15:53:29	Visita di radiologia interventistica	2017-02-13 21:39:00	KMERCR69R01B165K	Tutto a posto
2952	BRSNCL79A61D484Q	\N	MSTCST13E58L174Q	2016-08-04 20:35:28	Visita neurologica pediatrica	2016-08-06 01:37:19	GMPBBR68M63L137D	Tutto a posto
2962	BRSNCL79A61D484Q	\N	MSTCST13E58L174Q	2016-01-17 01:00:14	Visita dermatologica	2016-01-28 07:53:25	ZCCLNA85M05H004D	Si consiglia un ulteriore visita urologica/andrologica.
2972	BRSNCL79A61D484Q	\N	MSTCST13E58L174Q	2018-09-05 13:27:41	Visita radioterapica	2018-09-18 23:31:19	KMERCR69R01B165K	Si consiglia di assumere farmaco ESTREVA.
2982	BRSNCL79A61D484Q	\N	MSTCST13E58L174Q	2014-11-12 20:55:04	Visita di medicina estetica	2014-11-25 15:46:51	BNZGPP74L13H475Z	Si consiglia un ulteriore esame: trattamento ortodontico con apparecchi mobili.
2992	BRSNCL79A61D484Q	\N	GRNPTR17H14L033Y	2019-01-22 21:58:55	Visita oncologica	2019-02-01 13:40:13	ZCCLNA85M05H004D	Si consiglia un ulteriore esame: tomoscintigrafia miocardica (pet).
3002	BRSNCL79A61D484Q	\N	GRNPTR17H14L033Y	2018-11-13 03:32:24	Visita gastroenterologica	2018-11-27 12:18:07	BRSCRL82B24L201U	Tutto a posto
3012	BRSNCL79A61D484Q	\N	GRNPTR17H14L033Y	2018-12-13 22:46:52	Visita cardiochirurgica pediatrica	2018-12-20 02:21:18	BNZGPP74L13H475Z	Tutto a posto
3022	BRSNCL79A61D484Q	\N	GRNPTR17H14L033Y	2019-02-10 13:06:54	Visita cardiochirurgica	2019-02-15 22:15:16	BRNCHR83E66H299L	Si consiglia un ulteriore esame: estrazione di dente permanente.
3032	BRSNCL79A61D484Q	\N	GRNPTR17H14L033Y	2019-02-02 23:29:47	Visita radioterapica	2019-02-16 19:16:20	RZILSS83S27A507U	Si consiglia un ulteriore visita neurochirurgica.
3042	BRSNCL79A61D484Q	\N	BNZMRT74P01L106V	2009-05-12 21:49:17	Visita neurologica pediatrica	2009-05-23 18:51:07	RZILSS83S27A507U	Si consiglia di assumere farmaco ROSUVASTATINA PENSA.
3052	BRSNCL79A61D484Q	\N	BNZMRT74P01L106V	1987-10-03 18:58:47	Visita ostetrica	1987-10-05 12:37:54	BRNCHR83E66H299L	Si consiglia di assumere farmaco ENALAPRIL RANBAXY ITALIA.
3062	BRSNCL79A61D484Q	\N	MSTNMO60E66A635P	2018-12-06 19:22:34	Relazione per organi giudiziari	2018-12-12 18:07:45	BNZGPP74L13H475Z	Si consiglia un ulteriore esame: tomoscintigrafia globale.
3072	BRSNCL79A61D484Q	\N	MSTNMO60E66A635P	2019-04-20 03:05:34	Visita neuropsichiatrica infantile	2019-04-27 05:50:55	BTTLDN71C45B203N	Si consiglia un ulteriore esame: colorazione aggiuntiva in bande: actinomicina d.
3092	BRSNCL79A61D484Q	\N	MSTNMO60E66A635P	2018-07-18 04:12:13	Visita urologica pediatrica	2018-07-19 21:59:14	KMERCR69R01B165K	Si consiglia un ulteriore visita pediatrica.
3102	BRSNCL79A61D484Q	\N	MSTNMO60E66A635P	2018-11-12 05:42:07	Visita multidisciplinare per cure palliative	2018-11-18 15:01:46	BNZGPP74L13H475Z	Si consiglia un ulteriore visita proctologica.
3112	BRSNCL79A61D484Q	\N	MSTNMO60E66A635P	2018-06-24 00:12:41	Visita dietologica	2018-06-30 09:56:37	BNZGPP74L13H475Z	Si consiglia un ulteriore visita radioterapica.
3132	BRSNCL79A61D484Q	\N	ZCCLNE08T47D631A	2013-05-07 11:49:43	Visita reumatologica	2013-05-15 22:45:16	BRSCRL82B24L201U	Tutto a posto
3142	BRSNCL79A61D484Q	\N	ZCCLNE08T47D631A	2009-07-31 13:15:47	Visita reumatologica	2009-08-03 06:46:14	RSMMRT81S58G443J	Tutto a posto
3152	BRSNCL79A61D484Q	\N	ZCCLNE08T47D631A	2015-09-12 21:36:11	Visita reumatologica	2015-09-15 09:02:07	ZCCLNA85M05H004D	Tutto a posto
3162	BRSNCL79A61D484Q	\N	ZCCLNE08T47D631A	2010-01-19 21:45:12	Visita cardiochirurgica	2010-01-21 07:47:03	BRSCRL82B24L201U	Si consiglia un ulteriore esame: tipizzazione sierologica hla classe ii.
3172	BRSNCL79A61D484Q	\N	ZCCLNE08T47D631A	2017-01-11 05:24:33	Controllo/programmazione di neurostimolatore encefalico	2017-01-17 02:47:55	BRNCHR83E66H299L	Tutto a posto
3182	BRSNCL79A61D484Q	\N	ZCCLNE08T47D631A	2015-01-29 22:03:25	Visita senologica	2015-02-08 03:39:54	RZILSS83S27A507U	Si consiglia un ulteriore esame: analisi mutazione del dna con reverse dot blot.
3192	BRSNCL79A61D484Q	\N	ZZLDRN97B55C994H	2013-10-18 06:06:11	Visita chirurgia maxillo facciale	2013-10-30 07:51:24	BTTLDN71C45B203N	Si consiglia di assumere farmaco TAREG.
3202	BRSNCL79A61D484Q	\N	GGRPTR15D05L097I	2018-05-22 07:37:28	Visita chirurgia di controllo	2018-05-29 20:16:35	GMPBBR68M63L137D	Tutto a posto
3212	BRSNCL79A61D484Q	\N	GGRPTR15D05L097I	2015-08-30 08:11:05	Visita neurologica pediatrica	2015-09-06 07:50:22	BTTLDN71C45B203N	Si consiglia un ulteriore visita genetica medica.
3222	BRSNCL79A61D484Q	\N	GGRPTR15D05L097I	2015-12-22 16:18:52	Visita multidisciplinare per cure palliative	2016-01-03 07:44:38	ZCCLNA85M05H004D	Si consiglia un ulteriore visita ostetrica.
3232	BRSNCL79A61D484Q	\N	GGRPTR15D05L097I	2016-02-06 02:11:43	Visita medico-sportiva	2016-02-10 17:55:56	GMPBBR68M63L137D	Si consiglia un ulteriore visita urologica/andrologica.
3242	BRSNCL79A61D484Q	\N	GGRPTR15D05L097I	2017-10-14 01:29:49	Visita audiologica: visita foniatrica	2017-10-14 18:53:39	BNZGPP74L13H475Z	Tutto a posto
3252	BRSNCL79A61D484Q	\N	GGRPTR15D05L097I	2016-09-17 13:56:40	Visita chirurgia plastica	2016-09-24 04:15:09	BTTLDN71C45B203N	Tutto a posto
3262	BRSNCL79A61D484Q	\N	GGRPTR15D05L097I	2015-05-18 11:38:07	Visita oculistica pediatrica	2015-05-23 02:01:40	RZILSS83S27A507U	Si consiglia un ulteriore esame: applicazione corona e perno.
3272	BRSNCL79A61D484Q	\N	GMPPTR66C24D821H	2018-09-21 17:55:43	Visita cardiochirurgica	2018-10-04 12:35:01	ZCCLNA85M05H004D	Si consiglia un ulteriore esame: tipizzazione sierologica hla classe i.
3282	BRSNCL79A61D484Q	\N	DVRNLS18R50H152G	2018-11-22 21:35:46	Visita neurochirurgica pediatrica	2018-11-27 11:04:05	BRNCHR83E66H299L	Si consiglia un ulteriore esame: analisi mutazione del dna con reverse dot blot.
3292	BRSNCL79A61D484Q	\N	DVRNLS18R50H152G	2019-02-10 05:39:53	Visita dietologica	2019-02-23 19:23:58	BRSCRL82B24L201U	Si consiglia di assumere farmaco PEMETREXED SUN.
3302	BRSNCL79A61D484Q	\N	DVRNLS18R50H152G	2018-12-27 19:04:09	Visita psicodiagnostica	2019-01-05 23:52:14	KMERCR69R01B165K	Si consiglia un ulteriore esame: colorazione aggiuntiva in bande: actinomicina d.
42	DVRDRN73P08D075W	\N	BNNCRN43T59L108I	2019-06-10 19:29:01	Visita psicodiagnostica	\N	\N	\N
312	DVRDRN73P08D075W	\N	GSSMRA22A64L660R	2019-05-21 16:02:06	Visita radioterapica	\N	\N	\N
422	DVRDRN73P08D075W	\N	KFLRRT29A04I949D	2019-06-19 12:42:03	Visita reumatologica	\N	\N	\N
562	NRDLDI83E50E614G	\N	ZZLSLV15R62L162L	2019-05-26 17:42:36	Visita reumatologica	\N	\N	\N
882	NRDLDI83E50E614G	\N	BRSFNC35L17D336O	2019-06-18 00:44:29	Visita medico-sportiva	\N	\N	\N
1002	RZZSLV81R01E150H	\N	HLZNLS17L59F949S	2019-06-02 23:47:44	Visita reumatologica	\N	\N	\N
1032	RZZSLV81R01E150H	\N	GRDFLV28R05A178Z	2019-06-11 05:41:58	Visita reumatologica	\N	\N	\N
1042	RZZSLV81R01E150H	\N	SCRLDI19H55A839F	2019-06-24 08:31:02	Visita oncologica	\N	\N	\N
1102	RZZSLV81R01E150H	\N	RSSLCU31P45I173U	2019-06-09 17:16:50	Visita radioterapica	\N	\N	\N
1112	RZZSLV81R01E150H	\N	LSLFNC25M63H532S	2019-05-27 18:09:03	Visita oncologica	\N	\N	\N
1362	RZZSLV81R01E150H	\N	GFLDRD49M10F856Q	2019-06-03 09:23:07	Visita medico-sportiva	\N	\N	\N
1562	SMMTMS72M20G443H	\N	MSTRCC17M41A158X	2019-06-09 16:09:33	Visita ostetrica	\N	\N	\N
1592	SMMTMS72M20G443H	\N	MSTRCC17M41A158X	2019-06-08 09:22:37	Visita ostetrica	\N	\N	\N
1712	SMMTMS72M20G443H	\N	PRCDNL50L03F835T	2019-05-05 21:19:03	Visita psicodiagnostica	\N	\N	\N
1762	SMMTMS72M20G443H	\N	CHMLNE17M45L896F	2019-06-21 17:47:39	Visita cardiochirurgica	\N	\N	\N
3122	BRSNCL79A61D484Q	\N	MSTNMO60E66A635P	2019-05-17 08:27:32	Visita oncologica	\N	\N	\N
2312	PRTMRZ80C26A537X	\N	PRCLVC38B49G644S	2019-05-28 00:33:06	Visita reumatologica	\N	\N	\N
2532	MRIGZL85P49E481L	\N	BSLHLG45P45B160D	2019-05-07 15:16:21	Visita psicodiagnostica	\N	\N	\N
2762	MRIGZL85P49E481L	\N	RCKFRZ46T60E065G	2019-05-06 19:37:31	Visita cardiochirurgica	\N	\N	\N
2772	MRIGZL85P49E481L	\N	MNTNLS46S52I839M	2019-06-05 05:50:57	Visita cardiochirurgica	\N	\N	\N
2852	MRIGZL85P49E481L	\N	MNPMRA62E19A916J	2019-05-12 09:13:08	Visita psicodiagnostica	\N	\N	\N
3082	BRSNCL79A61D484Q	\N	MSTNMO60E66A635P	2019-05-30 10:15:16	Visita oncologica	\N	\N	\N
\.


--
-- TOC entry 2898 (class 0 OID 19128)
-- Dependencies: 198
-- Data for Name: serviziprovinciali; Type: TABLE DATA; Schema: progettoipw; Owner: postgres
--

COPY progettoipw.serviziprovinciali (id, userinfo, password, flag_recuperopassword, email) FROM stdin;
900888	servizio_provinciale_trento	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	servizioprovinciale_tn@dayrep.com
900777	servizio_provinciale_bolzano	5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8	f	servizioprovinciale_bz@dayrep.com
\.


--
-- TOC entry 2907 (class 0 OID 19263)
-- Dependencies: 207
-- Data for Name: ticket; Type: TABLE DATA; Schema: progettoipw; Owner: postgres
--

COPY progettoipw.ticket (id, tipo, prezzo, idfarmacia, idmedspec, idssp) FROM stdin;
0	FARMACO	3	404600	\N	\N
80	FARMACO	3	410000	\N	\N
90	FARMACO	3	404600	\N	\N
10	FARMACO	3	413200	\N	\N
50	FARMACO	3	413200	\N	\N
60	FARMACO	3	409800	\N	\N
40	FARMACO	3	413100	\N	\N
120	FARMACO	3	410000	\N	\N
100	FARMACO	3	413100	\N	\N
150	FARMACO	3	410000	\N	\N
140	FARMACO	3	413100	\N	\N
30	FARMACO	3	404600	\N	\N
20	FARMACO	3	413200	\N	\N
110	FARMACO	3	409800	\N	\N
130	FARMACO	3	409800	\N	\N
620	FARMACO	3	409800	\N	\N
550	FARMACO	3	410000	\N	\N
450	FARMACO	3	413200	\N	\N
670	FARMACO	3	404600	\N	\N
540	FARMACO	3	404600	\N	\N
690	FARMACO	3	409800	\N	\N
660	FARMACO	3	404600	\N	\N
430	FARMACO	3	410000	\N	\N
560	FARMACO	3	410000	\N	\N
680	FARMACO	3	413100	\N	\N
460	FARMACO	3	413100	\N	\N
470	FARMACO	3	404600	\N	\N
640	FARMACO	3	413200	\N	\N
600	FARMACO	3	404600	\N	\N
630	FARMACO	3	413200	\N	\N
590	FARMACO	3	413100	\N	\N
740	FARMACO	3	410000	\N	\N
700	FARMACO	3	404600	\N	\N
650	FARMACO	3	410000	\N	\N
610	FARMACO	3	413200	\N	\N
440	FARMACO	3	413100	\N	\N
710	FARMACO	3	413100	\N	\N
510	FARMACO	3	413100	\N	\N
730	FARMACO	3	409800	\N	\N
570	FARMACO	3	410000	\N	\N
580	FARMACO	3	413200	\N	\N
530	FARMACO	3	409800	\N	\N
720	FARMACO	3	409800	\N	\N
520	FARMACO	3	409800	\N	\N
1310	FARMACO	3	404600	\N	\N
1300	FARMACO	3	409800	\N	\N
1230	FARMACO	3	410000	\N	\N
1220	FARMACO	3	413100	\N	\N
1240	FARMACO	3	404600	\N	\N
1260	FARMACO	3	404600	\N	\N
1250	FARMACO	3	409800	\N	\N
1320	FARMACO	3	404600	\N	\N
1290	FARMACO	3	404600	\N	\N
1270	FARMACO	3	404600	\N	\N
1280	FARMACO	3	413100	\N	\N
351	ESAME	11	\N	\N	900888
1521	ESAME	11	\N	\N	900888
2551	ESAME	11	\N	\N	900888
1181	ESAME	11	\N	\N	900888
1081	ESAME	11	\N	\N	900888
481	ESAME	11	\N	\N	900888
1491	ESAME	11	\N	\N	900888
1541	ESAME	11	\N	\N	900888
951	ESAME	11	\N	\N	900888
2431	ESAME	11	\N	\N	900888
1531	ESAME	11	\N	\N	900888
1711	ESAME	11	\N	\N	900888
511	ESAME	11	\N	\N	900888
341	ESAME	11	\N	\N	900888
2491	ESAME	11	\N	\N	900888
431	ESAME	11	\N	\N	900888
1691	ESAME	11	\N	\N	900888
2241	ESAME	11	\N	\N	900888
2361	ESAME	11	\N	\N	900888
491	ESAME	11	\N	\N	900888
2331	ESAME	11	\N	\N	900888
2561	ESAME	11	\N	\N	900888
2471	ESAME	11	\N	\N	900888
541	ESAME	11	\N	\N	900888
2501	ESAME	11	\N	\N	900888
2141	ESAME	11	\N	\N	900888
451	ESAME	11	\N	\N	900888
1471	ESAME	11	\N	\N	900888
971	ESAME	11	\N	\N	900888
1331	ESAME	11	\N	\N	900888
941	ESAME	11	\N	\N	900888
2271	ESAME	11	\N	\N	900888
2131	ESAME	11	\N	\N	900888
2541	ESAME	11	\N	\N	900888
2371	ESAME	11	\N	\N	900888
1430	FARMACO	3	409800	\N	\N
1370	FARMACO	3	413100	\N	\N
1400	FARMACO	3	410000	\N	\N
1340	FARMACO	3	410000	\N	\N
1410	FARMACO	3	409800	\N	\N
1450	FARMACO	3	410000	\N	\N
1360	FARMACO	3	410000	\N	\N
1440	FARMACO	3	413100	\N	\N
1390	FARMACO	3	413100	\N	\N
1420	FARMACO	3	410000	\N	\N
1350	FARMACO	3	410000	\N	\N
1380	FARMACO	3	404600	\N	\N
2401	ESAME	11	\N	\N	900888
641	ESAME	11	\N	\N	900888
1571	ESAME	11	\N	\N	900888
1751	ESAME	11	\N	\N	900888
961	ESAME	11	\N	\N	900888
1781	ESAME	11	\N	\N	900888
1581	ESAME	11	\N	\N	900888
1301	ESAME	11	\N	\N	900888
2311	ESAME	11	\N	\N	900888
2121	ESAME	11	\N	\N	900888
1171	ESAME	11	\N	\N	900888
401	ESAME	11	\N	\N	900888
2191	ESAME	11	\N	\N	900888
2161	ESAME	11	\N	\N	900888
601	ESAME	11	\N	\N	900888
1701	ESAME	11	\N	\N	900888
2391	ESAME	11	\N	\N	900888
1651	ESAME	11	\N	\N	900888
1141	ESAME	11	\N	\N	900888
461	ESAME	11	\N	\N	900888
561	ESAME	11	\N	\N	900888
1431	ESAME	11	\N	\N	900888
1481	ESAME	11	\N	\N	900888
1031	ESAME	11	\N	\N	900888
1761	ESAME	11	\N	\N	900888
1771	ESAME	11	\N	\N	900888
551	ESAME	11	\N	\N	900888
1161	ESAME	11	\N	\N	900888
2461	ESAME	11	\N	\N	900888
1361	ESAME	11	\N	\N	900888
1371	ESAME	11	\N	\N	900888
1041	ESAME	11	\N	\N	900888
1661	ESAME	11	\N	\N	900888
521	ESAME	11	\N	\N	900888
1501	ESAME	11	\N	\N	900888
331	ESAME	11	\N	\N	900888
1561	ESAME	11	\N	\N	900888
2181	ESAME	11	\N	\N	900888
1021	ESAME	11	\N	\N	900888
1001	ESAME	11	\N	\N	900888
1221	ESAME	11	\N	\N	900888
2451	ESAME	11	\N	\N	900888
2291	ESAME	11	\N	\N	900888
1591	ESAME	11	\N	\N	900888
2321	ESAME	11	\N	\N	900888
1281	ESAME	11	\N	\N	900888
2381	ESAME	11	\N	\N	900888
1461	ESAME	11	\N	\N	900888
531	ESAME	11	\N	\N	900888
2151	ESAME	11	\N	\N	900888
2231	ESAME	11	\N	\N	900888
1211	ESAME	11	\N	\N	900888
321	ESAME	11	\N	\N	900888
1411	ESAME	11	\N	\N	900888
1261	ESAME	11	\N	\N	900888
2301	ESAME	11	\N	\N	900888
1681	ESAME	11	\N	\N	900888
571	ESAME	11	\N	\N	900888
1201	ESAME	11	\N	\N	900888
631	ESAME	11	\N	\N	900888
1741	ESAME	11	\N	\N	900888
1551	ESAME	11	\N	\N	900888
1071	ESAME	11	\N	\N	900888
1641	ESAME	11	\N	\N	900888
1241	ESAME	11	\N	\N	900888
471	ESAME	11	\N	\N	900888
2221	ESAME	11	\N	\N	900888
1611	ESAME	11	\N	\N	900888
1401	ESAME	11	\N	\N	900888
2531	ESAME	11	\N	\N	900888
1391	ESAME	11	\N	\N	900888
1051	ESAME	11	\N	\N	900888
2511	ESAME	11	\N	\N	900888
2251	ESAME	11	\N	\N	900888
581	ESAME	11	\N	\N	900888
2441	ESAME	11	\N	\N	900888
2351	ESAME	11	\N	\N	900888
661	ESAME	11	\N	\N	900888
2411	ESAME	11	\N	\N	900888
2261	ESAME	11	\N	\N	900888
2201	ESAME	11	\N	\N	900888
391	ESAME	11	\N	\N	900888
1311	ESAME	11	\N	\N	900888
2281	ESAME	11	\N	\N	900888
1441	ESAME	11	\N	\N	900888
1101	ESAME	11	\N	\N	900888
441	ESAME	11	\N	\N	900888
1271	ESAME	11	\N	\N	900888
1511	ESAME	11	\N	\N	900888
1321	ESAME	11	\N	\N	900888
371	ESAME	11	\N	\N	900888
2481	ESAME	11	\N	\N	900888
1631	ESAME	11	\N	\N	900888
591	ESAME	11	\N	\N	900888
2211	ESAME	11	\N	\N	900888
2421	ESAME	11	\N	\N	900888
2581	ESAME	11	\N	\N	900888
651	ESAME	11	\N	\N	900888
381	ESAME	11	\N	\N	900888
1291	ESAME	11	\N	\N	900888
1621	ESAME	11	\N	\N	900888
421	ESAME	11	\N	\N	900888
1731	ESAME	11	\N	\N	900888
411	ESAME	11	\N	\N	900888
611	ESAME	11	\N	\N	900888
501	ESAME	11	\N	\N	900888
1191	ESAME	11	\N	\N	900888
621	ESAME	11	\N	\N	900888
1341	ESAME	11	\N	\N	900888
1061	ESAME	11	\N	\N	900888
1131	ESAME	11	\N	\N	900888
1601	ESAME	11	\N	\N	900888
1231	ESAME	11	\N	\N	900888
1421	ESAME	11	\N	\N	900888
1111	ESAME	11	\N	\N	900888
1721	ESAME	11	\N	\N	900888
1251	ESAME	11	\N	\N	900888
1791	ESAME	11	\N	\N	900777
71	ESAME	11	\N	\N	900777
51	ESAME	11	\N	\N	900777
181	ESAME	11	\N	\N	900777
781	ESAME	11	\N	\N	900777
681	ESAME	11	\N	\N	900777
1971	ESAME	11	\N	\N	900777
761	ESAME	11	\N	\N	900777
201	ESAME	11	\N	\N	900777
861	ESAME	11	\N	\N	900777
701	ESAME	11	\N	\N	900777
1891	ESAME	11	\N	\N	900777
891	ESAME	11	\N	\N	900777
911	ESAME	11	\N	\N	900777
801	ESAME	11	\N	\N	900777
751	ESAME	11	\N	\N	900777
101	ESAME	11	\N	\N	900777
1831	ESAME	11	\N	\N	900777
241	ESAME	11	\N	\N	900777
31	ESAME	11	\N	\N	900777
271	ESAME	11	\N	\N	900777
1811	ESAME	11	\N	\N	900777
1871	ESAME	11	\N	\N	900777
111	ESAME	11	\N	\N	900777
851	ESAME	11	\N	\N	900777
711	ESAME	11	\N	\N	900777
1921	ESAME	11	\N	\N	900777
2051	ESAME	11	\N	\N	900777
1901	ESAME	11	\N	\N	900777
791	ESAME	11	\N	\N	900777
871	ESAME	11	\N	\N	900777
2041	ESAME	11	\N	\N	900777
311	ESAME	11	\N	\N	900777
261	ESAME	11	\N	\N	900777
11	ESAME	11	\N	\N	900777
821	ESAME	11	\N	\N	900777
161	ESAME	11	\N	\N	900777
121	ESAME	11	\N	\N	900777
2061	ESAME	11	\N	\N	900777
41	ESAME	11	\N	\N	900777
2091	ESAME	11	\N	\N	900777
1931	ESAME	11	\N	\N	900777
2111	ESAME	11	\N	\N	900777
2011	ESAME	11	\N	\N	900777
1801	ESAME	11	\N	\N	900777
741	ESAME	11	\N	\N	900777
1941	ESAME	11	\N	\N	900777
1851	ESAME	11	\N	\N	900777
291	ESAME	11	\N	\N	900777
771	ESAME	11	\N	\N	900777
2031	ESAME	11	\N	\N	900777
1911	ESAME	11	\N	\N	900777
2081	ESAME	11	\N	\N	900777
281	ESAME	11	\N	\N	900777
191	ESAME	11	\N	\N	900777
81	ESAME	11	\N	\N	900777
61	ESAME	11	\N	\N	900777
1951	ESAME	11	\N	\N	900777
141	ESAME	11	\N	\N	900777
1981	ESAME	11	\N	\N	900777
21	ESAME	11	\N	\N	900777
211	ESAME	11	\N	\N	900777
131	ESAME	11	\N	\N	900777
731	ESAME	11	\N	\N	900777
1961	ESAME	11	\N	\N	900777
901	ESAME	11	\N	\N	900777
721	ESAME	11	\N	\N	900777
2001	ESAME	11	\N	\N	900777
251	ESAME	11	\N	\N	900777
921	ESAME	11	\N	\N	900777
2101	ESAME	11	\N	\N	900777
1821	ESAME	11	\N	\N	900777
171	ESAME	11	\N	\N	900777
2071	ESAME	11	\N	\N	900777
931	ESAME	11	\N	\N	900777
831	ESAME	11	\N	\N	900777
811	ESAME	11	\N	\N	900777
1841	ESAME	11	\N	\N	900777
841	ESAME	11	\N	\N	900777
2021	ESAME	11	\N	\N	900777
151	ESAME	11	\N	\N	900777
91	ESAME	11	\N	\N	900777
2	VISITA	50	\N	KMERCR69R01B165K	\N
12	VISITA	50	\N	GMPBBR68M63L137D	\N
22	VISITA	50	\N	GMPBBR68M63L137D	\N
32	VISITA	50	\N	BRSCRL82B24L201U	\N
52	VISITA	50	\N	GMPBBR68M63L137D	\N
62	VISITA	50	\N	ZCCLNA85M05H004D	\N
72	VISITA	50	\N	KMERCR69R01B165K	\N
82	VISITA	50	\N	BNZGPP74L13H475Z	\N
92	VISITA	50	\N	ZCCLNA85M05H004D	\N
102	VISITA	50	\N	BRSSDR86D62D731H	\N
112	VISITA	50	\N	ZCCLNA85M05H004D	\N
122	VISITA	50	\N	RZILSS83S27A507U	\N
132	VISITA	50	\N	BTTLDN71C45B203N	\N
142	VISITA	50	\N	BTTLDN71C45B203N	\N
152	VISITA	50	\N	GMPBBR68M63L137D	\N
162	VISITA	50	\N	BNZGPP74L13H475Z	\N
172	VISITA	50	\N	BRNCHR83E66H299L	\N
182	VISITA	50	\N	ZCCLNA85M05H004D	\N
192	VISITA	50	\N	BRNCHR83E66H299L	\N
202	VISITA	50	\N	BTTLDN71C45B203N	\N
212	VISITA	50	\N	ZCCLNA85M05H004D	\N
222	VISITA	50	\N	BTTLDN71C45B203N	\N
232	VISITA	50	\N	BRNCHR83E66H299L	\N
242	VISITA	50	\N	ZCCLNA85M05H004D	\N
252	VISITA	50	\N	BRSCRL82B24L201U	\N
262	VISITA	50	\N	BRSCRL82B24L201U	\N
272	VISITA	50	\N	BRSSDR86D62D731H	\N
282	VISITA	50	\N	KMERCR69R01B165K	\N
292	VISITA	50	\N	RZILSS83S27A507U	\N
302	VISITA	50	\N	ZCCLNA85M05H004D	\N
322	VISITA	50	\N	ZCCLNA85M05H004D	\N
332	VISITA	50	\N	RSMMRT81S58G443J	\N
342	VISITA	50	\N	BRNCHR83E66H299L	\N
352	VISITA	50	\N	RZILSS83S27A507U	\N
362	VISITA	50	\N	KMERCR69R01B165K	\N
372	VISITA	50	\N	BRNCHR83E66H299L	\N
382	VISITA	50	\N	BRSSDR86D62D731H	\N
392	VISITA	50	\N	RSMMRT81S58G443J	\N
402	VISITA	50	\N	KMERCR69R01B165K	\N
412	VISITA	50	\N	RZILSS83S27A507U	\N
432	VISITA	50	\N	GMPBBR68M63L137D	\N
442	VISITA	50	\N	KMERCR69R01B165K	\N
452	VISITA	50	\N	BRNCHR83E66H299L	\N
462	VISITA	50	\N	RSMMRT81S58G443J	\N
472	VISITA	50	\N	RSMMRT81S58G443J	\N
482	VISITA	50	\N	BRSCRL82B24L201U	\N
492	VISITA	50	\N	BNZGPP74L13H475Z	\N
502	VISITA	50	\N	BRSSDR86D62D731H	\N
512	VISITA	50	\N	ZCCLNA85M05H004D	\N
522	VISITA	50	\N	BRSSDR86D62D731H	\N
532	VISITA	50	\N	BRSSDR86D62D731H	\N
542	VISITA	50	\N	RZILSS83S27A507U	\N
552	VISITA	50	\N	RSMMRT81S58G443J	\N
572	VISITA	50	\N	BRSSDR86D62D731H	\N
582	VISITA	50	\N	BRSSDR86D62D731H	\N
592	VISITA	50	\N	BNZGPP74L13H475Z	\N
602	VISITA	50	\N	RZILSS83S27A507U	\N
612	VISITA	50	\N	BRSSDR86D62D731H	\N
622	VISITA	50	\N	KMERCR69R01B165K	\N
632	VISITA	50	\N	BRSCRL82B24L201U	\N
642	VISITA	50	\N	GMPBBR68M63L137D	\N
652	VISITA	50	\N	RSMMRT81S58G443J	\N
662	VISITA	50	\N	KMERCR69R01B165K	\N
672	VISITA	50	\N	ZCCLNA85M05H004D	\N
682	VISITA	50	\N	ZCCLNA85M05H004D	\N
692	VISITA	50	\N	GMPBBR68M63L137D	\N
702	VISITA	50	\N	BTTLDN71C45B203N	\N
712	VISITA	50	\N	BNZGPP74L13H475Z	\N
722	VISITA	50	\N	RSMMRT81S58G443J	\N
732	VISITA	50	\N	BTTLDN71C45B203N	\N
742	VISITA	50	\N	BRNCHR83E66H299L	\N
752	VISITA	50	\N	GMPBBR68M63L137D	\N
762	VISITA	50	\N	BNZGPP74L13H475Z	\N
772	VISITA	50	\N	RSMMRT81S58G443J	\N
782	VISITA	50	\N	RZILSS83S27A507U	\N
792	VISITA	50	\N	BRSCRL82B24L201U	\N
802	VISITA	50	\N	BRSSDR86D62D731H	\N
812	VISITA	50	\N	KMERCR69R01B165K	\N
822	VISITA	50	\N	BRSSDR86D62D731H	\N
832	VISITA	50	\N	RSMMRT81S58G443J	\N
842	VISITA	50	\N	BRNCHR83E66H299L	\N
852	VISITA	50	\N	GMPBBR68M63L137D	\N
862	VISITA	50	\N	GMPBBR68M63L137D	\N
872	VISITA	50	\N	RZILSS83S27A507U	\N
892	VISITA	50	\N	RSMMRT81S58G443J	\N
902	VISITA	50	\N	BRSSDR86D62D731H	\N
912	VISITA	50	\N	RSMMRT81S58G443J	\N
922	VISITA	50	\N	BRNCHR83E66H299L	\N
932	VISITA	50	\N	BRNCHR83E66H299L	\N
942	VISITA	50	\N	BNZGPP74L13H475Z	\N
952	VISITA	50	\N	RSMMRT81S58G443J	\N
962	VISITA	50	\N	BTTLDN71C45B203N	\N
972	VISITA	50	\N	BRNCHR83E66H299L	\N
982	VISITA	50	\N	RZILSS83S27A507U	\N
992	VISITA	50	\N	ZCCLNA85M05H004D	\N
1012	VISITA	50	\N	RSMMRT81S58G443J	\N
1022	VISITA	50	\N	BRNCHR83E66H299L	\N
1052	VISITA	50	\N	BRNCHR83E66H299L	\N
1062	VISITA	50	\N	ZCCLNA85M05H004D	\N
1072	VISITA	50	\N	BTTLDN71C45B203N	\N
1082	VISITA	50	\N	BRNCHR83E66H299L	\N
1092	VISITA	50	\N	RSMMRT81S58G443J	\N
1122	VISITA	50	\N	GMPBBR68M63L137D	\N
1132	VISITA	50	\N	BTTLDN71C45B203N	\N
1142	VISITA	50	\N	BRNCHR83E66H299L	\N
1152	VISITA	50	\N	BNZGPP74L13H475Z	\N
1162	VISITA	50	\N	BRNCHR83E66H299L	\N
1172	VISITA	50	\N	BRSSDR86D62D731H	\N
1182	VISITA	50	\N	BRNCHR83E66H299L	\N
1192	VISITA	50	\N	BRSSDR86D62D731H	\N
1202	VISITA	50	\N	RSMMRT81S58G443J	\N
1212	VISITA	50	\N	GMPBBR68M63L137D	\N
1222	VISITA	50	\N	BTTLDN71C45B203N	\N
1232	VISITA	50	\N	RSMMRT81S58G443J	\N
1242	VISITA	50	\N	RSMMRT81S58G443J	\N
1252	VISITA	50	\N	BNZGPP74L13H475Z	\N
1262	VISITA	50	\N	ZCCLNA85M05H004D	\N
1272	VISITA	50	\N	RZILSS83S27A507U	\N
1282	VISITA	50	\N	BTTLDN71C45B203N	\N
1292	VISITA	50	\N	KMERCR69R01B165K	\N
1302	VISITA	50	\N	BNZGPP74L13H475Z	\N
1312	VISITA	50	\N	BRNCHR83E66H299L	\N
1322	VISITA	50	\N	RSMMRT81S58G443J	\N
1332	VISITA	50	\N	RZILSS83S27A507U	\N
1342	VISITA	50	\N	BRSCRL82B24L201U	\N
1352	VISITA	50	\N	BRSSDR86D62D731H	\N
1372	VISITA	50	\N	BTTLDN71C45B203N	\N
1382	VISITA	50	\N	BTTLDN71C45B203N	\N
1392	VISITA	50	\N	BRNCHR83E66H299L	\N
1402	VISITA	50	\N	KMERCR69R01B165K	\N
1412	VISITA	50	\N	BTTLDN71C45B203N	\N
1422	VISITA	50	\N	BRNCHR83E66H299L	\N
1432	VISITA	50	\N	RZILSS83S27A507U	\N
1442	VISITA	50	\N	BTTLDN71C45B203N	\N
1452	VISITA	50	\N	RZILSS83S27A507U	\N
1462	VISITA	50	\N	BRNCHR83E66H299L	\N
1472	VISITA	50	\N	GMPBBR68M63L137D	\N
1482	VISITA	50	\N	KMERCR69R01B165K	\N
1492	VISITA	50	\N	BRSSDR86D62D731H	\N
1502	VISITA	50	\N	GMPBBR68M63L137D	\N
1512	VISITA	50	\N	RSMMRT81S58G443J	\N
1522	VISITA	50	\N	BRSCRL82B24L201U	\N
1532	VISITA	50	\N	RSMMRT81S58G443J	\N
1542	VISITA	50	\N	BRSCRL82B24L201U	\N
1552	VISITA	50	\N	BTTLDN71C45B203N	\N
1572	VISITA	50	\N	BNZGPP74L13H475Z	\N
1582	VISITA	50	\N	GMPBBR68M63L137D	\N
1602	VISITA	50	\N	GMPBBR68M63L137D	\N
1612	VISITA	50	\N	GMPBBR68M63L137D	\N
1622	VISITA	50	\N	KMERCR69R01B165K	\N
1632	VISITA	50	\N	BRNCHR83E66H299L	\N
1642	VISITA	50	\N	BRSSDR86D62D731H	\N
1652	VISITA	50	\N	BRSCRL82B24L201U	\N
1662	VISITA	50	\N	GMPBBR68M63L137D	\N
1672	VISITA	50	\N	BNZGPP74L13H475Z	\N
1682	VISITA	50	\N	RSMMRT81S58G443J	\N
1692	VISITA	50	\N	BTTLDN71C45B203N	\N
1702	VISITA	50	\N	BRSCRL82B24L201U	\N
1722	VISITA	50	\N	RSMMRT81S58G443J	\N
1732	VISITA	50	\N	ZCCLNA85M05H004D	\N
1742	VISITA	50	\N	BRSSDR86D62D731H	\N
1752	VISITA	50	\N	BNZGPP74L13H475Z	\N
1772	VISITA	50	\N	BRSCRL82B24L201U	\N
1782	VISITA	50	\N	ZCCLNA85M05H004D	\N
1792	VISITA	50	\N	BRSCRL82B24L201U	\N
1802	VISITA	50	\N	BRSSDR86D62D731H	\N
1812	VISITA	50	\N	RZILSS83S27A507U	\N
1822	VISITA	50	\N	ZCCLNA85M05H004D	\N
1832	VISITA	50	\N	BTTLDN71C45B203N	\N
1842	VISITA	50	\N	RZILSS83S27A507U	\N
1852	VISITA	50	\N	GMPBBR68M63L137D	\N
1862	VISITA	50	\N	BRSSDR86D62D731H	\N
1872	VISITA	50	\N	RSMMRT81S58G443J	\N
1882	VISITA	50	\N	KMERCR69R01B165K	\N
1902	VISITA	50	\N	BRSCRL82B24L201U	\N
1912	VISITA	50	\N	RZILSS83S27A507U	\N
1922	VISITA	50	\N	ZCCLNA85M05H004D	\N
1932	VISITA	50	\N	BNZGPP74L13H475Z	\N
1942	VISITA	50	\N	RSMMRT81S58G443J	\N
1952	VISITA	50	\N	BRSSDR86D62D731H	\N
1962	VISITA	50	\N	RSMMRT81S58G443J	\N
1972	VISITA	50	\N	GMPBBR68M63L137D	\N
1982	VISITA	50	\N	BRSSDR86D62D731H	\N
1992	VISITA	50	\N	GMPBBR68M63L137D	\N
2002	VISITA	50	\N	BRNCHR83E66H299L	\N
2012	VISITA	50	\N	BNZGPP74L13H475Z	\N
2022	VISITA	50	\N	RZILSS83S27A507U	\N
2032	VISITA	50	\N	BRSCRL82B24L201U	\N
2042	VISITA	50	\N	ZCCLNA85M05H004D	\N
2052	VISITA	50	\N	BNZGPP74L13H475Z	\N
2062	VISITA	50	\N	ZCCLNA85M05H004D	\N
2072	VISITA	50	\N	RZILSS83S27A507U	\N
2082	VISITA	50	\N	KMERCR69R01B165K	\N
2092	VISITA	50	\N	RSMMRT81S58G443J	\N
2102	VISITA	50	\N	KMERCR69R01B165K	\N
2112	VISITA	50	\N	KMERCR69R01B165K	\N
2122	VISITA	50	\N	BTTLDN71C45B203N	\N
2132	VISITA	50	\N	ZCCLNA85M05H004D	\N
2142	VISITA	50	\N	BRNCHR83E66H299L	\N
2152	VISITA	50	\N	BNZGPP74L13H475Z	\N
2162	VISITA	50	\N	RZILSS83S27A507U	\N
2172	VISITA	50	\N	BRNCHR83E66H299L	\N
2182	VISITA	50	\N	GMPBBR68M63L137D	\N
2192	VISITA	50	\N	RSMMRT81S58G443J	\N
2202	VISITA	50	\N	BNZGPP74L13H475Z	\N
2212	VISITA	50	\N	BNZGPP74L13H475Z	\N
2222	VISITA	50	\N	BRSCRL82B24L201U	\N
2232	VISITA	50	\N	BNZGPP74L13H475Z	\N
2242	VISITA	50	\N	GMPBBR68M63L137D	\N
2252	VISITA	50	\N	ZCCLNA85M05H004D	\N
2262	VISITA	50	\N	RZILSS83S27A507U	\N
2272	VISITA	50	\N	BRNCHR83E66H299L	\N
2282	VISITA	50	\N	BTTLDN71C45B203N	\N
2292	VISITA	50	\N	GMPBBR68M63L137D	\N
2322	VISITA	50	\N	BRNCHR83E66H299L	\N
2332	VISITA	50	\N	GMPBBR68M63L137D	\N
2342	VISITA	50	\N	ZCCLNA85M05H004D	\N
2352	VISITA	50	\N	BNZGPP74L13H475Z	\N
2362	VISITA	50	\N	RZILSS83S27A507U	\N
2372	VISITA	50	\N	BRNCHR83E66H299L	\N
2382	VISITA	50	\N	BRNCHR83E66H299L	\N
2392	VISITA	50	\N	ZCCLNA85M05H004D	\N
2402	VISITA	50	\N	RSMMRT81S58G443J	\N
2412	VISITA	50	\N	GMPBBR68M63L137D	\N
2422	VISITA	50	\N	GMPBBR68M63L137D	\N
2432	VISITA	50	\N	GMPBBR68M63L137D	\N
2442	VISITA	50	\N	BNZGPP74L13H475Z	\N
2452	VISITA	50	\N	BNZGPP74L13H475Z	\N
2462	VISITA	50	\N	ZCCLNA85M05H004D	\N
2472	VISITA	50	\N	BRNCHR83E66H299L	\N
2482	VISITA	50	\N	RZILSS83S27A507U	\N
2492	VISITA	50	\N	GMPBBR68M63L137D	\N
2502	VISITA	50	\N	BTTLDN71C45B203N	\N
2512	VISITA	50	\N	RZILSS83S27A507U	\N
2522	VISITA	50	\N	BNZGPP74L13H475Z	\N
2542	VISITA	50	\N	KMERCR69R01B165K	\N
2552	VISITA	50	\N	GMPBBR68M63L137D	\N
2562	VISITA	50	\N	BRSSDR86D62D731H	\N
2572	VISITA	50	\N	RZILSS83S27A507U	\N
2582	VISITA	50	\N	BRSCRL82B24L201U	\N
2592	VISITA	50	\N	RSMMRT81S58G443J	\N
2602	VISITA	50	\N	ZCCLNA85M05H004D	\N
2612	VISITA	50	\N	KMERCR69R01B165K	\N
2622	VISITA	50	\N	KMERCR69R01B165K	\N
2632	VISITA	50	\N	ZCCLNA85M05H004D	\N
2642	VISITA	50	\N	ZCCLNA85M05H004D	\N
2652	VISITA	50	\N	BRSCRL82B24L201U	\N
2662	VISITA	50	\N	BRSCRL82B24L201U	\N
2672	VISITA	50	\N	BRSSDR86D62D731H	\N
2682	VISITA	50	\N	GMPBBR68M63L137D	\N
2692	VISITA	50	\N	KMERCR69R01B165K	\N
2702	VISITA	50	\N	KMERCR69R01B165K	\N
2712	VISITA	50	\N	BRNCHR83E66H299L	\N
2722	VISITA	50	\N	BRSCRL82B24L201U	\N
2732	VISITA	50	\N	BRSSDR86D62D731H	\N
2742	VISITA	50	\N	BTTLDN71C45B203N	\N
2752	VISITA	50	\N	RZILSS83S27A507U	\N
2782	VISITA	50	\N	KMERCR69R01B165K	\N
2792	VISITA	50	\N	BNZGPP74L13H475Z	\N
2802	VISITA	50	\N	BRSCRL82B24L201U	\N
2812	VISITA	50	\N	RZILSS83S27A507U	\N
2822	VISITA	50	\N	KMERCR69R01B165K	\N
2832	VISITA	50	\N	BTTLDN71C45B203N	\N
2842	VISITA	50	\N	RSMMRT81S58G443J	\N
2862	VISITA	50	\N	RZILSS83S27A507U	\N
2872	VISITA	50	\N	ZCCLNA85M05H004D	\N
2882	VISITA	50	\N	RSMMRT81S58G443J	\N
2892	VISITA	50	\N	RZILSS83S27A507U	\N
2902	VISITA	50	\N	KMERCR69R01B165K	\N
2912	VISITA	50	\N	RSMMRT81S58G443J	\N
2922	VISITA	50	\N	BRSSDR86D62D731H	\N
2932	VISITA	50	\N	BRNCHR83E66H299L	\N
2942	VISITA	50	\N	KMERCR69R01B165K	\N
2952	VISITA	50	\N	GMPBBR68M63L137D	\N
2962	VISITA	50	\N	ZCCLNA85M05H004D	\N
2972	VISITA	50	\N	KMERCR69R01B165K	\N
2982	VISITA	50	\N	BNZGPP74L13H475Z	\N
2992	VISITA	50	\N	ZCCLNA85M05H004D	\N
3002	VISITA	50	\N	BRSCRL82B24L201U	\N
3012	VISITA	50	\N	BNZGPP74L13H475Z	\N
3022	VISITA	50	\N	BRNCHR83E66H299L	\N
3032	VISITA	50	\N	RZILSS83S27A507U	\N
3042	VISITA	50	\N	RZILSS83S27A507U	\N
3052	VISITA	50	\N	BRNCHR83E66H299L	\N
3062	VISITA	50	\N	BNZGPP74L13H475Z	\N
3072	VISITA	50	\N	BTTLDN71C45B203N	\N
3092	VISITA	50	\N	KMERCR69R01B165K	\N
3102	VISITA	50	\N	BNZGPP74L13H475Z	\N
3112	VISITA	50	\N	BNZGPP74L13H475Z	\N
3132	VISITA	50	\N	BRSCRL82B24L201U	\N
3142	VISITA	50	\N	RSMMRT81S58G443J	\N
3152	VISITA	50	\N	ZCCLNA85M05H004D	\N
3162	VISITA	50	\N	BRSCRL82B24L201U	\N
3172	VISITA	50	\N	BRNCHR83E66H299L	\N
3182	VISITA	50	\N	RZILSS83S27A507U	\N
3192	VISITA	50	\N	BTTLDN71C45B203N	\N
3202	VISITA	50	\N	GMPBBR68M63L137D	\N
3212	VISITA	50	\N	BTTLDN71C45B203N	\N
3222	VISITA	50	\N	ZCCLNA85M05H004D	\N
3232	VISITA	50	\N	GMPBBR68M63L137D	\N
3242	VISITA	50	\N	BNZGPP74L13H475Z	\N
3252	VISITA	50	\N	BTTLDN71C45B203N	\N
3262	VISITA	50	\N	RZILSS83S27A507U	\N
3272	VISITA	50	\N	ZCCLNA85M05H004D	\N
3282	VISITA	50	\N	BRNCHR83E66H299L	\N
3292	VISITA	50	\N	BRSCRL82B24L201U	\N
3302	VISITA	50	\N	KMERCR69R01B165K	\N
1690	FARMACO	3	424100	\N	\N
820	FARMACO	3	423400	\N	\N
1000	FARMACO	3	424900	\N	\N
1580	FARMACO	3	424100	\N	\N
850	FARMACO	3	422600	\N	\N
370	FARMACO	3	422800	\N	\N
1630	FARMACO	3	422600	\N	\N
1560	FARMACO	3	424100	\N	\N
970	FARMACO	3	423400	\N	\N
930	FARMACO	3	422600	\N	\N
190	FARMACO	3	422600	\N	\N
1460	FARMACO	3	423400	\N	\N
350	FARMACO	3	422600	\N	\N
1020	FARMACO	3	423400	\N	\N
1060	FARMACO	3	422800	\N	\N
980	FARMACO	3	424900	\N	\N
1050	FARMACO	3	422800	\N	\N
1730	FARMACO	3	424100	\N	\N
900	FARMACO	3	424100	\N	\N
220	FARMACO	3	424100	\N	\N
1750	FARMACO	3	422800	\N	\N
1720	FARMACO	3	424100	\N	\N
170	FARMACO	3	424100	\N	\N
1590	FARMACO	3	424100	\N	\N
400	FARMACO	3	422800	\N	\N
330	FARMACO	3	422800	\N	\N
1740	FARMACO	3	422600	\N	\N
920	FARMACO	3	422800	\N	\N
180	FARMACO	3	424100	\N	\N
320	FARMACO	3	422800	\N	\N
870	FARMACO	3	424900	\N	\N
910	FARMACO	3	422600	\N	\N
1650	FARMACO	3	423400	\N	\N
770	FARMACO	3	423400	\N	\N
340	FARMACO	3	424900	\N	\N
1760	FARMACO	3	424100	\N	\N
290	FARMACO	3	422800	\N	\N
300	FARMACO	3	422800	\N	\N
1160	FARMACO	3	424900	\N	\N
1530	FARMACO	3	424900	\N	\N
1190	FARMACO	3	422600	\N	\N
940	FARMACO	3	422800	\N	\N
1180	FARMACO	3	422800	\N	\N
390	FARMACO	3	424100	\N	\N
200	FARMACO	3	423400	\N	\N
1620	FARMACO	3	422800	\N	\N
360	FARMACO	3	422800	\N	\N
280	FARMACO	3	424100	\N	\N
1570	FARMACO	3	424100	\N	\N
420	FARMACO	3	424100	\N	\N
410	FARMACO	3	423400	\N	\N
1210	FARMACO	3	422600	\N	\N
1140	FARMACO	3	424900	\N	\N
240	FARMACO	3	423400	\N	\N
990	FARMACO	3	424100	\N	\N
860	FARMACO	3	422800	\N	\N
1680	FARMACO	3	422800	\N	\N
230	FARMACO	3	422600	\N	\N
1480	FARMACO	3	422800	\N	\N
1110	FARMACO	3	422600	\N	\N
1490	FARMACO	3	424900	\N	\N
260	FARMACO	3	424900	\N	\N
1670	FARMACO	3	422600	\N	\N
880	FARMACO	3	424900	\N	\N
1090	FARMACO	3	422800	\N	\N
1600	FARMACO	3	424900	\N	\N
1080	FARMACO	3	422800	\N	\N
1470	FARMACO	3	424900	\N	\N
1170	FARMACO	3	424100	\N	\N
1130	FARMACO	3	423400	\N	\N
160	FARMACO	3	423400	\N	\N
1640	FARMACO	3	422600	\N	\N
760	FARMACO	3	423400	\N	\N
1510	FARMACO	3	424900	\N	\N
1520	FARMACO	3	422600	\N	\N
1150	FARMACO	3	422800	\N	\N
250	FARMACO	3	424100	\N	\N
840	FARMACO	3	424100	\N	\N
1500	FARMACO	3	423400	\N	\N
890	FARMACO	3	424100	\N	\N
310	FARMACO	3	424900	\N	\N
800	FARMACO	3	424100	\N	\N
1100	FARMACO	3	422800	\N	\N
1070	FARMACO	3	423400	\N	\N
1200	FARMACO	3	424100	\N	\N
1550	FARMACO	3	423400	\N	\N
960	FARMACO	3	423400	\N	\N
1710	FARMACO	3	424100	\N	\N
1700	FARMACO	3	424900	\N	\N
270	FARMACO	3	423400	\N	\N
1030	FARMACO	3	422800	\N	\N
\.


--
-- TOC entry 2744 (class 2606 OID 19143)
-- Name: esami esami_pkey; Type: CONSTRAINT; Schema: progettoipw; Owner: postgres
--

ALTER TABLE ONLY progettoipw.esami
    ADD CONSTRAINT esami_pkey PRIMARY KEY (id);


--
-- TOC entry 2740 (class 2606 OID 19127)
-- Name: farmaci farmaci_pkey; Type: CONSTRAINT; Schema: progettoipw; Owner: postgres
--

ALTER TABLE ONLY progettoipw.farmaci
    ADD CONSTRAINT farmaci_pkey PRIMARY KEY (idfarmaco);


--
-- TOC entry 2746 (class 2606 OID 19151)
-- Name: farmacie farmacie_pkey; Type: CONSTRAINT; Schema: progettoipw; Owner: postgres
--

ALTER TABLE ONLY progettoipw.farmacie
    ADD CONSTRAINT farmacie_pkey PRIMARY KEY (idfarmacia);


--
-- TOC entry 2748 (class 2606 OID 19159)
-- Name: medici medici_pkey; Type: CONSTRAINT; Schema: progettoipw; Owner: postgres
--

ALTER TABLE ONLY progettoipw.medici
    ADD CONSTRAINT medici_pkey PRIMARY KEY (codicefiscale);


--
-- TOC entry 2750 (class 2606 OID 19167)
-- Name: pazienti pazienti_pkey; Type: CONSTRAINT; Schema: progettoipw; Owner: postgres
--

ALTER TABLE ONLY progettoipw.pazienti
    ADD CONSTRAINT pazienti_pkey PRIMARY KEY (codicefiscale);


--
-- TOC entry 2754 (class 2606 OID 19214)
-- Name: prescrizione_esami prescrizione_esami_pkey; Type: CONSTRAINT; Schema: progettoipw; Owner: postgres
--

ALTER TABLE ONLY progettoipw.prescrizione_esami
    ADD CONSTRAINT prescrizione_esami_pkey PRIMARY KEY (id);


--
-- TOC entry 2752 (class 2606 OID 19191)
-- Name: prescrizione_farmaci prescrizione_farmaci_pkey; Type: CONSTRAINT; Schema: progettoipw; Owner: postgres
--

ALTER TABLE ONLY progettoipw.prescrizione_farmaci
    ADD CONSTRAINT prescrizione_farmaci_pkey PRIMARY KEY (id);


--
-- TOC entry 2756 (class 2606 OID 19242)
-- Name: prescrizione_visitespec prescrizione_visitespec_pkey; Type: CONSTRAINT; Schema: progettoipw; Owner: postgres
--

ALTER TABLE ONLY progettoipw.prescrizione_visitespec
    ADD CONSTRAINT prescrizione_visitespec_pkey PRIMARY KEY (id);


--
-- TOC entry 2742 (class 2606 OID 19135)
-- Name: serviziprovinciali serviziprovinciali_pkey; Type: CONSTRAINT; Schema: progettoipw; Owner: postgres
--

ALTER TABLE ONLY progettoipw.serviziprovinciali
    ADD CONSTRAINT serviziprovinciali_pkey PRIMARY KEY (id);


--
-- TOC entry 2758 (class 2606 OID 19270)
-- Name: ticket ticket_pkey; Type: CONSTRAINT; Schema: progettoipw; Owner: postgres
--

ALTER TABLE ONLY progettoipw.ticket
    ADD CONSTRAINT ticket_pkey PRIMARY KEY (id);


--
-- TOC entry 2775 (class 2606 OID 19292)
-- Name: fotopazienti fotopazienti_cf_paz_fkey; Type: FK CONSTRAINT; Schema: progettoipw; Owner: postgres
--

ALTER TABLE ONLY progettoipw.fotopazienti
    ADD CONSTRAINT fotopazienti_cf_paz_fkey FOREIGN KEY (cf_paz) REFERENCES progettoipw.pazienti(codicefiscale);


--
-- TOC entry 2759 (class 2606 OID 19174)
-- Name: med_has_paz med_has_paz_cf_med_fkey; Type: FK CONSTRAINT; Schema: progettoipw; Owner: postgres
--

ALTER TABLE ONLY progettoipw.med_has_paz
    ADD CONSTRAINT med_has_paz_cf_med_fkey FOREIGN KEY (cf_med) REFERENCES progettoipw.medici(codicefiscale);


--
-- TOC entry 2760 (class 2606 OID 19179)
-- Name: med_has_paz med_has_paz_cf_paz_fkey; Type: FK CONSTRAINT; Schema: progettoipw; Owner: postgres
--

ALTER TABLE ONLY progettoipw.med_has_paz
    ADD CONSTRAINT med_has_paz_cf_paz_fkey FOREIGN KEY (cf_paz) REFERENCES progettoipw.pazienti(codicefiscale);


--
-- TOC entry 2764 (class 2606 OID 19215)
-- Name: prescrizione_esami prescrizione_esami_cf_med_fkey; Type: FK CONSTRAINT; Schema: progettoipw; Owner: postgres
--

ALTER TABLE ONLY progettoipw.prescrizione_esami
    ADD CONSTRAINT prescrizione_esami_cf_med_fkey FOREIGN KEY (cf_med) REFERENCES progettoipw.medici(codicefiscale);


--
-- TOC entry 2766 (class 2606 OID 19225)
-- Name: prescrizione_esami prescrizione_esami_cf_paz_fkey; Type: FK CONSTRAINT; Schema: progettoipw; Owner: postgres
--

ALTER TABLE ONLY progettoipw.prescrizione_esami
    ADD CONSTRAINT prescrizione_esami_cf_paz_fkey FOREIGN KEY (cf_paz) REFERENCES progettoipw.pazienti(codicefiscale);


--
-- TOC entry 2767 (class 2606 OID 19230)
-- Name: prescrizione_esami prescrizione_esami_id_esame_fkey; Type: FK CONSTRAINT; Schema: progettoipw; Owner: postgres
--

ALTER TABLE ONLY progettoipw.prescrizione_esami
    ADD CONSTRAINT prescrizione_esami_id_esame_fkey FOREIGN KEY (id_esame) REFERENCES progettoipw.esami(id);


--
-- TOC entry 2765 (class 2606 OID 19220)
-- Name: prescrizione_esami prescrizione_esami_id_ssp_fkey; Type: FK CONSTRAINT; Schema: progettoipw; Owner: postgres
--

ALTER TABLE ONLY progettoipw.prescrizione_esami
    ADD CONSTRAINT prescrizione_esami_id_ssp_fkey FOREIGN KEY (id_ssp) REFERENCES progettoipw.serviziprovinciali(id);


--
-- TOC entry 2761 (class 2606 OID 19192)
-- Name: prescrizione_farmaci prescrizione_farmaci_cf_med_fkey; Type: FK CONSTRAINT; Schema: progettoipw; Owner: postgres
--

ALTER TABLE ONLY progettoipw.prescrizione_farmaci
    ADD CONSTRAINT prescrizione_farmaci_cf_med_fkey FOREIGN KEY (cf_med) REFERENCES progettoipw.medici(codicefiscale);


--
-- TOC entry 2762 (class 2606 OID 19197)
-- Name: prescrizione_farmaci prescrizione_farmaci_cf_paz_fkey; Type: FK CONSTRAINT; Schema: progettoipw; Owner: postgres
--

ALTER TABLE ONLY progettoipw.prescrizione_farmaci
    ADD CONSTRAINT prescrizione_farmaci_cf_paz_fkey FOREIGN KEY (cf_paz) REFERENCES progettoipw.pazienti(codicefiscale);


--
-- TOC entry 2763 (class 2606 OID 19202)
-- Name: prescrizione_farmaci prescrizione_farmaci_idfarmaco_fkey; Type: FK CONSTRAINT; Schema: progettoipw; Owner: postgres
--

ALTER TABLE ONLY progettoipw.prescrizione_farmaci
    ADD CONSTRAINT prescrizione_farmaci_idfarmaco_fkey FOREIGN KEY (idfarmaco) REFERENCES progettoipw.farmaci(idfarmaco);


--
-- TOC entry 2768 (class 2606 OID 19243)
-- Name: prescrizione_visitespec prescrizione_visitespec_cf_med_fkey; Type: FK CONSTRAINT; Schema: progettoipw; Owner: postgres
--

ALTER TABLE ONLY progettoipw.prescrizione_visitespec
    ADD CONSTRAINT prescrizione_visitespec_cf_med_fkey FOREIGN KEY (cf_med) REFERENCES progettoipw.medici(codicefiscale);


--
-- TOC entry 2771 (class 2606 OID 19258)
-- Name: prescrizione_visitespec prescrizione_visitespec_cf_medspec_fkey; Type: FK CONSTRAINT; Schema: progettoipw; Owner: postgres
--

ALTER TABLE ONLY progettoipw.prescrizione_visitespec
    ADD CONSTRAINT prescrizione_visitespec_cf_medspec_fkey FOREIGN KEY (cf_medspec) REFERENCES progettoipw.medici(codicefiscale);


--
-- TOC entry 2770 (class 2606 OID 19253)
-- Name: prescrizione_visitespec prescrizione_visitespec_cf_paz_fkey; Type: FK CONSTRAINT; Schema: progettoipw; Owner: postgres
--

ALTER TABLE ONLY progettoipw.prescrizione_visitespec
    ADD CONSTRAINT prescrizione_visitespec_cf_paz_fkey FOREIGN KEY (cf_paz) REFERENCES progettoipw.pazienti(codicefiscale);


--
-- TOC entry 2769 (class 2606 OID 19248)
-- Name: prescrizione_visitespec prescrizione_visitespec_id_ssp_fkey; Type: FK CONSTRAINT; Schema: progettoipw; Owner: postgres
--

ALTER TABLE ONLY progettoipw.prescrizione_visitespec
    ADD CONSTRAINT prescrizione_visitespec_id_ssp_fkey FOREIGN KEY (id_ssp) REFERENCES progettoipw.serviziprovinciali(id);


--
-- TOC entry 2772 (class 2606 OID 19271)
-- Name: ticket ticket_idfarmacia_fkey; Type: FK CONSTRAINT; Schema: progettoipw; Owner: postgres
--

ALTER TABLE ONLY progettoipw.ticket
    ADD CONSTRAINT ticket_idfarmacia_fkey FOREIGN KEY (idfarmacia) REFERENCES progettoipw.farmacie(idfarmacia);


--
-- TOC entry 2773 (class 2606 OID 19276)
-- Name: ticket ticket_idmedspec_fkey; Type: FK CONSTRAINT; Schema: progettoipw; Owner: postgres
--

ALTER TABLE ONLY progettoipw.ticket
    ADD CONSTRAINT ticket_idmedspec_fkey FOREIGN KEY (idmedspec) REFERENCES progettoipw.medici(codicefiscale);


--
-- TOC entry 2774 (class 2606 OID 19281)
-- Name: ticket ticket_idssp_fkey; Type: FK CONSTRAINT; Schema: progettoipw; Owner: postgres
--

ALTER TABLE ONLY progettoipw.ticket
    ADD CONSTRAINT ticket_idssp_fkey FOREIGN KEY (idssp) REFERENCES progettoipw.serviziprovinciali(id);


-- Completed on 2020-01-29 12:05:10

--
-- PostgreSQL database dump complete
--

